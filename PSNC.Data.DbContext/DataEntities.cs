﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSNC.Data.DbContext
{
    public partial class DataEntities
    {
        //[DbFunction("Model.Store", "IsBatchEligibleForStatusChange")]
        //public bool IsBatchEligibleForStatusChange(long batchID)
        //{
        //    List<ObjectParameter> parameters = new List<ObjectParameter>(3);
        //    parameters.Add(new ObjectParameter("batchID", batchID));
        //    var lObjectContext = ((IObjectContextAdapter)this).ObjectContext;
        //    var output = lObjectContext.
        //            CreateQuery<bool>("Model.Store.IsBatchEligibleForStatusChange(@batchID)", parameters.ToArray())
        //        .Execute(MergeOption.NoTracking)
        //        .FirstOrDefault();
        //    return output;
        //}

        public T SqlScalarResult<T>(string function, SqlParameter[] parameters)
        {

            List<string> functionArgs = new List<string>();
            foreach (SqlParameter parameter in parameters)
            {
                functionArgs.Add("@" + parameter.ParameterName);
            }
            string sql = string.Format("SELECT {0}({1});", function, string.Join(",", functionArgs));

            return (T)this.Database.SqlQuery<T>(sql, parameters).FirstOrDefault();

        }

        public MultiResultSetReader MultiResultSetSqlQuery(string query, params SqlParameter[] parameters)
        {
            return new MultiResultSetReader(this, query, parameters);
        }

        public int SaveChanges<T>(T t)
        {
            var added = this.ChangeTracker.Entries().Where(e => e.State == System.Data.Entity.EntityState.Added);

            // Do your thing, like changing the state to detached
            return base.SaveChanges();
        }
        
    }
}
