/*****************
TO DO:
Full Text index was removed, it either needs to be re-added or an alternative search method used in the below procedures
------
CREATE FULLTEXT CATALOG [PrismCatalog]WITH ACCENT_SENSITIVITY = OFF
------
The following stored proc's were dependant on Full Text indexing (which is not supported in the current version of
Azure databases, although it is avaiable in the preview build)
[dbo].[FreeTextContractorSearch]
[dbo].[FreeTextGuidanceSearch]
[dbo].[FreeTextManufacturerSearch]
[dbo].[FreeTextProductSearch]
[dbo].[FreeTextProductSearchForAudit]
[dmd].[FreeTextActualMedicinalProductSearch]
[dmd].[FreeTextSupplierSearch]
[dmd].[FreeTextVirtualMedicinalProductSearch]

*****************/

/****** Object:  Schema [dmd]    Script Date: 28/10/2015 11:24:28 ******/
CREATE SCHEMA [dmd]
GO
/****** Object:  Schema [ppd]    Script Date: 28/10/2015 11:24:28 ******/
CREATE SCHEMA [ppd]
GO
/****** Object:  UserDefinedDataType [dbo].[Quantity]    Script Date: 28/10/2015 11:24:28 ******/
CREATE TYPE [dbo].[Quantity] FROM [decimal](9, 2) NOT NULL
GO
/****** Object:  UserDefinedDataType [dbo].[Size]    Script Date: 28/10/2015 11:24:28 ******/
CREATE TYPE [dbo].[Size] FROM [decimal](7, 2) NOT NULL
GO
/****** Object:  UserDefinedFunction [dbo].[BatchItemDetails]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[BatchItemDetails](
	@batchID bigint, 
	@includeHiddenAdjustments bit = 0
) returns @results table (

	FormNumber varchar(6),
	ItemNumber tinyint not null,
	ElementNumber tinyint not null,
	NumberOfElements tinyint not null,
	UpdateType varchar(4) not null,

	OriginalItemVersionID bigint null,
	OriginalProductPackID bigint null,
	OriginalProductPackCode varchar(8) null,
	OriginalProductDescription varchar(60) null,
	OriginalPackDescription varchar(60) null,
	OriginalQuantity dbo.Quantity not null,
	OriginalPackPrice money not null,
	OriginalBasicPrice money not null,
	OriginalGroupTypeCode char(1) null,
	OriginalNumberOfCharges int null,
	OriginalChargeValue money null,
	OriginalNumberOfProfessionalFees int not null,
	OriginalValueOfProfessionalFees money null,
	OriginalValueOfAdditionalFees money null,
	OriginalValueofExpensiveItemFee money not null,
	OriginalValueofOutOfPocketExpenses money not null,
	OriginalZeroDiscountIndicator varchar(2) not null,
	OriginalPaymentForContainersIndicator varchar(2) not null,
	OriginalEndorsements varchar(max) null,
	
	CurrentItemVersionID bigint null,
	CurrentProductPackID bigint null,
	CurrentProductPackCode varchar(8) null,
	CurrentProductDescription varchar(60) null,
	CurrentPackDescription varchar(60) null,
	CurrentQuantity dbo.Quantity not null,
	CurrentPackPrice money not null,
	CurrentBasicPrice money not null,
	CurrentGroupTypeCode char(1) not null,
	CurrentNumberOfCharges int null,
	CurrentChargeValue money null,
	CurrentNumberOfProfessionalFees int not null,
	CurrentValueOfProfessionalFees money null,
	CurrentValueOfAdditionalFees money null,
	CurrentValueofExpensiveItemFee money not null,
	CurrentValueofOutOfPocketExpenses money not null,
	CurrentZeroDiscountIndicator varchar(2) not null,
	CurrentPaymentForContainersIndicator varchar(2) not null,
	CurrentEndorsements varchar(max) null,

	IsHiddenAdjustment bit,

	PriceDifference money null,
	TotalAdjustment money null

)

as

	begin

		declare @jurisdictionID integer, @financialPeriodID integer
		declare @applianceChargeAtCurrentRate money, @applianceChargeAtPreviousRate money,
			@drugsChargeAtCurrentRate money, @drugsChargeAtPreviousRate money,
			@professionalFeeAmount money;

		-- Determine the financial period and jurisdiction for the batch...	
		select @financialPeriodID = b.FinancialPeriodID,
			@jurisdictionID = c.JurisdictionID
		from dbo.Batch b
			inner join dbo.Contractor c on b.ContractorID = c.ContractorID
		where b.BatchID = @batchID;

		-- Get the charge rates for the current financial period/jurisdiction combination...
		select @applianceChargeAtCurrentRate = jp.ApplianceItemPrescriptionChargeValue,
			@drugsChargeAtCurrentRate = jp.DrugItemPrescriptionChargeValue,
			@professionalFeeAmount = jp.StandardProfessionalFeeValue
		from dbo.JurisdictionPeriodic jp
		where jp.JurisdictionID = @jurisdictionID and jp.FinancialPeriodID = @financialPeriodID;

		select top 1 @applianceChargeAtPreviousRate = jp.ApplianceItemPrescriptionChargeValue
		from dbo.JurisdictionPeriodic jp
		where jp.JurisdictionID = @jurisdictionID
			and jp.FinancialPeriodID < @financialPeriodID
			and jp.ApplianceItemPrescriptionChargeValue != @applianceChargeAtCurrentRate
		order by jp.FinancialPeriodID desc;

		select top 1 @drugsChargeAtPreviousRate = jp.DrugItemPrescriptionChargeValue
		from dbo.JurisdictionPeriodic jp
		where jp.JurisdictionID = @jurisdictionID
			and jp.FinancialPeriodID < @financialPeriodID
			and jp.DrugItemPrescriptionChargeValue != @drugsChargeAtCurrentRate
		order by jp.FinancialPeriodID desc;

		-- Select those forms where changes have been made...
		with batchItems as (
	
			select

				--d.DocumentNumber as FormNumber, 
				FormNumber = case
					when d.FragmentTypeCode is null then right('000000' + convert(varchar(6), d.DocumentNumber), 6)
					else d.FragmentTypeCode + right('00000' + convert(varchar(6), d.DocumentNumber), 5) 
					end,

				i.Sequence as ItemNumber,
				i.SubSequence as ElementNumber,
				(select count(*) from dbo.Item where Item.DocumentID = d.DocumentID and Item.Sequence = i.Sequence and Item.CurrentItemVersionID is not null) as NumberOfElements,
				case
					when cpp.Code is not null and opp.Code is null then'M'
					when cpp.Code is null and opp.Code is not null then 'E'
					when ISNULL(ogt.GroupTypeCode, '') != ISNULL(cgt.GroupTypeCode, '') then 'C'
					else ''
				end as UpdateType,
	
				oiv.ItemVersionID as OriginalItemVersionID,
				opp.ProductPackID as OriginalProductPackID, 
				opp.Code as OriginalProductPackCode, 
				op.Description as OriginalProductDescription, 
				opp.Description as OriginalPackDescription,
				isnull(oiv.Quantity, 0) as OriginalQuantity,
				isnull(oppp.PackPrice, 0) as OriginalPackPrice,
				isnull(oiv.BasicPrice, 0) as OriginalBasicPrice,
				ogt.GroupTypeCode as OriginalGroupTypeCode,
				case 
					when d.OriginalDocumentGroupTypeID is null or ogt.IsExemptFromCharges = 1 then 0 
					else isnull(oiv.NumberOfCharges, 0) end as OriginalNumberOfCharges, 
				case 
					when op.ProductTypeCode = 'A' then
						case
							when d.OriginalDocumentGroupTypeID is null or ogt.IsExemptFromCharges = 1 then 0 
							when ogt.IsOldRate = 0 then @applianceChargeAtCurrentRate 
							else @applianceChargeAtPreviousRate end
					else
						case
							when d.OriginalDocumentGroupTypeID is null or ogt.IsExemptFromCharges = 1 then 0 
							when ogt.IsOldRate = 0 then @drugsChargeAtCurrentRate 
							else @drugsChargeAtPreviousRate end
					end as OriginalChargeValue,
				isnull(oiv.NumberOfProfessionalFees, 0) as OriginalNumberOfProfessionalFees,
				isnull(oiv.NumberOfProfessionalFees, 0) * @professionalFeeAmount as OriginalValueOfProfessionalFees,
				isnull(oiv.ValueOfAdditionalFees, 0) - isnull(oiv.ValueOfExpensiveItemFee, 0) as OriginalValueOfAdditionalFees,
				isnull(oiv.ValueOfExpensiveItemFee, 0) as OriginalValueofExpensiveItemFee,
				isnull(oiv.ValueOfOutOfPocketExpenses, 0) as OriginalValueofOutOfPocketExpenses,
				case when oepZD.EndorsementCode = 'ZD' then 'ZD' else '' end as OriginalZeroDiscountIndicator,
				case when oepPK.EndorsementCode = 'PK' then 'PK' else '' end as OriginalPaymentForContainersIndicator,
				dbo.EndorsementCodeList(i.OriginalItemVersionID) as OriginalEndorsements,

				civ.ItemVersionID as CurrentItemVersionID,
				cpp.ProductPackID as CurrentProductPackID, 
				cpp.Code as CurrentProductPackCode, 
				cp.Description as CurrentProductDescription, 
				cpp.Description as CurrentPackDescription, 
				isnull(civ.Quantity, 0) as CurrentQuantity, 
				isnull(cppp.PackPrice, 0) as CurrentPackPrice,
				isnull(civ.BasicPrice, 0) as CurrentBasicPrice,
				cgt.GroupTypeCode as CurrentGroupTypeCode,
				case 
					when cgt.IsExemptFromCharges = 1 then 0 
					else isnull(civ.NumberOfCharges, 0) end as CurrentNumberOfCharges, 
				case 
					when cp.ProductTypeCode = 'A' then
						case
							when cgt.IsExemptFromCharges = 1 then 0 
							when cgt.IsOldRate = 0 then @applianceChargeAtCurrentRate 
							else @applianceChargeAtPreviousRate end
					else
						case
							when cgt.IsExemptFromCharges = 1 then 0 
							when cgt.IsOldRate = 0 then @drugsChargeAtCurrentRate 
							else @drugsChargeAtPreviousRate end
					end as CurrentChargeValue,
				isnull(civ.NumberOfProfessionalFees, 0) as CurrentNumberOfProfessionalFees,
				isnull(civ.NumberOfProfessionalFees, 0) * @professionalFeeAmount as CurrentValueOfProfessionalFees,
				isnull(civ.ValueOfAdditionalFees, 0) - isnull(civ.ValueOfExpensiveItemFee, 0) as CurrentValueOfAdditionalFees,
				isnull(civ.ValueOfExpensiveItemFee, 0) as CurrentValueofExpensiveItemFee,
				isnull(civ.ValueOfOutOfPocketExpenses, 0) as CurrentValueofOutOfPocketExpenses,
				case when cppp.IsEligibleForZeroDiscount = 1 then 'ZD' else '' end as CurrentZeroDiscountIndicator,
				case when cepPK.EndorsementCode = 'PK' then 'PK' else '' end as CurrentPaymentForContainersIndicator,
				dbo.EndorsementCodeList(i.CurrentItemVersionID) as CurrentEndorsements,
				civ.IsHiddenAdjustment

			from dbo.Batch b
				inner join dbo.Document d 
		
					left join dbo.DocumentGroupType odgt 
						inner join dbo.GroupType ogt on odgt.GroupTypeCode = ogt.GroupTypeCode
					on d.OriginalDocumentGroupTypeID = odgt.DocumentGroupTypeID
					inner join dbo.DocumentGroupType cdgt 
						inner join dbo.GroupType cgt on cdgt.GroupTypeCode = cgt.GroupTypeCode
					on d.CurrentDocumentGroupTypeID = cdgt.DocumentGroupTypeID
			
					inner join dbo.Item i 
			
						left join dbo.ItemVersion oiv 
							left join dbo.ProductPackPeriodic oppp 
								inner join dbo.ProductPack opp 
									inner join dbo.Product op on opp.ProductID = op.ProductID
								on oppp.ProductPackID = opp.ProductPackID
								left join dbo.ProductPeriodic oppc on oppc.ProductPeriodicID = oppp.ProductPeriodicID
							on oiv.ProductPackPeriodicID = oppp.ProductPackPeriodicID
							left join dbo.ItemVersionEndorsement oiveZD
								inner join dbo.EndorsementPeriodic oepZD on oiveZD.EndorsementPeriodicID = oepZD.EndorsementPeriodicID and oepZD.EndorsementCode = 'ZD'
							on oiv.ItemVersionID = oiveZD.ItemVersionID
							left join dbo.ItemVersionEndorsement oivePK
								inner join dbo.EndorsementPeriodic oepPK on oivePK.EndorsementPeriodicID = oepPK.EndorsementPeriodicID and oepPK.EndorsementCode = 'PK'
							on oiv.ItemVersionID = oivePK.ItemVersionID
						on i.OriginalItemVersionID = oiv.ItemVersionID
			
						left join dbo.ItemVersion civ  
							left join dbo.ProductPackPeriodic cppp 
								inner join dbo.ProductPack cpp 
									inner join dbo.Product cp on cpp.ProductID = cp.ProductID
								on cppp.ProductPackID = cpp.ProductPackID
							on civ.ProductPackPeriodicID = cppp.ProductPackPeriodicID
							left join dbo.ItemVersionEndorsement civePK
								inner join dbo.EndorsementPeriodic cepPK on civePK.EndorsementPeriodicID = cepPK.EndorsementPeriodicID and cepPK.EndorsementCode = 'PK'
							on civ.ItemVersionID = civePK.ItemVersionID
						on i.CurrentItemVersionID = civ.ItemVersionID
			
					on d.DocumentID = i.DocumentID
			
				on b.BatchID = d.BatchID
		
		
			where b.BatchID = @batchID
			  and (@includeHiddenAdjustments = 1 or civ.IsHiddenAdjustment = 0)

		)

		insert into @results
		select *,
			PriceDifference = CurrentBasicPrice - OriginalBasicPrice,
			TotalAdjustment = (CurrentBasicPrice + CurrentValueOfProfessionalFees + CurrentValueOfAdditionalFees + CurrentValueofExpensiveItemFee + ROUND(CurrentChargeValue * (0 - CurrentNumberOfCharges), 2)) + CurrentValueofOutOfPocketExpenses
				- (OriginalBasicPrice + OriginalValueOfProfessionalFees + OriginalValueOfAdditionalFees + OriginalValueofExpensiveItemFee + ROUND(OriginalChargeValue * (0 - OriginalNumberOfCharges), 2) + OriginalValueofOutOfPocketExpenses)
		from batchItems;
		
		return;

	end

GO
/****** Object:  UserDefinedFunction [dbo].[CalculateBasicPrice]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[CalculateBasicPrice] (
	@itemVersionID bigint
) returns money

as

	begin

	declare @basicPrice money = 0,
		@containerSize decimal (7, 2),
		@packPrice money, 
		@packSize decimal (7, 2), 
		@productPackPeriodicID bigint,
		@quantity decimal (9, 2),
		@specialContainerIndicatorCode char(1),
		@unitPrice money;

	select @basicPrice = iv.BasicPrice,
		@containerSize = pp.ContainerSize,
		@packPrice = ppp.PackPrice, 
		@packSize = pp.PackSize, 
		@productPackPeriodicID = iv.ProductPackPeriodicID,
		@quantity = iv.Quantity,
		@specialContainerIndicatorCode = ppp.SpecialContainerIndicatorCode,
		@unitPrice = ppp.UnitPrice
	from dbo.ItemVersion iv
		left join dbo.ProductPackPeriodic ppp
			inner join dbo.ProductPack pp on ppp.ProductPackID = pp.ProductPackID
		on iv.ProductPackPeriodicID = ppp.ProductPackPeriodicID
	where iv.ItemVersionID = @itemVersionID;

	if (dbo.IsNotDispensedOrReferredBack(@itemVersionID) = 0) begin

		if (dbo.IsInvoicePrice(@itemVersionID) = 0) begin

			if (@productPackPeriodicID is not null) begin

				if (@packPrice is not null and @packPrice > 0) begin

					set @basicPrice = @packPrice * 100;

					if (@specialContainerIndicatorCode = 'S') begin

						if (@containerSize = 1 and @packSize > 1 and @packPrice != @unitPrice) begin

							set @basicPrice = (@basicPrice / @packSize) * @quantity;

						end else if (@containerSize != 0) begin

							set @basicPrice = @basicPrice * (@quantity / @containerSize);

						end
						
					end else if (@specialContainerIndicatorCode = 'B' and @containerSize != 0) begin

						set @basicPrice = (@basicPrice * @quantity) / @containerSize;

					end else if (@packSize != 0) begin

						set @basicPrice = @basicPrice * @quantity;
						set @basicPrice = @basicPrice / @packSize;

					end

					set @basicPrice = Round(@basicPrice, 0);
					set @basicPrice = @basicPrice / 100;

				end

			end

		end

	end

	return @basicPrice;

	end

GO
/****** Object:  UserDefinedFunction [dbo].[CalculateNumberOfHoursRequiredToQualifyForFullPracticePayment]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[CalculateNumberOfHoursRequiredToQualifyForFullPracticePayment] (
	@financialPeriodID integer, @jurisdictionID integer, @numberOfProfessionalFees integer)
returns integer

as

	begin

		declare @practicePaymentAdditionalHoursPerWeek integer = 0,
			@practicePaymentAdditionalItems integer,
			@practicePaymentHoursPerWeek integer = 0,
			@practicePaymentItemsFrom integer;


		-- We start by looking to see whether the number of items being used for the calculation of the 
		-- Practice Payment is subject to a "Minimum dispensing staff level (Hours per week)"...
		select @practicePaymentHoursPerWeek = ppid.HoursPerWeek,
			@practicePaymentItemsFrom = ppid.ItemsFrom,
			@practicePaymentAdditionalHoursPerWeek = ppid.AdditionalHoursPerWeek,
			@practicePaymentAdditionalItems = ppid.AdditionalItems
		from dbo.TariffPracticePaymentItemsDispensed ppid
		where ppid.FinancialPeriodID = @financialPeriodID
			and @numberOfProfessionalFees between ppid.ItemsFrom and ppid.ItemsTo;

		if (@@rowcount = 1) begin
	
			-- There is a "Minimum dispensing staff level" stipulation for this number of items

			-- Is this the top of the range? If so, there may be additional hours per items to be calculated...
			if (@practicePaymentAdditionalItems is not null and @practicePaymentAdditionalItems > 0) begin
		
				-- Yes, this is the top of the declared range, we must therefore start calculating the "virtual ranges"
				-- and determine where exactly the number of items dispensed fits.
			
				-- Note that if @numberOfSupportHoursDeclared is lower than @practicePaymentHoursPerWeek we will bypass this block
				-- because of the testing of @remainder and @numberOfSupportHoursDeclared inside the loop...

				declare @remainder integer = @numberOfProfessionalFees - @practicePaymentItemsFrom;
				while (@remainder > 0) begin
			
					if (@remainder >= @practicePaymentAdditionalItems) begin
				
						-- We have yet to find the virtual band, keep incrementing...
					
						set @practicePaymentHoursPerWeek = @practicePaymentHoursPerWeek + @practicePaymentAdditionalHoursPerWeek;
						set @remainder = @remainder - @practicePaymentAdditionalItems;

					end else begin
				
						-- We cannot meet the criteria of this band. We will therefore use the last one...
						set @remainder = 0;

					end
			
				end
			
			end
		
		end

		return @practicePaymentHoursPerWeek;

	end

GO
/****** Object:  UserDefinedFunction [dbo].[CalculateNumberOfNewMedicineServiceItems]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[CalculateNumberOfNewMedicineServiceItems] (
	@batchID bigint,
	@financialPeriodID integer = null
) returns integer

as

	begin

		declare @numberOfNewMedicineServiceItems integer = 0;

		if (@financialPeriodID is null)
			select @financialPeriodID = b.FinancialPeriodID
			from dbo.Batch b
			where b.BatchID = @batchID;

		if (@financialPeriodID <= 201204) begin

			select @numberOfNewMedicineServiceItems = isnull(sd.NewMedicineServiceItems , @numberOfNewMedicineServiceItems)
			from dbo.Batch b
				inner join dbo.ScheduleDocumentData sd on b.ScheduleID = sd.ScheduleID
			where b.BatchID = @batchID

		end else begin

			select @numberOfNewMedicineServiceItems = count(*)
			from (
				select d.DocumentID, i.Sequence
				from dbo.Document d
					inner join dbo.Item i
						inner join dbo.ItemVersion iv on i.CurrentItemVersionID = iv.ItemVersionID
					on d.DocumentID = i.DocumentID
				where d.BatchID = @batchID
					and iv.ProductPackPeriodicID is not null
				group by d.DocumentID, i.Sequence
			) items;

		end

		return @numberOfNewMedicineServiceItems;

	end

GO
/****** Object:  UserDefinedFunction [dbo].[CalculateValueOfNewMedicineServiceFee]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[CalculateValueOfNewMedicineServiceFee] (
	@batchID bigint,
	@financialPeriodID integer = null,
	@numberOfNewMedicineServiceItems integer = null,
	@numberOfNewMedicineServicesUndertaken integer
) returns money

as

	/*
	 * For the moment, we simply copy the fee value paid by the NHSBSA on the original SOP.
	 * This may change later once the PSNC and the NHS agree on how the fee should be calculated but until that time
	 * the PSNC do not plan to audit the calculation.
	 *
	 */

	begin

		declare @valueOfNewMedicineServiceFee money = 0.00;

		if (@financialPeriodID is null)
			select @financialPeriodID = b.FinancialPeriodID
			from dbo.Batch b
			where b.BatchID = @batchID;

		if (@financialPeriodID <= 201204) begin

			-- Prior to May 2012, we simply copy across any values provided by the BSA on the original schedule.

			select @valueOfNewMedicineServiceFee = isnull(sf.OtherFeesNewMedicineServiceConsultation , @valueOfNewMedicineServiceFee)
			from dbo.Batch b
				inner join dbo.ScheduleFee sf on b.ScheduleID = sf.ScheduleID
			where b.BatchID = @batchID;

		end else begin

			-- In May 2012, the way the fee is calculated was revised and agreed by PSNC (& the contractors).
			-- As such, for subsequent batches we will actually recalculate the fee.

			if (@numberOfNewMedicineServicesUndertaken > 0) begin

				-- Make sure we know how many items were eligible for NMS...
				if (@numberOfNewMedicineServiceItems is null)
					set @numberOfNewMedicineServiceItems = dbo.CalculateNumberOfNewMedicineServiceItems(@batchID, @financialPeriodID);

				-- First we need to try and work out the banding into which the number of eligible items falls...
				declare @numberOfQualifyingItemsLowerLimit integer, @numberOfQualifyingItemsIncrementor integer,
					@maximumNumberOfItemsPayable integer, @maximumNumberOfItemsPayableIncrementor integer;
			
				select @numberOfQualifyingItemsLowerLimit = tb.NumberOfQualifyingItemsLowerLimit,
					@numberOfQualifyingItemsIncrementor = tb.NumberOfQualifyingItemsIncrementor,
					@maximumNumberOfItemsPayable = tb.MaximumNumberOfItemsPayable,
					@maximumNumberOfItemsPayableIncrementor = tb.MaximumNumberOfItemsPayableIncrementor
				from dbo.NewMedicineServiceTargetBand tb
				where tb.FinancialPeriodID = @financialPeriodID
					and @numberOfNewMedicineServiceItems between tb.NumberOfQualifyingItemsLowerLimit and tb.NumberOfQualifyingItemsUpperLimit;

				if (@@rowcount = 1) begin

					-- We have a match!

					if (@numberOfQualifyingItemsIncrementor is not null and @numberOfQualifyingItemsIncrementor > 0) begin

						-- The final band is a "catch-all" and has additional data specifying how to calculate "virtual" bands.
						-- We need to check if this is the band that we matched and, if so, generate those virtual bands until we find one that fits
						declare @numberOfQualifyingItemsUpperLimit integer = @numberOfQualifyingItemsLowerLimit + @numberOfQualifyingItemsIncrementor;
						while (@numberOfNewMedicineServiceItems > @numberOfQualifyingItemsUpperLimit) begin

							set @numberOfQualifyingItemsUpperLimit += @numberOfQualifyingItemsIncrementor;
							set @maximumNumberOfItemsPayable += @maximumNumberOfItemsPayableIncrementor;

						end

					end

					-- We should now know the banding.
					-- We need to make sure that the contractor does not claim more than the maximum number of NMS consultations allowed
					if (@numberOfNewMedicineServicesUndertaken > @maximumNumberOfItemsPayable)
						set @numberOfNewMedicineServicesUndertaken = @maximumNumberOfItemsPayable;

					-- Payment is made based on the percentage of the maximum number of consultations that the contractor has declared
					declare @percentageOfTargetConsultationsAcheived money = (cast(@numberOfNewMedicineServicesUndertaken as money) / cast(@maximumNumberOfItemsPayable as money)) * 100;

					-- We now go back to the database to find the fee payable based upon the percentage acheived
					select top 1 @valueOfNewMedicineServiceFee = isnull(PaymentValuePerConsultation, 0.00)
					from dbo.NewMedicineServiceConsultationFee cf
					where cf.FinancialPeriodID = @financialPeriodID
						and @percentageOfTargetConsultationsAcheived >= cf.PercentageOfTargetConsultationsAcheived
					order by cf.PercentageOfTargetConsultationsAcheived desc;

					--- and finally, multiply that rate by the number of consultations declared
					set @valueOfNewMedicineServiceFee = @valueOfNewMedicineServiceFee * @numberOfNewMedicineServicesUndertaken;

				end

			end
			
		end

		return @valueOfNewMedicineServiceFee;

	end

GO
/****** Object:  UserDefinedFunction [dbo].[CalculateValueOfPracticePayment]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[CalculateValueOfPracticePayment] (
	@financialPeriodID integer, @jurisdictionID integer, @numberOfProfessionalFees integer, @numberOfSupportHoursDeclared integer, @originalScheduleId integer)
returns money
as

	begin

		declare @contributionInPracticePaymentFixed money,
			@contributionInPracticePaymentPerItem money,
			@practicePaymentAdditionalHoursPerWeek integer,
			@practicePaymentAdditionalItems integer,
			@practicePaymentFixed money,
			@practicePaymentHoursPerWeek integer,
			@practicePaymentItemsFrom integer,
			@practicePaymentItemsTo integer,
			@practicePaymentPerItem money,
			@ProtectedAdditionalPayment money,
			@DisposalOfUnwantedMedicinesAndSignposting money,
			@PromotionOfHealthyLifestyleAndSupportForSelfCare money,
			@ClinicalGovernance money,
			@SupportForPeopleWithDisabilities money,
			@valueOfPracticePayment money = 0.00;
			
			select  @ProtectedAdditionalPayment = ProtectedAdditionalPayment,
					@DisposalOfUnwantedMedicinesAndSignposting = DisposalOfUnwantedMedicinesAndSignposting,
					@PromotionOfHealthyLifestyleAndSupportForSelfCare = PromotionOfHealthyLifestyleAndSupportForSelfCare,
					@ClinicalGovernance = ClinicalGovernance,
					@SupportForPeopleWithDisabilities = SupportForPeopleWithDisabilities
			from	dbo.ScheduleFee
			where	ScheduleId = @originalScheduleId;

		if (@numberOfProfessionalFees > 0) begin
		
			-- We start by looking to see whether the number of items being used for the calculation of the 
			-- Practice Payment is subject to a "Minimum dispensing staff level (Hours per week)"...
			select @practicePaymentHoursPerWeek = ppid.HoursPerWeek,
				@practicePaymentItemsFrom = ppid.ItemsFrom,
				@practicePaymentAdditionalHoursPerWeek = ppid.AdditionalHoursPerWeek,
				@practicePaymentAdditionalItems = ppid.AdditionalItems
			from dbo.TariffPracticePaymentItemsDispensed ppid
			where ppid.FinancialPeriodID = @financialPeriodID
				and @numberOfProfessionalFees between ppid.ItemsFrom and ppid.ItemsTo;

			if (@@rowcount = 1) begin
		
				-- There is a "Minimum dispensing staff level" stipulation for this number of items

				-- Is this the top of the range? If so, there may be additional hours per items to be calculated...
				if (@practicePaymentAdditionalItems is not null and @practicePaymentAdditionalItems > 0) begin
				
					set @practicePaymentItemsTo = @practicePaymentItemsFrom + (@practicePaymentAdditionalItems - 1);
					while (@numberOfProfessionalFees > @practicePaymentItemsTo and @numberOfSupportHoursDeclared >= (@practicePaymentHoursPerWeek + @practicePaymentAdditionalHoursPerWeek)) begin
					
						set @practicePaymentItemsFrom += @practicePaymentAdditionalItems;
						set @practicePaymentItemsTo += @practicePaymentAdditionalItems;
						set @practicePaymentHoursPerWeek += @practicePaymentAdditionalHoursPerWeek;
						
					end
				
					if (@numberOfProfessionalFees > @practicePaymentItemsTo)
						set @numberOfProfessionalFees = @practicePaymentItemsFrom
				
				end
			
				-- We now test that our number of items and support hours is fully supported within the band.
				-- Note that we should simply fall through this if we moved into the virtual banding above...
			
				if (@practicePaymentHoursPerWeek > @numberOfSupportHoursDeclared) begin

					-- The number of support hours specified is lower than the number of support hours required
					-- for the number of @numberOfProfessionalFees specified.
					-- We must therefore base the payment on the number of hours declared...

					select @numberOfProfessionalFees = max(ppid.ItemsFrom)
					from dbo.TariffPracticePaymentItemsDispensed ppid
					where ppid.FinancialPeriodID = @financialPeriodID
						and ppid.HoursPerWeek <= @numberOfSupportHoursDeclared;

					if (@numberOfProfessionalFees is null or @numberOfProfessionalFees = 0) begin

						-- The number of hours declared is lower than the minimum allowed in the table.
						-- We will therefore penalise the contractor by paying the minimum entry instead...

						set @numberOfProfessionalFees = 0

					end

				end

			end
		
			-- We should now have determined the number of items that we are allowed to use for the determination of the
			-- practice payment based upon the number of professional fees and the number of support hours declared.
		
			-- We can now look up the number of items to determine the value of the payment to be made...
			select @practicePaymentFixed = PracticePaymentFixed, 
				@practicePaymentPerItem = PracticePaymentPerItem, 
				@contributionInPracticePaymentFixed = ContributionInPracticePaymentFixed, 
				@contributionInPracticePaymentPerItem = ContributionInPracticePaymentPerItem
			from dbo.TariffPracticePayment pp
			where pp.FinancialPeriodID = @financialPeriodID and pp.JurisdictionID = @jurisdictionID
				and @numberOfProfessionalFees between pp.ItemsFrom and pp.ItemsTo;

			if (@@rowcount = 1) begin

				-- This could be a calculated value rather than fixed...
				set @valueOfPracticePayment = @practicePaymentFixed;
				if (@valueOfPracticePayment is null or @valueOfPracticePayment = 0.00) begin

					set @valueOfPracticePayment = @practicePaymentPerItem * @numberOfProfessionalFees;

				end else begin

					declare @practicePaymentDivisor integer;

					select @practicePaymentDivisor = jp.PracticePaymentDivisor
					from dbo.JurisdictionPeriodic jp
					where jp.JurisdictionID = @jurisdictionID and jp.FinancialPeriodID = @financialPeriodID;
					
					--set @valueOfPracticePayment = @valueOfPracticePayment / @practicePaymentDivisor;
					set @valueOfPracticePayment = (ISNULL(@valueOfPracticePayment, 0) + ISNULL(@ProtectedAdditionalPayment,0) + ISNULL(@DisposalOfUnwantedMedicinesAndSignposting,0) + ISNULL(@PromotionOfHealthyLifestyleAndSupportForSelfCare,0) + ISNULL(@ClinicalGovernance,0) + ISNULL(@SupportForPeopleWithDisabilities,0)) / @practicePaymentDivisor;

				end

			end
		
		end
	
		return round(@valueOfPracticePayment, 2);

	end

GO
/****** Object:  UserDefinedFunction [dbo].[ContractorSearch]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ContractorSearch] (
	@likePredicate varchar(4000),
	@freeTextPredicate varchar(4000),
	@potentiallyClosedOnly bit = 0
) returns @results table (
	ID bigint primary key,
	Code varchar(7),
	Name varchar(128),
	Address varchar(max) null,
	Postcode varchar(8) null,
	IsRegisteredForStomaCustomisation bit,
	IsRegisteredForApplianceUseReview bit,
	Notes varchar(max) null,
	DateClosed datetime null
)

as

	begin

		-- 2 Financial periods ago...
		declare @financialPeriodID integer  = (
			select top 1 FinancialPeriodID
			from (
				select top 2 FinancialPeriodID
				from dbo.FinancialPeriod
				where FinancialPeriodID < (
					select FinancialPeriodID
					from dbo.FinancialPeriod
					where IsDefault = 1
				)
				order by FinancialPeriodID desc
			) fp
			order by FinancialPeriodID
		);

		insert into @results
		select ContractorID,
			Code, Name, Address, Postcode, 
			IsRegisteredForStomaCustomisation, IsRegisteredForApplianceUseReview,
			Notes, DateClosed
		from (

			select *
			from dbo.Contractor c
			where (@freeTextPredicate != '' and contains(c.*, @freeTextPredicate))
				and (@potentiallyClosedOnly = 0 or (@potentiallyClosedOnly = 1 and (select count(*) from dbo.Batch b where c.ContractorID = b.ContractorID and @financialPeriodID <= b.FinancialPeriodID) = 0))

			union

			select *
			from dbo.Contractor c
			where (@likePredicate != '' and c.Code like @likePredicate)
				and (@potentiallyClosedOnly = 0 or (@potentiallyClosedOnly = 1 and (select count(*) from dbo.Batch b where c.ContractorID = b.ContractorID and @financialPeriodID <= b.FinancialPeriodID) = 0))

			union

			select *
			from dbo.Contractor c
			where (@likePredicate != '' and c.Name like @likePredicate)
				and (@potentiallyClosedOnly = 0 or (@potentiallyClosedOnly = 1 and (select count(*) from dbo.Batch b where c.ContractorID = b.ContractorID and @financialPeriodID <= b.FinancialPeriodID) = 0))

			union

			select *
			from dbo.Contractor c
			where (@likePredicate != '' and c.Postcode like @likePredicate)
				and (@potentiallyClosedOnly = 0 or (@potentiallyClosedOnly = 1 and (select count(*) from dbo.Batch b where c.ContractorID = b.ContractorID and @financialPeriodID <= b.FinancialPeriodID) = 0))

		) searchResults;

	return;

	end

GO
/****** Object:  UserDefinedFunction [dbo].[EndorsementCodeList]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[EndorsementCodeList] (
	@itemVersionID bigint)

returns varchar(max)

as

	begin

	declare @endorsementCodeList varchar(max) = '';
	
	select @endorsementCodeList = @endorsementCodeList + ep.EndorsementCode + ' '
	from dbo.ItemVersionEndorsement ive
		inner join dbo.EndorsementPeriodic ep on ive.EndorsementPeriodicID = ep.EndorsementPeriodicID
	where ive.ItemVersionID = @itemVersionID
	order by ep.EndorsementCode;

	return rtrim(@endorsementCodeList);

	end

GO
/****** Object:  UserDefinedFunction [dbo].[IsBatchEligibleForStatusChange]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[IsBatchEligibleForStatusChange] (
	@batchID bigint
)  returns bit

as

	begin

	declare @batchStatusID integer,
		@isEligibleForStatusChange bit = 0;

	select @batchStatusID = BatchStatusID
	from dbo.Batch
	where BatchID = @batchID;

	if (@batchStatusID = 320) begin
	
		-- 1st check in progress

		if not exists (
			
			select iv.ItemVersionID
			from dbo.Document d
				inner join dbo.DocumentGroupType dgt on d.CurrentDocumentGroupTypeID = dgt.DocumentGroupTypeID
				inner join dbo.Item i
					inner join dbo.ItemVersion iv 
						left join dbo.ItemVersionEndorsement ive
							inner join dbo.EndorsementPeriodic ep on ive.EndorsementPeriodicID = ep.EndorsementPeriodicID and ep.EndorsementCode in ('DA', 'RB')
						on iv.ItemVersionID = ive.ItemVersionID
					on i.CurrentItemVersionID = iv.ItemVersionID
				on d.DocumentID = i.DocumentID
			where d.BatchID = @batchID
				and ((dgt.IsVerified = 0 or iv.IsVerified = 0) and ive.ItemVersionID is null)
									
		) and not exists (

			select d.DocumentID
			from dbo.Document d
				inner join dbo.DocumentGroupType dgt on d.CurrentDocumentGroupTypeID = dgt.DocumentGroupTypeID
			where d.BatchID = @batchID
			  and (isnull(d.OriginalDocumentGroupTypeID, 0) != isnull(d.CurrentDocumentGroupTypeID, 0) and dgt.IsVerified = 0)

		) begin

			set @isEligibleForStatusChange = 1;

		end

	end else if (@batchStatusID = 420) begin

	-- 2nd check in progress

		if not exists (

			select iv.ItemVersionID
			from dbo.Document d
				inner join dbo.Item i
					inner join dbo.ItemVersion iv on i.CurrentItemVersionID = iv.ItemVersionID
				on d.DocumentID = i.DocumentID
			where d.BatchID = @batchID
				and (isnull(i.OriginalItemVersionID, 0) != isnull(i.CurrentItemVersionID, 0) and iv.IsConfirmed = 0)

		) and not exists (

			select d.DocumentID
			from dbo.Document d
				inner join dbo.DocumentGroupType dgt on d.CurrentDocumentGroupTypeID = dgt.DocumentGroupTypeID
			where d.BatchID = @batchID
			  and (isnull(d.OriginalDocumentGroupTypeID, 0) != isnull(d.CurrentDocumentGroupTypeID, 0) and dgt.IsConfirmed = 0)

		) and not exists (

			select d.DocumentID
			from dbo.Document d
				inner join dbo.Item i
					inner join dbo.ItemVersion iv on i.CurrentItemVersionID = iv.ItemVersionID
				on d.DocumentID = i.DocumentID
				inner join dbo.DocumentGroupType dgt on d.CurrentDocumentGroupTypeID = dgt.DocumentGroupTypeID
			where d.BatchID = @batchID
			  and i.IsRandomAccuracyCheck = 1 and (iv.IsConfirmed = 0 or dgt.IsConfirmed = 0)

		) begin

			set @isEligibleForStatusChange = 1;

		end

	end

	return @isEligibleForStatusChange;

	end;

GO
/****** Object:  UserDefinedFunction [dbo].[IsInvoicePrice]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[IsInvoicePrice] (
	@itemVersionID bigint)
returns bit

as

	begin

	declare @isInvoicePrice bit = 0;

	if exists (
		select *
		from dbo.ItemVersionEndorsement ive
			inner join dbo.EndorsementPeriodic ep on ive.EndorsementPeriodicID = ep.EndorsementPeriodicID
		where ive.ItemVersionID = @itemVersionID
		  and ep.EndorsementCode = 'IP') begin

			set @isInvoicePrice = 1;

		end

	return @isInvoicePrice;

	end

GO
/****** Object:  UserDefinedFunction [dbo].[IsNotDispensedOrReferredBack]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[IsNotDispensedOrReferredBack] (
	@itemVersionID bigint)
returns bit

as

	begin

	declare @isNotDispensedOrReferredBack bit = 0;

	if exists (
		select *
		from dbo.ItemVersionEndorsement ive
			inner join dbo.EndorsementPeriodic ep on ive.EndorsementPeriodicID = ep.EndorsementPeriodicID
		where ive.ItemVersionID = @itemVersionID
		  and ep.EndorsementCode in ('ND', 'RB')) begin

			set @isNotDispensedOrReferredBack = 1;

		end

	return @isNotDispensedOrReferredBack;

	end

GO
/****** Object:  UserDefinedFunction [dbo].[ProductSearch]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ProductSearch] (
	@likePredicate varchar(4000),
	@freeTextPredicate varchar(4000))

returns @results table (
	ProductID bigint primary key)

as

	begin

		insert into @results
		select *
		from (

			select ProductID
			from dbo.Product p
			where contains(p.*, @freeTextPredicate)

			/*

			union

			select ProductID
			from dbo.Product p
			where p.Code like @likePredicate

			union

			select ProductID
			from dbo.Product p
			where p.Description like @likePredicate

			*/

		) results

	return
	end

GO
/****** Object:  Table [dbo].[Activity]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Activity](
	[ActivityID] [int] NOT NULL,
	[Description] [varchar](128) NOT NULL,
	[IsUpdate] [bit] NOT NULL CONSTRAINT [DF_Activity_IsUpdate]  DEFAULT ((0)),
 CONSTRAINT [PK_Activity] PRIMARY KEY CLUSTERED 
(
	[ActivityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Advisory]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Advisory](
	[AdvisoryID] [int] IDENTITY(1,1) NOT NULL,
	[AdvisoryGroupID] [int] NOT NULL,
	[Name] [varchar](256) NOT NULL,
	[Description] [varchar](max) NULL,
	[IsProductPrescribedRequired] [bit] NOT NULL CONSTRAINT [DF_Advisory_IsProductPrescribedRequired]  DEFAULT ((0)),
	[IsDispenserEndorsementsRequired] [bit] NOT NULL CONSTRAINT [DF_Advisory_IsDispenserEndorsementsRequired]  DEFAULT ((0)),
	[IsProductPaidRequired] [bit] NOT NULL CONSTRAINT [DF_Advisory_IsProductPaidRequired]  DEFAULT ((0)),
	[DefaultComment] [varchar](max) NULL,
	[IsCommentRequired] [bit] NOT NULL CONSTRAINT [DF_Advisory_IsCommentRequired]  DEFAULT ((0)),
	[IsDetailedListRequired] [bit] NOT NULL CONSTRAINT [DF_Advisory_IsDetailedListRequired]  DEFAULT ((1)),
	[ShowAtBottomOfGroup] [bit] NOT NULL CONSTRAINT [DF_Advisory_ShowAtBottomOfGroup]  DEFAULT ((0)),
	[RecommendedNumberOfInstances] [int] NULL,
 CONSTRAINT [PK_Advisory] PRIMARY KEY CLUSTERED 
(
	[AdvisoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)  

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AdvisoryGroup]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AdvisoryGroup](
	[AdvisoryGroupID] [int] IDENTITY(1,1) NOT NULL,
	[Header] [varchar](max) NOT NULL,
	[Description] [varchar](max) NOT NULL,
	[DisplaySequence] [int] NOT NULL,
 CONSTRAINT [PK_AdvisoryGroup] PRIMARY KEY CLUSTERED 
(
	[AdvisoryGroupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)  

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ApplianceTypeIndicator]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ApplianceTypeIndicator](
	[ApplianceTypeIndicatorCode] [varchar](1) NOT NULL,
	[Description] [varchar](max) NOT NULL,
	[IsMeasuredAndFitted] [bit] NOT NULL CONSTRAINT [DF_ApplianceTypeIndicator_IsMeasuredAndFitted]  DEFAULT ((0)),
	[IsHosiery] [bit] NOT NULL CONSTRAINT [DF_ApplianceTypeIndicator_IsHosiery]  DEFAULT ((0)),
 CONSTRAINT [PK_ApplianceTypeIndicator] PRIMARY KEY CLUSTERED 
(
	[ApplianceTypeIndicatorCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)  

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AuditFeedback]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AuditFeedback](
	[ItemID] [bigint] NOT NULL,
	[ProductPrescribed] [varchar](max) NULL,
	[DispenserEndorsements] [varchar](max) NULL,
	[ProductPaid] [varchar](max) NULL,
	[ProductChecked] [varchar](max) NULL,
	[Comment] [varchar](max) NULL,
	[UserProfileID] [bigint] NULL,
 CONSTRAINT [PK_AuditFeedback] PRIMARY KEY CLUSTERED 
(
	[ItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)  

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AuditType]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AuditType](
	[AuditTypeID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](128) NOT NULL,
	[DisplaySequence] [int] NOT NULL CONSTRAINT [DF_AuditType_DisplaySequence]  DEFAULT ((0)),
	[IsEnabled] [bit] NOT NULL CONSTRAINT [DF_AuditType_IsEnabled]  DEFAULT ((1)),
 CONSTRAINT [PK_AuditType] PRIMARY KEY CLUSTERED 
(
	[AuditTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Batch]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Batch](
	[BatchID] [bigint] IDENTITY(1,1) NOT NULL,
	[ContractorID] [bigint] NOT NULL,
	[FinancialPeriodID] [int] NOT NULL,
	[Name] [varchar](128) NULL,
	[TradingAs] [varchar](128) NULL,
	[Address] [varchar](max) NULL,
	[Postcode] [varchar](8) NULL,
	[IsLocalPharmaceuticalScheme] [bit] NOT NULL CONSTRAINT [DF_Batch_IsLPS]  DEFAULT ((0)),
	[BatchStatusID] [int] NOT NULL,
	[ScheduleID] [bigint] NULL,
	[DivisionID] [int] NULL,
	[LocalPharmaceuticalCommitteeID] [int] NULL,
	[PrimaryCareTrustCode] [varchar](3) NULL,
	[DataRequestID] [bigint] NULL,
	[AuditTypeID] [int] NULL,
	[LastStatusChangeByUserProfileID] [int] NULL,
	[PsncScheduleID] [bigint] NULL,
	[CurrentBundleTrackingID] [bigint] NULL,
	[CurrentFragmentTrackingID] [bigint] NULL,
 CONSTRAINT [PK_Batch] PRIMARY KEY CLUSTERED 
(
	[BatchID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)  

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BatchAuditStatistics]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BatchAuditStatistics](
	[BatchID] [bigint] NOT NULL,
	[NumberOfVerifiedItems] [int] NOT NULL,
	[NumberOfMultiLineChanges] [int] NOT NULL CONSTRAINT [DF_BatchAuditStatistics_NumberOfMultiLineChanges]  DEFAULT ((0)),
	[NumberOfMissedItems] [int] NOT NULL,
	[NumberOfExtraItems] [int] NOT NULL,
	[NumberOfProductChanges] [int] NOT NULL,
	[NumberOfQuantityChanges] [int] NOT NULL,
	[NumberOfPackChanges] [int] NOT NULL,
	[NumberOfProfessionalFeeChanges] [int] NOT NULL,
	[NumberOfPatientChargeChanges] [int] NOT NULL,
	[NumberOfSurchargeChanges] [int] NOT NULL,
	[NumberOfInvoicePriceChanges] [int] NOT NULL,
	[NumberOfOtherChanges] [int] NOT NULL CONSTRAINT [DF_BatchAuditStatistics_NumberOfOtherChanges]  DEFAULT ((0)),
	[TotalNumberOfChanges] [int] NOT NULL,
	[NumberOfVerifiedItemsWithChanges] [int] NOT NULL,
	[NumberOfVerifiedItemsWithChangesThatHaveFinancialImpact] [int] NOT NULL,
	[NetCashVariance] [money] NOT NULL,
	[AbsCashVariance] [money] NOT NULL,
	[NetProfessionalFeeVariance] [int] NOT NULL,
	[AbsProfessionalFeeVariance] [int] NOT NULL,
	[NetPaymentForContainersVariance] [int] NOT NULL,
	[AbsPaymentForContainersVariance] [int] NOT NULL,
 CONSTRAINT [PK_BatchItemErrorStatistics] PRIMARY KEY CLUSTERED 
(
	[BatchID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
/****** Object:  Table [dbo].[BatchManualDeleteHistory]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BatchManualDeleteHistory](
	[BatchID] [int] NULL,
	[ContractorID] [int] NULL,
	[Code] [varchar](25) NULL,
	[Name] [varchar](50) NULL,
	[TradingAs] [varchar](50) NULL,
	[Address] [varchar](75) NULL,
	[Postcode] [varchar](15) NULL,
	[BatchStatusID] [int] NULL,
	[FinancialPeriodID] [int] NULL,
	[DeletedDate] [datetime] NULL
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BatchStatus]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BatchStatus](
	[BatchStatusID] [int] NOT NULL,
	[Description] [varchar](128) NOT NULL,
	[ShowOnDashboard] [bit] NOT NULL CONSTRAINT [DF_BatchStatus_ShowOnDashboard]  DEFAULT ((0)),
	[AdditionalDataPrompt] [varchar](128) NULL,
	[IsVisibleInAuditClientTo1stChecker] [bit] NOT NULL CONSTRAINT [DF_BatchStatus_IsVisibleInAuditClient1]  DEFAULT ((0)),
	[IsVisibleInAuditClientTo2ndChecker] [bit] NOT NULL CONSTRAINT [DF_BatchStatus_IsVisibleInAuditClient2]  DEFAULT ((0)),
 CONSTRAINT [PK_BatchStatus] PRIMARY KEY CLUSTERED 
(
	[BatchStatusID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BatchStatusHistory]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BatchStatusHistory](
	[BatchStatusHistoryID] [bigint] IDENTITY(1,1) NOT NULL,
	[BatchID] [bigint] NOT NULL,
	[BatchStatusID] [int] NOT NULL,
	[DateTime] [datetime] NOT NULL,
	[UserProfileID] [bigint] NOT NULL,
	[AdditionalData] [varchar](max) NULL,
 CONSTRAINT [PK_BatchStatusHistory] PRIMARY KEY CLUSTERED 
(
	[BatchStatusHistoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)  

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BatchUserProfile]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BatchUserProfile](
	[BatchUserProfileID] [bigint] IDENTITY(1,1) NOT NULL,
	[BatchID] [bigint] NOT NULL,
	[UserProfileID] [bigint] NOT NULL,
	[LastDocumentID] [bigint] NOT NULL,
 CONSTRAINT [PK_BatchUserProfile] PRIMARY KEY CLUSTERED 
(
	[BatchUserProfileID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
/****** Object:  Table [dbo].[BorderlineSubstanceIndicator]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BorderlineSubstanceIndicator](
	[BorderlineSubstanceIndicatorCode] [varchar](1) NOT NULL,
	[Description] [varchar](max) NOT NULL,
 CONSTRAINT [PK_BorderlineSubstanceIndicator] PRIMARY KEY CLUSTERED 
(
	[BorderlineSubstanceIndicatorCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)  

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Contractor]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Contractor](
	[ContractorID] [bigint] IDENTITY(1,1) NOT NULL,
	[JurisdictionID] [int] NOT NULL,
	[Code] [varchar](7) NOT NULL,
	[Name] [varchar](128) NOT NULL,
	[TradingAs] [varchar](128) NULL,
	[Address] [varchar](max) NULL,
	[Postcode] [varchar](8) NULL,
	[IsRegisteredForStomaCustomisation] [bit] NOT NULL CONSTRAINT [DF_Contractor_IsStomaCustomisationAvailable]  DEFAULT ((0)),
	[RegisteredForStomaCustomisationSince] [int] NULL,
	[IsRegisteredForApplianceUseReview] [bit] NOT NULL CONSTRAINT [DF_Contractor_IsRegisteredForApplianceUseReview]  DEFAULT ((0)),
	[RegisteredForApplianceUseReviewSince] [int] NULL,
	[DivisionID] [int] NULL,
	[LocalPharmaceuticalCommitteeID] [int] NULL,
	[DateOpened] [datetime] NULL,
	[DateClosed] [datetime] NULL,
	[Notes] [varchar](max) NULL,
	[LastAuditFinancialPeriodID] [int] NULL,
	[YPCode] [varchar](12) NOT NULL CONSTRAINT [DF_Contractor_YPCode]  DEFAULT (''),
 CONSTRAINT [PK_Contractor] PRIMARY KEY CLUSTERED 
(
	[ContractorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)  

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ContractorDelta]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContractorDelta](
	[ContractorDeltaID] [bigint] IDENTITY(1,1) NOT NULL,
	[ContractorID] [bigint] NOT NULL,
	[JurisdictionID] [int] NOT NULL,
	[Code] [varchar](7) NOT NULL,
	[Name] [varchar](128) NOT NULL,
	[TradingAs] [varchar](128) NULL,
	[Address] [varchar](max) NULL,
	[Postcode] [varchar](8) NULL,
	[IsRegisteredForStomaCustomisation] [bit] NOT NULL,
	[RegisteredForStomaCustomisationSince] [int] NULL,
	[IsRegisteredForApplianceUseReview] [bit] NOT NULL,
	[RegisteredForApplianceUseReviewSince] [int] NULL,
	[DivisionID] [int] NULL,
	[LocalPharmaceuticalCommitteeID] [int] NULL,
	[DateOpened] [datetime] NULL,
	[DateClosed] [datetime] NULL,
	[Notes] [varchar](max) NULL,
	[LastAuditFinancialPeriodID] [int] NULL,
	[YPCode] [varchar](12) NOT NULL,
 CONSTRAINT [PK_ContractorDelta] PRIMARY KEY CLUSTERED 
(
	[ContractorDeltaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)  

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ContractorDeltaHistory]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContractorDeltaHistory](
	[ContractorDeltaID] [bigint] NOT NULL,
	[ContractorID] [bigint] NOT NULL,
	[JurisdictionID] [int] NOT NULL,
	[Code] [varchar](7) NOT NULL,
	[Name] [varchar](128) NOT NULL,
	[TradingAs] [varchar](128) NULL,
	[Address] [varchar](max) NULL,
	[Postcode] [varchar](8) NULL,
	[IsRegisteredForStomaCustomisation] [bit] NOT NULL,
	[RegisteredForStomaCustomisationSince] [int] NULL,
	[IsRegisteredForApplianceUseReview] [bit] NOT NULL,
	[RegisteredForApplianceUseReviewSince] [int] NULL,
	[DivisionID] [int] NULL,
	[LocalPharmaceuticalCommitteeID] [int] NULL,
	[DateOpened] [datetime] NULL,
	[DateClosed] [datetime] NULL,
	[Notes] [varchar](max) NULL,
	[LastAuditFinancialPeriodID] [int] NULL,
	[YPCode] [varchar](12) NOT NULL,
	[ImportedDate] [datetime] NULL,
	[ReportingDate] [datetime] NULL,
	[ExportedDate] [datetime] NULL,
	[CreatedDate] [datetime] NULL
)  

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ControlledSubstanceIndicator]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ControlledSubstanceIndicator](
	[ControlledSubstanceIndicatorCode] [varchar](1) NOT NULL,
	[Description] [varchar](max) NOT NULL,
 CONSTRAINT [PK_ControlledSubstanceIndicator] PRIMARY KEY CLUSTERED 
(
	[ControlledSubstanceIndicatorCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)  

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ControlledSubstanceIndicatorPeriodic]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ControlledSubstanceIndicatorPeriodic](
	[ControlledSubstanceIndicatorPeriodicID] [bigint] IDENTITY(1,1) NOT NULL,
	[JurisdictionID] [int] NOT NULL,
	[FinancialPeriodID] [int] NOT NULL,
	[ControlledSubstanceIndicatorCode] [varchar](1) NOT NULL,
	[AdditionalProfessionalFeeValue] [money] NOT NULL,
 CONSTRAINT [PK_ControlledSubstanceIndicatorPeriodic] PRIMARY KEY CLUSTERED 
(
	[ControlledSubstanceIndicatorPeriodicID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DataRequest]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DataRequest](
	[DataRequestID] [bigint] IDENTITY(1,1) NOT NULL,
	[JurisdictionID] [int] NOT NULL CONSTRAINT [DF_DataRequest_JurisdictionID]  DEFAULT ((1)),
	[IssuedAt] [datetime] NULL,
 CONSTRAINT [PK_DataRequest] PRIMARY KEY CLUSTERED 
(
	[DataRequestID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
/****** Object:  Table [dbo].[DentalFormularyIndicator]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DentalFormularyIndicator](
	[DentalFormularyIndicatorCode] [varchar](1) NOT NULL,
	[Description] [varchar](max) NOT NULL,
	[IsUsedEng] [bit] NULL,
	[IsUsedGue] [bit] NULL,
	[IsUsedJer] [bit] NULL,
 CONSTRAINT [PK_DentalFormularyIndicator] PRIMARY KEY CLUSTERED 
(
	[DentalFormularyIndicatorCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)  

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DiluentIndicator]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DiluentIndicator](
	[DiluentIndicatorCode] [varchar](1) NOT NULL,
	[Description] [varchar](max) NOT NULL,
 CONSTRAINT [PK_DiluentIndicator] PRIMARY KEY CLUSTERED 
(
	[DiluentIndicatorCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)  

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Division]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Division](
	[DivisionID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](3) NULL,
	[Name] [varchar](50) NULL,
 CONSTRAINT [PK_Division] PRIMARY KEY CLUSTERED 
(
	[DivisionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DivisionAlias]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DivisionAlias](
	[DivisionAliasID] [int] IDENTITY(1,1) NOT NULL,
	[DivisionID] [int] NOT NULL,
	[Code] [varchar](3) NULL,
	[Description] [varchar](50) NULL,
 CONSTRAINT [PK_DivisionAlias] PRIMARY KEY CLUSTERED 
(
	[DivisionAliasID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Document]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Document](
	[DocumentID] [bigint] IDENTITY(1,1) NOT NULL,
	[BatchID] [bigint] NOT NULL,
	[SubmissionGroupCode] [varchar](2) NULL,
	[FragmentTypeCode] [char](1) NULL,
	[DocumentNumber] [int] NOT NULL,
	[FormTypeCode] [varchar](2) NULL,
	[FormSubTypeCode] [varchar](2) NULL,
	[FulfilmentDate] [date] NULL,
	[OriginalDocumentGroupTypeID] [bigint] NULL,
	[CurrentDocumentGroupTypeID] [bigint] NULL,
	[ExemptionCode] [char](1) NULL,
	[AmountPaid] [money] NOT NULL,
	[SignedByPatient] [bit] NOT NULL,
	[SignedByRepresentative] [bit] NOT NULL,
	[DocumentStatusID] [int] NOT NULL,
	[UUID] [nvarchar](36) NULL CONSTRAINT [DF_Document_UUID]  DEFAULT (NULL),
 CONSTRAINT [PK_Document] PRIMARY KEY CLUSTERED 
(
	[DocumentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DocumentGroupType]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DocumentGroupType](
	[DocumentGroupTypeID] [bigint] IDENTITY(1,1) NOT NULL,
	[DocumentID] [bigint] NOT NULL,
	[GroupTypeCode] [char](1) NOT NULL,
	[InsertedOn] [datetime] NOT NULL CONSTRAINT [DF_DocumentGroupType_InsertedOn]  DEFAULT (getdate()),
	[InsertedBy] [varchar](128) NOT NULL CONSTRAINT [DF_DocumentGroupType_InsertedBy]  DEFAULT (original_login()),
	[IsReferred] [bit] NOT NULL CONSTRAINT [DF_DocumentGroupType_Referred]  DEFAULT ((0)),
	[IsVerified] [bit] NOT NULL CONSTRAINT [DF_DocumentGroupType_Verified]  DEFAULT ((0)),
	[VerifiedOn] [datetime] NULL,
	[VerifiedBy] [varchar](128) NULL,
	[IsConfirmed] [bit] NOT NULL CONSTRAINT [DF_DocumentGroupType_Confirmed]  DEFAULT ((0)),
	[ConfirmedOn] [datetime] NULL,
	[ConfirmedBy] [varchar](128) NULL,
 CONSTRAINT [PK_DocumentGroupType] PRIMARY KEY CLUSTERED 
(
	[DocumentGroupTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DocumentImage]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DocumentImage](
	[DocumentImageID] [bigint] IDENTITY(1,1) NOT NULL,
	[DocumentID] [bigint] NOT NULL,
	[DocumentImageTypeID] [tinyint] NOT NULL,
	[OriginalFilename] [varchar](128) NULL,
	[ImageData] [varbinary](max) NULL,
 CONSTRAINT [PK_DocumentImage] PRIMARY KEY CLUSTERED 
(
	[DocumentImageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DocumentImageType]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DocumentImageType](
	[DocumentImageTypeID] [tinyint] NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[DisplaySequence] [tinyint] NOT NULL,
 CONSTRAINT [PK_DocumentImageType] PRIMARY KEY CLUSTERED 
(
	[DocumentImageTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DocumentStatus]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DocumentStatus](
	[DocumentStatusID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](128) NOT NULL,
 CONSTRAINT [PK_DocumentStatus] PRIMARY KEY CLUSTERED 
(
	[DocumentStatusID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DrugTariffCategory]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DrugTariffCategory](
	[DrugTariffCategoryCode] [varchar](5) NOT NULL,
	[Description] [varchar](max) NOT NULL,
	[StandardDrugClassIndicatorCode] [varchar](1) NULL,
	[PaymentCategoryID] [bigint] NULL,
	[Category] [int] NOT NULL,
	[ToolTipText] [varchar](max) NULL,
 CONSTRAINT [PK_DrugTariffCategory] PRIMARY KEY CLUSTERED 
(
	[DrugTariffCategoryCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)  

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EndorsementPeriodic]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EndorsementPeriodic](
	[EndorsementPeriodicID] [bigint] IDENTITY(1,1) NOT NULL,
	[JurisdictionID] [int] NOT NULL,
	[FinancialPeriodID] [int] NOT NULL,
	[EndorsementCode] [varchar](2) NOT NULL,
	[Description] [varchar](max) NULL,
	[ZeroCharges] [bit] NOT NULL,
	[SpecialFeeValue] [money] NULL,
	[SpecialFeeBasis] [varchar](1) NULL,
	[IsUserSelectable] [bit] NOT NULL CONSTRAINT [DF_EndorsementPeriodic_IsUserSelectable]  DEFAULT ((0)),
	[DisplaySequence] [int] NULL,
 CONSTRAINT [PK_EndorsementPeriodic] PRIMARY KEY CLUSTERED 
(
	[EndorsementPeriodicID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)  

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EpsDispensedItem]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EpsDispensedItem](
	[EpsDispensedItemID] [bigint] IDENTITY(1,1) NOT NULL,
	[EpsPrescribedItemID] [bigint] NOT NULL,
	[SnomedCode] [varchar](255) NULL,
	[PpaCode] [varchar](255) NULL,
	[Description] [varchar](255) NULL,
	[Quantity] [dbo].[Quantity] NULL,
	[UnitOfMeasure] [varchar](255) NULL,
	[Notes] [varchar](255) NULL,
 CONSTRAINT [PK_EpsDispensedItem] PRIMARY KEY CLUSTERED 
(
	[EpsDispensedItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EpsHeader]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EpsHeader](
	[EpsHeaderID] [bigint] IDENTITY(1,1) NOT NULL,
	[FormNumber] [int] NULL,
	[Period] [int] NULL,
	[ContractorAccount] [varchar](5) NULL,
	[ExemptionCategory] [varchar](1) NULL,
	[PaperType] [varchar](10) NULL,
	[PrescriberType] [varchar](2) NULL,
	[DocumentID] [bigint] NULL,
 CONSTRAINT [PK_EpsHeader] PRIMARY KEY CLUSTERED 
(
	[EpsHeaderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EpsPrescribedItem]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EpsPrescribedItem](
	[EpsPrescribedItemID] [bigint] IDENTITY(1,1) NOT NULL,
	[EpsHeaderID] [bigint] NOT NULL,
	[ItemNumber] [smallint] NOT NULL,
	[SnomedCode] [varchar](255) NULL,
	[PpaCode] [varchar](255) NULL,
	[Description] [varchar](255) NULL,
	[Quantity] [dbo].[Quantity] NULL,
	[UnitOfMeasure] [varchar](255) NULL,
	[Notes] [varchar](255) NULL,
 CONSTRAINT [PK_EpsPrescribedItem] PRIMARY KEY CLUSTERED 
(
	[EpsPrescribedItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Exemption]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Exemption](
	[ExemptionCode] [char](1) NOT NULL,
	[Description] [varchar](128) NOT NULL,
 CONSTRAINT [PK_Exemption] PRIMARY KEY CLUSTERED 
(
	[ExemptionCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FinancialPeriod]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FinancialPeriod](
	[FinancialPeriodID] [int] NOT NULL,
	[FinancialYear] [smallint] NOT NULL,
	[FinancialMonth] [tinyint] NOT NULL,
	[IsOpen] [bit] NOT NULL CONSTRAINT [DF_FinancialPeriod_IsOpen]  DEFAULT ((0)),
	[IsDefault] [bit] NOT NULL CONSTRAINT [DF_FinancialPeriod_IsDefault]  DEFAULT ((0)),
	[RandomAccuracyCheckPercentage] [money] NOT NULL CONSTRAINT [DF_FinancialPeriod_RandomAccuracyCheckPercentage]  DEFAULT ((1.5)),
 CONSTRAINT [PK_FinancialPeriod] PRIMARY KEY CLUSTERED 
(
	[FinancialPeriodID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
/****** Object:  Table [dbo].[FormSubType]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FormSubType](
	[FormSubTypeCode] [varchar](2) NOT NULL,
	[Description] [varchar](128) NULL,
 CONSTRAINT [PK_FormSubType] PRIMARY KEY CLUSTERED 
(
	[FormSubTypeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FormType]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FormType](
	[FormTypeCode] [varchar](2) NOT NULL,
	[Description] [varchar](128) NOT NULL,
 CONSTRAINT [PK_FormType] PRIMARY KEY CLUSTERED 
(
	[FormTypeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FragmentType]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FragmentType](
	[FragmentTypeCode] [char](1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
 CONSTRAINT [PK_FragmentType] PRIMARY KEY CLUSTERED 
(
	[FragmentTypeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GroupType]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GroupType](
	[GroupTypeCode] [char](1) NOT NULL,
	[Description] [varchar](50) NULL,
	[IsExemptFromCharges] [bit] NOT NULL,
	[IsOldRate] [bit] NOT NULL CONSTRAINT [DF_GroupType_IsOldRate]  DEFAULT ((0)),
 CONSTRAINT [PK_GroupType] PRIMARY KEY CLUSTERED 
(
	[GroupTypeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GuidanceArticle]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GuidanceArticle](
	[GuidanceArticleID] [int] IDENTITY(1,1) NOT NULL,
	[GuidanceCategoryID] [int] NOT NULL,
	[Title] [varchar](512) NOT NULL,
	[Introduction] [varchar](max) NOT NULL,
	[Body] [varchar](max) NOT NULL,
 CONSTRAINT [PK_GuidanceArticle] PRIMARY KEY CLUSTERED 
(
	[GuidanceArticleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)  

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GuidanceAttachment]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GuidanceAttachment](
	[GuidanceAttachmentID] [int] IDENTITY(1,1) NOT NULL,
	[GuidanceArticleID] [int] NOT NULL,
	[Title] [varchar](255) NOT NULL,
	[Description] [varchar](max) NOT NULL,
	[Content] [varbinary](max) NOT NULL,
	[Filename] [varchar](255) NOT NULL,
	[Extension] [varchar](255) NOT NULL,
	[MimeType] [varchar](255) NOT NULL,
 CONSTRAINT [PK_GuidanceAttachment] PRIMARY KEY CLUSTERED 
(
	[GuidanceAttachmentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)  

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GuidanceCategory]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GuidanceCategory](
	[GuidanceCategoryID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](255) NOT NULL,
	[Description] [varchar](max) NOT NULL,
 CONSTRAINT [PK_GuidanceCategory] PRIMARY KEY CLUSTERED 
(
	[GuidanceCategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)  

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HealthAuthority]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HealthAuthority](
	[HealthAuthorityCode] [varchar](3) NOT NULL,
	[RegionalDirectorateCode] [varchar](3) NULL,
	[Name] [varchar](128) NULL,
	[Address] [varchar](max) NULL,
	[Postcode] [varchar](8) NULL,
 CONSTRAINT [PK_HealthAuthority] PRIMARY KEY CLUSTERED 
(
	[HealthAuthorityCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)  

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HomeDeliveryIndicator]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HomeDeliveryIndicator](
	[HomeDeliveryIndicatorCode] [varchar](1) NOT NULL,
	[Description] [varchar](max) NOT NULL,
 CONSTRAINT [PK_HomeDeliveryIndicator] PRIMARY KEY CLUSTERED 
(
	[HomeDeliveryIndicatorCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)  

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[InputQuantityUnitIndicator]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[InputQuantityUnitIndicator](
	[InputQuantityUnitIndicatorCode] [varchar](1) NOT NULL,
	[Description] [varchar](max) NOT NULL,
 CONSTRAINT [PK_InputQuantityUnitIndicator] PRIMARY KEY CLUSTERED 
(
	[InputQuantityUnitIndicatorCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)  

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Item]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Item](
	[ItemID] [bigint] IDENTITY(1,1) NOT NULL,
	[DocumentID] [bigint] NOT NULL,
	[Sequence] [tinyint] NOT NULL,
	[SubSequence] [tinyint] NOT NULL CONSTRAINT [DF_Item_SubSequence]  DEFAULT ((1)),
	[OriginalItemVersionID] [bigint] NULL,
	[PreAuditItemVersionID] [bigint] NULL,
	[CurrentItemVersionID] [bigint] NULL,
	[IsRandomAccuracyCheck] [bit] NOT NULL CONSTRAINT [DF_Item_IsRandomAccuracyCheck]  DEFAULT ((0)),
 CONSTRAINT [PK_Item] PRIMARY KEY CLUSTERED 
(
	[ItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
/****** Object:  Table [dbo].[ItemAdvisory]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ItemAdvisory](
	[ItemAdvisoryID] [bigint] IDENTITY(1,1) NOT NULL,
	[ItemID] [bigint] NOT NULL,
	[AdvisoryID] [int] NOT NULL,
	[ProductPrescribed] [varchar](max) NULL,
	[DispenserEndorsements] [varchar](max) NULL,
	[ProductPaid] [varchar](max) NULL,
	[Comment] [varchar](max) NULL,
 CONSTRAINT [PK_ItemAdvisory] PRIMARY KEY CLUSTERED 
(
	[ItemAdvisoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)  

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ItemNote]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ItemNote](
	[ItemNoteID] [bigint] IDENTITY(1,1) NOT NULL,
	[ItemID] [bigint] NOT NULL,
	[Note] [varchar](max) NULL,
	[InsertedBy] [varchar](128) NOT NULL CONSTRAINT [DF_ItemNote_InsertedBy]  DEFAULT (original_login()),
	[InsertedOn] [datetime] NOT NULL CONSTRAINT [DF_ItemNote_InsertedOn]  DEFAULT (getdate()),
 CONSTRAINT [PK_ItemNote] PRIMARY KEY CLUSTERED 
(
	[ItemNoteID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)  

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ItemVersion]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ItemVersion](
	[ItemVersionID] [bigint] IDENTITY(1,1) NOT NULL,
	[ItemID] [bigint] NOT NULL,
	[ProductPackPeriodicID] [bigint] NULL,
	[Quantity] [dbo].[Quantity] NOT NULL,
	[BasicPrice] [money] NOT NULL,
	[ValueOfExpensiveItemFee] [money] NOT NULL CONSTRAINT [DF_ItemVersion_ValueOfExpensiveItemFee]  DEFAULT ((0)),
	[NumberOfTimesDispensed] [int] NOT NULL,
	[NumberOfProfessionalFees] [int] NOT NULL CONSTRAINT [DF_ItemVersion_NumberOfProfessionalFees]  DEFAULT ((0)),
	[ValueOfAdditionalFees] [money] NOT NULL CONSTRAINT [DF_ItemVersion_ValueOfAdditionalFees]  DEFAULT ((0)),
	[ValueOfOutOfPocketExpenses] [money] NOT NULL CONSTRAINT [DF_ItemVersion_ValueOfOutOfPocketExpenses]  DEFAULT ((0)),
	[NumberOfCharges] [int] NOT NULL,
	[IsBasicPriceVerifiedAgainstMdr] [bit] NOT NULL CONSTRAINT [DF_ItemVersion_IsPriceCalculatedCorrectly]  DEFAULT ((0)),
	[InsertedOn] [datetime] NOT NULL CONSTRAINT [DF_ItemVersion_Inserted]  DEFAULT (getdate()),
	[InsertedBy] [varchar](128) NOT NULL CONSTRAINT [DF_ItemVersion_InsertedBy]  DEFAULT (original_login()),
	[IsReferred] [bit] NOT NULL CONSTRAINT [DF_ItemVersion_IsReferred]  DEFAULT ((0)),
	[ReferredOn] [datetime] NULL,
	[ReferredBy] [varchar](128) NULL,
	[IsVerified] [bit] NOT NULL CONSTRAINT [DF_ItemVersion_IsVerified]  DEFAULT ((0)),
	[VerifiedOn] [datetime] NULL,
	[VerifiedBy] [varchar](128) NULL,
	[IsConfirmed] [bit] NOT NULL CONSTRAINT [DF_ItemVersion_IsConfirmed]  DEFAULT ((0)),
	[ConfirmedOn] [datetime] NULL,
	[ConfirmedBy] [varchar](128) NULL,
	[IsHiddenAdjustment] [bit] NOT NULL CONSTRAINT [DF_ItemVersion_IsVisibleToContractor]  DEFAULT ((0)),
 CONSTRAINT [PK_ItemVersion] PRIMARY KEY CLUSTERED 
(
	[ItemVersionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ItemVersionEndorsement]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ItemVersionEndorsement](
	[ItemVersionID] [bigint] NOT NULL,
	[EndorsementPeriodicID] [bigint] NOT NULL,
	[Quantity] [int] NOT NULL,
 CONSTRAINT [PK_ItemVersionEndorsement] PRIMARY KEY CLUSTERED 
(
	[ItemVersionID] ASC,
	[EndorsementPeriodicID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
/****** Object:  Table [dbo].[Jurisdiction]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Jurisdiction](
	[JurisdictionID] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[DataRequestEmailTo] [varchar](max) NULL,
	[DataRequestEmailCc] [varchar](max) NULL,
	[DataRequestEmailReplyTo] [varchar](max) NULL,
	[DataRequestEmailSubject] [varchar](max) NULL,
	[DataRequestEmailBody] [varchar](max) NULL,
	[DataRequestAttachmentFileName] [varchar](max) NULL,
	[PaperRequestEmailTo] [varchar](max) NULL,
	[PaperRequestEmailCc] [varchar](max) NULL,
	[PaperRequestEmailReplyTo] [varchar](max) NULL,
	[PaperRequestEmailSubject] [varchar](max) NULL,
	[PaperRequestEmailBody] [varchar](max) NULL,
	[PaperRequestAttachmentFileName] [varchar](max) NULL,
 CONSTRAINT [PK_Jurisdiction] PRIMARY KEY CLUSTERED 
(
	[JurisdictionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)  

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[JurisdictionPeriodic]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JurisdictionPeriodic](
	[JurisdictionPeriodicID] [bigint] IDENTITY(1,1) NOT NULL,
	[JurisdictionID] [int] NOT NULL,
	[FinancialPeriodID] [int] NOT NULL,
	[ApplianceItemPrescriptionChargeValue] [money] NOT NULL,
	[DrugItemPrescriptionChargeValue] [money] NOT NULL CONSTRAINT [DF_JurisdictionPeriodic_DrugItemPrescriptionChargeValue]  DEFAULT ((0)),
	[StandardProfessionalFeeValue] [money] NOT NULL,
	[ExpensiveItemThreshold] [money] NOT NULL,
	[ExpensiveItemFeePercentage] [money] NOT NULL,
	[OutOfPocketExpenseDeductionValue] [money] NULL,
	[ContainerAllowanceValue] [money] NULL,
	[RepeatDispensingFee] [money] NULL,
	[MedicinesUseReviewFee] [money] NULL,
	[StomaCustomisationFee] [money] NULL,
	[ApplianceUseReviewAtPharmacyFee] [money] NULL,
	[ApplianceUseReviewAtHomeFee] [money] NULL,
	[ContractTransitionPaymentDivisor] [int] NULL,
	[ContractTransitionPaymentMultiplier] [money] NULL,
	[PracticePaymentDivisor] [int] NULL CONSTRAINT [DF_JurisdictionPeriodic_PracticePaymentDivisor]  DEFAULT ((6)),
	[EstablishmentPaymentDivisor] [int] NULL CONSTRAINT [DF_JurisdictionPeriodic_EstablishmentPaymentDivisor]  DEFAULT ((12)),
	[OutOfPocketExpenseMinimumClaimValue] [money] NULL,
	[ValueOfSupplementaryControlledDrugFee] [money] NULL,
	[ValueOfConsumableAllowanceFee] [money] NULL,
	[PppaTargetPercentage] [money] NOT NULL CONSTRAINT [DF_JurisdictionPeriodic_PppaTargetPercentage]  DEFAULT ((0.975)),
	[NcvLowerTargetPercentage] [money] NOT NULL CONSTRAINT [DF_JurisdictionPeriodic_CashVarianceLowerLimitPercentage]  DEFAULT ((0.998)),
	[NcvUpperTargetPercentage] [money] NOT NULL CONSTRAINT [DF_JurisdictionPeriodic_CashVarianceUpperLimitPercentage]  DEFAULT ((1.002)),
	[AcvLowerTargetPercentage] [money] NOT NULL CONSTRAINT [DF_JurisdictionPeriodic_AcvLowerLimitPercentage]  DEFAULT ((0.998)),
 CONSTRAINT [PK_JurisdictionPeriodic] PRIMARY KEY CLUSTERED 
(
	[JurisdictionPeriodicID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
/****** Object:  Table [dbo].[LocalPharmaceuticalCommittee]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LocalPharmaceuticalCommittee](
	[LocalPharmaceuticalCommitteeID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](7) NULL,
	[Name] [varchar](128) NULL,
	[Address] [varchar](max) NULL,
	[Postcode] [varchar](8) NULL,
	[Telephone] [varchar](15) NULL,
	[Contact] [varchar](128) NULL,
	[Title] [varchar](32) NULL,
	[Firstname] [varchar](128) NULL,
	[Surname] [varchar](128) NULL,
	[Suffix] [varchar](32) NULL,
	[EmailAddress] [varchar](254) NULL,
	[Notes] [varchar](max) NULL,
 CONSTRAINT [PK_LocalPharmaceuticalCommittee] PRIMARY KEY CLUSTERED 
(
	[LocalPharmaceuticalCommitteeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)  

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Logging]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Logging](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MinimumFinancialPeriodIDToRetain] [int] NULL,
	[StartTime] [datetime] NULL,
	[StopTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[Type] [varchar](100) NULL,
	[BatchSize] [int] NULL,
	[RowCountRemaining] [int] NULL,
 CONSTRAINT [PK_Logging] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Manufacturer]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Manufacturer](
	[ManufacturerID] [bigint] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](4) NOT NULL,
	[Name] [varchar](25) NOT NULL,
	[ManufacturerCategoryCode] [varchar](1) NOT NULL,
	[Parent] [varchar](4) NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_Manufacturer] PRIMARY KEY CLUSTERED 
(
	[ManufacturerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ManufacturerCategory]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ManufacturerCategory](
	[ManufacturerCategoryCode] [varchar](1) NOT NULL,
	[Description] [varchar](max) NOT NULL,
 CONSTRAINT [PK_ManufacturerCategory] PRIMARY KEY CLUSTERED 
(
	[ManufacturerCategoryCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)  

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NewMedicineServiceConsultationFee]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewMedicineServiceConsultationFee](
	[NewMedicineServiceConsultationFeeID] [bigint] IDENTITY(1,1) NOT NULL,
	[FinancialPeriodID] [int] NOT NULL,
	[PercentageOfTargetConsultationsAcheived] [money] NOT NULL,
	[PaymentValuePerConsultation] [money] NOT NULL,
 CONSTRAINT [PK_NewMedicineServiceConsultationFee] PRIMARY KEY CLUSTERED 
(
	[NewMedicineServiceConsultationFeeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
/****** Object:  Table [dbo].[NewMedicineServiceTargetBand]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewMedicineServiceTargetBand](
	[NewMedicineServiceTargetBandID] [bigint] IDENTITY(1,1) NOT NULL,
	[FinancialPeriodID] [int] NOT NULL,
	[NumberOfQualifyingItemsLowerLimit] [int] NOT NULL,
	[NumberOfQualifyingItemsUpperLimit] [int] NOT NULL,
	[NumberOfQualifyingItemsIncrementor] [int] NULL,
	[MaximumNumberOfItemsPayable] [int] NOT NULL,
	[MaximumNumberOfItemsPayableIncrementor] [int] NULL,
 CONSTRAINT [PK_NewMedicineServiceTargetBand] PRIMARY KEY CLUSTERED 
(
	[NewMedicineServiceTargetBandID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
/****** Object:  Table [dbo].[PaperRequest]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PaperRequest](
	[PaperRequestID] [bigint] IDENTITY(1,1) NOT NULL,
	[BatchID] [bigint] NOT NULL,
	[PaperRequestHistoryID] [bigint] NULL,
 CONSTRAINT [PK_PaperRequest] PRIMARY KEY CLUSTERED 
(
	[PaperRequestID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
/****** Object:  Table [dbo].[PaperRequestHistory]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PaperRequestHistory](
	[PaperRequestHistoryID] [bigint] IDENTITY(1,1) NOT NULL,
	[PaperRequestID] [bigint] NOT NULL,
	[BatchID] [bigint] NULL,
	[PaperRequestStatusID] [int] NOT NULL,
	[Notes] [varchar](max) NULL,
	[InsertedBy] [varchar](128) NOT NULL CONSTRAINT [DF_PaperRequestHistory_InsertedBy]  DEFAULT (original_login()),
	[InsertedOn] [datetime] NOT NULL CONSTRAINT [DF_PaperRequestHistory_InsertedOn]  DEFAULT (getdate()),
 CONSTRAINT [PK_PaperRequestHistory] PRIMARY KEY CLUSTERED 
(
	[PaperRequestHistoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)  

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PaperRequestStatus]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PaperRequestStatus](
	[PaperRequestStatusID] [int] NOT NULL,
	[Description] [varchar](128) NOT NULL,
 CONSTRAINT [PK_PaperRequestStatus] PRIMARY KEY CLUSTERED 
(
	[PaperRequestStatusID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PaperTracking]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PaperTracking](
	[PaperTrackingID] [bigint] IDENTITY(1,1) NOT NULL,
	[BatchID] [bigint] NOT NULL,
	[PaperTypeID] [int] NOT NULL,
	[DateTime] [datetime2](7) NOT NULL,
	[PaperTrackingStatusID] [int] NOT NULL,
	[Notes] [varchar](max) NULL,
	[InsertedBy] [varchar](128) NOT NULL CONSTRAINT [DF_PaperTracking_InsertedBy]  DEFAULT (original_login()),
	[InsertedOn] [datetime] NOT NULL CONSTRAINT [DF_PaperTracking_InsertedOn]  DEFAULT (getdate()),
 CONSTRAINT [PK_PaperTracking] PRIMARY KEY CLUSTERED 
(
	[PaperTrackingID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)  

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PaperTrackingStatus]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PaperTrackingStatus](
	[PaperTrackingStatusID] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[IsUserAssignable] [bit] NOT NULL,
 CONSTRAINT [PK_PaperTrackingStatus] PRIMARY KEY CLUSTERED 
(
	[PaperTrackingStatusID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PaperType]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PaperType](
	[PaperTypeID] [int] NOT NULL,
	[Description] [varchar](50) NOT NULL,
 CONSTRAINT [PK_PaperType] PRIMARY KEY CLUSTERED 
(
	[PaperTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PersonalAdministrationIndicator]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PersonalAdministrationIndicator](
	[PersonalAdministrationIndicatorCode] [varchar](1) NOT NULL,
	[Description] [varchar](max) NOT NULL,
 CONSTRAINT [PK_PersonalAdministrationIndicator] PRIMARY KEY CLUSTERED 
(
	[PersonalAdministrationIndicatorCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)  

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PreperationClassIndicator]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PreperationClassIndicator](
	[PreperationClassIndicatorCode] [varchar](1) NOT NULL,
	[Description] [varchar](max) NOT NULL,
 CONSTRAINT [PK_PreperationClassIndicator] PRIMARY KEY CLUSTERED 
(
	[PreperationClassIndicatorCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)  

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PrimaryCareTrust]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PrimaryCareTrust](
	[PrimaryCareTrustCode] [varchar](3) NOT NULL,
	[HealthAuthorityCode] [varchar](3) NULL,
	[Name] [varchar](128) NULL,
	[Address] [varchar](max) NULL,
	[Postcode] [varchar](8) NULL,
 CONSTRAINT [PK_PrimaryCareTrust] PRIMARY KEY CLUSTERED 
(
	[PrimaryCareTrustCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)  

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Product]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Product](
	[ProductID] [bigint] IDENTITY(1,1) NOT NULL,
	[ManufacturerID] [bigint] NULL,
	[Code] [varchar](8) NOT NULL,
	[ProductTypeCode] [varchar](1) NOT NULL,
	[ManufacturersOwnReference] [varchar](128) NULL,
	[Description] [varchar](60) NOT NULL,
	[PeriodIntroduced] [int] NULL,
	[PeriodFirstPrescribed] [int] NULL,
	[PpCode] [int] NULL,
	[IsEligibleForStomaCustomisationFee] [bit] NOT NULL CONSTRAINT [DF_Product_IsCustomisableStoma]  DEFAULT ((0)),
	[Notes] [varchar](max) NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[ProductID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)  

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductAllowedIndicator]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductAllowedIndicator](
	[ProductAllowedIndicatorCode] [varchar](1) NOT NULL,
	[Description] [varchar](max) NOT NULL,
	[IsUsedEng] [bit] NULL,
	[IsUsedGue] [bit] NULL,
	[IsUsedJer] [bit] NULL,
 CONSTRAINT [PK_ProductAllowedIndicator] PRIMARY KEY CLUSTERED 
(
	[ProductAllowedIndicatorCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)  

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductPack]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductPack](
	[ProductPackID] [bigint] IDENTITY(1,1) NOT NULL,
	[ProductID] [bigint] NOT NULL,
	[Code] [varchar](8) NOT NULL,
	[CheckDigit] [varchar](1) NOT NULL,
	[Description] [varchar](60) NOT NULL,
	[PeriodIntroduced] [int] NULL,
	[PeriodDiscontinued] [int] NULL,
	[CurrentPriceDate] [date] NULL,
	[PackSize] [dbo].[Size] NOT NULL,
	[ContainerSize] [dbo].[Size] NOT NULL,
	[SnomedCode] [bigint] NULL,
	[PpCode] [int] NULL,
	[PpkCode] [int] NULL,
	[Notes] [varchar](max) NULL,
 CONSTRAINT [PK_ProductPack] PRIMARY KEY CLUSTERED 
(
	[ProductPackID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)  

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductPackPeriodic]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductPackPeriodic](
	[ProductPackPeriodicID] [bigint] IDENTITY(1,1) NOT NULL,
	[ProductPackID] [bigint] NOT NULL,
	[ProductPeriodicID] [bigint] NOT NULL,
	[DrugTariffCategoryCode] [varchar](5) NULL,
	[SpecialContainerIndicatorCode] [varchar](1) NULL,
	[DentalFormularyIndicatorCode] [varchar](1) NULL,
	[PackPrice] [money] NOT NULL,
	[UnitPrice] [money] NOT NULL,
	[IsCommonPack] [bit] NOT NULL CONSTRAINT [DF_ProductPackPeriodic_IsCommonPack]  DEFAULT ((0)),
	[IsEligibleForZeroDiscount] [bit] NOT NULL CONSTRAINT [DF_ProductPackPeriodic_IsEligibleForZeroDiscount]  DEFAULT ((0)),
	[VelocityCode] [varchar](4) NULL,
	[VelocityCodeCheckDigit] [varchar](1) NULL,
	[MappingPackCode] [varchar](8) NULL,
	[MappingPackCheckDigit] [varchar](1) NULL,
	[IsEnabled] [bit] NOT NULL CONSTRAINT [DF_ProductPackPeriodic_IsEnabled]  DEFAULT ((1)),
	[ActualMedicinalProductPackId] [bigint] NULL,
	[VirtualMedicinalProductPackId] [bigint] NULL,
	[IsPatientPack] [bit] NOT NULL CONSTRAINT [DF_ProductPackPeriodic_IsPatientPack]  DEFAULT ((0)),
	[HomeDeliveryIndicatorCode] [varchar](1) NULL,
 CONSTRAINT [PK_ProductPackPeriodic] PRIMARY KEY CLUSTERED 
(
	[ProductPackPeriodicID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductPeriodic]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductPeriodic](
	[ProductPeriodicID] [bigint] IDENTITY(1,1) NOT NULL,
	[ProductID] [bigint] NOT NULL,
	[FinancialPeriodID] [int] NOT NULL,
	[IsContraceptive] [bit] NOT NULL,
	[ProductAllowedIndicatorCode] [varchar](1) NULL,
	[ControlledSubstanceIndicatorCode] [varchar](1) NULL,
	[DentalFormularyIndicatorCode] [varchar](1) NULL,
	[PersonalAdministrationIndicatorCode] [varchar](1) NULL,
	[StandardDrugClassIndicatorCode] [varchar](1) NULL,
	[PreperationClassIndicatorCode] [varchar](1) NOT NULL,
	[DiluentIndicatorCode] [varchar](1) NULL,
	[InputQuantityUnitIndicatorCode] [varchar](1) NULL,
	[StandardQuantityUnitIndicatorCode] [varchar](1) NOT NULL,
	[BorderlineSubstanceIndicatorCode] [varchar](1) NULL,
	[UnusualFeeIndicatorCode] [varchar](1) NULL,
	[IsPrescriptionEventMonitored] [bit] NULL,
	[IsCommonPack] [bit] NULL,
	[IsAdditionalFeeAllowed] [bit] NULL,
	[WeightedIndicator] [varchar](1) NOT NULL,
	[ZeroDiscountIndicator] [varchar](1) NOT NULL,
	[PoisonIndicator] [varchar](1) NULL,
	[NumberOfExtraFees] [int] NOT NULL,
	[NumberOfExtraCharges] [int] NOT NULL,
	[SmallQuantity] [int] NOT NULL,
	[LargeQuantity] [int] NOT NULL,
	[AverageMonthlyDose] [int] NULL,
	[IsCombinationPack] [bit] NULL,
	[WithdrawalStatusIndicatorCode] [varchar](1) NULL,
	[EquivelentProductCode] [varchar](8) NULL,
	[TariffIndicator] [varchar](1) NULL,
	[MedicamentCode] [varchar](2) NOT NULL,
	[IsApplianceContractorAllowed] [bit] NULL,
	[ApplianceTypeIndicatorCode] [varchar](1) NULL,
	[IsMagenta] [bit] NULL,
	[IsBrandedMedicine] [bit] NOT NULL CONSTRAINT [DF_ProductPeriodic_IsBrandedMedicine]  DEFAULT ((0)),
	[IsEligibleForSupplementaryControlledDrugFee] [bit] NOT NULL CONSTRAINT [DF_ProductPeriodic_IsEligibleForSupplementaryControlledDrugFee]  DEFAULT ((0)),
	[IsEligibleForReconstitutionFee] [bit] NOT NULL CONSTRAINT [DF_ProductPeriodic_IsEligibleForReconstitutionFee]  DEFAULT ((0)),
	[IsUnlicensedMedicine] [bit] NOT NULL CONSTRAINT [DF_ProductPeriodic_IsUnlicensedMedicine]  DEFAULT ((0)),
	[IsDrugTariffSpecialOrder] [bit] NOT NULL CONSTRAINT [DF_ProductPeriodic_IsDrugTariffSpecialOrder]  DEFAULT ((0)),
 CONSTRAINT [PK_ProductPeriodic] PRIMARY KEY CLUSTERED 
(
	[ProductPeriodicID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductType]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductType](
	[ProductTypeCode] [varchar](1) NOT NULL,
	[Description] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ProductType] PRIMARY KEY CLUSTERED 
(
	[ProductTypeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PSNC_Batch_Load]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PSNC_Batch_Load](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FileName] [varchar](75) NULL,
	[dateCreated] [datetime] NULL,
	[Size] [int] NULL
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RegionalDirectorate]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RegionalDirectorate](
	[RegionalDirectorateCode] [varchar](3) NOT NULL,
	[Name] [varchar](128) NOT NULL,
	[Address] [varchar](max) NOT NULL,
	[Postcode] [varchar](8) NOT NULL,
 CONSTRAINT [PK_RegionalDirectorate] PRIMARY KEY CLUSTERED 
(
	[RegionalDirectorateCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)  

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Schedule]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Schedule](
	[ScheduleID] [bigint] IDENTITY(1,1) NOT NULL,
	[BatchID] [bigint] NOT NULL,
	[TotalOfDrugAndApplianceCosts] [money] NULL,
	[TotalOfAllFees] [money] NULL,
	[TotalOfDrugAndApplianceCostsPlusFees] [money] NULL,
	[TotalOfChargesIncludingFP57Refunds] [money] NULL,
	[TotalOfAccount] [money] NULL,
	[RecoveryOfAdvancePayment] [money] NULL,
	[RecoveryOfAdvancePaymentInRespectOfALateRegisteredBatch] [money] NULL,
	[RecoveryOfAdditionalAdvancePayment] [money] NULL,
	[RecoveryOfCredit] [money] NULL,
	[BalanceDueInRespectOfValue] [money] NULL,
	[PaymentOnAccountForFinancialPeriodID] [int] NULL,
	[PaymentOnAccountForItems] [int] NULL,
	[PaymentOnAccountForCharges] [int] NULL,
	[PaymentOnAccountForValue] [money] NULL,
	[AdvancePaymentInRespectOfALateRegisteredBatch] [money] NULL,
	[AdditionalAdvancePayment] [money] NULL,
	[TotalAmountAuthorisedByPPD] [money] NULL,
	[TotalAmountAuthorisedByPCT] [money] NULL,
	[TotalAmountAuthorisedByOther] [money] NULL,
	[NetPaymentMadeByPPD] [money] NULL,
	[DateNetPaymentMadeByPPD] [date] NULL,
	[GeneratedBy] [varchar](128) NULL,
	[GeneratedOn] [datetime] NULL,
 CONSTRAINT [PK_Schedule] PRIMARY KEY CLUSTERED 
(
	[ScheduleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ScheduleCharge]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ScheduleCharge](
	[ScheduleID] [bigint] NOT NULL,
	[DrugsR1Quantity] [int] NULL,
	[DrugsR1Price] [money] NULL,
	[DrugsR1Value] [money] NULL,
	[AppliancesR1Quantity] [int] NULL,
	[AppliancesR1Price] [money] NULL,
	[AppliancesR1Value] [money] NULL,
	[DrugsR2Quantity] [int] NULL,
	[DrugsR2Price] [money] NULL,
	[DrugsR2Value] [money] NULL,
	[AppliancesR2Quantity] [int] NULL,
	[AppliancesR2Price] [money] NULL,
	[AppliancesR2Value] [money] NULL,
	[ExclHoiseryR1Quantity] [int] NULL,
	[ExclHoiseryR1Price] [money] NULL,
	[ExclHoiseryR1Value] [money] NULL,
	[ExclHoiseryR2Quantity] [int] NULL,
	[ExclHoiseryR2Price] [money] NULL,
	[ExclHoiseryR2Value] [money] NULL,
	[ElasticHoiseryValue] [money] NULL,
	[FP57Refunds] [money] NULL,
	[TotalIncludingFP57] [money] NULL,
	[ExclWelshHoiseryR1Quantity] [int] NULL,
	[ExclWelshHoiseryR1Price] [money] NULL,
	[ExclWelshHoiseryR1Value] [money] NULL,
	[ExclWelshHoiseryR2Quantity] [int] NULL,
	[ExclWelshHoiseryR2Price] [money] NULL,
	[ExclWelshHoiseryR2Value] [money] NULL,
	[Group4HoiseryQuantity] [int] NULL,
	[Group4HoiseryPrice] [money] NULL,
	[Group4HoiseryValue] [money] NULL,
	[Group4ExclHoiseryQuantity] [int] NULL,
	[Group4ExclHoiseryValue] [money] NULL,
 CONSTRAINT [PK_ScheduleCharge] PRIMARY KEY CLUSTERED 
(
	[ScheduleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
/****** Object:  Table [dbo].[ScheduleCost]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ScheduleCost](
	[ScheduleID] [bigint] NOT NULL,
	[TotalOfBasicPricesAtStandardDiscountRate] [money] NULL,
	[DiscountPercentage] [money] NULL,
	[DiscountValue] [money] NULL,
	[TotalOfBasicPricesAtZeroDiscount] [money] NULL,
	[SubTotalOfBasicPrices] [money] NULL,
	[OutOfPocketExpenses] [money] NULL,
	[ConsumableAllowanceQuantity] [int] NULL,
	[ConsumableAllowancePrice] [money] NULL,
	[ConsumableAllowanceValue] [money] NULL,
	[ContainerAllowanceQuantity] [int] NULL,
	[ContainerAllowancePrice] [money] NULL,
	[ContainerAllowanceValue] [money] NULL,
	[TotalOfDrugAndApplianceCosts] [money] NULL,
	[Adjustment] [money] NULL,
	[OxygenCylindersAndMasks] [money] NULL,
	[OxygenExpenses] [money] NULL,
	[OxygenAdjustment] [money] NULL,
 CONSTRAINT [PK_ScheduleCost] PRIMARY KEY CLUSTERED 
(
	[ScheduleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
/****** Object:  Table [dbo].[ScheduleDocumentData]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ScheduleDocumentData](
	[ScheduleID] [bigint] NOT NULL,
	[TotalFormsReceived] [int] NULL,
	[TotalEpsFormsReceived] [int] NULL,
	[TotalEpsItemsReceived] [int] NULL,
	[ItemsAtZeroDiscountRateForWhichAFeeIsPaid] [int] NULL,
	[ItemsAtStandardDiscountRateForWhichAFeeIsPaidIncludingOxygen] [int] NULL,
	[TotalOfItemsForWhichAFeeIsPaid] [int] NULL,
	[AverageItemValue] [money] NULL,
	[ReferredBackItems] [int] NULL,
	[ReferredBackForms] [int] NULL,
	[DisallowedItems] [int] NULL,
	[DisallowedForms] [int] NULL,
	[MedicineUseReviewsDeclared] [int] NULL,
	[DispensingStaffNumberOfHoursDeclared] [int] NULL,
	[FP57FormsDeclared] [int] NULL,
	[ApplianceUseReviewsCarriedOutAtPatientsHomeDeclared] [int] NULL,
	[ApplianceUseReviewsCarriedOutAtPremisesDeclared] [int] NULL,
	[NewMedicineServicesUndertaken] [int] NULL,
	[NewMedicineServiceItems] [int] NULL,
 CONSTRAINT [PK_ScheduleDocumentData] PRIMARY KEY CLUSTERED 
(
	[ScheduleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
/****** Object:  Table [dbo].[ScheduleExpensiveItem]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ScheduleExpensiveItem](
	[ScheduleExpensiveItemID] [bigint] IDENTITY(1,1) NOT NULL,
	[ScheduleID] [bigint] NOT NULL,
	[FormNumber] [varchar](8) NULL,
	[ItemNumber] [int] NULL,
	[Description] [varchar](128) NULL,
	[PackSize] [dbo].[Size] NULL,
	[Quantity] [dbo].[Quantity] NULL,
	[BasicPrice] [money] NULL,
 CONSTRAINT [PK_ScheduleExpensiveItem] PRIMARY KEY CLUSTERED 
(
	[ScheduleExpensiveItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ScheduleExpensiveItemSummary]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ScheduleExpensiveItemSummary](
	[ScheduleID] [bigint] NOT NULL,
	[LowerLimitValue] [money] NULL,
	[UpperLimitValue] [money] NULL,
	[NumberOfItemsBetweenLowerAndUpperLimit] [int] NULL,
	[ValueOfItemsBetweenLowerAndUpperLimit] [money] NULL,
	[NumberOfItemsAboveUpperLimit] [int] NULL,
	[ValueOfItemsAboveUpperLimit] [money] NULL,
	[NumberOfItemsAboveLowerLimit] [int] NULL,
	[ValueOfItemsAboveLowerLimit] [money] NULL,
 CONSTRAINT [PK_ScheduleExpensiveItemSummary] PRIMARY KEY CLUSTERED 
(
	[ScheduleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
/****** Object:  Table [dbo].[ScheduleFee]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ScheduleFee](
	[ScheduleID] [bigint] NOT NULL,
	[ProfessionalFeeQuantity] [int] NULL,
	[ProfessionalFeeAmount] [money] NULL,
	[ProfessionalFeeValue] [money] NULL,
	[PharmacyContractTransitionPayment] [money] NULL,
	[RepeatDispensingFee] [money] NULL,
	[PracticePayment] [money] NULL,
	[AdditionalFees2a] [money] NULL,
	[AdditionalFees2bMeasuredFitted] [money] NULL,
	[AdditionalFees2bHomeDelivery] [money] NULL,
	[AdditionalFees2c] [money] NULL,
	[AdditionalFees2d] [money] NULL,
	[AdditionalFees2e] [money] NULL,
	[AdditionalFeesMethadone] [money] NULL,
	[AdditionalFees2fCount] [int] NULL,
	[AdditionalFees2f] [money] NULL,
	[EstablishmentPayment] [money] NULL,
	[ManuallyPriced] [money] NULL,
	[SubTotalOfPrescriptionFees] [money] NULL,
	[OtherFeesMedicinalUseReview] [money] NULL,
	[OtherFeesApplianceUseReviewHome] [money] NULL,
	[OtherFeesApplianceUseReviewPremises] [money] NULL,
	[OtherFeesStomaCustomisation] [money] NULL,
	[TotalOfAllFees] [money] NULL,
	[OtherFeesNewMedicineServiceConsultation] [money] NULL,
	[ProtectedAdditionalPayment] [money] NULL,
	[DisposalOfUnwantedMedicinesAndSignposting] [money] NULL,
	[PromotionOfHealthyLifestyleAndSupportForSelfCare] [money] NULL,
	[ClinicalGovernance] [money] NULL,
	[SupportForPeopleWithDisabilities] [money] NULL,
 CONSTRAINT [PK_ScheduleFee] PRIMARY KEY CLUSTERED 
(
	[ScheduleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
/****** Object:  Table [dbo].[ScheduleGroupSwitchDirection]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ScheduleGroupSwitchDirection](
	[ScheduleGroupSwitchDirectionID] [int] NOT NULL,
	[Description] [varchar](128) NOT NULL,
	[DisplaySequence] [int] NULL,
 CONSTRAINT [PK_ScheduleGroupSwitchDirection] PRIMARY KEY CLUSTERED 
(
	[ScheduleGroupSwitchDirectionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ScheduleGroupSwitchSummary]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ScheduleGroupSwitchSummary](
	[ScheduleGroupSwitchSummaryID] [bigint] IDENTITY(1,1) NOT NULL,
	[ScheduleID] [bigint] NOT NULL,
	[ScheduleGroupSwitchDirectionID] [int] NOT NULL,
	[Reason] [varchar](128) NOT NULL,
	[Number] [int] NOT NULL,
	[OldRate] [bit] NOT NULL,
 CONSTRAINT [PK_ScheduleGroupSwitchSummary] PRIMARY KEY CLUSTERED 
(
	[ScheduleGroupSwitchSummaryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ScheduleLocalPayment]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ScheduleLocalPayment](
	[ScheduleLocalPaymentID] [bigint] IDENTITY(1,1) NOT NULL,
	[ScheduleID] [bigint] NOT NULL,
	[AuthorisedBy] [varchar](128) NOT NULL,
	[Description] [varchar](128) NOT NULL,
	[Value] [money] NOT NULL,
 CONSTRAINT [PK_ScheduleLocalPayment] PRIMARY KEY CLUSTERED 
(
	[ScheduleLocalPaymentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ScheduleOtherPayment]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ScheduleOtherPayment](
	[ScheduleOtherPaymentID] [bigint] IDENTITY(1,1) NOT NULL,
	[ScheduleID] [bigint] NOT NULL,
	[Description] [varchar](128) NOT NULL,
	[Value] [money] NOT NULL,
 CONSTRAINT [PK_ScheduleOtherPayment] PRIMARY KEY CLUSTERED 
(
	[ScheduleOtherPaymentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ScheduleSpecialItem]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ScheduleSpecialItem](
	[ScheduleSpecialItemID] [bigint] IDENTITY(1,1) NOT NULL,
	[ScheduleID] [bigint] NOT NULL,
	[FormNumber] [varchar](8) NULL,
	[ItemNumber] [int] NULL,
	[Description] [varchar](128) NULL,
	[PackSize] [dbo].[Size] NULL,
	[Quantity] [dbo].[Quantity] NULL,
	[BasicPrice] [money] NULL,
 CONSTRAINT [PK_ScheduleSpecialItem] PRIMARY KEY CLUSTERED 
(
	[ScheduleSpecialItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ScheduleTop10CatM]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ScheduleTop10CatM](
	[ScheduleTop10CatMID] [bigint] IDENTITY(1,1) NOT NULL,
	[ScheduleID] [bigint] NOT NULL,
	[Description] [varchar](128) NULL,
	[Quantity] [dbo].[Quantity] NULL,
	[BasicPrice] [money] NULL,
 CONSTRAINT [PK_ScheduleTop10CatM] PRIMARY KEY CLUSTERED 
(
	[ScheduleTop10CatMID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ScheduleTop10ZD]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ScheduleTop10ZD](
	[ScheduleTop10ZDID] [bigint] IDENTITY(1,1) NOT NULL,
	[ScheduleID] [bigint] NOT NULL,
	[Description] [varchar](128) NULL,
	[Quantity] [dbo].[Quantity] NULL,
	[BasicPrice] [money] NULL,
 CONSTRAINT [PK_ScheduleTop10ZD] PRIMARY KEY CLUSTERED 
(
	[ScheduleTop10ZDID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SpecialContainerIndicator]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SpecialContainerIndicator](
	[SpecialContainerIndicatorCode] [varchar](1) NOT NULL,
	[Description] [varchar](max) NOT NULL,
	[BasisOfPrice] [char](1) NULL,
	[CanSplit] [bit] NOT NULL CONSTRAINT [DF_SpecialContainerIndicator_CanSplit]  DEFAULT ((1)),
 CONSTRAINT [PK_SpecialContainerIndicator] PRIMARY KEY CLUSTERED 
(
	[SpecialContainerIndicatorCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)  

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[StandardDrugClassIndicator]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StandardDrugClassIndicator](
	[StandardDrugClassIndicatorCode] [varchar](1) NOT NULL,
	[Description] [varchar](max) NOT NULL,
	[IsPartVIII] [bit] NOT NULL,
 CONSTRAINT [PK_StandardDrugClassIndicator] PRIMARY KEY CLUSTERED 
(
	[StandardDrugClassIndicatorCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)  

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[StandardQuantityUnitIndicator]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StandardQuantityUnitIndicator](
	[StandardQuantityUnitIndicatorCode] [varchar](1) NOT NULL,
	[Description] [varchar](max) NOT NULL,
 CONSTRAINT [PK_StandardQuantityUnitIndicator] PRIMARY KEY CLUSTERED 
(
	[StandardQuantityUnitIndicatorCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)  

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Submission]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Submission](
	[BatchID] [bigint] NOT NULL,
	[SubmissionDate] [date] NULL,
	[ExemptDocuments] [int] NULL,
	[ExemptItems] [int] NULL,
	[PaidDocuments] [int] NULL,
	[PaidItems] [int] NULL,
	[PaidDocumentsOldRate] [int] NULL,
	[PaidItemsOldRate] [int] NULL,
	[TotalDocuments] [int] NULL,
	[TotalItems] [int] NULL,
	[ETPTokens] [bit] NULL,
	[RepeatAuthorisingForms] [bit] NULL,
	[Electronic] [bit] NULL,
	[FP57Count] [int] NULL,
	[FP57TotalRefund] [money] NULL,
	[SupportHours] [int] NULL,
	[MedicineUseReviews] [int] NULL,
	[ApplianceUseReviews] [int] NULL,
	[ApplianceUseReviewsHome] [int] NULL,
	[OutOfPocketExpenses] [money] NULL,
	[OutOfPocketExpenseItems] [int] NULL,
	[NewMedicineServiceConsultations] [int] NULL,
 CONSTRAINT [PK_Submission] PRIMARY KEY CLUSTERED 
(
	[BatchID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
/****** Object:  Table [dbo].[SubmissionGroup]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SubmissionGroup](
	[SubmissionGroupCode] [varchar](2) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[IsChargeable] [bit] NOT NULL CONSTRAINT [DF_SubmissionGroup_IsChargeable]  DEFAULT ((0)),
	[IsOldRate] [bit] NOT NULL CONSTRAINT [DF_SubmissionGroup_IsOldRate]  DEFAULT ((0)),
	[IsTagged] [bit] NOT NULL CONSTRAINT [DF_SubmissionGroup_IsTagged]  DEFAULT ((0)),
 CONSTRAINT [PK_SubmissionGroup] PRIMARY KEY CLUSTERED 
(
	[SubmissionGroupCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SubmissionImage]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SubmissionImage](
	[SubmissionImageID] [bigint] IDENTITY(1,1) NOT NULL,
	[BatchID] [bigint] NOT NULL,
	[DocumentImageTypeID] [tinyint] NOT NULL,
	[OriginalFilename] [varchar](128) NULL,
	[ImageData] [varbinary](max) NULL,
 CONSTRAINT [PK_SubmissionImage] PRIMARY KEY CLUSTERED 
(
	[SubmissionImageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TariffDiscountDeductionScale]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TariffDiscountDeductionScale](
	[TariffDiscountDeductionScaleID] [int] IDENTITY(1,1) NOT NULL,
	[FinancialPeriodID] [int] NOT NULL,
	[From] [int] NOT NULL,
	[To] [int] NOT NULL,
	[DeductionRate] [money] NOT NULL,
 CONSTRAINT [PK_TariffDiscountDeductionScale] PRIMARY KEY CLUSTERED 
(
	[TariffDiscountDeductionScaleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
/****** Object:  Table [dbo].[TariffEstablishmentPaymentScale]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TariffEstablishmentPaymentScale](
	[TariffEstablishmentPaymentScaleID] [int] IDENTITY(1,1) NOT NULL,
	[FinancialPeriodID] [int] NOT NULL,
	[JurisdictionID] [int] NOT NULL,
	[ItemsFrom] [int] NOT NULL,
	[ItemsTo] [int] NOT NULL,
	[EstablishmentPayment] [money] NOT NULL,
 CONSTRAINT [PK_TariffEstablishmentPaymentScale] PRIMARY KEY CLUSTERED 
(
	[TariffEstablishmentPaymentScaleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
/****** Object:  Table [dbo].[TariffPracticePayment]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TariffPracticePayment](
	[TariffPracticePaymentID] [int] IDENTITY(1,1) NOT NULL,
	[FinancialPeriodID] [int] NOT NULL,
	[JurisdictionID] [int] NOT NULL,
	[ItemsFrom] [int] NOT NULL,
	[ItemsTo] [int] NOT NULL,
	[PracticePaymentFixed] [money] NULL,
	[PracticePaymentPerItem] [money] NULL,
	[ContributionInPracticePaymentFixed] [money] NULL,
	[ContributionInPracticePaymentPerItem] [money] NULL,
 CONSTRAINT [PK_TariffPracticePayment] PRIMARY KEY CLUSTERED 
(
	[TariffPracticePaymentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
/****** Object:  Table [dbo].[TariffPracticePaymentItemsDispensed]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TariffPracticePaymentItemsDispensed](
	[TariffPracticePaymentItemsDispensedID] [int] IDENTITY(1,1) NOT NULL,
	[FinancialPeriodID] [int] NOT NULL,
	[ItemsFrom] [int] NOT NULL,
	[ItemsTo] [int] NOT NULL,
	[HoursPerWeek] [int] NOT NULL,
	[AdditionalHoursPerWeek] [int] NULL,
	[AdditionalItems] [int] NULL,
 CONSTRAINT [PK_TariffPracticePaymentItemsDispensed] PRIMARY KEY CLUSTERED 
(
	[TariffPracticePaymentItemsDispensedID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
/****** Object:  Table [dbo].[TariffTransitionalPayment]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TariffTransitionalPayment](
	[TariffTransitionalPaymentID] [int] IDENTITY(1,1) NOT NULL,
	[FinancialPeriodID] [int] NOT NULL,
	[JurisdictionID] [int] NOT NULL,
	[ItemsFrom] [int] NOT NULL,
	[ItemsTo] [int] NOT NULL,
	[Payment] [money] NOT NULL,
 CONSTRAINT [PK_TariffTransitionalPayment] PRIMARY KEY CLUSTERED 
(
	[TariffTransitionalPaymentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
/****** Object:  Table [dbo].[UnusualFeeIndicator]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UnusualFeeIndicator](
	[UnusualFeeIndicatorCode] [varchar](1) NOT NULL,
	[Description] [varchar](max) NOT NULL,
 CONSTRAINT [PK_UnusualFeeIndicator] PRIMARY KEY CLUSTERED 
(
	[UnusualFeeIndicatorCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)  

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserActivity]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserActivity](
	[UserActivityID] [bigint] IDENTITY(1,1) NOT NULL,
	[DateTime] [datetime] NOT NULL,
	[UserProfileID] [bigint] NOT NULL,
	[ActivityID] [int] NOT NULL,
	[BatchID] [bigint] NULL,
	[FormID] [bigint] NULL,
	[ItemID] [bigint] NULL,
	[OldItemVersionID] [bigint] NULL,
	[NewItemVersionID] [bigint] NULL,
 CONSTRAINT [PK_UserActivity] PRIMARY KEY CLUSTERED 
(
	[UserActivityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
/****** Object:  Table [dbo].[UserProfile]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserProfile](
	[UserProfileID] [bigint] IDENTITY(1,1) NOT NULL,
	[LoginName] [nvarchar](128) NOT NULL,
	[Can1stCheck] [bit] NOT NULL CONSTRAINT [DF_UserProfile_CanFirstCheck]  DEFAULT ((1)),
	[Can2ndCheck] [bit] NOT NULL CONSTRAINT [DF_UserProfile_CanSecondCheck]  DEFAULT ((0)),
 CONSTRAINT [PK_UserProfile] PRIMARY KEY CLUSTERED 
(
	[UserProfileID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
/****** Object:  Table [dbo].[WithdrawalStatusIndicator]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[WithdrawalStatusIndicator](
	[WithdrawalStatusIndicatorCode] [varchar](1) NOT NULL,
	[Description] [varchar](max) NOT NULL,
 CONSTRAINT [PK_WithdrawalStatusIndicator] PRIMARY KEY CLUSTERED 
(
	[WithdrawalStatusIndicatorCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
)  

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dmd].[ActualCombinationPackContent]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dmd].[ActualCombinationPackContent](
	[ActualMedicinalProductPackId] [bigint] NOT NULL,
	[ConstituentActualProductPackId] [bigint] NOT NULL,
 CONSTRAINT [PK_ActualCombinationPackContent] PRIMARY KEY CLUSTERED 
(
	[ActualMedicinalProductPackId] ASC,
	[ConstituentActualProductPackId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
/****** Object:  Table [dmd].[ActualMedicinalProduct]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dmd].[ActualMedicinalProduct](
	[ActualMedicinalProductId] [bigint] NOT NULL,
	[Invalid] [bit] NULL CONSTRAINT [DF_ActualMedicinalProduct_InvalidityFlag]  DEFAULT ((0)),
	[VirtualMedicinalProductId] [bigint] NOT NULL,
	[CombinationProductId] [bigint] NULL,
	[Name] [varchar](300) NOT NULL,
	[NameApplicableFrom] [datetime] NULL,
	[PreviousName] [varchar](300) NULL,
	[AbbreviatedName] [varchar](60) NULL,
	[Description] [varchar](2000) NOT NULL,
	[SupplierId] [bigint] NULL,
	[FlavourId] [bigint] NULL,
	[IsChmMonitoring] [bit] NULL CONSTRAINT [DF_ActualMedicinalProduct_CHMMonitoringIndicator]  DEFAULT ((0)),
	[IsParallelImport] [bit] NULL CONSTRAINT [DF_ActualMedicinalProduct_ParallelImportIndicator]  DEFAULT ((0)),
	[CurrentLicensingAuthorityId] [bigint] NOT NULL CONSTRAINT [DF_ActualMedicinalProduct_CurrentLicensingAuthority]  DEFAULT ((0)),
	[PreviousLicensingAuthorityId] [bigint] NULL,
	[DateOfChangeOfLicensingAuthority] [datetime] NULL,
	[LicensingAuthorityChangeReasonId] [bigint] NULL,
	[RestrictionOnAvailabilityId] [bigint] NOT NULL CONSTRAINT [DF_ActualMedicinalProduct_RestrictionsonAvailability]  DEFAULT ((1)),
	[LicensedRouteId] [bigint] NULL,
	[ApplianceProductInformationId] [bigint] NULL,
	[ActualProductExcipentId] [bigint] NULL,
 CONSTRAINT [PK_ActualMedicinalProduct] PRIMARY KEY CLUSTERED 
(
	[ActualMedicinalProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dmd].[ActualMedicinalProductPack]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dmd].[ActualMedicinalProductPack](
	[ActualMedicinalProductPackId] [bigint] NOT NULL,
	[Invalid] [bit] NULL CONSTRAINT [DF_ActualMedicinalProductPack_InvalidityFlag]  DEFAULT ((0)),
	[Name] [varchar](300) NOT NULL,
	[VirtualMedicinalProductPackId] [bigint] NOT NULL,
	[ActualMedicinalProductId] [bigint] NOT NULL,
	[CombinationPackIndicatorId] [bigint] NULL,
	[LegalCategoryId] [bigint] NOT NULL,
	[SubPackInformation] [varchar](30) NULL,
	[DiscontinuedFlagId] [bigint] NULL CONSTRAINT [DF_ActualMedicinalProductPack_DiscountedFlag]  DEFAULT ((0)),
	[DiscontinuedFlagChangeDate] [datetime] NULL,
 CONSTRAINT [PK_ActualMedicinalProductPack] PRIMARY KEY CLUSTERED 
(
	[ActualMedicinalProductPackId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dmd].[ActualProductExcipient]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dmd].[ActualProductExcipient](
	[ActualMedicinalProductId] [bigint] NOT NULL,
	[IngredientSubstanceId] [bigint] NOT NULL,
	[PharmaceuticalStrengthNumericalValue] [decimal](18, 0) NULL,
	[PharmaceuticalStrengthUnitOfMeasurementId] [bigint] NULL,
 CONSTRAINT [PK_ActualProductExcipient] PRIMARY KEY CLUSTERED 
(
	[ActualMedicinalProductId] ASC,
	[IngredientSubstanceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
/****** Object:  Table [dmd].[AppliancePackInformation]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dmd].[AppliancePackInformation](
	[ActualMedicinalProductPackId] [bigint] NOT NULL,
	[ReimbursementStatusId] [bigint] NOT NULL CONSTRAINT [DF_AppliancePackInformation_ApplianceReimbursementStatus]  DEFAULT ((1)),
	[ReimbursementStatusDate] [datetime] NULL,
	[PreviousReimbursementStatusId] [bigint] NULL,
	[PackOrderNumber] [varchar](20) NULL,
 CONSTRAINT [PK_AppliancePackInformation_1] PRIMARY KEY CLUSTERED 
(
	[ActualMedicinalProductPackId] ASC,
	[ReimbursementStatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dmd].[ApplianceProductInformation]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dmd].[ApplianceProductInformation](
	[ActualMedicinalProductId] [bigint] NOT NULL,
	[SizeWeight] [varchar](100) NULL,
	[ColourId] [bigint] NULL,
	[ProductOrderNumber] [varchar](20) NULL,
 CONSTRAINT [PK_ApplianceProductInformation] PRIMARY KEY CLUSTERED 
(
	[ActualMedicinalProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dmd].[BasisOfName]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dmd].[BasisOfName](
	[BasisOfNameId] [bigint] NOT NULL,
	[Description] [varchar](200) NOT NULL,
 CONSTRAINT [PK_BasisOfName] PRIMARY KEY CLUSTERED 
(
	[BasisOfNameId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dmd].[BasisOfPharmaceuticalStrength]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dmd].[BasisOfPharmaceuticalStrength](
	[BasisOfPharmaceuticalStrengthId] [bigint] NOT NULL,
	[Description] [varchar](150) NOT NULL,
 CONSTRAINT [PK_BasisOfPharmaceuticalStrength] PRIMARY KEY CLUSTERED 
(
	[BasisOfPharmaceuticalStrengthId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dmd].[Colour]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dmd].[Colour](
	[ColourId] [bigint] NOT NULL,
	[Description] [varchar](60) NOT NULL,
 CONSTRAINT [PK_Colour] PRIMARY KEY CLUSTERED 
(
	[ColourId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dmd].[CombinationPackContent]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dmd].[CombinationPackContent](
	[VirtualMedicinalProductPackId] [bigint] NOT NULL,
	[ConstituentVirtualProductPackId] [bigint] NOT NULL,
 CONSTRAINT [PK_CombinationPackContent] PRIMARY KEY CLUSTERED 
(
	[VirtualMedicinalProductPackId] ASC,
	[ConstituentVirtualProductPackId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
/****** Object:  Table [dmd].[CombinationPackIndicator]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dmd].[CombinationPackIndicator](
	[CombinationPackIndicatorId] [bigint] NOT NULL,
	[Description] [varchar](50) NOT NULL,
 CONSTRAINT [PK_CombinationPackIndicator] PRIMARY KEY CLUSTERED 
(
	[CombinationPackIndicatorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dmd].[CombinationProductIndicator]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dmd].[CombinationProductIndicator](
	[CombinationProductIndicatorId] [bigint] NOT NULL,
	[Description] [varchar](60) NOT NULL,
 CONSTRAINT [PK_CombinationProductIndicator] PRIMARY KEY CLUSTERED 
(
	[CombinationProductIndicatorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dmd].[ControlledDrugCategory]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dmd].[ControlledDrugCategory](
	[ControlledDrugCategoryId] [bigint] NOT NULL,
	[Description] [varchar](100) NOT NULL,
 CONSTRAINT [PK_ControlledDrugCategory] PRIMARY KEY CLUSTERED 
(
	[ControlledDrugCategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dmd].[ControlledDrugPrescribingInformation]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dmd].[ControlledDrugPrescribingInformation](
	[VirtualMedicinalProductId] [bigint] NOT NULL,
	[ControlledDrugCategoryId] [bigint] NOT NULL CONSTRAINT [DF_ControlledDrugPrescribingInformation_ControlledDrugCategory]  DEFAULT ((0)),
	[DateLastChanged] [datetime] NULL,
	[PreviousControlledDrugCategoryId] [bigint] NULL,
 CONSTRAINT [PK_ControlledDrugPrescribingInformation] PRIMARY KEY CLUSTERED 
(
	[VirtualMedicinalProductId] ASC,
	[ControlledDrugCategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
/****** Object:  Table [dmd].[DiscontinuedFlag]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dmd].[DiscontinuedFlag](
	[DiscontinuedFlagId] [bigint] NOT NULL,
	[Description] [varchar](60) NOT NULL,
 CONSTRAINT [PK_DiscontinuedFlag] PRIMARY KEY CLUSTERED 
(
	[DiscontinuedFlagId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dmd].[DiscountNotDeductedIndicator]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dmd].[DiscountNotDeductedIndicator](
	[DiscountNotDeductedIndicatorId] [bigint] NOT NULL,
	[Description] [varchar](60) NOT NULL,
 CONSTRAINT [PK_DiscountNotDeductedIndicator] PRIMARY KEY CLUSTERED 
(
	[DiscountNotDeductedIndicatorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dmd].[DoseFormIndicator]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dmd].[DoseFormIndicator](
	[DoseFormIndicatorId] [bigint] NOT NULL,
	[Description] [varchar](20) NOT NULL,
 CONSTRAINT [PK_DoseFormIndicator] PRIMARY KEY CLUSTERED 
(
	[DoseFormIndicatorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dmd].[DrugTariffCategoryInformation]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dmd].[DrugTariffCategoryInformation](
	[VirtualMedicinalProductPackId] [bigint] NOT NULL,
	[PaymentCategoryId] [bigint] NOT NULL CONSTRAINT [DF_DrugTariffCategoryInformation_DTPaymentCategory]  DEFAULT ((1)),
	[Price] [int] NULL,
	[PriceApplicableFrom] [date] NULL,
	[PreviousPrice] [int] NULL,
 CONSTRAINT [PK_DrugTariffCategoryInformation_1] PRIMARY KEY CLUSTERED 
(
	[VirtualMedicinalProductPackId] ASC,
	[PaymentCategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
/****** Object:  Table [dmd].[Flavour]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dmd].[Flavour](
	[FlavourId] [bigint] NOT NULL,
	[Description] [varchar](60) NOT NULL,
 CONSTRAINT [PK_Flavour] PRIMARY KEY CLUSTERED 
(
	[FlavourId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dmd].[Form]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dmd].[Form](
	[FormId] [bigint] NOT NULL,
	[ValidFrom] [datetime] NULL,
	[PreviousId] [bigint] NULL,
	[Name] [varchar](60) NOT NULL,
 CONSTRAINT [PK_Form] PRIMARY KEY CLUSTERED 
(
	[FormId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dmd].[FormInformation]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dmd].[FormInformation](
	[VirtualMedicinalProductId] [bigint] NOT NULL,
	[FormId] [bigint] NOT NULL,
 CONSTRAINT [PK_FormInformation_1] PRIMARY KEY CLUSTERED 
(
	[VirtualMedicinalProductId] ASC,
	[FormId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
/****** Object:  Table [dmd].[IngredientSubstance]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dmd].[IngredientSubstance](
	[IngredientSubstanceId] [bigint] NOT NULL,
	[ApplicableFrom] [datetime] NULL,
	[PreviousId] [bigint] NULL,
	[Name] [varchar](300) NULL,
	[Invalid] [bit] NULL CONSTRAINT [DF_IngrediantSubstanceIdentifer_InvalidityFlag]  DEFAULT ((0)),
 CONSTRAINT [PK_IngrediantSubstanceIdentifer] PRIMARY KEY CLUSTERED 
(
	[IngredientSubstanceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dmd].[LegalCategory]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dmd].[LegalCategory](
	[LegalCategoryId] [bigint] NOT NULL,
	[Description] [varchar](60) NOT NULL,
 CONSTRAINT [PK_LegalCategory] PRIMARY KEY CLUSTERED 
(
	[LegalCategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dmd].[LicensedRoute]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dmd].[LicensedRoute](
	[RouteId] [bigint] NOT NULL,
	[ActualMedicinalProductId] [bigint] NOT NULL,
 CONSTRAINT [PK_LicensedRoute] PRIMARY KEY CLUSTERED 
(
	[RouteId] ASC,
	[ActualMedicinalProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
/****** Object:  Table [dmd].[LicensingAuthority]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dmd].[LicensingAuthority](
	[LicensingAuthorityId] [bigint] NOT NULL,
	[Description] [varchar](60) NOT NULL,
 CONSTRAINT [PK_CurrentLicensingAuthority] PRIMARY KEY CLUSTERED 
(
	[LicensingAuthorityId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dmd].[LicensingAuthorityChangeReason]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dmd].[LicensingAuthorityChangeReason](
	[LicensingAuthorityChangeReasonId] [bigint] NOT NULL,
	[Description] [varchar](60) NOT NULL,
 CONSTRAINT [PK_LicensingAuthorityChnageReason] PRIMARY KEY CLUSTERED 
(
	[LicensingAuthorityChangeReasonId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dmd].[MedicinalProductPrice]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dmd].[MedicinalProductPrice](
	[MedicinalProductPriceID] [bigint] IDENTITY(1,1) NOT NULL,
	[ActualMedicinalProductPackId] [bigint] NOT NULL,
	[Price] [bigint] NULL,
	[DateofPriceValidity] [datetime] NULL,
	[PricePriortoChangeDate] [bigint] NULL,
	[PriceBasisFlagId] [bigint] NOT NULL CONSTRAINT [DF_MedicinalProductPrice_PriceBasisFlag]  DEFAULT ((1)),
 CONSTRAINT [PK_MedicinalProductPrice] PRIMARY KEY CLUSTERED 
(
	[MedicinalProductPriceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
/****** Object:  Table [dmd].[NonAvailabilityIndicator]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dmd].[NonAvailabilityIndicator](
	[NonAvailabilityIndicatorId] [bigint] NOT NULL,
	[Description] [varchar](60) NOT NULL,
 CONSTRAINT [PK_NonAvailabilityIndicator] PRIMARY KEY CLUSTERED 
(
	[NonAvailabilityIndicatorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dmd].[OntologyFormAndRouteInformation]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dmd].[OntologyFormAndRouteInformation](
	[VirtualMedicinalProductId] [bigint] NOT NULL,
	[VirtualMedicinalProductFormAndRouteId] [bigint] NOT NULL CONSTRAINT [DF_OntologyFormandRouteInformation_VirtualMedicinalProductFormandRoute]  DEFAULT ((1)),
 CONSTRAINT [PK_OntologyFormandRouteInformation_1] PRIMARY KEY CLUSTERED 
(
	[VirtualMedicinalProductId] ASC,
	[VirtualMedicinalProductFormAndRouteId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
/****** Object:  Table [dmd].[PaymentCategory]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dmd].[PaymentCategory](
	[PaymentCategoryId] [bigint] NOT NULL,
	[Description] [varchar](60) NOT NULL,
 CONSTRAINT [PK_PaymentCategory] PRIMARY KEY CLUSTERED 
(
	[PaymentCategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dmd].[PriceBasisFlag]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dmd].[PriceBasisFlag](
	[PriceBasisFlagId] [bigint] NOT NULL,
	[Description] [varchar](60) NOT NULL,
 CONSTRAINT [PK_PriceBasisFlag] PRIMARY KEY CLUSTERED 
(
	[PriceBasisFlagId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dmd].[ProductPrescribingInformation]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dmd].[ProductPrescribingInformation](
	[ProductPrescribingInformationId] [bigint] IDENTITY(1,1) NOT NULL,
	[ActualMedicinalProductPackId] [bigint] NOT NULL,
	[IsSchedule2] [bit] NULL,
	[IsSchedule1] [bit] NULL,
	[IsAllowedOnHospitalPrescriptionOnly] [bit] NULL CONSTRAINT [DF_ProductPrescribingInformation_HospitalIndicator]  DEFAULT ((0)),
	[IsBorderlineSubstance] [bit] NULL CONSTRAINT [DF_ProductPrescribingInformation_ACBSIndicator]  DEFAULT ((0)),
	[IsPersonallyAdministered] [bit] NULL CONSTRAINT [DF_ProductPrescribingInformation_PersonallyAdministeredIndicator]  DEFAULT ((0)),
	[IsAllowedOnMdaPrescription] [bit] NULL CONSTRAINT [DF_ProductPrescribingInformation_FP10MDAPrescription]  DEFAULT ((0)),
	[IsAllowedOnNursingFormularyPrescription] [bit] NULL CONSTRAINT [DF_ProductPrescribingInformation_NursingFormularyIndicator]  DEFAULT ((0)),
	[IsAllowedOnNurseExtendedFormularyPrescription] [bit] NULL CONSTRAINT [DF_ProductPrescribingInformation_NurseExtendedFormularyIndicator]  DEFAULT ((0)),
	[IsAllowedOnDentalFormularyPrescription] [bit] NULL CONSTRAINT [DF_ProductPrescribingInformation_DentalFormularyIndicator]  DEFAULT ((0)),
 CONSTRAINT [PK_ProductPrescribingInformation] PRIMARY KEY CLUSTERED 
(
	[ProductPrescribingInformationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
/****** Object:  Table [dmd].[ReasonForNameChange]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dmd].[ReasonForNameChange](
	[ReasonforNameChangeId] [bigint] NOT NULL,
	[Description] [varchar](200) NOT NULL,
 CONSTRAINT [PK_ReasonForNameChange] PRIMARY KEY CLUSTERED 
(
	[ReasonforNameChangeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dmd].[ReimbursementInformation]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dmd].[ReimbursementInformation](
	[ReimbursementInformationId] [bigint] IDENTITY(1,1) NOT NULL,
	[ActualMedicinalProductPackId] [bigint] NOT NULL,
	[NumberOfPrescriptionCharges] [bigint] NULL,
	[NumberOfDispensingFees] [bigint] NULL,
	[IsEligibleForBrokenBulk] [bit] NULL CONSTRAINT [DF_ReimbursementInformation_BrokenBulkIndicator]  DEFAULT ((0)),
	[IsLimitedStability] [bit] NULL CONSTRAINT [DF_ReimbursementInformation_LimitedStabilityIndicator]  DEFAULT ((0)),
	[IsCalendarPack] [bit] NULL CONSTRAINT [DF_ReimbursementInformation_CalendarPackIndicator]  DEFAULT ((0)),
	[SpecialContainerIndicatorId] [bigint] NULL,
	[DiscountNotDeductedIndicatorId] [bigint] NULL,
	[IsAllowedAsFP34dBulkVaccine] [bit] NULL CONSTRAINT [DF_ReimbursementInformation_FP34DPrescriptionItem]  DEFAULT ((0)),
 CONSTRAINT [PK_ReimbursementInformation] PRIMARY KEY CLUSTERED 
(
	[ReimbursementInformationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
/****** Object:  Table [dmd].[ReimbursementStatus]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dmd].[ReimbursementStatus](
	[ReimbursementStatusId] [bigint] NOT NULL,
	[Description] [varchar](60) NOT NULL,
 CONSTRAINT [PK_ApplianceReimbursementStatus] PRIMARY KEY CLUSTERED 
(
	[ReimbursementStatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dmd].[RestrictionOnAvailability]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dmd].[RestrictionOnAvailability](
	[RestrictionOnAvailabilityId] [bigint] NOT NULL,
	[Description] [varchar](60) NOT NULL,
 CONSTRAINT [PK_RestrictionOnAvailability] PRIMARY KEY CLUSTERED 
(
	[RestrictionOnAvailabilityId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dmd].[Route]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dmd].[Route](
	[RouteId] [bigint] NOT NULL,
	[ValidFrom] [datetime] NULL,
	[PreviousId] [bigint] NULL,
	[Name] [varchar](60) NOT NULL,
 CONSTRAINT [PK_Route] PRIMARY KEY CLUSTERED 
(
	[RouteId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dmd].[RouteInformation]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dmd].[RouteInformation](
	[VirtualMedicinalProductId] [bigint] NOT NULL,
	[RouteId] [bigint] NOT NULL,
 CONSTRAINT [PK_RouteInformation_1] PRIMARY KEY CLUSTERED 
(
	[VirtualMedicinalProductId] ASC,
	[RouteId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
/****** Object:  Table [dmd].[SpecialContainerIndicator]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dmd].[SpecialContainerIndicator](
	[SpecialContainerIndicatorId] [bigint] NOT NULL,
	[Description] [varchar](60) NOT NULL,
 CONSTRAINT [PK_SpecialContainerIndicator] PRIMARY KEY CLUSTERED 
(
	[SpecialContainerIndicatorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dmd].[Supplier]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dmd].[Supplier](
	[SupplierId] [bigint] NOT NULL,
	[Invalid] [bit] NULL,
	[ValidFrom] [datetime] NULL,
	[PreviousId] [bigint] NULL,
	[Name] [varchar](80) NOT NULL,
 CONSTRAINT [PK_Supplier] PRIMARY KEY CLUSTERED 
(
	[SupplierId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dmd].[UnitOfMeasurement]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dmd].[UnitOfMeasurement](
	[UnitOfMeasurementId] [bigint] NOT NULL,
	[ValidFrom] [datetime] NULL,
	[PreviousId] [bigint] NULL CONSTRAINT [DF_UnitofMeasurement_Active]  DEFAULT ((1)),
	[Name] [varchar](150) NOT NULL,
 CONSTRAINT [PK_UnitOfMeasurement] PRIMARY KEY CLUSTERED 
(
	[UnitOfMeasurementId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dmd].[VersionHistory]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dmd].[VersionHistory](
	[VersionHistoryId] [int] IDENTITY(1,1) NOT NULL,
	[Source] [varchar](max) NOT NULL,
	[Applied] [datetime] NOT NULL,
	[ErrorMessage] [varchar](max) NULL,
 CONSTRAINT [PK_VersionHistory] PRIMARY KEY CLUSTERED 
(
	[VersionHistoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dmd].[VirtualMedicinalProduct]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dmd].[VirtualMedicinalProduct](
	[VirtualMedicinalProductId] [bigint] NOT NULL,
	[Invalid] [bit] NULL CONSTRAINT [DF_VirtualMedicinalProduct_InvalidityFlag]  DEFAULT ((0)),
	[VirtualTherapeuticMoietyId] [bigint] NULL,
	[ApplicableFrom] [datetime] NULL,
	[PreviousId] [bigint] NULL,
	[CombinationProductIndicatorId] [bigint] NULL CONSTRAINT [DF_VirtualMedicinalProduct_CombinationProductIndicator]  DEFAULT ((1)),
	[Name] [varchar](300) NOT NULL,
	[BasisofPreferredNameId] [bigint] NOT NULL CONSTRAINT [DF_VirtualMedicinalProduct_BasisofPreferredName]  DEFAULT ((1)),
	[PreferredNameApplicableFrom] [datetime] NULL,
	[PreviousName] [varchar](500) NULL,
	[BasisofPreviousNameId] [bigint] NULL,
	[ReasonforNameChangeId] [bigint] NULL,
	[AbbreviatedName] [varchar](60) NULL,
	[IsSugarFree] [bit] NULL CONSTRAINT [DF_VirtualMedicinalProduct_SugarFreeIndicator]  DEFAULT ((0)),
	[IsGlutenFree] [bit] NULL,
	[IsPreservativeFree] [bit] NULL CONSTRAINT [DF_VirtualMedicinalProduct_PreservativeFreeIndicator]  DEFAULT ((0)),
	[IsCfcFree] [bit] NULL CONSTRAINT [DF_VirtualMedicinalProduct_CFCFreeIndicator]  DEFAULT ((0)),
	[VirtualMedicinalProductPrescribingStatusId] [bigint] NOT NULL CONSTRAINT [DF_VirtualMedicinalProduct_VirtualMedicinalProductPrescribingStatus]  DEFAULT ((1)),
	[NonAvailabilityIndicatorId] [bigint] NULL,
	[NonAvailabilityStatusDate] [datetime] NULL,
	[DoseFormIndicatorId] [bigint] NULL CONSTRAINT [DF_VirtualMedicinalProduct_DoseFormIndicator]  DEFAULT ((1)),
	[UnitDoseFormSize] [numeric](18, 0) NULL,
	[UnitDoseFormUnits] [bigint] NULL,
	[UnitDoseUnitofMeasure] [bigint] NULL,
 CONSTRAINT [PK_VirtualMedicinalProduct] PRIMARY KEY CLUSTERED 
(
	[VirtualMedicinalProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dmd].[VirtualMedicinalProductFormAndRoute]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dmd].[VirtualMedicinalProductFormAndRoute](
	[VirtualMedicinalProductFormAndRouteId] [bigint] NOT NULL,
	[Description] [varchar](60) NOT NULL,
 CONSTRAINT [PK_VirtualMedicinalProductFormAndRoute] PRIMARY KEY CLUSTERED 
(
	[VirtualMedicinalProductFormAndRouteId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dmd].[VirtualMedicinalProductPack]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dmd].[VirtualMedicinalProductPack](
	[VirtualMedicinalProductPackId] [bigint] NOT NULL,
	[VirtualMedicinalProductId] [bigint] NOT NULL,
	[Invalid] [bit] NULL CONSTRAINT [DF_VirtualMedicinalProductPack_InvalidityFlag]  DEFAULT ((0)),
	[Description] [varchar](300) NOT NULL,
	[CombinationPackIndicatorId] [bigint] NULL,
	[VirtualMedicinalProductPackQuantity] [numeric](18, 2) NULL,
	[VirtualMedicinalProductPackUnitOfMeasurementId] [bigint] NULL,
	[CombinationPackContentId] [bigint] NULL,
	[DrugTariffCategoryInformationId] [bigint] NULL,
 CONSTRAINT [PK_VirtualMedicinalProductPack] PRIMARY KEY CLUSTERED 
(
	[VirtualMedicinalProductPackId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dmd].[VirtualMedicinalProductPrescribingStatus]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dmd].[VirtualMedicinalProductPrescribingStatus](
	[VirtualMedicinalProductPrescribingStatusId] [bigint] NOT NULL,
	[Description] [varchar](200) NOT NULL,
 CONSTRAINT [PK_VirtualMedicinalProductPrescribingStatus] PRIMARY KEY CLUSTERED 
(
	[VirtualMedicinalProductPrescribingStatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dmd].[VirtualProductIngredient]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dmd].[VirtualProductIngredient](
	[VirtualMedicinalProductId] [bigint] NOT NULL,
	[IngredientSubstanceId] [bigint] NOT NULL CONSTRAINT [DF_VirtualProductIngrediant_VirtualMedicinalProductFormandRoute]  DEFAULT ((1)),
	[BasisOfStrengthIngredientSubstanceId] [bigint] NULL,
	[BasisofPharmaceuticalStrengthId] [bigint] NULL,
	[StrengthValueNumerator] [numeric](18, 0) NULL,
	[StrengthValueNumeratorUnitOfMeasurementId] [bigint] NULL,
	[StrengthValueDenominator] [numeric](18, 0) NULL,
	[StrengthValueDenominatorUnitOfMeasurementId] [bigint] NULL,
 CONSTRAINT [PK_VirtualProductIngrediant_1] PRIMARY KEY CLUSTERED 
(
	[VirtualMedicinalProductId] ASC,
	[IngredientSubstanceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
/****** Object:  Table [dmd].[VirtualTherapeuticMoiety]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dmd].[VirtualTherapeuticMoiety](
	[VirtualTherapeuticMoietyId] [bigint] NOT NULL,
	[ApplicableFrom] [datetime] NULL,
	[PreviousId] [bigint] NULL,
	[Invalid] [bit] NULL CONSTRAINT [DF_VirtualTherapeuticMoiety_InvalidityFlag]  DEFAULT ((0)),
	[Name] [varchar](100) NOT NULL,
	[AbbreviatedName] [varchar](50) NULL,
 CONSTRAINT [PK_VirtualTherapeuticMoiety] PRIMARY KEY CLUSTERED 
(
	[VirtualTherapeuticMoietyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ppd].[APPL2]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ppd].[APPL2](
	[ProductCode] [varchar](7) NOT NULL,
	[Description] [varchar](60) NOT NULL,
	[ManufacturerCode] [varchar](4) NULL,
	[ManufacturerOwnReference] [varchar](12) NULL,
	[PeriodIntroduced] [int] NULL,
	[PeriodFirstPrescribed] [int] NULL,
	[ContraceptiveIndicator] [bit] NOT NULL,
	[PersonalAdministrationIndicatorCode] [varchar](1) NULL,
	[StandardQuantityUnitIndicatorCode] [varchar](1) NOT NULL,
	[PreparationClassIndicatorCode] [varchar](1) NOT NULL,
	[MedicamentCode] [varchar](2) NOT NULL,
	[WeightingIndicator] [varchar](1) NULL,
	[ApplianceContractorIndicator] [bit] NOT NULL,
	[LargeQuantity] [int] NOT NULL,
	[SmallQuantity] [int] NOT NULL,
	[ChtApplianceTypeIndicator] [varchar](1) NOT NULL,
	[ZeroDiscountIndicator] [tinyint] NOT NULL,
	[ProductAllowedIndicatorCodeEngland] [varchar](1) NULL,
	[ProductAllowedIndicatorCodeGuernsey] [varchar](1) NULL,
	[ExtraFees] [int] NOT NULL,
	[ExtraCharges] [int] NOT NULL,
	[CombinationPack] [bit] NOT NULL,
	[PpCode] [int] NOT NULL,
	[IsMagenta] [bit] NOT NULL,
	[HomeDeliveryIndicatorCode] [varchar](1) NULL,
	[StomaCustomisationIndicator] [bit] NOT NULL,
 CONSTRAINT [PK_APPL2] PRIMARY KEY CLUSTERED 
(
	[ProductCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ppd].[APPL2_History]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ppd].[APPL2_History](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ProductCode] [varchar](7) NOT NULL,
	[Description] [varchar](60) NOT NULL,
	[ManufacturerCode] [varchar](4) NULL,
	[ManufacturerOwnReference] [varchar](12) NULL,
	[PeriodIntroduced] [int] NULL,
	[PeriodFirstPrescribed] [int] NULL,
	[ContraceptiveIndicator] [bit] NOT NULL,
	[PersonalAdministrationIndicatorCode] [varchar](1) NULL,
	[StandardQuantityUnitIndicatorCode] [varchar](1) NOT NULL,
	[PreparationClassIndicatorCode] [varchar](1) NOT NULL,
	[MedicamentCode] [varchar](2) NOT NULL,
	[WeightingIndicator] [varchar](1) NULL,
	[ApplianceContractorIndicator] [bit] NOT NULL,
	[LargeQuantity] [int] NOT NULL,
	[SmallQuantity] [int] NOT NULL,
	[ChtApplianceTypeIndicator] [varchar](1) NOT NULL,
	[ZeroDiscountIndicator] [tinyint] NOT NULL,
	[ProductAllowedIndicatorCodeEngland] [varchar](1) NULL,
	[ProductAllowedIndicatorCodeGuernsey] [varchar](1) NULL,
	[ExtraFees] [int] NOT NULL,
	[ExtraCharges] [int] NOT NULL,
	[CombinationPack] [bit] NOT NULL,
	[PpCode] [int] NOT NULL,
	[IsMagenta] [bit] NOT NULL,
	[HomeDeliveryIndicatorCode] [varchar](1) NULL,
	[StomaCustomisationIndicator] [bit] NOT NULL,
	[Updated_Date] [datetime] NULL
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ppd].[APPL3]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ppd].[APPL3](
	[PackCode] [varchar](8) NOT NULL,
	[CheckDigit] [varchar](1) NOT NULL,
	[ProductCode] [varchar](7) NOT NULL,
	[PackDescription] [varchar](20) NOT NULL,
	[PeriodDiscontinued] [int] NULL,
	[CurrentPriceDate] [date] NULL,
	[SpecialContainerIndicatorCode] [varchar](1) NULL,
	[PackSize] [decimal](7, 2) NOT NULL,
	[ContainerSize] [decimal](7, 2) NOT NULL,
	[CurrentPackPrice] [decimal](7, 2) NOT NULL,
	[CurrentUnitPrice] [decimal](11, 4) NOT NULL,
	[VelocityCode] [varchar](4) NULL,
	[VelocityCodeCheckDigit] [varchar](1) NULL,
	[MappingPackCode] [varchar](8) NULL,
	[MappingPackCheckDigit] [varchar](1) NULL,
	[PpCode] [int] NOT NULL,
	[PpkCode] [int] NOT NULL,
	[PatientPackIndicator] [bit] NOT NULL,
 CONSTRAINT [PK_APPL3] PRIMARY KEY CLUSTERED 
(
	[PackCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ppd].[APPL3_History]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ppd].[APPL3_History](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PackCode] [varchar](8) NOT NULL,
	[CheckDigit] [varchar](1) NOT NULL,
	[ProductCode] [varchar](7) NOT NULL,
	[PackDescription] [varchar](20) NOT NULL,
	[PeriodDiscontinued] [int] NULL,
	[CurrentPriceDate] [date] NULL,
	[SpecialContainerIndicatorCode] [varchar](1) NULL,
	[PackSize] [decimal](7, 2) NOT NULL,
	[ContainerSize] [decimal](7, 2) NOT NULL,
	[CurrentPackPrice] [decimal](7, 2) NOT NULL,
	[CurrentUnitPrice] [decimal](11, 4) NOT NULL,
	[VelocityCode] [varchar](4) NULL,
	[VelocityCodeCheckDigit] [varchar](1) NULL,
	[MappingPackCode] [varchar](8) NULL,
	[MappingPackCheckDigit] [varchar](1) NULL,
	[PpCode] [int] NOT NULL,
	[PpkCode] [int] NOT NULL,
	[PatientPackIndicator] [bit] NOT NULL,
	[Updated_Date] [datetime] NULL
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ppd].[DRUG2]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ppd].[DRUG2](
	[ProductCode] [varchar](7) NOT NULL,
	[ManufacturerCode] [varchar](4) NULL,
	[Description] [varchar](40) NOT NULL,
	[PeriodIntroduced] [int] NULL,
	[PeriodFirstPrescribed] [int] NULL,
	[PersonalAdministrationIndicatorCode] [varchar](1) NULL,
	[ControlledDrugIndicatorCodeEngland] [varchar](1) NULL,
	[ContraceptiveIndicator] [bit] NOT NULL,
	[BorderlineSubstanceIndicatorCode] [varchar](1) NULL,
	[TariffIndicatorCode] [varchar](1) NULL,
	[StandardDrugClassIndicatorCode] [varchar](1) NULL,
	[CombinationPack] [bit] NOT NULL,
	[ControlledDrugIndicatorCodeJersey] [varchar](1) NULL,
	[ControlledDrugIndicatorCodeGuernsey] [varchar](1) NULL,
	[StandardQuantityUnitIndicatorCode] [varchar](1) NOT NULL,
	[WithdrawnStatusIndicatorCode] [varchar](1) NULL,
	[PreparationClassIndicatorCode] [varchar](1) NOT NULL,
	[MedicamentCode] [varchar](2) NOT NULL,
	[WeightingIndicator] [varchar](1) NULL,
	[Top150Indicator] [bit] NOT NULL,
	[DiluentIndicatorCode] [varchar](1) NULL,
	[EquivelentDrugCode] [varchar](7) NULL,
	[LargeQuantity] [int] NOT NULL,
	[SmallQuantity] [int] NOT NULL,
	[InputQuantityUnitIndicatorCode] [varchar](1) NOT NULL,
	[PrescriptionEventMonitoredIndicator] [bit] NOT NULL,
	[S3aItemsIndicator] [varchar](1) NULL,
	[CommonPackIndicator] [bit] NOT NULL,
	[ExtraFees] [int] NOT NULL,
	[ExtraCharges] [int] NOT NULL,
	[UnusualFeeIndicatorCode] [varchar](1) NULL,
	[AdditionalFeeAllowedIndicator] [bit] NOT NULL,
	[AverageMonthlyDose] [int] NOT NULL,
	[DentalFormularyIndicatorCodeEngland] [varchar](1) NULL,
	[S4PoisonIndicator] [varchar](1) NULL,
	[ZeroDiscountIndicator] [varchar](1) NOT NULL,
	[DentalFormularyIndicatorCodeJersey] [varchar](1) NULL,
	[DentalFormularyIndicatorCodeGuernsey] [varchar](1) NULL,
	[ProductAllowedIndicatorCodeJersey] [varchar](1) NULL,
	[ProductAllowedIndicatorCodeGuernsey] [varchar](1) NULL,
	[PpCode] [int] NOT NULL,
	[IsMagenta] [bit] NOT NULL,
	[BrandedMedicineIndicator] [bit] NOT NULL,
	[SupplementaryControlledDrugFeeIndicator] [bit] NOT NULL,
	[ReconstitutionIndicator] [bit] NOT NULL,
 CONSTRAINT [PK_DRUG2] PRIMARY KEY CLUSTERED 
(
	[ProductCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ppd].[DRUG3]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ppd].[DRUG3](
	[PackCode] [varchar](8) NOT NULL,
	[CheckDigit] [varchar](1) NOT NULL,
	[ProductCode] [varchar](7) NOT NULL,
	[PackDescription] [varchar](20) NOT NULL,
	[PeriodDiscontinued] [int] NULL,
	[CurrentPriceDate] [date] NULL,
	[SpecialContainerIndicatorCode] [varchar](1) NULL,
	[PackSize] [decimal](7, 2) NOT NULL,
	[ContainerSize] [decimal](7, 2) NOT NULL,
	[CurrentPackPrice] [decimal](7, 2) NOT NULL,
	[CurrentUnitPrice] [decimal](11, 4) NOT NULL,
	[VelocityCode] [varchar](4) NULL,
	[VelocityCodeCheckDigit] [varchar](1) NULL,
	[CommonPackIndicator] [bit] NOT NULL,
	[MappingPackCode] [varchar](8) NULL,
	[MappingPackCheckDigit] [varchar](1) NULL,
	[PpCode] [int] NOT NULL,
	[PpkCode] [int] NOT NULL,
	[PatientPackIndicator] [bit] NOT NULL,
 CONSTRAINT [PK_DRUG3] PRIMARY KEY CLUSTERED 
(
	[PackCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ppd].[DRUG3_History]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ppd].[DRUG3_History](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PackCode] [varchar](8) NOT NULL,
	[CheckDigit] [varchar](1) NOT NULL,
	[ProductCode] [varchar](7) NOT NULL,
	[PackDescription] [varchar](20) NOT NULL,
	[PeriodDiscontinued] [int] NULL,
	[CurrentPriceDate] [date] NULL,
	[SpecialContainerIndicatorCode] [varchar](1) NULL,
	[PackSize] [decimal](7, 2) NOT NULL,
	[ContainerSize] [decimal](7, 2) NOT NULL,
	[CurrentPackPrice] [decimal](7, 2) NOT NULL,
	[CurrentUnitPrice] [decimal](11, 4) NOT NULL,
	[VelocityCode] [varchar](4) NULL,
	[VelocityCodeCheckDigit] [varchar](1) NULL,
	[CommonPackIndicator] [bit] NOT NULL,
	[MappingPackCode] [varchar](8) NULL,
	[MappingPackCheckDigit] [varchar](1) NULL,
	[PpCode] [int] NOT NULL,
	[PpkCode] [int] NOT NULL,
	[PatientPackIndicator] [bit] NOT NULL,
	[Updated_Date] [datetime] NULL
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ppd].[eDispensary]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ppd].[eDispensary](
	[ContractorCode] [varchar](5) NOT NULL,
	[PctCode] [varchar](3) NULL,
 CONSTRAINT [PK_eDispensary] PRIMARY KEY CLUSTERED 
(
	[ContractorCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ppd].[MANU]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ppd].[MANU](
	[Cde] [varchar](4) NOT NULL,
	[Nme] [varchar](25) NULL,
	[Cat] [varchar](1) NULL,
	[BenOwnr] [varchar](4) NULL,
	[DelInd] [bit] NULL,
 CONSTRAINT [PK_MANU] PRIMARY KEY CLUSTERED 
(
	[Cde] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ppd].[MANU_History]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ppd].[MANU_History](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Cde] [varchar](4) NOT NULL,
	[Nme] [varchar](25) NULL,
	[Cat] [varchar](1) NULL,
	[BenOwnr] [varchar](4) NULL,
	[DelInd] [bit] NULL,
	[Updated_Date] [datetime] NULL
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ppd].[PartIX]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ppd].[PartIX](
	[ActualMedicinalProductPackId] [bigint] NOT NULL,
	[VirtualMedicinalProductPackId] [bigint] NOT NULL,
	[AmpProductCode] [int] NULL,
	[AmppProductPackCode] [int] NULL,
	[VmpProductCode] [int] NULL,
	[VmppProductPackCode] [int] NULL,
	[HomeDelivery] [varchar](1) NULL,
	[StomaCustomisation] [varchar](1) NULL,
 CONSTRAINT [PK_PartIX] PRIMARY KEY CLUSTERED 
(
	[ActualMedicinalProductPackId] ASC,
	[VirtualMedicinalProductPackId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ppd].[PartIX_1104]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ppd].[PartIX_1104](
	[ActualMedicinalProductPackId] [bigint] NOT NULL,
	[VirtualMedicinalProductPackId] [bigint] NOT NULL,
	[AmpProductCode] [int] NULL,
	[AmppProductPackCode] [int] NULL,
	[VmpProductCode] [int] NULL,
	[VmppProductPackCode] [int] NULL,
	[HomeDelivery] [varchar](1) NULL,
	[StomaCustomisation] [varchar](1) NULL,
 CONSTRAINT [PK_PartIX_1104] PRIMARY KEY CLUSTERED 
(
	[ActualMedicinalProductPackId] ASC,
	[VirtualMedicinalProductPackId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ppd].[PartIX_1105]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ppd].[PartIX_1105](
	[ActualMedicinalProductPackId] [bigint] NOT NULL,
	[VirtualMedicinalProductPackId] [bigint] NOT NULL,
	[AmpProductCode] [int] NULL,
	[AmppProductPackCode] [int] NULL,
	[VmpProductCode] [int] NULL,
	[VmppProductPackCode] [int] NULL,
	[HomeDelivery] [varchar](1) NULL,
	[StomaCustomisation] [varchar](1) NULL,
 CONSTRAINT [PK_PartIX_1105] PRIMARY KEY CLUSTERED 
(
	[ActualMedicinalProductPackId] ASC,
	[VirtualMedicinalProductPackId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ppd].[PartIX_1106]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ppd].[PartIX_1106](
	[ActualMedicinalProductPackId] [bigint] NOT NULL,
	[VirtualMedicinalProductPackId] [bigint] NOT NULL,
	[AmpProductCode] [int] NULL,
	[AmppProductPackCode] [int] NULL,
	[VmpProductCode] [int] NULL,
	[VmppProductPackCode] [int] NULL,
	[HomeDelivery] [varchar](1) NULL,
	[StomaCustomisation] [varchar](1) NULL,
 CONSTRAINT [PK_PartIX_1106] PRIMARY KEY CLUSTERED 
(
	[ActualMedicinalProductPackId] ASC,
	[VirtualMedicinalProductPackId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ppd].[PartIX_1107]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ppd].[PartIX_1107](
	[ActualMedicinalProductPackId] [bigint] NOT NULL,
	[VirtualMedicinalProductPackId] [bigint] NOT NULL,
	[AmpProductCode] [int] NULL,
	[AmppProductPackCode] [int] NULL,
	[VmpProductCode] [int] NULL,
	[VmppProductPackCode] [int] NULL,
	[HomeDelivery] [varchar](1) NULL,
	[StomaCustomisation] [varchar](1) NULL,
 CONSTRAINT [PK_PartIX_1107] PRIMARY KEY CLUSTERED 
(
	[ActualMedicinalProductPackId] ASC,
	[VirtualMedicinalProductPackId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ppd].[PartIX_1108]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ppd].[PartIX_1108](
	[ActualMedicinalProductPackId] [bigint] NOT NULL,
	[VirtualMedicinalProductPackId] [bigint] NOT NULL,
	[AmpProductCode] [int] NULL,
	[AmppProductPackCode] [int] NULL,
	[VmpProductCode] [int] NULL,
	[VmppProductPackCode] [int] NULL,
	[HomeDelivery] [varchar](1) NULL,
	[StomaCustomisation] [varchar](1) NULL,
 CONSTRAINT [PK_PartIX_1108] PRIMARY KEY CLUSTERED 
(
	[ActualMedicinalProductPackId] ASC,
	[VirtualMedicinalProductPackId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ppd].[PartIX_1109]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ppd].[PartIX_1109](
	[ActualMedicinalProductPackId] [bigint] NOT NULL,
	[VirtualMedicinalProductPackId] [bigint] NOT NULL,
	[AmpProductCode] [int] NULL,
	[AmppProductPackCode] [int] NULL,
	[VmpProductCode] [int] NULL,
	[VmppProductPackCode] [int] NULL,
	[HomeDelivery] [varchar](1) NULL,
	[StomaCustomisation] [varchar](1) NULL,
 CONSTRAINT [PK_PartIX_1109] PRIMARY KEY CLUSTERED 
(
	[ActualMedicinalProductPackId] ASC,
	[VirtualMedicinalProductPackId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ppd].[PartIX_1110]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ppd].[PartIX_1110](
	[ActualMedicinalProductPackId] [bigint] NOT NULL,
	[VirtualMedicinalProductPackId] [bigint] NOT NULL,
	[AmpProductCode] [int] NULL,
	[AmppProductPackCode] [int] NULL,
	[VmpProductCode] [int] NULL,
	[VmppProductPackCode] [int] NULL,
	[HomeDelivery] [varchar](1) NULL,
	[StomaCustomisation] [varchar](1) NULL,
 CONSTRAINT [PK_PartIX_1110] PRIMARY KEY CLUSTERED 
(
	[ActualMedicinalProductPackId] ASC,
	[VirtualMedicinalProductPackId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ppd].[PartIX_1111]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ppd].[PartIX_1111](
	[ActualMedicinalProductPackId] [bigint] NOT NULL,
	[VirtualMedicinalProductPackId] [bigint] NOT NULL,
	[AmpProductCode] [int] NULL,
	[AmppProductPackCode] [int] NULL,
	[VmpProductCode] [int] NULL,
	[VmppProductPackCode] [int] NULL,
	[HomeDelivery] [varchar](1) NULL,
	[StomaCustomisation] [varchar](1) NULL,
 CONSTRAINT [PK_PartIX_1111] PRIMARY KEY CLUSTERED 
(
	[ActualMedicinalProductPackId] ASC,
	[VirtualMedicinalProductPackId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ppd].[PartIX_1112]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ppd].[PartIX_1112](
	[ActualMedicinalProductPackId] [bigint] NOT NULL,
	[VirtualMedicinalProductPackId] [bigint] NOT NULL,
	[AmpProductCode] [int] NULL,
	[AmppProductPackCode] [int] NULL,
	[VmpProductCode] [int] NULL,
	[VmppProductPackCode] [int] NULL,
	[HomeDelivery] [varchar](1) NULL,
	[StomaCustomisation] [varchar](1) NULL,
 CONSTRAINT [PK_PartIX_1112] PRIMARY KEY CLUSTERED 
(
	[ActualMedicinalProductPackId] ASC,
	[VirtualMedicinalProductPackId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ppd].[PartIX_1201]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ppd].[PartIX_1201](
	[ActualMedicinalProductPackId] [bigint] NOT NULL,
	[VirtualMedicinalProductPackId] [bigint] NOT NULL,
	[AmpProductCode] [int] NULL,
	[AmppProductPackCode] [int] NULL,
	[VmpProductCode] [int] NULL,
	[VmppProductPackCode] [int] NULL,
	[HomeDelivery] [varchar](1) NULL,
	[StomaCustomisation] [varchar](1) NULL,
 CONSTRAINT [PK_PartIX_1201] PRIMARY KEY CLUSTERED 
(
	[ActualMedicinalProductPackId] ASC,
	[VirtualMedicinalProductPackId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ppd].[PartIX_1202]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ppd].[PartIX_1202](
	[ActualMedicinalProductPackId] [bigint] NOT NULL,
	[VirtualMedicinalProductPackId] [bigint] NOT NULL,
	[AmpProductCode] [int] NULL,
	[AmppProductPackCode] [int] NULL,
	[VmpProductCode] [int] NULL,
	[VmppProductPackCode] [int] NULL,
	[HomeDelivery] [varchar](1) NULL,
	[StomaCustomisation] [varchar](1) NULL,
 CONSTRAINT [PK_PartIX_1202] PRIMARY KEY CLUSTERED 
(
	[ActualMedicinalProductPackId] ASC,
	[VirtualMedicinalProductPackId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ppd].[PartIX_1203]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ppd].[PartIX_1203](
	[ActualMedicinalProductPackId] [bigint] NOT NULL,
	[VirtualMedicinalProductPackId] [bigint] NOT NULL,
	[AmpProductCode] [int] NULL,
	[AmppProductPackCode] [int] NULL,
	[VmpProductCode] [int] NULL,
	[VmppProductPackCode] [int] NULL,
	[HomeDelivery] [varchar](1) NULL,
	[StomaCustomisation] [varchar](1) NULL,
 CONSTRAINT [PK_PartIX_1203] PRIMARY KEY CLUSTERED 
(
	[ActualMedicinalProductPackId] ASC,
	[VirtualMedicinalProductPackId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ppd].[SnomedMapping]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ppd].[SnomedMapping](
	[PpaPackCode] [varchar](50) NOT NULL,
	[SnomedCode] [bigint] NOT NULL,
 CONSTRAINT [PK_SnomedMapping] PRIMARY KEY CLUSTERED 
(
	[PpaPackCode] ASC,
	[SnomedCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [ppd].[SnomedMapping_History]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [ppd].[SnomedMapping_History](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PpaPackCode] [varchar](50) NOT NULL,
	[SnomedCode] [bigint] NOT NULL,
	[Updated_Date] [datetime] NULL
) 

GO
SET ANSI_PADDING OFF
GO
/****** Object:  UserDefinedFunction [dbo].[NonLiquidMethadoneContainerPayments]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[NonLiquidMethadoneContainerPayments] (@batchID bigint)
returns table

as

	return (
		SELECT DocumentId, Sequence, 
			Sum(TotalCurrentNonLiquidMethadoneContainerPayments) - Sum(TotalOriginalNonLiquidMethadoneContainerPayments) AS 'NetNonLiquidMethadoneContainerPaymentsVariance'
		FROM (

		select DocumentId, Sequence,
			sum(TotalCurrentNonLiquidMethadoneContainerPayments) as 'TotalCurrentNonLiquidMethadoneContainerPayments',
			sum(TotalOriginalNonLiquidMethadoneContainerPayments) as 'TotalOriginalNonLiquidMethadoneContainerPayments'
			from (
				select DocumentID, Sequence,
					TotalCurrentNonLiquidMethadoneContainerPayments = sum(case when NumberOfContainerAllowanceEndorsements > 0 AND HasPackagedDoseEndorsement = 0 AND NumberOfProfessionalFees > 1 AND (FormTypeCode = 'S' OR FormTypeCode = 'T' OR FormTypeCode = 'D') then (NumberOfProfessionalFees - 1) else 0 end),
					TotalOriginalNonLiquidMethadoneContainerPayments = 0
				from (
					select 
						d.documentid,
						d.FragmentTypeCode, 
						d.FormTypeCode, 
						d.DocumentNumber, 
						i.Sequence,
						BasicPrice = sum(iv.BasicPrice),
						BasicPriceForPartX = sum(case when ppp.IsEligibleForZeroDiscount = 1 and dtc.Category = 10 then BasicPrice else 0.00 end),
						NumberOfProfessionalFees = sum(iv.NumberOfProfessionalFees),
						IsEligibleForZeroDiscount = cast(max(cast (ppp.IsEligibleForZeroDiscount as integer)) as bit),
						ValueOfOutOfPocketExpenses = sum(iv.ValueOfOutOfPocketExpenses),
						NumberOfContainerAllowanceEndorsements = count(ep.EndorsementPeriodicID),
						HasPackagedDoseEndorsement = case when count(eep.EndorsementPeriodicID) > 0 then 1 else 0 end
					from dbo.Batch b
						inner join dbo.Contractor c on b.ContractorID = c.ContractorID
						inner join dbo.Document d on b.BatchID = d.BatchID
						inner join dbo.Item i
							inner join dbo.ItemVersion iv
							inner join dbo.ProductPackPeriodic ppp left join dbo.DrugTariffCategory dtc on ppp.DrugTariffCategoryCode = dtc.DrugTariffCategoryCode
								on iv.ProductPackPeriodicID = ppp.ProductPackPeriodicID
									left join dbo.ItemVersionEndorsement ive
										inner join dbo.EndorsementPeriodic ep on ive.EndorsementPeriodicID = ep.EndorsementPeriodicID and ep.EndorsementCode = 'PK'						
								on iv.ItemVersionID = ive.ItemVersionID						
									left join dbo.ItemVersionEndorsement iive
										inner join dbo.EndorsementPeriodic eep on iive.EndorsementPeriodicID = eep.EndorsementPeriodicID and (eep.EndorsementCode = 'CS')
								on iv.ItemVersionID = iive.ItemVersionID	
							on i.CurrentItemVersionID = iv.ItemVersionID
						on d.DocumentID = i.DocumentID
					where d.BatchID = @batchid
					group by c.JurisdictionID, b.FinancialPeriodID, d.DocumentID, i.Sequence, FormTypeCode, FragmentTypeCode, d.DocumentNumber, i.Sequence, d.documentid,iv.quantity
					having count(ep.EndorsementPeriodicID) > 0 AND (FormTypeCode = 'S' OR FormTypeCode = 'T' OR FormTypeCode = 'D')							
				) c
				group by DocumentID, Sequence
				having sum(case when NumberOfContainerAllowanceEndorsements > 0 AND HasPackagedDoseEndorsement = 0 AND NumberOfProfessionalFees > 1 AND (FormTypeCode = 'S' OR FormTypeCode = 'T' OR FormTypeCode = 'D') then (NumberOfProfessionalFees - 1) else 0 end) != 0.00

				union

				select DocumentID, Sequence,
					TotalCurrentNonLiquidMethadoneContainerPayments = 0,
					TotalOriginalNonLiquidMethadoneContainerPayments = sum(case when NumberOfContainerAllowanceEndorsements > 0 AND HasPackagedDoseEndorsement = 0 AND NumberOfProfessionalFees > 1 AND (FormTypeCode = 'S' OR FormTypeCode = 'T' OR FormTypeCode = 'D') then (NumberOfProfessionalFees - 1) else 0 end)
				from (
					select 
						d.documentid,
						d.FragmentTypeCode, 
						d.FormTypeCode, 
						d.DocumentNumber, 
						i.Sequence,
						BasicPrice = sum(iv.BasicPrice),
						BasicPriceForPartX = sum(case when ppp.IsEligibleForZeroDiscount = 1 and dtc.Category = 10 then BasicPrice else 0.00 end),
						NumberOfProfessionalFees = sum(iv.NumberOfProfessionalFees),
						IsEligibleForZeroDiscount = cast(max(cast (ppp.IsEligibleForZeroDiscount as integer)) as bit),
						ValueOfOutOfPocketExpenses = sum(iv.ValueOfOutOfPocketExpenses),
						NumberOfContainerAllowanceEndorsements = count(ep.EndorsementPeriodicID),
						HasPackagedDoseEndorsement = case when count(eep.EndorsementPeriodicID) > 0 then 1 else 0 end
					from dbo.Batch b
						inner join dbo.Contractor c on b.ContractorID = c.ContractorID
						inner join dbo.Document d on b.BatchID = d.BatchID
						inner join dbo.Item i
							inner join dbo.ItemVersion iv
							inner join dbo.ProductPackPeriodic ppp left join dbo.DrugTariffCategory dtc on ppp.DrugTariffCategoryCode = dtc.DrugTariffCategoryCode
								on iv.ProductPackPeriodicID = ppp.ProductPackPeriodicID
									left join dbo.ItemVersionEndorsement ive
										inner join dbo.EndorsementPeriodic ep on ive.EndorsementPeriodicID = ep.EndorsementPeriodicID and ep.EndorsementCode = 'PK'						
								on iv.ItemVersionID = ive.ItemVersionID						
									left join dbo.ItemVersionEndorsement iive
										inner join dbo.EndorsementPeriodic eep on iive.EndorsementPeriodicID = eep.EndorsementPeriodicID and (eep.EndorsementCode = 'CS')
								on iv.ItemVersionID = iive.ItemVersionID	
							on i.OriginalItemVersionID = iv.ItemVersionID
						on d.DocumentID = i.DocumentID
					where d.BatchID = @batchid
					group by c.JurisdictionID, b.FinancialPeriodID, d.DocumentID, i.Sequence, FormTypeCode, FragmentTypeCode, d.DocumentNumber, i.Sequence, d.documentid
					having count(ep.EndorsementPeriodicID) > 0 AND (FormTypeCode = 'S' OR FormTypeCode = 'T' OR FormTypeCode = 'D')							
				) o
				group by DocumentID, Sequence
				having sum(case when NumberOfContainerAllowanceEndorsements > 0 AND HasPackagedDoseEndorsement = 0 AND NumberOfProfessionalFees > 1 AND (FormTypeCode = 'S' OR FormTypeCode = 'T' OR FormTypeCode = 'D') then (NumberOfProfessionalFees - 1) else 0 end) != 0.00
			) total
			group by DocumentId, Sequence
			having Sum(TotalCurrentNonLiquidMethadoneContainerPayments) - Sum(TotalOriginalNonLiquidMethadoneContainerPayments) <> 0
		) errors
		group by DocumentId, Sequence
	)

GO
/****** Object:  UserDefinedFunction [dbo].[NonLiquidMethadoneContainerPaymentsMultiplier]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE function [dbo].[NonLiquidMethadoneContainerPaymentsMultiplier] (@batchID bigint)
returns table

as

	return (
		select documentId, count(sequence) as 'Multiplier' from NonLiquidMethadoneContainerPayments (@batchid)
		group by documentId
	)


GO
/****** Object:  UserDefinedFunction [dbo].[BatchItemErrorAnalysis]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[BatchItemErrorAnalysis] (@batchID bigint)
returns table

as

	return (

		select DocumentID, Sequence,
			IsMultiLine,
			IsMissedItem,
			IsExtraItem,
			IsProductChange,
			IsQuantityChange,
			IsPackChange,
			IsProfessionalFeeChange,
			IsPatientChargeChange,
			IsSurchargeChange,
			IsInvoicePriceChange,
			IsOtherChange,
			TotalNumberOfChanges = IsMultiLine + IsMissedItem + IsExtraItem
				+ IsProductChange + IsQuantityChange + IsPackChange + IsProfessionalFeeChange + IsPatientChargeChange
				+ IsSurchargeChange + IsInvoicePriceChange + IsOtherChange,
			HasFinancialImpact,
			NetCashVariance,
			NetProfessionalFeeVariance,
			NetPaymentForContainersVariance,
			NetNonLiquidMethadoneContainerPaymentsVariance	

		from (

			select DocumentID, Sequence,
				IsMultiLine,
				IsMissedItem = case when IsMultiLine = 0 then IsMissedItem else 0 end,
				IsExtraItem = case when IsMultiLine = 0 then IsExtraItem else 0 end,
				IsProductChange = case when IsMultiLine = 0 then IsProductChange else 0 end,
				IsQuantityChange = case when IsMultiLine = 0 then IsQuantityChange else 0 end,
				IsPackChange = case when IsMultiLine = 0 then IsPackChange else 0 end,
				IsProfessionalFeeChange = case when IsMultiLine = 0 then IsProfessionalFeeChange else 0 end,
				IsPatientChargeChange,
				IsSurchargeChange = case when IsMultiLine = 0 then IsSurchargeChange else 0 end,
				IsInvoicePriceChange = case when IsMultiLine = 0 then IsInvoicePriceChange else 0 end,
				IsOtherChange = case when IsMultiLine = 0 and (IsMissedItem + IsExtraItem + IsProductChange + IsQuantityChange + IsPackChange + IsProfessionalFeeChange + IsPatientChargeChange	+ IsSurchargeChange + IsInvoicePriceChange) = 0 then 1 else 0 end,
				HasFinancialImpact = case when (NetCashVariance + NetProfessionalFeeVariance + NetPaymentForContainersVariance) = 0 then 0 else 1 end,
				NetCashVariance,
				NetProfessionalFeeVariance,
				NetPaymentForContainersVariance,
				NetNonLiquidMethadoneContainerPaymentsVariance

			from (

				select DocumentID, Sequence,
					IsMultiLine = case when (select count(*) from dbo.Item where DocumentID = errors.DocumentID and Sequence = errors.Sequence) > 1 then 1 else 0 end,
					IsMissedItem = max(IsMissedItem),
					IsExtraItem = max(IsExtraItem),
					IsProductChange = max(IsProductChange),
					IsQuantityChange = max(IsQuantityChange),
					IsPackChange = max(IsPackChange),
					IsProfessionalFeeChange = max(IsProfessionalFeeChange),
					IsPatientChargeChange = max(IsPatientChargeChange),
					IsSurchargeChange = max(IsSurchargeChange),
					IsInvoicePriceChange = max(IsInvoicePriceChange),
					HasFinancialImpact = max(HasFinancialImpact),
					NetCashVariance = sum(NetCashVariance),
					NetProfessionalFeeVariance = sum(NetProfessionalFeeVariance),
					NetPaymentForContainersVariance = sum(NetPaymentForContainersVariance),
					NetNonLiquidMethadoneContainerPaymentsVariance = sum(NetNonLiquidMethadoneContainerPaymentsVariance)
		
				from (

					select DocumentID, Sequence,
						IsMissedItem = max(IsMissedItem),
						IsExtraItem = max(IsExtraItem),
						IsProductChange = 0,
						IsQuantityChange = 0,
						IsPackChange = 0,
						IsProfessionalFeeChange = 0,
						IsPatientChargeChange = 0,
						IsSurchargeChange = 0,
						IsInvoicePriceChange = 0,
						HasFinancialImpact = 0,
						NetCashVariance = 0.00,
						NetProfessionalFeeVariance = 0,
						NetPaymentForContainersVariance = 0,
						NetNonLiquidMethadoneContainerPaymentsVariance = 0
					from (

						select d.DocumentID, i.Sequence,
							IsMissedItem = case when op.PpCode is null and cp.PpCode is not null then 1 else 0 end,
							IsExtraItem = case when op.PpCode is not null and cp.PpCode is null then 1 else 0 end
						from dbo.Batch b
							inner join dbo.Document d
								inner join dbo.Item i
									left join dbo.ItemVersion oiv
										inner join dbo.ProductPackPeriodic oppp
											inner join dbo.ProductPack op on oppp.ProductPackID = op.ProductPackID
										on oiv.ProductPackPeriodicID = oppp.ProductPackPeriodicID
									on i.OriginalItemVersionID = oiv.ItemVersionID
									left join dbo.ItemVersion civ
										inner join dbo.ProductPackPeriodic cppp
											inner join dbo.ProductPack cp on cppp.ProductPackID = cp.ProductPackID
										on civ.ProductPackPeriodicID = cppp.ProductPackPeriodicID
									on i.CurrentItemVersionID = civ.ItemVersionID
								on d.DocumentID = i.DocumentID
							on b.BatchID = d.BatchID
						where b.BatchID = @batchID
							and (op.PpCode is null or cp.PpCode is null)

					) errors
					where IsMissedItem != 0 or IsExtraItem != 0
					group by DocumentID, Sequence
		
					union all

					select DocumentID, Sequence,
						IsMissedItem = 0,
						IsExtraItem = 0,
						IsProductChange = max(ProductChanged),
						IsQuantityChange = max(QuantityChanged),
						IsPackChange = max(PackChanged),
						IsProfessionalFeeChange = 0, --sum(ProfessionalFeesChanged),
						IsPatientChargeChange = 0, --sum(NumberOfPatientChargesChanged),
						IsSurchargeChange = 0,
						IsInvoicePriceChange = 0,
						HasFinancialImpact = 0,
						NetCashVariance = 0.00,
						NetProfessionalFeeVariance = 0,
						NetPaymentForContainersVariance = 0,
						NetNonLiquidMethadoneContainerPaymentsVariance = 0
					from (

						select d.DocumentID, i.Sequence,
							ProductChanged = case
								when op.PpCode != cp.PpCode then 1 else 0 end,
							QuantityChanged = case
								when op.PpCode = cp.PpCode and oiv.Quantity != civ.Quantity then 1 else 0 end,
							PackChanged = case
								when op.PpCode = cp.PpCode and oiv.Quantity = civ.Quantity and oiv.ProductPackPeriodicID != civ.ProductPackPeriodicID then 1 else 0 end--,
							--ProfessionalFeesChanged = case
							--	when oiv.NumberOfTimesDispensed != civ.NumberOfTimesDispensed then 1 else 0 end,
							--NumberOfPatientChargesChanged = case
							--	when oiv.NumberOfCharges != civ.NumberOfCharges then 1 else 0 end
						from dbo.Batch b
							inner join dbo.Document d
								inner join dbo.Item i
									inner join dbo.ItemVersion oiv 
										inner join dbo.ProductPackPeriodic oppp
											inner join dbo.ProductPack op on oppp.ProductPackID = op.ProductPackID
										on oiv.ProductPackPeriodicID = oppp.ProductPackPeriodicID
									on i.OriginalItemVersionID = oiv.ItemVersionID
									inner join dbo.ItemVersion civ 
										inner join dbo.ProductPackPeriodic cppp
											inner join dbo.ProductPack cp on cppp.ProductPackID = cp.ProductPackID
										on civ.ProductPackPeriodicID = cppp.ProductPackPeriodicID
									on i.CurrentItemVersionID = civ.ItemVersionID
								on d.DocumentID = i.DocumentID
							on b.BatchID = d.BatchID
						where b.BatchID = @batchID
							and (op.PpCode is not null and cp.PpCode is not null)
							and isnull(civ.IsHiddenAdjustment, 0) = 0

					) errors
					where ProductChanged != 0 or QuantityChanged != 0 or PackChanged != 0 -- or ProfessionalFeesChanged != 0 or NumberOfPatientChargesChanged != 0
					group by DocumentID, Sequence

					union all

					select DocumentID, Sequence,
						IsMissedItem = 0,
						IsExtraItem = 0,
						IsProductChange = 0,
						IsQuantityChange = 0,
						IsPackChange = 0,
						IsProfessionalFeeChange = 0,
						IsPatientChargeChange = 0,
						IsSurchargeChange = max(SurchargesChanged),
						IsInvoicePriceChange = 0,
						HasFinancialImpact = 0,
						NetCashVariance = 0.00,
						NetProfessionalFeeVariance = 0,
						NetPaymentForContainersVariance = 0,
						NetNonLiquidMethadoneContainerPaymentsVariance = 0
					from (

						select d.DocumentID, i.Sequence,
							SurchargesChanged = case
								when isnull(oep.EndorsementPeriodicID, 0) != isnull(cep.EndorsementPeriodicID, 0) then 1 else 0 end
						from dbo.Batch b
							inner join dbo.Document d
								inner join dbo.Item i
									inner join dbo.ItemVersion oiv 
										inner join dbo.ProductPackPeriodic oppp
											inner join dbo.ProductPack op on oppp.ProductPackID = op.ProductPackID
										on oiv.ProductPackPeriodicID = oppp.ProductPackPeriodicID
										left join dbo.ItemVersionEndorsement oive
											inner join dbo.EndorsementPeriodic oep on oive.EndorsementPeriodicID = oep.EndorsementPeriodicID and oep.EndorsementCode = 'MF'
										on oiv.ItemVersionID = oive.ItemVersionID
									on i.OriginalItemVersionID = oiv.ItemVersionID
									inner join dbo.ItemVersion civ 
										inner join dbo.ProductPackPeriodic cppp
											inner join dbo.ProductPack cp on cppp.ProductPackID = cp.ProductPackID
										on civ.ProductPackPeriodicID = cppp.ProductPackPeriodicID
										left join dbo.ItemVersionEndorsement cive
											inner join dbo.EndorsementPeriodic cep on cive.EndorsementPeriodicID = cep.EndorsementPeriodicID and cep.EndorsementCode = 'MF'
										on civ.ItemVersionID = cive.ItemVersionID
									on i.CurrentItemVersionID = civ.ItemVersionID
								on d.DocumentID = i.DocumentID
							on b.BatchID = d.BatchID
						where b.BatchID = @batchID
							and (op.PpCode is not null and cp.PpCode is not null)
							and (oep.EndorsementPeriodicID is not null or cep.EndorsementPeriodicID is not null)
							and isnull(civ.IsHiddenAdjustment, 0) = 0

						union all

						select d.DocumentID, i.Sequence,
							SurchargesChanged = case
								when isnull(oep.EndorsementPeriodicID, 0) != isnull(cep.EndorsementPeriodicID, 0) then 1 else 0 end
						from dbo.Batch b
							inner join dbo.Document d
								inner join dbo.Item i
									inner join dbo.ItemVersion oiv 
										inner join dbo.ProductPackPeriodic oppp
											inner join dbo.ProductPack op on oppp.ProductPackID = op.ProductPackID
										on oiv.ProductPackPeriodicID = oppp.ProductPackPeriodicID
										left join dbo.ItemVersionEndorsement oive
											inner join dbo.EndorsementPeriodic oep on oive.EndorsementPeriodicID = oep.EndorsementPeriodicID and oep.EndorsementCode = 'LB'
										on oiv.ItemVersionID = oive.ItemVersionID
									on i.OriginalItemVersionID = oiv.ItemVersionID
									inner join dbo.ItemVersion civ 
										inner join dbo.ProductPackPeriodic cppp
											inner join dbo.ProductPack cp on cppp.ProductPackID = cp.ProductPackID
										on civ.ProductPackPeriodicID = cppp.ProductPackPeriodicID
										left join dbo.ItemVersionEndorsement cive
											inner join dbo.EndorsementPeriodic cep on cive.EndorsementPeriodicID = cep.EndorsementPeriodicID and cep.EndorsementCode = 'LB'
										on civ.ItemVersionID = cive.ItemVersionID
									on i.CurrentItemVersionID = civ.ItemVersionID
								on d.DocumentID = i.DocumentID
							on b.BatchID = d.BatchID
						where b.BatchID = @batchID
							and (op.PpCode is not null and cp.PpCode is not null)
							and (oep.EndorsementPeriodicID is not null or cep.EndorsementPeriodicID is not null)
							and isnull(civ.IsHiddenAdjustment, 0) = 0

						union all

						select d.DocumentID, i.Sequence,
							SurchargesChanged = case
								when isnull(oep.EndorsementPeriodicID, 0) != isnull(cep.EndorsementPeriodicID, 0) then 1 else 0 end
						from dbo.Batch b
							inner join dbo.Document d
								inner join dbo.Item i
									inner join dbo.ItemVersion oiv 
										inner join dbo.ProductPackPeriodic oppp
											inner join dbo.ProductPack op on oppp.ProductPackID = op.ProductPackID
										on oiv.ProductPackPeriodicID = oppp.ProductPackPeriodicID
										left join dbo.ItemVersionEndorsement oive
											inner join dbo.EndorsementPeriodic oep on oive.EndorsementPeriodicID = oep.EndorsementPeriodicID and oep.EndorsementCode = 'LC'
										on oiv.ItemVersionID = oive.ItemVersionID
									on i.OriginalItemVersionID = oiv.ItemVersionID
									inner join dbo.ItemVersion civ 
										inner join dbo.ProductPackPeriodic cppp
											inner join dbo.ProductPack cp on cppp.ProductPackID = cp.ProductPackID
										on civ.ProductPackPeriodicID = cppp.ProductPackPeriodicID
										left join dbo.ItemVersionEndorsement cive
											inner join dbo.EndorsementPeriodic cep on cive.EndorsementPeriodicID = cep.EndorsementPeriodicID and cep.EndorsementCode = 'LC'
										on civ.ItemVersionID = cive.ItemVersionID
									on i.CurrentItemVersionID = civ.ItemVersionID
								on d.DocumentID = i.DocumentID
							on b.BatchID = d.BatchID
						where b.BatchID = @batchID
							and (op.PpCode is not null and cp.PpCode is not null)
							and (oep.EndorsementPeriodicID is not null or cep.EndorsementPeriodicID is not null)
							and isnull(civ.IsHiddenAdjustment, 0) = 0

						union all

						select d.DocumentID, i.Sequence,
							SurchargesChanged = case
								when isnull(oep.EndorsementPeriodicID, 0) != isnull(cep.EndorsementPeriodicID, 0) OR isnull(oive.Quantity, 0) != isnull(cive.Quantity, 0) then 1 else 0 end
						from dbo.Batch b
							inner join dbo.Document d
								inner join dbo.Item i
									inner join dbo.ItemVersion oiv 
										inner join dbo.ProductPackPeriodic oppp
											inner join dbo.ProductPack op on oppp.ProductPackID = op.ProductPackID
										on oiv.ProductPackPeriodicID = oppp.ProductPackPeriodicID
										left join dbo.ItemVersionEndorsement oive
											inner join dbo.EndorsementPeriodic oep on oive.EndorsementPeriodicID = oep.EndorsementPeriodicID and oep.EndorsementCode = 'CS'
										on oiv.ItemVersionID = oive.ItemVersionID
									on i.OriginalItemVersionID = oiv.ItemVersionID
									inner join dbo.ItemVersion civ 
										inner join dbo.ProductPackPeriodic cppp
											inner join dbo.ProductPack cp on cppp.ProductPackID = cp.ProductPackID
										on civ.ProductPackPeriodicID = cppp.ProductPackPeriodicID
										left join dbo.ItemVersionEndorsement cive
											inner join dbo.EndorsementPeriodic cep on cive.EndorsementPeriodicID = cep.EndorsementPeriodicID and cep.EndorsementCode = 'CS'
										on civ.ItemVersionID = cive.ItemVersionID
									on i.CurrentItemVersionID = civ.ItemVersionID
								on d.DocumentID = i.DocumentID
							on b.BatchID = d.BatchID
						where b.BatchID = @batchID
							and (op.PpCode is not null and cp.PpCode is not null)
							and (oep.EndorsementPeriodicID is not null or cep.EndorsementPeriodicID is not null)
							and isnull(civ.IsHiddenAdjustment, 0) = 0						

						union all

						select d.DocumentID, i.Sequence,
							SurchargesChanged = 1
						from dbo.Batch b
							inner join dbo.Document d
								inner join dbo.Item i
									inner join dbo.ItemVersion oiv on i.OriginalItemVersionID = oiv.ItemVersionID
									inner join dbo.ItemVersion civ on i.CurrentItemVersionID = civ.ItemVersionID
								on d.DocumentID = i.DocumentID
							on b.BatchID = d.BatchID
						where b.BatchID = @batchID
							and (oiv.ValueOfOutOfPocketExpenses != civ.ValueOfOutOfPocketExpenses)
							and isnull(civ.IsHiddenAdjustment, 0) = 0

					) errors
					where SurchargesChanged != 0
					group by DocumentID, Sequence

					union all

					select DocumentID, Sequence,
						IsMissedItem = 0,
						IsExtraItem = 0,
						IsProductChange = 0,
						IsQuantityChange = 0,
						IsPackChange = 0,
						IsProfessionalFeeChange = 0,
						IsPatientChargeChange = 0,
						IsSurchargeChange = 0,
						IsInvoicePriceChange = max(InvoicePriceChanged),
						HasFinancialImpact = 0,
						NetCashVariance = 0.00,
						NetProfessionalFeeVariance = 0,
						NetPaymentForContainersVariance = 0,
						NetNonLiquidMethadoneContainerPaymentsVariance = 0
					from (

						select d.DocumentID, i.Sequence,
							InvoicePriceChanged = 1
						from dbo.Batch b
							inner join dbo.Document d
								inner join dbo.Item i
									inner join dbo.ItemVersion oiv 
										inner join dbo.ProductPackPeriodic oppp
											inner join dbo.ProductPack op on oppp.ProductPackID = op.ProductPackID
										on oiv.ProductPackPeriodicID = oppp.ProductPackPeriodicID
										inner join dbo.ItemVersionEndorsement oive
											inner join dbo.EndorsementPeriodic oep on oive.EndorsementPeriodicID = oep.EndorsementPeriodicID and oep.EndorsementCode = 'IP'
										on oiv.ItemVersionID = oive.ItemVersionID
									on i.OriginalItemVersionID = oiv.ItemVersionID
									inner join dbo.ItemVersion civ 
										inner join dbo.ProductPackPeriodic cppp
											inner join dbo.ProductPack cp on cppp.ProductPackID = cp.ProductPackID
										on civ.ProductPackPeriodicID = cppp.ProductPackPeriodicID
										inner join dbo.ItemVersionEndorsement cive
											inner join dbo.EndorsementPeriodic cep on cive.EndorsementPeriodicID = cep.EndorsementPeriodicID and cep.EndorsementCode = 'IP'
										on civ.ItemVersionID = cive.ItemVersionID
									on i.CurrentItemVersionID = civ.ItemVersionID
								on d.DocumentID = i.DocumentID
							on b.BatchID = d.BatchID
						where b.BatchID = @batchID
							and (op.PpCode is not null and cp.PpCode is not null)
							and (oiv.BasicPrice != civ.BasicPrice)
							and isnull(civ.IsHiddenAdjustment, 0) = 0

					) errors
					where InvoicePriceChanged != 0
					group by DocumentID, Sequence

					union all

					select DocumentID, Sequence,
						IsMissedItem = 0,
						IsExtraItem = 0,
						IsProductChange = 0,
						IsQuantityChange = 0,
						IsPackChange = 0,
						IsProfessionalFeeChange = 0,
						IsPatientChargeChange = 0,
						IsSurchargeChange = 0,
						IsInvoicePriceChange = 0,
						HasFinancialImpact = 1,
						NetCashVariance = 0.00,
						NetProfessionalFeeVariance = 0,
						NetPaymentForContainersVariance = sum(NetPaymentForContainersVariance),
						NetNonLiquidMethadoneContainerPaymentsVariance = 0
					from (

						select d.DocumentID, i.Sequence,
							NetPaymentForContainersVariance = isnull(count(cive.EndorsementPeriodicID), 0) - isnull(count(oive.EndorsementPeriodicID), 0)
						from dbo.Batch b
							inner join dbo.Document d
								inner join dbo.Item i
									left join dbo.ItemVersion oiv 
										left join dbo.ProductPackPeriodic oppp
											inner join dbo.ProductPack op on oppp.ProductPackID = op.ProductPackID
										on oiv.ProductPackPeriodicID = oppp.ProductPackPeriodicID
										left join dbo.ItemVersionEndorsement oive
											inner join dbo.EndorsementPeriodic oep on oive.EndorsementPeriodicID = oep.EndorsementPeriodicID and oep.EndorsementCode = 'PK'
										on oiv.ItemVersionID = oive.ItemVersionID
									on i.OriginalItemVersionID = oiv.ItemVersionID
									left join dbo.ItemVersion civ 
										inner join dbo.ProductPackPeriodic cppp
											inner join dbo.ProductPack cp on cppp.ProductPackID = cp.ProductPackID
										on civ.ProductPackPeriodicID = cppp.ProductPackPeriodicID
										inner join dbo.ItemVersionEndorsement cive
											inner join dbo.EndorsementPeriodic cep on cive.EndorsementPeriodicID = cep.EndorsementPeriodicID and cep.EndorsementCode = 'PK'
										on civ.ItemVersionID = cive.ItemVersionID
									on i.CurrentItemVersionID = civ.ItemVersionID
								on d.DocumentID = i.DocumentID
							on b.BatchID = d.BatchID
						where b.BatchID = @batchID
							and (op.PpCode is not null or cp.PpCode is not null)
							and isnull(civ.IsHiddenAdjustment, 0) = 0
						group by d.DocumentID, i.Sequence

					) errors
					where NetPaymentForContainersVariance != 0
					group by DocumentID, Sequence

				union all

					select DocumentID, Sequence,
						IsMissedItem = 0,
						IsExtraItem = 0,
						IsProductChange = 0,
						IsQuantityChange = 0,
						IsPackChange = 0,
						IsProfessionalFeeChange = max(IsProfessionalFeeChange),
						IsPatientChargeChange = 0,
						IsSurchargeChange = 0,
						IsInvoicePriceChange = 0,
						HasFinancialImpact = 1,
						NetCashVariance = 0,
						NetProfessionalFeeVariance = 0,
						NetPaymentForContainersVariance = 0,
						NetNonLiquidMethadoneContainerPaymentsVariance = 0
					from (

						select DocumentID, Sequence,
							IsProfessionalFeeChange = case when OriginalProductPackPeriodicID is not null and CurrentProductPackPeriodicID is not null then 1 else 0 end
						from (

							select d.DocumentID, i.Sequence,
								OriginalProductPackPeriodicID = oiv.ProductPackPeriodicID, 
								CurrentProductPackPeriodicID = civ.ProductPackPeriodicID, 
								OriginalNumberOfProfessionalFees = isnull(oiv.NumberOfProfessionalFees, 0),
								CurrentNumberOfProfessionalFees = isnull(civ.NumberOfProfessionalFees, 0),
								OriginalValueOfProfessionalFees = isnull(oiv.NumberOfProfessionalFees, 0) * jp.StandardProfessionalFeeValue,
								CurrentValueOfProfessionalFees = isnull(civ.NumberOfProfessionalFees, 0) * jp.StandardProfessionalFeeValue
							from dbo.Batch b
								inner join dbo.Contractor c on b.ContractorID = c.ContractorID
								inner join dbo.Document d
									inner join dbo.Item i
										left join dbo.ItemVersion oiv on i.OriginalItemVersionID = oiv.ItemVersionID
										left join dbo.ItemVersion civ on i.CurrentItemVersionID = civ.ItemVersionID
									on d.DocumentID = i.DocumentID
								on b.BatchID = d.BatchID
								inner join dbo.JurisdictionPeriodic jp on c.JurisdictionID = jp.JurisdictionID and b.FinancialPeriodID = jp.FinancialPeriodID
							where b.BatchID = @batchID
								and isnull(civ.IsHiddenAdjustment, 0) = 0
								and d.FormTypeCode IN ('D','T','S')
							group by c.JurisdictionID, b.FinancialPeriodID, d.DocumentID, i.Sequence,
								jp.StandardProfessionalFeeValue, oiv.ProductPackPeriodicID, oiv.NumberOfProfessionalFees, civ.ProductPackPeriodicID, civ.NumberOfProfessionalFees

						) errors
						where OriginalValueOfProfessionalFees - CurrentValueOfProfessionalFees != 0.00
					) errors
					group by DocumentID, Sequence

					union all

					select DocumentID, Sequence,
						IsMissedItem = 0,
						IsExtraItem = 0,
						IsProductChange = 0,
						IsQuantityChange = 0,
						IsPackChange = 0,
						IsProfessionalFeeChange = 0,
						IsPatientChargeChange = max(IsPatientChargeChange),
						IsSurchargeChange = 0,
						IsInvoicePriceChange = 0,
						HasFinancialImpact = 1,
						NetCashVariance = sum(NetCashVariance),
						NetProfessionalFeeVariance = sum(NetProfessionalFeeVariance),
						NetPaymentForContainersVariance = 0,
						NetNonLiquidMethadoneContainerPaymentsVariance = 0
					from (

						select DocumentID, Sequence,
							IsProfessionalFeeChange = case when OriginalProductPackPeriodicID is not null and CurrentProductPackPeriodicID is not null then 1 else 0 end,
							IsPatientChargeChange = 0,
							NetCashVariance = CurrentValueOfProfessionalFees - OriginalValueOfProfessionalFees,
							NetProfessionalFeeVariance = CurrentNumberOfProfessionalFees - OriginalNumberOfProfessionalFees
						from (

							select d.DocumentID, i.Sequence,
								OriginalProductPackPeriodicID = oiv.ProductPackPeriodicID, 
								CurrentProductPackPeriodicID = civ.ProductPackPeriodicID, 
								OriginalNumberOfProfessionalFees = isnull(oiv.NumberOfProfessionalFees, 0),
								CurrentNumberOfProfessionalFees = isnull(civ.NumberOfProfessionalFees, 0),
								OriginalValueOfProfessionalFees = isnull(oiv.NumberOfProfessionalFees, 0) * jp.StandardProfessionalFeeValue,
								CurrentValueOfProfessionalFees = isnull(civ.NumberOfProfessionalFees, 0) * jp.StandardProfessionalFeeValue
							from dbo.Batch b
								inner join dbo.Contractor c on b.ContractorID = c.ContractorID
								inner join dbo.Document d
									inner join dbo.Item i
										left join dbo.ItemVersion oiv on i.OriginalItemVersionID = oiv.ItemVersionID
										left join dbo.ItemVersion civ on i.CurrentItemVersionID = civ.ItemVersionID
									on d.DocumentID = i.DocumentID
								on b.BatchID = d.BatchID
								inner join dbo.JurisdictionPeriodic jp on c.JurisdictionID = jp.JurisdictionID and b.FinancialPeriodID = jp.FinancialPeriodID
							where b.BatchID = @batchID
								and isnull(civ.IsHiddenAdjustment, 0) = 0
							group by c.JurisdictionID, b.FinancialPeriodID, d.DocumentID, i.Sequence,
								jp.StandardProfessionalFeeValue, oiv.ProductPackPeriodicID, oiv.NumberOfProfessionalFees, civ.ProductPackPeriodicID, civ.NumberOfProfessionalFees

						) errors
						where OriginalValueOfProfessionalFees - CurrentValueOfProfessionalFees != 0.00

						union all

						select d.DocumentID, i.Sequence,
							IsProfessionalFeeChange = 0,
							IsPatientChargeChange = 0,
							NetCashVariance = (sum(isnull(civ.BasicPrice, 0.00)) - sum(isnull(oiv.BasicPrice, 0.00))) 
								+ (sum(isnull(civ.ValueOfAdditionalFees, 0.00)) - sum(isnull(oiv.ValueOfAdditionalFees, 0.00))) 
								+ (sum(isnull(civ.ValueOfOutOfPocketExpenses, 0.00)) - sum(isnull(oiv.ValueOfOutOfPocketExpenses, 0.00))),
							NetProfessionalFeeVariance = 0
						from dbo.Batch b
							inner join dbo.Document d
								inner join dbo.Item i
									left join dbo.ItemVersion oiv on i.OriginalItemVersionID = oiv.ItemVersionID
									left join dbo.ItemVersion civ on i.CurrentItemVersionID = civ.ItemVersionID
								on d.DocumentID = i.DocumentID
							on b.BatchID = d.BatchID
						where b.BatchID = @batchID
							and isnull(civ.IsHiddenAdjustment, 0) = 0
						group by d.DocumentID, i.Sequence
						having ((sum(isnull(oiv.BasicPrice, 0.00)) != sum(isnull(civ.BasicPrice, 0.00)))
								or (sum(isnull(oiv.ValueOfAdditionalFees, 0.00)) != sum(isnull(civ.ValueOfAdditionalFees, 0.00)))
								or (sum(isnull(oiv.ValueOfOutOfPocketExpenses, 0.00)) != sum(isnull(civ.ValueOfOutOfPocketExpenses, 0.00))))

						union all

						select DocumentID, Sequence,
							IsProfessionalFeeChange = 0,
							IsPatientChargeChange = case when OriginalProductPackPeriodicID is not null and CurrentProductPackPeriodicID is not null then 1 else 0 end,
							NetCashVariance = OriginalValueOfCharges - CurrentValueOfCharges,
							NetProfessionalFeeVariance = 0
						from (

							select d.DocumentID, i.Sequence,
								OriginalProductPackPeriodicID = oiv.ProductPackPeriodicID,
								OriginalValueOfCharges = case
									when d.OriginalDocumentGroupTypeID is null or ogt.IsExemptFromCharges = 1 then 0.00
									else isnull(oiv.NumberOfCharges, 0.00) * case 
										when op.ProductTypeCode = 'A' then case
											when ogt.IsOldRate = 0 then (
												select jp.ApplianceItemPrescriptionChargeValue 
												from dbo.JurisdictionPeriodic jp 
												where jp.JurisdictionID = c.JurisdictionID and jp.FinancialPeriodID = b.FinancialPeriodID
												)
											else (
												select top 1 jp.ApplianceItemPrescriptionChargeValue 
												from dbo.JurisdictionPeriodic jp 
												where jp.JurisdictionID = c.JurisdictionID and jp.FinancialPeriodID <= b.FinancialPeriodID and jp.ApplianceItemPrescriptionChargeValue != (
													select jp.ApplianceItemPrescriptionChargeValue 
													from dbo.JurisdictionPeriodic jp 
													where jp.JurisdictionID = c.JurisdictionID and jp.FinancialPeriodID = b.FinancialPeriodID
												)
												order by jp.FinancialPeriodID desc) 
											end
										else case
											when ogt.IsOldRate = 0 then (
												select jp.DrugItemPrescriptionChargeValue 
												from dbo.JurisdictionPeriodic jp 
												where jp.JurisdictionID = c.JurisdictionID and jp.FinancialPeriodID = b.FinancialPeriodID
												)
											else (
												select top 1 jp.DrugItemPrescriptionChargeValue 
												from dbo.JurisdictionPeriodic jp 
												where jp.JurisdictionID = c.JurisdictionID and jp.FinancialPeriodID <= b.FinancialPeriodID and jp.DrugItemPrescriptionChargeValue != (
													select jp.DrugItemPrescriptionChargeValue 
													from dbo.JurisdictionPeriodic jp 
													where jp.JurisdictionID = c.JurisdictionID and jp.FinancialPeriodID = b.FinancialPeriodID
												)
												order by jp.FinancialPeriodID desc) 
											end 
										end
									end,
								CurrentProductPackPeriodicID = civ.ProductPackPeriodicID,
								CurrentValueOfCharges = case
									when d.CurrentDocumentGroupTypeID is null or cgt.IsExemptFromCharges = 1 then 0.00
									else isnull(civ.NumberOfCharges, 0.00) * case 
										when cp.ProductTypeCode = 'A' then case
											when cgt.IsOldRate = 0 then (
												select jp.ApplianceItemPrescriptionChargeValue 
												from dbo.JurisdictionPeriodic jp 
												where jp.JurisdictionID = c.JurisdictionID and jp.FinancialPeriodID = b.FinancialPeriodID
												)
											else (
												select top 1 jp.ApplianceItemPrescriptionChargeValue 
												from dbo.JurisdictionPeriodic jp 
												where jp.JurisdictionID = c.JurisdictionID and jp.FinancialPeriodID <= b.FinancialPeriodID and jp.ApplianceItemPrescriptionChargeValue != (
													select jp.ApplianceItemPrescriptionChargeValue 
													from dbo.JurisdictionPeriodic jp 
													where jp.JurisdictionID = c.JurisdictionID and jp.FinancialPeriodID = b.FinancialPeriodID
												)
												order by jp.FinancialPeriodID desc) 
											end
										else case
											when cgt.IsOldRate = 0 then (
												select jp.DrugItemPrescriptionChargeValue 
												from dbo.JurisdictionPeriodic jp 
												where jp.JurisdictionID = c.JurisdictionID and jp.FinancialPeriodID = b.FinancialPeriodID
												)
											else (
												select top 1 jp.DrugItemPrescriptionChargeValue 
												from dbo.JurisdictionPeriodic jp 
												where jp.JurisdictionID = c.JurisdictionID and jp.FinancialPeriodID <= b.FinancialPeriodID and jp.DrugItemPrescriptionChargeValue != (
													select jp.DrugItemPrescriptionChargeValue 
													from dbo.JurisdictionPeriodic jp 
													where jp.JurisdictionID = c.JurisdictionID and jp.FinancialPeriodID = b.FinancialPeriodID
												)
												order by jp.FinancialPeriodID desc) 
											end 
										end
									end
							from dbo.Batch b
								inner join dbo.Contractor c on b.ContractorID = c.ContractorID
								inner join dbo.Document d
									left join dbo.DocumentGroupType odgt 
										inner join dbo.GroupType ogt on odgt.GroupTypeCode = ogt.GroupTypeCode
									on d.OriginalDocumentGroupTypeID = odgt.DocumentGroupTypeID
									inner join dbo.DocumentGroupType cdgt 
										inner join dbo.GroupType cgt on cdgt.GroupTypeCode = cgt.GroupTypeCode
									on d.CurrentDocumentGroupTypeID = cdgt.DocumentGroupTypeID
									inner join dbo.Item i
										left join dbo.ItemVersion oiv
											left join dbo.ProductPackPeriodic oppp 
												inner join dbo.ProductPeriodic opp 
													inner join dbo.Product op on opp.ProductID = op.ProductID
												on oppp.ProductPeriodicID = opp.ProductPeriodicID
											on oiv.ProductPackPeriodicID = oppp.ProductPackPeriodicID
										on i.OriginalItemVersionID = oiv.ItemVersionID
										left join dbo.ItemVersion civ
											left join dbo.ProductPackPeriodic cppp 
												inner join dbo.ProductPeriodic cpp 
													inner join dbo.Product cp on cpp.ProductID = cp.ProductID
												on cppp.ProductPeriodicID = cpp.ProductPeriodicID
											on civ.ProductPackPeriodicID = cppp.ProductPackPeriodicID
										on i.CurrentItemVersionID = civ.ItemVersionID
									on d.DocumentID = i.DocumentID
								on b.BatchID = d.BatchID
							where b.BatchID = @batchID
								and isnull(civ.IsHiddenAdjustment, 0) = 0
							group by c.JurisdictionID, b.FinancialPeriodID, d.DocumentID, i.Sequence, 
								d.OriginalDocumentGroupTypeID, oiv.ProductPackPeriodicID, op.ProductTypeCode, ogt.IsExemptFromCharges, ogt.IsOldRate, oiv.NumberOfCharges,
								d.CurrentDocumentGroupTypeID, civ.ProductPackPeriodicID, cp.ProductTypeCode, cgt.IsExemptFromCharges, cgt.IsOldRate, civ.NumberOfCharges
							--having (((max(cast (ogt.IsExemptFromCharges as integer)) != max(cast(cgt.IsExemptFromCharges as integer))) and (sum(oiv.NumberOfCharges) != 0 or sum(civ.NumberOfCharges) != 0))
							--		or ((max(cast (ogt.IsOldRate as integer)) != max(cast (cgt.IsOldRate as integer))) and (sum(oiv.NumberOfCharges) != 0 or sum(civ.NumberOfCharges) != 0))
							--		or (max(cast(cgt.IsExemptFromCharges as integer)) != 0 and (sum(oiv.NumberOfCharges) != sum(civ.NumberOfCharges))))

						) errors
						where OriginalValueOfCharges - CurrentValueOfCharges != 0.00

					) errors
					group by DocumentID, Sequence

					union all

					select DocumentId, Sequence,
						IsMissedItem = 0,
						IsExtraItem = 0,
						IsProductChange = 0,
						IsQuantityChange = 0,
						IsPackChange = 0,
						IsProfessionalFeeChange = 0,
						IsPatientChargeChange = 0,
						IsSurchargeChange = 0,
						IsInvoicePriceChange = 0,
						HasFinancialImpact = 1,
						NetCashVariance = 0,
						NetProfessionalFeeVariance = 0,
						NetPaymentForContainersVariance = 0,
						NetNonLiquidMethadoneContainerPaymentsVariance = sum(NetNonLiquidMethadoneContainerPaymentsVariance * Multiplier)
					from (					
						select nlm.*, m.Multiplier from NonLiquidMethadoneContainerPayments (@batchid) nlm
						INNER JOIN [NonLiquidMethadoneContainerPaymentsMultiplier] (@batchid) m ON nlm.documentId = m.documentId							
					) errors
					group by DocumentID, Sequence
				) errors
				group by DocumentID, Sequence

			) errors

		) errors

	)

GO
/****** Object:  UserDefinedFunction [dbo].[fnTally]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnTally](
     @pStartValue bigint= 1,
     @pEndValue bigint= 1000000,
     @pIncrement bigint= 1 
 )
 returns table
 as
 return(
     with BaseNum (
         N 
     ) as (
     select 1 union all
     select 1 union all
     select 1 union all
     select 1 union all
     select 1 union all
     select 1 union all
     select 1 union all
     select 1 union all
     select 1 union all
     select 1 
     ),
     L1 (
         N 
     ) as (
     select
         bn1.N 
     from
         BaseNum bn1 
         cross join BaseNum bn2 
     ),
     L2 (
         N 
     ) as (
     select
         a1.N 
     from
         L1 a1 
         cross join L1 a2 
     ),
     L3 (
         N 
     ) as (
     select top ((abs(case when @pStartValue < @pEndValue
                          then @pEndValue
                          else @pStartValue
                      end - 
                      case when @pStartValue < @pEndValue
                          then @pStartValue
                          else @pEndValue
                      end))/abs(@pIncrement)+ 1)
         a1.N 
     from
         L2 a1 
         cross join L2 a2 
     ),
     Tally (
         N 
     ) as (
     select
         row_number()over (order by a1.N)
     from
         L3 a1 
     )
     select
         ((N - 1) * @pIncrement) + @pStartValue as N 
     from
         Tally 
 );

GO
/****** Object:  View [ppd].[PartIX_All]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [ppd].[PartIX_All]
AS
SELECT     ActualMedicinalProductPackId, VirtualMedicinalProductPackId, AmpProductCode, AmppProductPackCode, VmpProductCode, VmppProductPackCode, HomeDelivery, 
                      StomaCustomisation
FROM         (SELECT     ActualMedicinalProductPackId, VirtualMedicinalProductPackId, AmpProductCode, AmppProductPackCode, VmpProductCode, VmppProductPackCode, 
                                              HomeDelivery, StomaCustomisation
                       FROM          ppd.PartIX_1104
                       UNION
                       SELECT     ActualMedicinalProductPackId, VirtualMedicinalProductPackId, AmpProductCode, AmppProductPackCode, VmpProductCode, VmppProductPackCode, 
                                             HomeDelivery, StomaCustomisation
                       FROM         ppd.PartIX_1105
                       UNION
                       SELECT     ActualMedicinalProductPackId, VirtualMedicinalProductPackId, AmpProductCode, AmppProductPackCode, VmpProductCode, VmppProductPackCode, 
                                             HomeDelivery, StomaCustomisation
                       FROM         ppd.PartIX_1106
                       UNION
                       SELECT     ActualMedicinalProductPackId, VirtualMedicinalProductPackId, AmpProductCode, AmppProductPackCode, VmpProductCode, VmppProductPackCode, 
                                             HomeDelivery, StomaCustomisation
                       FROM         ppd.PartIX_1107
                       UNION
                       SELECT     ActualMedicinalProductPackId, VirtualMedicinalProductPackId, AmpProductCode, AmppProductPackCode, VmpProductCode, VmppProductPackCode, 
                                             HomeDelivery, StomaCustomisation
                       FROM         ppd.PartIX_1108
                       UNION
                       SELECT     ActualMedicinalProductPackId, VirtualMedicinalProductPackId, AmpProductCode, AmppProductPackCode, VmpProductCode, VmppProductPackCode, 
                                             HomeDelivery, StomaCustomisation
                       FROM         ppd.PartIX_1109
                       UNION
                       SELECT     ActualMedicinalProductPackId, VirtualMedicinalProductPackId, AmpProductCode, AmppProductPackCode, VmpProductCode, VmppProductPackCode, 
                                             HomeDelivery, StomaCustomisation
                       FROM         ppd.PartIX_1110
                       UNION
                       SELECT     ActualMedicinalProductPackId, VirtualMedicinalProductPackId, AmpProductCode, AmppProductPackCode, VmpProductCode, VmppProductPackCode, 
                                             HomeDelivery, StomaCustomisation
                       FROM         ppd.PartIX_1111
                       UNION
                       SELECT     ActualMedicinalProductPackId, VirtualMedicinalProductPackId, AmpProductCode, AmppProductPackCode, VmpProductCode, VmppProductPackCode, 
                                             HomeDelivery, StomaCustomisation
                       FROM         ppd.PartIX_1112
                       UNION
                       SELECT     ActualMedicinalProductPackId, VirtualMedicinalProductPackId, AmpProductCode, AmppProductPackCode, VmpProductCode, VmppProductPackCode, 
                                             HomeDelivery, StomaCustomisation
                       FROM         ppd.PartIX_1201
                       UNION
                       SELECT     ActualMedicinalProductPackId, VirtualMedicinalProductPackId, AmpProductCode, AmppProductPackCode, VmpProductCode, VmppProductPackCode, 
                                             HomeDelivery, StomaCustomisation
                       FROM         ppd.PartIX_1202
                       UNION
                       SELECT     ActualMedicinalProductPackId, VirtualMedicinalProductPackId, AmpProductCode, AmppProductPackCode, VmpProductCode, VmppProductPackCode, 
                                             HomeDelivery, StomaCustomisation
                       FROM         ppd.PartIX_1203) AS PartIX

GO
ALTER TABLE [ppd].[APPL3] ADD  CONSTRAINT [DF_APPL3_PatientPackIndicator]  DEFAULT ((0)) FOR [PatientPackIndicator]
GO
ALTER TABLE [dbo].[Advisory]  WITH NOCHECK ADD  CONSTRAINT [FK_Advisory_AdvisoryGroup] FOREIGN KEY([AdvisoryGroupID])
REFERENCES [dbo].[AdvisoryGroup] ([AdvisoryGroupID])
GO
ALTER TABLE [dbo].[Advisory] CHECK CONSTRAINT [FK_Advisory_AdvisoryGroup]
GO
ALTER TABLE [dbo].[AuditFeedback]  WITH CHECK ADD  CONSTRAINT [FK_AuditFeedback_Item] FOREIGN KEY([ItemID])
REFERENCES [dbo].[Item] ([ItemID])
GO
ALTER TABLE [dbo].[AuditFeedback] CHECK CONSTRAINT [FK_AuditFeedback_Item]
GO
ALTER TABLE [dbo].[Batch]  WITH CHECK ADD  CONSTRAINT [FK_Batch_AuditType] FOREIGN KEY([AuditTypeID])
REFERENCES [dbo].[AuditType] ([AuditTypeID])
GO
ALTER TABLE [dbo].[Batch] CHECK CONSTRAINT [FK_Batch_AuditType]
GO
ALTER TABLE [dbo].[Batch]  WITH CHECK ADD  CONSTRAINT [FK_Batch_BatchStatus] FOREIGN KEY([BatchStatusID])
REFERENCES [dbo].[BatchStatus] ([BatchStatusID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Batch] CHECK CONSTRAINT [FK_Batch_BatchStatus]
GO
ALTER TABLE [dbo].[Batch]  WITH CHECK ADD  CONSTRAINT [FK_Batch_BsaSchedule] FOREIGN KEY([ScheduleID])
REFERENCES [dbo].[Schedule] ([ScheduleID])
GO
ALTER TABLE [dbo].[Batch] CHECK CONSTRAINT [FK_Batch_BsaSchedule]
GO
ALTER TABLE [dbo].[Batch]  WITH CHECK ADD  CONSTRAINT [FK_Batch_Contractor] FOREIGN KEY([ContractorID])
REFERENCES [dbo].[Contractor] ([ContractorID])
GO
ALTER TABLE [dbo].[Batch] CHECK CONSTRAINT [FK_Batch_Contractor]
GO
ALTER TABLE [dbo].[Batch]  WITH CHECK ADD  CONSTRAINT [FK_Batch_DataRequest] FOREIGN KEY([DataRequestID])
REFERENCES [dbo].[DataRequest] ([DataRequestID])
GO
ALTER TABLE [dbo].[Batch] CHECK CONSTRAINT [FK_Batch_DataRequest]
GO
ALTER TABLE [dbo].[Batch]  WITH CHECK ADD  CONSTRAINT [FK_Batch_Division] FOREIGN KEY([DivisionID])
REFERENCES [dbo].[Division] ([DivisionID])
GO
ALTER TABLE [dbo].[Batch] CHECK CONSTRAINT [FK_Batch_Division]
GO
ALTER TABLE [dbo].[Batch]  WITH CHECK ADD  CONSTRAINT [FK_Batch_FinancialPeriod] FOREIGN KEY([FinancialPeriodID])
REFERENCES [dbo].[FinancialPeriod] ([FinancialPeriodID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Batch] CHECK CONSTRAINT [FK_Batch_FinancialPeriod]
GO
ALTER TABLE [dbo].[Batch]  WITH CHECK ADD  CONSTRAINT [FK_Batch_LocalPharmaceuticalCommittee] FOREIGN KEY([LocalPharmaceuticalCommitteeID])
REFERENCES [dbo].[LocalPharmaceuticalCommittee] ([LocalPharmaceuticalCommitteeID])
GO
ALTER TABLE [dbo].[Batch] CHECK CONSTRAINT [FK_Batch_LocalPharmaceuticalCommittee]
GO
ALTER TABLE [dbo].[Batch]  WITH CHECK ADD  CONSTRAINT [FK_Batch_PaperTracking_CurrentBundleTrackingStatus] FOREIGN KEY([CurrentBundleTrackingID])
REFERENCES [dbo].[PaperTracking] ([PaperTrackingID])
GO
ALTER TABLE [dbo].[Batch] CHECK CONSTRAINT [FK_Batch_PaperTracking_CurrentBundleTrackingStatus]
GO
ALTER TABLE [dbo].[Batch]  WITH CHECK ADD  CONSTRAINT [FK_Batch_PaperTracking_CurrentFragmentTrackingStatus] FOREIGN KEY([CurrentFragmentTrackingID])
REFERENCES [dbo].[PaperTracking] ([PaperTrackingID])
GO
ALTER TABLE [dbo].[Batch] CHECK CONSTRAINT [FK_Batch_PaperTracking_CurrentFragmentTrackingStatus]
GO
ALTER TABLE [dbo].[Batch]  WITH CHECK ADD  CONSTRAINT [FK_Batch_PrimaryCareTrust] FOREIGN KEY([PrimaryCareTrustCode])
REFERENCES [dbo].[PrimaryCareTrust] ([PrimaryCareTrustCode])
GO
ALTER TABLE [dbo].[Batch] CHECK CONSTRAINT [FK_Batch_PrimaryCareTrust]
GO
ALTER TABLE [dbo].[Batch]  WITH CHECK ADD  CONSTRAINT [FK_Batch_PsncSchedule] FOREIGN KEY([PsncScheduleID])
REFERENCES [dbo].[Schedule] ([ScheduleID])
GO
ALTER TABLE [dbo].[Batch] CHECK CONSTRAINT [FK_Batch_PsncSchedule]
GO
ALTER TABLE [dbo].[BatchAuditStatistics]  WITH CHECK ADD  CONSTRAINT [FK_BatchAuditStatistics_Batch] FOREIGN KEY([BatchID])
REFERENCES [dbo].[Batch] ([BatchID])
GO
ALTER TABLE [dbo].[BatchAuditStatistics] CHECK CONSTRAINT [FK_BatchAuditStatistics_Batch]
GO
ALTER TABLE [dbo].[BatchStatusHistory]  WITH CHECK ADD  CONSTRAINT [FK_BatchStatusHistory_Batch] FOREIGN KEY([BatchID])
REFERENCES [dbo].[Batch] ([BatchID])
GO
ALTER TABLE [dbo].[BatchStatusHistory] CHECK CONSTRAINT [FK_BatchStatusHistory_Batch]
GO
ALTER TABLE [dbo].[BatchStatusHistory]  WITH CHECK ADD  CONSTRAINT [FK_BatchStatusHistory_BatchStatus] FOREIGN KEY([BatchStatusID])
REFERENCES [dbo].[BatchStatus] ([BatchStatusID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[BatchStatusHistory] CHECK CONSTRAINT [FK_BatchStatusHistory_BatchStatus]
GO
ALTER TABLE [dbo].[BatchStatusHistory]  WITH CHECK ADD  CONSTRAINT [FK_BatchStatusHistory_UserProfile] FOREIGN KEY([UserProfileID])
REFERENCES [dbo].[UserProfile] ([UserProfileID])
GO
ALTER TABLE [dbo].[BatchStatusHistory] CHECK CONSTRAINT [FK_BatchStatusHistory_UserProfile]
GO
ALTER TABLE [dbo].[BatchUserProfile]  WITH CHECK ADD  CONSTRAINT [FK_BatchUserProfile_Batch] FOREIGN KEY([BatchID])
REFERENCES [dbo].[Batch] ([BatchID])
GO
ALTER TABLE [dbo].[BatchUserProfile] CHECK CONSTRAINT [FK_BatchUserProfile_Batch]
GO
ALTER TABLE [dbo].[BatchUserProfile]  WITH CHECK ADD  CONSTRAINT [FK_BatchUserProfile_Document] FOREIGN KEY([LastDocumentID])
REFERENCES [dbo].[Document] ([DocumentID])
GO
ALTER TABLE [dbo].[BatchUserProfile] CHECK CONSTRAINT [FK_BatchUserProfile_Document]
GO
ALTER TABLE [dbo].[BatchUserProfile]  WITH CHECK ADD  CONSTRAINT [FK_BatchUserProfile_UserProfile] FOREIGN KEY([UserProfileID])
REFERENCES [dbo].[UserProfile] ([UserProfileID])
GO
ALTER TABLE [dbo].[BatchUserProfile] CHECK CONSTRAINT [FK_BatchUserProfile_UserProfile]
GO
ALTER TABLE [dbo].[Contractor]  WITH CHECK ADD  CONSTRAINT [FK_Contractor_Division] FOREIGN KEY([DivisionID])
REFERENCES [dbo].[Division] ([DivisionID])
GO
ALTER TABLE [dbo].[Contractor] CHECK CONSTRAINT [FK_Contractor_Division]
GO
ALTER TABLE [dbo].[Contractor]  WITH CHECK ADD  CONSTRAINT [FK_Contractor_Jurisdiction] FOREIGN KEY([JurisdictionID])
REFERENCES [dbo].[Jurisdiction] ([JurisdictionID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Contractor] CHECK CONSTRAINT [FK_Contractor_Jurisdiction]
GO
ALTER TABLE [dbo].[Contractor]  WITH CHECK ADD  CONSTRAINT [FK_Contractor_LocalPharmaceuticalCommittee] FOREIGN KEY([LocalPharmaceuticalCommitteeID])
REFERENCES [dbo].[LocalPharmaceuticalCommittee] ([LocalPharmaceuticalCommitteeID])
GO
ALTER TABLE [dbo].[Contractor] CHECK CONSTRAINT [FK_Contractor_LocalPharmaceuticalCommittee]
GO
ALTER TABLE [dbo].[ControlledSubstanceIndicatorPeriodic]  WITH CHECK ADD  CONSTRAINT [FK_ControlledSubstanceIndicatorPeriodic_ControlledSubstanceIndicator] FOREIGN KEY([ControlledSubstanceIndicatorCode])
REFERENCES [dbo].[ControlledSubstanceIndicator] ([ControlledSubstanceIndicatorCode])
GO
ALTER TABLE [dbo].[ControlledSubstanceIndicatorPeriodic] CHECK CONSTRAINT [FK_ControlledSubstanceIndicatorPeriodic_ControlledSubstanceIndicator]
GO
ALTER TABLE [dbo].[ControlledSubstanceIndicatorPeriodic]  WITH CHECK ADD  CONSTRAINT [FK_ControlledSubstanceIndicatorPeriodic_FinancialPeriod] FOREIGN KEY([FinancialPeriodID])
REFERENCES [dbo].[FinancialPeriod] ([FinancialPeriodID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[ControlledSubstanceIndicatorPeriodic] CHECK CONSTRAINT [FK_ControlledSubstanceIndicatorPeriodic_FinancialPeriod]
GO
ALTER TABLE [dbo].[ControlledSubstanceIndicatorPeriodic]  WITH CHECK ADD  CONSTRAINT [FK_ControlledSubstanceIndicatorPeriodic_Jurisdiction] FOREIGN KEY([JurisdictionID])
REFERENCES [dbo].[Jurisdiction] ([JurisdictionID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[ControlledSubstanceIndicatorPeriodic] CHECK CONSTRAINT [FK_ControlledSubstanceIndicatorPeriodic_Jurisdiction]
GO
ALTER TABLE [dbo].[DataRequest]  WITH CHECK ADD  CONSTRAINT [FK_DataRequest_Jurisdiction] FOREIGN KEY([JurisdictionID])
REFERENCES [dbo].[Jurisdiction] ([JurisdictionID])
GO
ALTER TABLE [dbo].[DataRequest] CHECK CONSTRAINT [FK_DataRequest_Jurisdiction]
GO
ALTER TABLE [dbo].[DivisionAlias]  WITH CHECK ADD  CONSTRAINT [FK_DivisionAlias_Division] FOREIGN KEY([DivisionID])
REFERENCES [dbo].[Division] ([DivisionID])
GO
ALTER TABLE [dbo].[DivisionAlias] CHECK CONSTRAINT [FK_DivisionAlias_Division]
GO
ALTER TABLE [dbo].[Document]  WITH CHECK ADD  CONSTRAINT [FK_Document_Batch] FOREIGN KEY([BatchID])
REFERENCES [dbo].[Batch] ([BatchID])
GO
ALTER TABLE [dbo].[Document] CHECK CONSTRAINT [FK_Document_Batch]
GO
ALTER TABLE [dbo].[Document]  WITH CHECK ADD  CONSTRAINT [FK_Document_CurrentDocumentGroupType] FOREIGN KEY([CurrentDocumentGroupTypeID])
REFERENCES [dbo].[DocumentGroupType] ([DocumentGroupTypeID])
GO
ALTER TABLE [dbo].[Document] CHECK CONSTRAINT [FK_Document_CurrentDocumentGroupType]
GO
ALTER TABLE [dbo].[Document]  WITH CHECK ADD  CONSTRAINT [FK_Document_DocumentStatus] FOREIGN KEY([DocumentStatusID])
REFERENCES [dbo].[DocumentStatus] ([DocumentStatusID])
GO
ALTER TABLE [dbo].[Document] CHECK CONSTRAINT [FK_Document_DocumentStatus]
GO
ALTER TABLE [dbo].[Document]  WITH CHECK ADD  CONSTRAINT [FK_Document_Exemption] FOREIGN KEY([ExemptionCode])
REFERENCES [dbo].[Exemption] ([ExemptionCode])
GO
ALTER TABLE [dbo].[Document] CHECK CONSTRAINT [FK_Document_Exemption]
GO
ALTER TABLE [dbo].[Document]  WITH CHECK ADD  CONSTRAINT [FK_Document_FormSubType] FOREIGN KEY([FormSubTypeCode])
REFERENCES [dbo].[FormSubType] ([FormSubTypeCode])
GO
ALTER TABLE [dbo].[Document] CHECK CONSTRAINT [FK_Document_FormSubType]
GO
ALTER TABLE [dbo].[Document]  WITH CHECK ADD  CONSTRAINT [FK_Document_FormType] FOREIGN KEY([FormTypeCode])
REFERENCES [dbo].[FormType] ([FormTypeCode])
GO
ALTER TABLE [dbo].[Document] CHECK CONSTRAINT [FK_Document_FormType]
GO
ALTER TABLE [dbo].[Document]  WITH CHECK ADD  CONSTRAINT [FK_Document_FragmentType] FOREIGN KEY([FragmentTypeCode])
REFERENCES [dbo].[FragmentType] ([FragmentTypeCode])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Document] CHECK CONSTRAINT [FK_Document_FragmentType]
GO
ALTER TABLE [dbo].[Document]  WITH CHECK ADD  CONSTRAINT [FK_Document_OriginalDocumentGroupType] FOREIGN KEY([OriginalDocumentGroupTypeID])
REFERENCES [dbo].[DocumentGroupType] ([DocumentGroupTypeID])
GO
ALTER TABLE [dbo].[Document] CHECK CONSTRAINT [FK_Document_OriginalDocumentGroupType]
GO
ALTER TABLE [dbo].[Document]  WITH CHECK ADD  CONSTRAINT [FK_Document_SubmissionGroup] FOREIGN KEY([SubmissionGroupCode])
REFERENCES [dbo].[SubmissionGroup] ([SubmissionGroupCode])
GO
ALTER TABLE [dbo].[Document] CHECK CONSTRAINT [FK_Document_SubmissionGroup]
GO
ALTER TABLE [dbo].[DocumentGroupType]  WITH CHECK ADD  CONSTRAINT [FK_DocumentGroupType_Document] FOREIGN KEY([DocumentID])
REFERENCES [dbo].[Document] ([DocumentID])
GO
ALTER TABLE [dbo].[DocumentGroupType] CHECK CONSTRAINT [FK_DocumentGroupType_Document]
GO
ALTER TABLE [dbo].[DocumentGroupType]  WITH CHECK ADD  CONSTRAINT [FK_DocumentGroupType_GroupType] FOREIGN KEY([GroupTypeCode])
REFERENCES [dbo].[GroupType] ([GroupTypeCode])
GO
ALTER TABLE [dbo].[DocumentGroupType] CHECK CONSTRAINT [FK_DocumentGroupType_GroupType]
GO
ALTER TABLE [dbo].[DocumentImage]  WITH CHECK ADD  CONSTRAINT [FK_DocumentImage_Document] FOREIGN KEY([DocumentID])
REFERENCES [dbo].[Document] ([DocumentID])
GO
ALTER TABLE [dbo].[DocumentImage] CHECK CONSTRAINT [FK_DocumentImage_Document]
GO
ALTER TABLE [dbo].[DocumentImage]  WITH CHECK ADD  CONSTRAINT [FK_DocumentImage_DocumentImageType] FOREIGN KEY([DocumentImageTypeID])
REFERENCES [dbo].[DocumentImageType] ([DocumentImageTypeID])
GO
ALTER TABLE [dbo].[DocumentImage] CHECK CONSTRAINT [FK_DocumentImage_DocumentImageType]
GO
ALTER TABLE [dbo].[EndorsementPeriodic]  WITH CHECK ADD  CONSTRAINT [FK_EndorsementPeriodic_FinancialPeriod] FOREIGN KEY([FinancialPeriodID])
REFERENCES [dbo].[FinancialPeriod] ([FinancialPeriodID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[EndorsementPeriodic] CHECK CONSTRAINT [FK_EndorsementPeriodic_FinancialPeriod]
GO
ALTER TABLE [dbo].[EndorsementPeriodic]  WITH CHECK ADD  CONSTRAINT [FK_EndorsementPeriodic_Jurisdiction] FOREIGN KEY([JurisdictionID])
REFERENCES [dbo].[Jurisdiction] ([JurisdictionID])
GO
ALTER TABLE [dbo].[EndorsementPeriodic] CHECK CONSTRAINT [FK_EndorsementPeriodic_Jurisdiction]
GO
ALTER TABLE [dbo].[EpsDispensedItem]  WITH CHECK ADD  CONSTRAINT [FK_EpsDispensedItem_EpsPrescribedItem] FOREIGN KEY([EpsPrescribedItemID])
REFERENCES [dbo].[EpsPrescribedItem] ([EpsPrescribedItemID])
GO
ALTER TABLE [dbo].[EpsDispensedItem] CHECK CONSTRAINT [FK_EpsDispensedItem_EpsPrescribedItem]
GO
ALTER TABLE [dbo].[EpsHeader]  WITH CHECK ADD  CONSTRAINT [FK_EpsHeader_Document] FOREIGN KEY([DocumentID])
REFERENCES [dbo].[Document] ([DocumentID])
GO
ALTER TABLE [dbo].[EpsHeader] CHECK CONSTRAINT [FK_EpsHeader_Document]
GO
ALTER TABLE [dbo].[EpsPrescribedItem]  WITH CHECK ADD  CONSTRAINT [FK_EpsPrescribedItem_EpsHeader] FOREIGN KEY([EpsHeaderID])
REFERENCES [dbo].[EpsHeader] ([EpsHeaderID])
GO
ALTER TABLE [dbo].[EpsPrescribedItem] CHECK CONSTRAINT [FK_EpsPrescribedItem_EpsHeader]
GO
ALTER TABLE [dbo].[GuidanceArticle]  WITH CHECK ADD  CONSTRAINT [FK_GuidanceArticle_GuidanceCategory] FOREIGN KEY([GuidanceCategoryID])
REFERENCES [dbo].[GuidanceCategory] ([GuidanceCategoryID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[GuidanceArticle] CHECK CONSTRAINT [FK_GuidanceArticle_GuidanceCategory]
GO
ALTER TABLE [dbo].[GuidanceAttachment]  WITH CHECK ADD  CONSTRAINT [FK_GuidanceAttachment_GuidanceArticle] FOREIGN KEY([GuidanceArticleID])
REFERENCES [dbo].[GuidanceArticle] ([GuidanceArticleID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[GuidanceAttachment] CHECK CONSTRAINT [FK_GuidanceAttachment_GuidanceArticle]
GO
ALTER TABLE [dbo].[HealthAuthority]  WITH CHECK ADD  CONSTRAINT [FK_HealthAuthority_RegionalDirectorate] FOREIGN KEY([RegionalDirectorateCode])
REFERENCES [dbo].[RegionalDirectorate] ([RegionalDirectorateCode])
GO
ALTER TABLE [dbo].[HealthAuthority] CHECK CONSTRAINT [FK_HealthAuthority_RegionalDirectorate]
GO
ALTER TABLE [dbo].[Item]  WITH CHECK ADD  CONSTRAINT [FK_Item_CurrentItemVersion] FOREIGN KEY([CurrentItemVersionID])
REFERENCES [dbo].[ItemVersion] ([ItemVersionID])
GO
ALTER TABLE [dbo].[Item] CHECK CONSTRAINT [FK_Item_CurrentItemVersion]
GO
ALTER TABLE [dbo].[Item]  WITH CHECK ADD  CONSTRAINT [FK_Item_Document] FOREIGN KEY([DocumentID])
REFERENCES [dbo].[Document] ([DocumentID])
GO
ALTER TABLE [dbo].[Item] CHECK CONSTRAINT [FK_Item_Document]
GO
ALTER TABLE [dbo].[Item]  WITH CHECK ADD  CONSTRAINT [FK_Item_OriginalItemVersion] FOREIGN KEY([OriginalItemVersionID])
REFERENCES [dbo].[ItemVersion] ([ItemVersionID])
GO
ALTER TABLE [dbo].[Item] CHECK CONSTRAINT [FK_Item_OriginalItemVersion]
GO
ALTER TABLE [dbo].[ItemAdvisory]  WITH NOCHECK ADD  CONSTRAINT [FK_ItemAdvisory_Advisory] FOREIGN KEY([AdvisoryID])
REFERENCES [dbo].[Advisory] ([AdvisoryID])
GO
ALTER TABLE [dbo].[ItemAdvisory] CHECK CONSTRAINT [FK_ItemAdvisory_Advisory]
GO
ALTER TABLE [dbo].[ItemAdvisory]  WITH CHECK ADD  CONSTRAINT [FK_ItemAdvisoryNote_Item] FOREIGN KEY([ItemID])
REFERENCES [dbo].[Item] ([ItemID])
GO
ALTER TABLE [dbo].[ItemAdvisory] CHECK CONSTRAINT [FK_ItemAdvisoryNote_Item]
GO
ALTER TABLE [dbo].[ItemNote]  WITH CHECK ADD  CONSTRAINT [FK_ItemNote_Item] FOREIGN KEY([ItemID])
REFERENCES [dbo].[Item] ([ItemID])
GO
ALTER TABLE [dbo].[ItemNote] CHECK CONSTRAINT [FK_ItemNote_Item]
GO
ALTER TABLE [dbo].[ItemVersion]  WITH CHECK ADD  CONSTRAINT [FK_ItemVersion_Item] FOREIGN KEY([ItemID])
REFERENCES [dbo].[Item] ([ItemID])
GO
ALTER TABLE [dbo].[ItemVersion] CHECK CONSTRAINT [FK_ItemVersion_Item]
GO
ALTER TABLE [dbo].[ItemVersion]  WITH CHECK ADD  CONSTRAINT [FK_ItemVersion_ProductPackPeriodic] FOREIGN KEY([ProductPackPeriodicID])
REFERENCES [dbo].[ProductPackPeriodic] ([ProductPackPeriodicID])
GO
ALTER TABLE [dbo].[ItemVersion] CHECK CONSTRAINT [FK_ItemVersion_ProductPackPeriodic]
GO
ALTER TABLE [dbo].[ItemVersionEndorsement]  WITH CHECK ADD  CONSTRAINT [FK_ItemVersionEndorsement_EndorsementPeriodic] FOREIGN KEY([EndorsementPeriodicID])
REFERENCES [dbo].[EndorsementPeriodic] ([EndorsementPeriodicID])
GO
ALTER TABLE [dbo].[ItemVersionEndorsement] CHECK CONSTRAINT [FK_ItemVersionEndorsement_EndorsementPeriodic]
GO
ALTER TABLE [dbo].[ItemVersionEndorsement]  WITH CHECK ADD  CONSTRAINT [FK_ItemVersionEndorsement_ItemVersion] FOREIGN KEY([ItemVersionID])
REFERENCES [dbo].[ItemVersion] ([ItemVersionID])
GO
ALTER TABLE [dbo].[ItemVersionEndorsement] CHECK CONSTRAINT [FK_ItemVersionEndorsement_ItemVersion]
GO
ALTER TABLE [dbo].[JurisdictionPeriodic]  WITH CHECK ADD  CONSTRAINT [FK_JurisdictionPeriodic_FinancialPeriod] FOREIGN KEY([FinancialPeriodID])
REFERENCES [dbo].[FinancialPeriod] ([FinancialPeriodID])
GO
ALTER TABLE [dbo].[JurisdictionPeriodic] CHECK CONSTRAINT [FK_JurisdictionPeriodic_FinancialPeriod]
GO
ALTER TABLE [dbo].[JurisdictionPeriodic]  WITH CHECK ADD  CONSTRAINT [FK_JurisdictionPeriodic_Jurisdiction] FOREIGN KEY([JurisdictionID])
REFERENCES [dbo].[Jurisdiction] ([JurisdictionID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[JurisdictionPeriodic] CHECK CONSTRAINT [FK_JurisdictionPeriodic_Jurisdiction]
GO
ALTER TABLE [dbo].[Manufacturer]  WITH CHECK ADD  CONSTRAINT [FK_Manufacturer_ManufacturerCategory] FOREIGN KEY([ManufacturerCategoryCode])
REFERENCES [dbo].[ManufacturerCategory] ([ManufacturerCategoryCode])
GO
ALTER TABLE [dbo].[Manufacturer] CHECK CONSTRAINT [FK_Manufacturer_ManufacturerCategory]
GO
ALTER TABLE [dbo].[PaperRequestHistory]  WITH CHECK ADD  CONSTRAINT [FK_PaperRequestHistory_Batch] FOREIGN KEY([BatchID])
REFERENCES [dbo].[Batch] ([BatchID])
GO
ALTER TABLE [dbo].[PaperRequestHistory] CHECK CONSTRAINT [FK_PaperRequestHistory_Batch]
GO
ALTER TABLE [dbo].[PaperRequestHistory]  WITH CHECK ADD  CONSTRAINT [FK_PaperRequestHistory_PaperRequestStatus] FOREIGN KEY([PaperRequestStatusID])
REFERENCES [dbo].[PaperRequestStatus] ([PaperRequestStatusID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[PaperRequestHistory] CHECK CONSTRAINT [FK_PaperRequestHistory_PaperRequestStatus]
GO
ALTER TABLE [dbo].[PaperTracking]  WITH CHECK ADD  CONSTRAINT [FK_PaperTracking_Batch] FOREIGN KEY([BatchID])
REFERENCES [dbo].[Batch] ([BatchID])
GO
ALTER TABLE [dbo].[PaperTracking] CHECK CONSTRAINT [FK_PaperTracking_Batch]
GO
ALTER TABLE [dbo].[PaperTracking]  WITH CHECK ADD  CONSTRAINT [FK_PaperTracking_PaperTrackingStatus] FOREIGN KEY([PaperTrackingStatusID])
REFERENCES [dbo].[PaperTrackingStatus] ([PaperTrackingStatusID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[PaperTracking] CHECK CONSTRAINT [FK_PaperTracking_PaperTrackingStatus]
GO
ALTER TABLE [dbo].[PaperTracking]  WITH CHECK ADD  CONSTRAINT [FK_PaperTracking_PaperType] FOREIGN KEY([PaperTypeID])
REFERENCES [dbo].[PaperType] ([PaperTypeID])
GO
ALTER TABLE [dbo].[PaperTracking] CHECK CONSTRAINT [FK_PaperTracking_PaperType]
GO
ALTER TABLE [dbo].[PrimaryCareTrust]  WITH CHECK ADD  CONSTRAINT [FK_PrimaryCareTrust_HealthAuthority] FOREIGN KEY([HealthAuthorityCode])
REFERENCES [dbo].[HealthAuthority] ([HealthAuthorityCode])
GO
ALTER TABLE [dbo].[PrimaryCareTrust] CHECK CONSTRAINT [FK_PrimaryCareTrust_HealthAuthority]
GO
ALTER TABLE [dbo].[Product]  WITH NOCHECK ADD  CONSTRAINT [FK_Product_Manufacturer] FOREIGN KEY([ManufacturerID])
REFERENCES [dbo].[Manufacturer] ([ManufacturerID])
GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_Product_Manufacturer]
GO
ALTER TABLE [dbo].[Product]  WITH NOCHECK ADD  CONSTRAINT [FK_Product_ProductType] FOREIGN KEY([ProductTypeCode])
REFERENCES [dbo].[ProductType] ([ProductTypeCode])
GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_Product_ProductType]
GO
ALTER TABLE [dbo].[ProductPack]  WITH NOCHECK ADD  CONSTRAINT [FK_ProductPack_Product] FOREIGN KEY([ProductID])
REFERENCES [dbo].[Product] ([ProductID])
GO
ALTER TABLE [dbo].[ProductPack] CHECK CONSTRAINT [FK_ProductPack_Product]
GO
ALTER TABLE [dbo].[ProductPackPeriodic]  WITH CHECK ADD  CONSTRAINT [FK_ProductPackPeriodic_DrugTariffCategory] FOREIGN KEY([DrugTariffCategoryCode])
REFERENCES [dbo].[DrugTariffCategory] ([DrugTariffCategoryCode])
GO
ALTER TABLE [dbo].[ProductPackPeriodic] CHECK CONSTRAINT [FK_ProductPackPeriodic_DrugTariffCategory]
GO
ALTER TABLE [dbo].[ProductPackPeriodic]  WITH CHECK ADD  CONSTRAINT [FK_ProductPackPeriodic_HomeDeliveryIndicator] FOREIGN KEY([HomeDeliveryIndicatorCode])
REFERENCES [dbo].[HomeDeliveryIndicator] ([HomeDeliveryIndicatorCode])
GO
ALTER TABLE [dbo].[ProductPackPeriodic] CHECK CONSTRAINT [FK_ProductPackPeriodic_HomeDeliveryIndicator]
GO
ALTER TABLE [dbo].[ProductPackPeriodic]  WITH CHECK ADD  CONSTRAINT [FK_ProductPackPeriodic_ProductPack] FOREIGN KEY([ProductPackID])
REFERENCES [dbo].[ProductPack] ([ProductPackID])
GO
ALTER TABLE [dbo].[ProductPackPeriodic] CHECK CONSTRAINT [FK_ProductPackPeriodic_ProductPack]
GO
ALTER TABLE [dbo].[ProductPackPeriodic]  WITH CHECK ADD  CONSTRAINT [FK_ProductPackPeriodic_ProductPeriodic] FOREIGN KEY([ProductPeriodicID])
REFERENCES [dbo].[ProductPeriodic] ([ProductPeriodicID])
GO
ALTER TABLE [dbo].[ProductPackPeriodic] CHECK CONSTRAINT [FK_ProductPackPeriodic_ProductPeriodic]
GO
ALTER TABLE [dbo].[ProductPackPeriodic]  WITH CHECK ADD  CONSTRAINT [FK_ProductPackPeriodic_SpecialContainerIndicator] FOREIGN KEY([SpecialContainerIndicatorCode])
REFERENCES [dbo].[SpecialContainerIndicator] ([SpecialContainerIndicatorCode])
GO
ALTER TABLE [dbo].[ProductPackPeriodic] CHECK CONSTRAINT [FK_ProductPackPeriodic_SpecialContainerIndicator]
GO
ALTER TABLE [dbo].[ProductPeriodic]  WITH CHECK ADD  CONSTRAINT [FK_ProductPeriodic_ApplianceTypeIndicator] FOREIGN KEY([ApplianceTypeIndicatorCode])
REFERENCES [dbo].[ApplianceTypeIndicator] ([ApplianceTypeIndicatorCode])
GO
ALTER TABLE [dbo].[ProductPeriodic] CHECK CONSTRAINT [FK_ProductPeriodic_ApplianceTypeIndicator]
GO
ALTER TABLE [dbo].[ProductPeriodic]  WITH CHECK ADD  CONSTRAINT [FK_ProductPeriodic_BorderlineSubstanceIndicator] FOREIGN KEY([BorderlineSubstanceIndicatorCode])
REFERENCES [dbo].[BorderlineSubstanceIndicator] ([BorderlineSubstanceIndicatorCode])
GO
ALTER TABLE [dbo].[ProductPeriodic] CHECK CONSTRAINT [FK_ProductPeriodic_BorderlineSubstanceIndicator]
GO
ALTER TABLE [dbo].[ProductPeriodic]  WITH CHECK ADD  CONSTRAINT [FK_ProductPeriodic_ControlledSubstanceIndicator] FOREIGN KEY([ControlledSubstanceIndicatorCode])
REFERENCES [dbo].[ControlledSubstanceIndicator] ([ControlledSubstanceIndicatorCode])
GO
ALTER TABLE [dbo].[ProductPeriodic] CHECK CONSTRAINT [FK_ProductPeriodic_ControlledSubstanceIndicator]
GO
ALTER TABLE [dbo].[ProductPeriodic]  WITH CHECK ADD  CONSTRAINT [FK_ProductPeriodic_DentalFormularyIndicator] FOREIGN KEY([DentalFormularyIndicatorCode])
REFERENCES [dbo].[DentalFormularyIndicator] ([DentalFormularyIndicatorCode])
GO
ALTER TABLE [dbo].[ProductPeriodic] CHECK CONSTRAINT [FK_ProductPeriodic_DentalFormularyIndicator]
GO
ALTER TABLE [dbo].[ProductPeriodic]  WITH CHECK ADD  CONSTRAINT [FK_ProductPeriodic_DiluentIndicator] FOREIGN KEY([DiluentIndicatorCode])
REFERENCES [dbo].[DiluentIndicator] ([DiluentIndicatorCode])
GO
ALTER TABLE [dbo].[ProductPeriodic] CHECK CONSTRAINT [FK_ProductPeriodic_DiluentIndicator]
GO
ALTER TABLE [dbo].[ProductPeriodic]  WITH CHECK ADD  CONSTRAINT [FK_ProductPeriodic_FinancialPeriod] FOREIGN KEY([FinancialPeriodID])
REFERENCES [dbo].[FinancialPeriod] ([FinancialPeriodID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[ProductPeriodic] CHECK CONSTRAINT [FK_ProductPeriodic_FinancialPeriod]
GO
ALTER TABLE [dbo].[ProductPeriodic]  WITH CHECK ADD  CONSTRAINT [FK_ProductPeriodic_InputQuantityUnitIndicator] FOREIGN KEY([InputQuantityUnitIndicatorCode])
REFERENCES [dbo].[InputQuantityUnitIndicator] ([InputQuantityUnitIndicatorCode])
GO
ALTER TABLE [dbo].[ProductPeriodic] CHECK CONSTRAINT [FK_ProductPeriodic_InputQuantityUnitIndicator]
GO
ALTER TABLE [dbo].[ProductPeriodic]  WITH CHECK ADD  CONSTRAINT [FK_ProductPeriodic_PersonalAdministrationIndicator] FOREIGN KEY([PersonalAdministrationIndicatorCode])
REFERENCES [dbo].[PersonalAdministrationIndicator] ([PersonalAdministrationIndicatorCode])
GO
ALTER TABLE [dbo].[ProductPeriodic] CHECK CONSTRAINT [FK_ProductPeriodic_PersonalAdministrationIndicator]
GO
ALTER TABLE [dbo].[ProductPeriodic]  WITH CHECK ADD  CONSTRAINT [FK_ProductPeriodic_PreperationClassIndicator] FOREIGN KEY([PreperationClassIndicatorCode])
REFERENCES [dbo].[PreperationClassIndicator] ([PreperationClassIndicatorCode])
GO
ALTER TABLE [dbo].[ProductPeriodic] CHECK CONSTRAINT [FK_ProductPeriodic_PreperationClassIndicator]
GO
ALTER TABLE [dbo].[ProductPeriodic]  WITH NOCHECK ADD  CONSTRAINT [FK_ProductPeriodic_Product] FOREIGN KEY([ProductID])
REFERENCES [dbo].[Product] ([ProductID])
GO
ALTER TABLE [dbo].[ProductPeriodic] CHECK CONSTRAINT [FK_ProductPeriodic_Product]
GO
ALTER TABLE [dbo].[ProductPeriodic]  WITH CHECK ADD  CONSTRAINT [FK_ProductPeriodic_ProductAllowedIndicator] FOREIGN KEY([ProductAllowedIndicatorCode])
REFERENCES [dbo].[ProductAllowedIndicator] ([ProductAllowedIndicatorCode])
GO
ALTER TABLE [dbo].[ProductPeriodic] CHECK CONSTRAINT [FK_ProductPeriodic_ProductAllowedIndicator]
GO
ALTER TABLE [dbo].[ProductPeriodic]  WITH CHECK ADD  CONSTRAINT [FK_ProductPeriodic_StandardDrugClassIndicator] FOREIGN KEY([StandardDrugClassIndicatorCode])
REFERENCES [dbo].[StandardDrugClassIndicator] ([StandardDrugClassIndicatorCode])
GO
ALTER TABLE [dbo].[ProductPeriodic] CHECK CONSTRAINT [FK_ProductPeriodic_StandardDrugClassIndicator]
GO
ALTER TABLE [dbo].[ProductPeriodic]  WITH CHECK ADD  CONSTRAINT [FK_ProductPeriodic_StandardQuantityUnitIndicator] FOREIGN KEY([StandardQuantityUnitIndicatorCode])
REFERENCES [dbo].[StandardQuantityUnitIndicator] ([StandardQuantityUnitIndicatorCode])
GO
ALTER TABLE [dbo].[ProductPeriodic] CHECK CONSTRAINT [FK_ProductPeriodic_StandardQuantityUnitIndicator]
GO
ALTER TABLE [dbo].[ProductPeriodic]  WITH CHECK ADD  CONSTRAINT [FK_ProductPeriodic_UnusualFeeIndicator] FOREIGN KEY([UnusualFeeIndicatorCode])
REFERENCES [dbo].[UnusualFeeIndicator] ([UnusualFeeIndicatorCode])
GO
ALTER TABLE [dbo].[ProductPeriodic] CHECK CONSTRAINT [FK_ProductPeriodic_UnusualFeeIndicator]
GO
ALTER TABLE [dbo].[ProductPeriodic]  WITH CHECK ADD  CONSTRAINT [FK_ProductPeriodic_WithdrawalStatusIndicator] FOREIGN KEY([WithdrawalStatusIndicatorCode])
REFERENCES [dbo].[WithdrawalStatusIndicator] ([WithdrawalStatusIndicatorCode])
GO
ALTER TABLE [dbo].[ProductPeriodic] CHECK CONSTRAINT [FK_ProductPeriodic_WithdrawalStatusIndicator]
GO
ALTER TABLE [dbo].[Schedule]  WITH CHECK ADD  CONSTRAINT [FK_Schedule_Batch] FOREIGN KEY([BatchID])
REFERENCES [dbo].[Batch] ([BatchID])
GO
ALTER TABLE [dbo].[Schedule] CHECK CONSTRAINT [FK_Schedule_Batch]
GO
ALTER TABLE [dbo].[Schedule]  WITH CHECK ADD  CONSTRAINT [FK_Schedule_FinancialPeriod_PaymentOnAccount] FOREIGN KEY([PaymentOnAccountForFinancialPeriodID])
REFERENCES [dbo].[FinancialPeriod] ([FinancialPeriodID])
GO
ALTER TABLE [dbo].[Schedule] CHECK CONSTRAINT [FK_Schedule_FinancialPeriod_PaymentOnAccount]
GO
ALTER TABLE [dbo].[ScheduleCharge]  WITH CHECK ADD  CONSTRAINT [FK_ScheduleCharge_Schedule] FOREIGN KEY([ScheduleID])
REFERENCES [dbo].[Schedule] ([ScheduleID])
GO
ALTER TABLE [dbo].[ScheduleCharge] CHECK CONSTRAINT [FK_ScheduleCharge_Schedule]
GO
ALTER TABLE [dbo].[ScheduleCost]  WITH CHECK ADD  CONSTRAINT [FK_ScheduleCost_Schedule] FOREIGN KEY([ScheduleID])
REFERENCES [dbo].[Schedule] ([ScheduleID])
GO
ALTER TABLE [dbo].[ScheduleCost] CHECK CONSTRAINT [FK_ScheduleCost_Schedule]
GO
ALTER TABLE [dbo].[ScheduleDocumentData]  WITH CHECK ADD  CONSTRAINT [FK_ScheduleDocumentData_Schedule] FOREIGN KEY([ScheduleID])
REFERENCES [dbo].[Schedule] ([ScheduleID])
GO
ALTER TABLE [dbo].[ScheduleDocumentData] CHECK CONSTRAINT [FK_ScheduleDocumentData_Schedule]
GO
ALTER TABLE [dbo].[ScheduleExpensiveItem]  WITH CHECK ADD  CONSTRAINT [FK_ScheduleExpensiveItem_Schedule] FOREIGN KEY([ScheduleID])
REFERENCES [dbo].[Schedule] ([ScheduleID])
GO
ALTER TABLE [dbo].[ScheduleExpensiveItem] CHECK CONSTRAINT [FK_ScheduleExpensiveItem_Schedule]
GO
ALTER TABLE [dbo].[ScheduleExpensiveItemSummary]  WITH CHECK ADD  CONSTRAINT [FK_ScheduleExpensiveItemSummary_Schedule] FOREIGN KEY([ScheduleID])
REFERENCES [dbo].[Schedule] ([ScheduleID])
GO
ALTER TABLE [dbo].[ScheduleExpensiveItemSummary] CHECK CONSTRAINT [FK_ScheduleExpensiveItemSummary_Schedule]
GO
ALTER TABLE [dbo].[ScheduleFee]  WITH CHECK ADD  CONSTRAINT [FK_ScheduleFee_Schedule] FOREIGN KEY([ScheduleID])
REFERENCES [dbo].[Schedule] ([ScheduleID])
GO
ALTER TABLE [dbo].[ScheduleFee] CHECK CONSTRAINT [FK_ScheduleFee_Schedule]
GO
ALTER TABLE [dbo].[ScheduleGroupSwitchSummary]  WITH CHECK ADD  CONSTRAINT [FK_ScheduleGroupSwitchSummary_Schedule] FOREIGN KEY([ScheduleID])
REFERENCES [dbo].[Schedule] ([ScheduleID])
GO
ALTER TABLE [dbo].[ScheduleGroupSwitchSummary] CHECK CONSTRAINT [FK_ScheduleGroupSwitchSummary_Schedule]
GO
ALTER TABLE [dbo].[ScheduleGroupSwitchSummary]  WITH CHECK ADD  CONSTRAINT [FK_ScheduleGroupSwitchSummary_ScheduleGroupSwitchDirection] FOREIGN KEY([ScheduleGroupSwitchDirectionID])
REFERENCES [dbo].[ScheduleGroupSwitchDirection] ([ScheduleGroupSwitchDirectionID])
GO
ALTER TABLE [dbo].[ScheduleGroupSwitchSummary] CHECK CONSTRAINT [FK_ScheduleGroupSwitchSummary_ScheduleGroupSwitchDirection]
GO
ALTER TABLE [dbo].[ScheduleLocalPayment]  WITH CHECK ADD  CONSTRAINT [FK_ScheduleLocalPayment_Schedule] FOREIGN KEY([ScheduleID])
REFERENCES [dbo].[Schedule] ([ScheduleID])
GO
ALTER TABLE [dbo].[ScheduleLocalPayment] CHECK CONSTRAINT [FK_ScheduleLocalPayment_Schedule]
GO
ALTER TABLE [dbo].[ScheduleOtherPayment]  WITH CHECK ADD  CONSTRAINT [FK_ScheduleOtherPayment_Schedule] FOREIGN KEY([ScheduleID])
REFERENCES [dbo].[Schedule] ([ScheduleID])
GO
ALTER TABLE [dbo].[ScheduleOtherPayment] CHECK CONSTRAINT [FK_ScheduleOtherPayment_Schedule]
GO
ALTER TABLE [dbo].[ScheduleSpecialItem]  WITH NOCHECK ADD  CONSTRAINT [FK_ScheduleSpecialItem_Schedule] FOREIGN KEY([ScheduleID])
REFERENCES [dbo].[Schedule] ([ScheduleID])
GO
ALTER TABLE [dbo].[ScheduleSpecialItem] NOCHECK CONSTRAINT [FK_ScheduleSpecialItem_Schedule]
GO
ALTER TABLE [dbo].[ScheduleTop10CatM]  WITH NOCHECK ADD  CONSTRAINT [FK_ScheduleTop10CatM_Schedule] FOREIGN KEY([ScheduleID])
REFERENCES [dbo].[Schedule] ([ScheduleID])
GO
ALTER TABLE [dbo].[ScheduleTop10CatM] NOCHECK CONSTRAINT [FK_ScheduleTop10CatM_Schedule]
GO
ALTER TABLE [dbo].[ScheduleTop10ZD]  WITH NOCHECK ADD  CONSTRAINT [FK_ScheduleTop10ZD_Schedule] FOREIGN KEY([ScheduleID])
REFERENCES [dbo].[Schedule] ([ScheduleID])
GO
ALTER TABLE [dbo].[ScheduleTop10ZD] NOCHECK CONSTRAINT [FK_ScheduleTop10ZD_Schedule]
GO
ALTER TABLE [dbo].[Submission]  WITH CHECK ADD  CONSTRAINT [FK_Submission_Batch] FOREIGN KEY([BatchID])
REFERENCES [dbo].[Batch] ([BatchID])
GO
ALTER TABLE [dbo].[Submission] CHECK CONSTRAINT [FK_Submission_Batch]
GO
ALTER TABLE [dbo].[SubmissionImage]  WITH CHECK ADD  CONSTRAINT [FK_SubmissionImage_DocumentImageType] FOREIGN KEY([DocumentImageTypeID])
REFERENCES [dbo].[DocumentImageType] ([DocumentImageTypeID])
GO
ALTER TABLE [dbo].[SubmissionImage] CHECK CONSTRAINT [FK_SubmissionImage_DocumentImageType]
GO
ALTER TABLE [dbo].[SubmissionImage]  WITH CHECK ADD  CONSTRAINT [FK_SubmissionImage_Submission] FOREIGN KEY([BatchID])
REFERENCES [dbo].[Submission] ([BatchID])
GO
ALTER TABLE [dbo].[SubmissionImage] CHECK CONSTRAINT [FK_SubmissionImage_Submission]
GO
ALTER TABLE [dbo].[TariffDiscountDeductionScale]  WITH CHECK ADD  CONSTRAINT [FK_TariffDiscountDeductionScale_FinancialPeriod] FOREIGN KEY([FinancialPeriodID])
REFERENCES [dbo].[FinancialPeriod] ([FinancialPeriodID])
GO
ALTER TABLE [dbo].[TariffDiscountDeductionScale] CHECK CONSTRAINT [FK_TariffDiscountDeductionScale_FinancialPeriod]
GO
ALTER TABLE [dbo].[TariffEstablishmentPaymentScale]  WITH CHECK ADD  CONSTRAINT [FK_TariffEstablishmentPaymentScale_FinancialPeriod] FOREIGN KEY([FinancialPeriodID])
REFERENCES [dbo].[FinancialPeriod] ([FinancialPeriodID])
GO
ALTER TABLE [dbo].[TariffEstablishmentPaymentScale] CHECK CONSTRAINT [FK_TariffEstablishmentPaymentScale_FinancialPeriod]
GO
ALTER TABLE [dbo].[TariffEstablishmentPaymentScale]  WITH CHECK ADD  CONSTRAINT [FK_TariffEstablishmentPaymentScale_Jurisdiction] FOREIGN KEY([JurisdictionID])
REFERENCES [dbo].[Jurisdiction] ([JurisdictionID])
GO
ALTER TABLE [dbo].[TariffEstablishmentPaymentScale] CHECK CONSTRAINT [FK_TariffEstablishmentPaymentScale_Jurisdiction]
GO
ALTER TABLE [dbo].[TariffPracticePayment]  WITH CHECK ADD  CONSTRAINT [FK_TariffPracticePayment_FinancialPeriod] FOREIGN KEY([FinancialPeriodID])
REFERENCES [dbo].[FinancialPeriod] ([FinancialPeriodID])
GO
ALTER TABLE [dbo].[TariffPracticePayment] CHECK CONSTRAINT [FK_TariffPracticePayment_FinancialPeriod]
GO
ALTER TABLE [dbo].[TariffPracticePayment]  WITH CHECK ADD  CONSTRAINT [FK_TariffPracticePayment_Jurisdiction] FOREIGN KEY([JurisdictionID])
REFERENCES [dbo].[Jurisdiction] ([JurisdictionID])
GO
ALTER TABLE [dbo].[TariffPracticePayment] CHECK CONSTRAINT [FK_TariffPracticePayment_Jurisdiction]
GO
ALTER TABLE [dbo].[TariffPracticePaymentItemsDispensed]  WITH CHECK ADD  CONSTRAINT [FK_TariffPracticePaymentItemsDispensed_FinancialPeriod] FOREIGN KEY([FinancialPeriodID])
REFERENCES [dbo].[FinancialPeriod] ([FinancialPeriodID])
GO
ALTER TABLE [dbo].[TariffPracticePaymentItemsDispensed] CHECK CONSTRAINT [FK_TariffPracticePaymentItemsDispensed_FinancialPeriod]
GO
ALTER TABLE [dbo].[TariffTransitionalPayment]  WITH CHECK ADD  CONSTRAINT [FK_TariffTransitionalPayment_FinancialPeriod] FOREIGN KEY([FinancialPeriodID])
REFERENCES [dbo].[FinancialPeriod] ([FinancialPeriodID])
GO
ALTER TABLE [dbo].[TariffTransitionalPayment] CHECK CONSTRAINT [FK_TariffTransitionalPayment_FinancialPeriod]
GO
ALTER TABLE [dbo].[TariffTransitionalPayment]  WITH CHECK ADD  CONSTRAINT [FK_TariffTransitionalPayment_Jurisdiction] FOREIGN KEY([JurisdictionID])
REFERENCES [dbo].[Jurisdiction] ([JurisdictionID])
GO
ALTER TABLE [dbo].[TariffTransitionalPayment] CHECK CONSTRAINT [FK_TariffTransitionalPayment_Jurisdiction]
GO
ALTER TABLE [dbo].[UserActivity]  WITH CHECK ADD  CONSTRAINT [FK_UserActivity_Activity] FOREIGN KEY([ActivityID])
REFERENCES [dbo].[Activity] ([ActivityID])
GO
ALTER TABLE [dbo].[UserActivity] CHECK CONSTRAINT [FK_UserActivity_Activity]
GO
ALTER TABLE [dbo].[UserActivity]  WITH CHECK ADD  CONSTRAINT [FK_UserActivity_Batch] FOREIGN KEY([BatchID])
REFERENCES [dbo].[Batch] ([BatchID])
GO
ALTER TABLE [dbo].[UserActivity] CHECK CONSTRAINT [FK_UserActivity_Batch]
GO
ALTER TABLE [dbo].[UserActivity]  WITH CHECK ADD  CONSTRAINT [FK_UserActivity_Document] FOREIGN KEY([FormID])
REFERENCES [dbo].[Document] ([DocumentID])
GO
ALTER TABLE [dbo].[UserActivity] CHECK CONSTRAINT [FK_UserActivity_Document]
GO
ALTER TABLE [dbo].[UserActivity]  WITH CHECK ADD  CONSTRAINT [FK_UserActivity_Item] FOREIGN KEY([ItemID])
REFERENCES [dbo].[Item] ([ItemID])
GO
ALTER TABLE [dbo].[UserActivity] CHECK CONSTRAINT [FK_UserActivity_Item]
GO
ALTER TABLE [dbo].[UserActivity]  WITH CHECK ADD  CONSTRAINT [FK_UserActivity_NewItemVersion] FOREIGN KEY([NewItemVersionID])
REFERENCES [dbo].[ItemVersion] ([ItemVersionID])
GO
ALTER TABLE [dbo].[UserActivity] CHECK CONSTRAINT [FK_UserActivity_NewItemVersion]
GO
ALTER TABLE [dbo].[UserActivity]  WITH CHECK ADD  CONSTRAINT [FK_UserActivity_OldItemVersion] FOREIGN KEY([OldItemVersionID])
REFERENCES [dbo].[ItemVersion] ([ItemVersionID])
GO
ALTER TABLE [dbo].[UserActivity] CHECK CONSTRAINT [FK_UserActivity_OldItemVersion]
GO
ALTER TABLE [dbo].[UserActivity]  WITH CHECK ADD  CONSTRAINT [FK_UserActivity_UserProfile] FOREIGN KEY([UserProfileID])
REFERENCES [dbo].[UserProfile] ([UserProfileID])
GO
ALTER TABLE [dbo].[UserActivity] CHECK CONSTRAINT [FK_UserActivity_UserProfile]
GO
ALTER TABLE [dmd].[ActualCombinationPackContent]  WITH CHECK ADD  CONSTRAINT [FK_ActualCombinationPackContent_ActualMedicinalProductPack] FOREIGN KEY([ActualMedicinalProductPackId])
REFERENCES [dmd].[ActualMedicinalProductPack] ([ActualMedicinalProductPackId])
GO
ALTER TABLE [dmd].[ActualCombinationPackContent] CHECK CONSTRAINT [FK_ActualCombinationPackContent_ActualMedicinalProductPack]
GO
ALTER TABLE [dmd].[ActualCombinationPackContent]  WITH CHECK ADD  CONSTRAINT [FK_ActualCombinationPackContent_ConstituentActualMedicinalProductPack] FOREIGN KEY([ConstituentActualProductPackId])
REFERENCES [dmd].[ActualMedicinalProductPack] ([ActualMedicinalProductPackId])
GO
ALTER TABLE [dmd].[ActualCombinationPackContent] CHECK CONSTRAINT [FK_ActualCombinationPackContent_ConstituentActualMedicinalProductPack]
GO
ALTER TABLE [dmd].[ActualMedicinalProduct]  WITH CHECK ADD  CONSTRAINT [FK_ActualMedicinalProduct_ApplianceProductInformation] FOREIGN KEY([ApplianceProductInformationId])
REFERENCES [dmd].[ApplianceProductInformation] ([ActualMedicinalProductId])
GO
ALTER TABLE [dmd].[ActualMedicinalProduct] CHECK CONSTRAINT [FK_ActualMedicinalProduct_ApplianceProductInformation]
GO
ALTER TABLE [dmd].[ActualMedicinalProduct]  WITH CHECK ADD  CONSTRAINT [FK_ActualMedicinalProduct_Flavour] FOREIGN KEY([FlavourId])
REFERENCES [dmd].[Flavour] ([FlavourId])
GO
ALTER TABLE [dmd].[ActualMedicinalProduct] CHECK CONSTRAINT [FK_ActualMedicinalProduct_Flavour]
GO
ALTER TABLE [dmd].[ActualMedicinalProduct]  WITH CHECK ADD  CONSTRAINT [FK_ActualMedicinalProduct_LicensingAuthority] FOREIGN KEY([CurrentLicensingAuthorityId])
REFERENCES [dmd].[LicensingAuthority] ([LicensingAuthorityId])
GO
ALTER TABLE [dmd].[ActualMedicinalProduct] CHECK CONSTRAINT [FK_ActualMedicinalProduct_LicensingAuthority]
GO
ALTER TABLE [dmd].[ActualMedicinalProduct]  WITH CHECK ADD  CONSTRAINT [FK_ActualMedicinalProduct_LicensingAuthorityChangeReason] FOREIGN KEY([LicensingAuthorityChangeReasonId])
REFERENCES [dmd].[LicensingAuthorityChangeReason] ([LicensingAuthorityChangeReasonId])
GO
ALTER TABLE [dmd].[ActualMedicinalProduct] CHECK CONSTRAINT [FK_ActualMedicinalProduct_LicensingAuthorityChangeReason]
GO
ALTER TABLE [dmd].[ActualMedicinalProduct]  WITH CHECK ADD  CONSTRAINT [FK_ActualMedicinalProduct_PreviousLicensingAuthority] FOREIGN KEY([PreviousLicensingAuthorityId])
REFERENCES [dmd].[LicensingAuthority] ([LicensingAuthorityId])
GO
ALTER TABLE [dmd].[ActualMedicinalProduct] CHECK CONSTRAINT [FK_ActualMedicinalProduct_PreviousLicensingAuthority]
GO
ALTER TABLE [dmd].[ActualMedicinalProduct]  WITH CHECK ADD  CONSTRAINT [FK_ActualMedicinalProduct_RestrictionOnAvailability1] FOREIGN KEY([RestrictionOnAvailabilityId])
REFERENCES [dmd].[RestrictionOnAvailability] ([RestrictionOnAvailabilityId])
GO
ALTER TABLE [dmd].[ActualMedicinalProduct] CHECK CONSTRAINT [FK_ActualMedicinalProduct_RestrictionOnAvailability1]
GO
ALTER TABLE [dmd].[ActualMedicinalProduct]  WITH CHECK ADD  CONSTRAINT [FK_ActualMedicinalProduct_Supplier] FOREIGN KEY([SupplierId])
REFERENCES [dmd].[Supplier] ([SupplierId])
GO
ALTER TABLE [dmd].[ActualMedicinalProduct] CHECK CONSTRAINT [FK_ActualMedicinalProduct_Supplier]
GO
ALTER TABLE [dmd].[ActualMedicinalProduct]  WITH CHECK ADD  CONSTRAINT [FK_ActualMedicinalProduct_VirtualMedicinalProduct] FOREIGN KEY([VirtualMedicinalProductId])
REFERENCES [dmd].[VirtualMedicinalProduct] ([VirtualMedicinalProductId])
GO
ALTER TABLE [dmd].[ActualMedicinalProduct] CHECK CONSTRAINT [FK_ActualMedicinalProduct_VirtualMedicinalProduct]
GO
ALTER TABLE [dmd].[ActualMedicinalProductPack]  WITH CHECK ADD  CONSTRAINT [FK_ActualMedicinalProductPack_ActualMedicinalProduct2] FOREIGN KEY([ActualMedicinalProductId])
REFERENCES [dmd].[ActualMedicinalProduct] ([ActualMedicinalProductId])
GO
ALTER TABLE [dmd].[ActualMedicinalProductPack] CHECK CONSTRAINT [FK_ActualMedicinalProductPack_ActualMedicinalProduct2]
GO
ALTER TABLE [dmd].[ActualMedicinalProductPack]  WITH CHECK ADD  CONSTRAINT [FK_ActualMedicinalProductPack_CombinationPackIndicator] FOREIGN KEY([CombinationPackIndicatorId])
REFERENCES [dmd].[CombinationPackIndicator] ([CombinationPackIndicatorId])
GO
ALTER TABLE [dmd].[ActualMedicinalProductPack] CHECK CONSTRAINT [FK_ActualMedicinalProductPack_CombinationPackIndicator]
GO
ALTER TABLE [dmd].[ActualMedicinalProductPack]  WITH CHECK ADD  CONSTRAINT [FK_ActualMedicinalProductPack_DiscontinuedFlag] FOREIGN KEY([DiscontinuedFlagId])
REFERENCES [dmd].[DiscontinuedFlag] ([DiscontinuedFlagId])
GO
ALTER TABLE [dmd].[ActualMedicinalProductPack] CHECK CONSTRAINT [FK_ActualMedicinalProductPack_DiscontinuedFlag]
GO
ALTER TABLE [dmd].[ActualMedicinalProductPack]  WITH CHECK ADD  CONSTRAINT [FK_ActualMedicinalProductPack_LegalCategory] FOREIGN KEY([LegalCategoryId])
REFERENCES [dmd].[LegalCategory] ([LegalCategoryId])
GO
ALTER TABLE [dmd].[ActualMedicinalProductPack] CHECK CONSTRAINT [FK_ActualMedicinalProductPack_LegalCategory]
GO
ALTER TABLE [dmd].[ActualMedicinalProductPack]  WITH CHECK ADD  CONSTRAINT [FK_ActualMedicinalProductPack_VirtualMedicinalProductPack] FOREIGN KEY([VirtualMedicinalProductPackId])
REFERENCES [dmd].[VirtualMedicinalProductPack] ([VirtualMedicinalProductPackId])
GO
ALTER TABLE [dmd].[ActualMedicinalProductPack] CHECK CONSTRAINT [FK_ActualMedicinalProductPack_VirtualMedicinalProductPack]
GO
ALTER TABLE [dmd].[ActualProductExcipient]  WITH CHECK ADD  CONSTRAINT [FK_ActualProductExcipient_ActualMedicinalProduct] FOREIGN KEY([ActualMedicinalProductId])
REFERENCES [dmd].[ActualMedicinalProduct] ([ActualMedicinalProductId])
GO
ALTER TABLE [dmd].[ActualProductExcipient] CHECK CONSTRAINT [FK_ActualProductExcipient_ActualMedicinalProduct]
GO
ALTER TABLE [dmd].[ActualProductExcipient]  WITH CHECK ADD  CONSTRAINT [FK_ActualProductExcipient_IngredientSubstance] FOREIGN KEY([IngredientSubstanceId])
REFERENCES [dmd].[IngredientSubstance] ([IngredientSubstanceId])
GO
ALTER TABLE [dmd].[ActualProductExcipient] CHECK CONSTRAINT [FK_ActualProductExcipient_IngredientSubstance]
GO
ALTER TABLE [dmd].[ActualProductExcipient]  WITH CHECK ADD  CONSTRAINT [FK_ActualProductExcipient_UnitOfMeasurement] FOREIGN KEY([PharmaceuticalStrengthUnitOfMeasurementId])
REFERENCES [dmd].[UnitOfMeasurement] ([UnitOfMeasurementId])
GO
ALTER TABLE [dmd].[ActualProductExcipient] CHECK CONSTRAINT [FK_ActualProductExcipient_UnitOfMeasurement]
GO
ALTER TABLE [dmd].[AppliancePackInformation]  WITH CHECK ADD  CONSTRAINT [FK_AppliancePackInformation_ActualMedicinalProductPack] FOREIGN KEY([ActualMedicinalProductPackId])
REFERENCES [dmd].[ActualMedicinalProductPack] ([ActualMedicinalProductPackId])
GO
ALTER TABLE [dmd].[AppliancePackInformation] CHECK CONSTRAINT [FK_AppliancePackInformation_ActualMedicinalProductPack]
GO
ALTER TABLE [dmd].[AppliancePackInformation]  WITH CHECK ADD  CONSTRAINT [FK_AppliancePackInformation_ApplianceReimbursementStatus] FOREIGN KEY([ReimbursementStatusId])
REFERENCES [dmd].[ReimbursementStatus] ([ReimbursementStatusId])
GO
ALTER TABLE [dmd].[AppliancePackInformation] CHECK CONSTRAINT [FK_AppliancePackInformation_ApplianceReimbursementStatus]
GO
ALTER TABLE [dmd].[AppliancePackInformation]  WITH CHECK ADD  CONSTRAINT [FK_AppliancePackInformation_PreviousApplianceReimbursementStatus] FOREIGN KEY([PreviousReimbursementStatusId])
REFERENCES [dmd].[ReimbursementStatus] ([ReimbursementStatusId])
GO
ALTER TABLE [dmd].[AppliancePackInformation] CHECK CONSTRAINT [FK_AppliancePackInformation_PreviousApplianceReimbursementStatus]
GO
ALTER TABLE [dmd].[ApplianceProductInformation]  WITH CHECK ADD  CONSTRAINT [FK_ApplianceProductInformation_Colour] FOREIGN KEY([ColourId])
REFERENCES [dmd].[Colour] ([ColourId])
GO
ALTER TABLE [dmd].[ApplianceProductInformation] CHECK CONSTRAINT [FK_ApplianceProductInformation_Colour]
GO
ALTER TABLE [dmd].[CombinationPackContent]  WITH CHECK ADD  CONSTRAINT [FK_CombinationPackContent_ConstituentVirtualMedicinalProductPack] FOREIGN KEY([ConstituentVirtualProductPackId])
REFERENCES [dmd].[VirtualMedicinalProductPack] ([VirtualMedicinalProductPackId])
GO
ALTER TABLE [dmd].[CombinationPackContent] CHECK CONSTRAINT [FK_CombinationPackContent_ConstituentVirtualMedicinalProductPack]
GO
ALTER TABLE [dmd].[CombinationPackContent]  WITH CHECK ADD  CONSTRAINT [FK_CombinationPackContent_VirtualMedicinalProductPack] FOREIGN KEY([VirtualMedicinalProductPackId])
REFERENCES [dmd].[VirtualMedicinalProductPack] ([VirtualMedicinalProductPackId])
GO
ALTER TABLE [dmd].[CombinationPackContent] CHECK CONSTRAINT [FK_CombinationPackContent_VirtualMedicinalProductPack]
GO
ALTER TABLE [dmd].[ControlledDrugPrescribingInformation]  WITH CHECK ADD  CONSTRAINT [FK_ControlledDrugPrescribingInformation_ControlledDrugCategory] FOREIGN KEY([ControlledDrugCategoryId])
REFERENCES [dmd].[ControlledDrugCategory] ([ControlledDrugCategoryId])
GO
ALTER TABLE [dmd].[ControlledDrugPrescribingInformation] CHECK CONSTRAINT [FK_ControlledDrugPrescribingInformation_ControlledDrugCategory]
GO
ALTER TABLE [dmd].[ControlledDrugPrescribingInformation]  WITH CHECK ADD  CONSTRAINT [FK_ControlledDrugPrescribingInformation_PreviousControlledDrugCategory] FOREIGN KEY([PreviousControlledDrugCategoryId])
REFERENCES [dmd].[ControlledDrugCategory] ([ControlledDrugCategoryId])
GO
ALTER TABLE [dmd].[ControlledDrugPrescribingInformation] CHECK CONSTRAINT [FK_ControlledDrugPrescribingInformation_PreviousControlledDrugCategory]
GO
ALTER TABLE [dmd].[ControlledDrugPrescribingInformation]  WITH CHECK ADD  CONSTRAINT [FK_ControlledDrugPrescribingInformation_VirtualMedicinalProduct] FOREIGN KEY([VirtualMedicinalProductId])
REFERENCES [dmd].[VirtualMedicinalProduct] ([VirtualMedicinalProductId])
GO
ALTER TABLE [dmd].[ControlledDrugPrescribingInformation] CHECK CONSTRAINT [FK_ControlledDrugPrescribingInformation_VirtualMedicinalProduct]
GO
ALTER TABLE [dmd].[DrugTariffCategoryInformation]  WITH CHECK ADD  CONSTRAINT [FK_DrugTariffCategoryInformation_PaymentCategory] FOREIGN KEY([PaymentCategoryId])
REFERENCES [dmd].[PaymentCategory] ([PaymentCategoryId])
GO
ALTER TABLE [dmd].[DrugTariffCategoryInformation] CHECK CONSTRAINT [FK_DrugTariffCategoryInformation_PaymentCategory]
GO
ALTER TABLE [dmd].[DrugTariffCategoryInformation]  WITH CHECK ADD  CONSTRAINT [FK_DrugTariffCategoryInformation_VirtualMedicinalProductPack] FOREIGN KEY([VirtualMedicinalProductPackId])
REFERENCES [dmd].[VirtualMedicinalProductPack] ([VirtualMedicinalProductPackId])
GO
ALTER TABLE [dmd].[DrugTariffCategoryInformation] CHECK CONSTRAINT [FK_DrugTariffCategoryInformation_VirtualMedicinalProductPack]
GO
ALTER TABLE [dmd].[FormInformation]  WITH CHECK ADD  CONSTRAINT [FK_FormInformation_Form] FOREIGN KEY([FormId])
REFERENCES [dmd].[Form] ([FormId])
GO
ALTER TABLE [dmd].[FormInformation] CHECK CONSTRAINT [FK_FormInformation_Form]
GO
ALTER TABLE [dmd].[FormInformation]  WITH CHECK ADD  CONSTRAINT [FK_FormInformation_VirtualMedicinalProduct] FOREIGN KEY([VirtualMedicinalProductId])
REFERENCES [dmd].[VirtualMedicinalProduct] ([VirtualMedicinalProductId])
GO
ALTER TABLE [dmd].[FormInformation] CHECK CONSTRAINT [FK_FormInformation_VirtualMedicinalProduct]
GO
ALTER TABLE [dmd].[LicensedRoute]  WITH CHECK ADD  CONSTRAINT [FK_LicensedRoute_ActualMedicinalProduct] FOREIGN KEY([ActualMedicinalProductId])
REFERENCES [dmd].[ActualMedicinalProduct] ([ActualMedicinalProductId])
GO
ALTER TABLE [dmd].[LicensedRoute] CHECK CONSTRAINT [FK_LicensedRoute_ActualMedicinalProduct]
GO
ALTER TABLE [dmd].[LicensedRoute]  WITH NOCHECK ADD  CONSTRAINT [FK_LicensedRoute_Route] FOREIGN KEY([RouteId])
REFERENCES [dmd].[Route] ([RouteId])
GO
ALTER TABLE [dmd].[LicensedRoute] CHECK CONSTRAINT [FK_LicensedRoute_Route]
GO
ALTER TABLE [dmd].[MedicinalProductPrice]  WITH CHECK ADD  CONSTRAINT [FK_MedicinalProductPrice_ActualMedicinalProductPack] FOREIGN KEY([ActualMedicinalProductPackId])
REFERENCES [dmd].[ActualMedicinalProductPack] ([ActualMedicinalProductPackId])
GO
ALTER TABLE [dmd].[MedicinalProductPrice] CHECK CONSTRAINT [FK_MedicinalProductPrice_ActualMedicinalProductPack]
GO
ALTER TABLE [dmd].[MedicinalProductPrice]  WITH CHECK ADD  CONSTRAINT [FK_MedicinalProductPrice_PriceBasisFlag] FOREIGN KEY([PriceBasisFlagId])
REFERENCES [dmd].[PriceBasisFlag] ([PriceBasisFlagId])
GO
ALTER TABLE [dmd].[MedicinalProductPrice] CHECK CONSTRAINT [FK_MedicinalProductPrice_PriceBasisFlag]
GO
ALTER TABLE [dmd].[OntologyFormAndRouteInformation]  WITH CHECK ADD  CONSTRAINT [FK_OntologyFormAndRouteInformation_VirtualMedicinalProduct] FOREIGN KEY([VirtualMedicinalProductId])
REFERENCES [dmd].[VirtualMedicinalProduct] ([VirtualMedicinalProductId])
GO
ALTER TABLE [dmd].[OntologyFormAndRouteInformation] CHECK CONSTRAINT [FK_OntologyFormAndRouteInformation_VirtualMedicinalProduct]
GO
ALTER TABLE [dmd].[OntologyFormAndRouteInformation]  WITH CHECK ADD  CONSTRAINT [FK_OntologyFormandRouteInformation_VirtualMedicinalProductFormAndRoute] FOREIGN KEY([VirtualMedicinalProductFormAndRouteId])
REFERENCES [dmd].[VirtualMedicinalProductFormAndRoute] ([VirtualMedicinalProductFormAndRouteId])
GO
ALTER TABLE [dmd].[OntologyFormAndRouteInformation] CHECK CONSTRAINT [FK_OntologyFormandRouteInformation_VirtualMedicinalProductFormAndRoute]
GO
ALTER TABLE [dmd].[ProductPrescribingInformation]  WITH CHECK ADD  CONSTRAINT [FK_ProductPrescribingInformation_ActualMedicinalProductPack] FOREIGN KEY([ActualMedicinalProductPackId])
REFERENCES [dmd].[ActualMedicinalProductPack] ([ActualMedicinalProductPackId])
GO
ALTER TABLE [dmd].[ProductPrescribingInformation] CHECK CONSTRAINT [FK_ProductPrescribingInformation_ActualMedicinalProductPack]
GO
ALTER TABLE [dmd].[ReimbursementInformation]  WITH CHECK ADD  CONSTRAINT [FK_ReimbursementInformation_ActualMedicinalProductPack] FOREIGN KEY([ActualMedicinalProductPackId])
REFERENCES [dmd].[ActualMedicinalProductPack] ([ActualMedicinalProductPackId])
GO
ALTER TABLE [dmd].[ReimbursementInformation] CHECK CONSTRAINT [FK_ReimbursementInformation_ActualMedicinalProductPack]
GO
ALTER TABLE [dmd].[ReimbursementInformation]  WITH CHECK ADD  CONSTRAINT [FK_ReimbursementInformation_DiscountNotDeductedIndicator] FOREIGN KEY([DiscountNotDeductedIndicatorId])
REFERENCES [dmd].[DiscountNotDeductedIndicator] ([DiscountNotDeductedIndicatorId])
GO
ALTER TABLE [dmd].[ReimbursementInformation] CHECK CONSTRAINT [FK_ReimbursementInformation_DiscountNotDeductedIndicator]
GO
ALTER TABLE [dmd].[ReimbursementInformation]  WITH CHECK ADD  CONSTRAINT [FK_ReimbursementInformation_SpecialContainerIndicator] FOREIGN KEY([SpecialContainerIndicatorId])
REFERENCES [dmd].[SpecialContainerIndicator] ([SpecialContainerIndicatorId])
GO
ALTER TABLE [dmd].[ReimbursementInformation] CHECK CONSTRAINT [FK_ReimbursementInformation_SpecialContainerIndicator]
GO
ALTER TABLE [dmd].[RouteInformation]  WITH CHECK ADD  CONSTRAINT [FK_RouteInformation_VirtualMedicinalProduct] FOREIGN KEY([VirtualMedicinalProductId])
REFERENCES [dmd].[VirtualMedicinalProduct] ([VirtualMedicinalProductId])
GO
ALTER TABLE [dmd].[RouteInformation] CHECK CONSTRAINT [FK_RouteInformation_VirtualMedicinalProduct]
GO
ALTER TABLE [dmd].[VirtualMedicinalProduct]  WITH CHECK ADD  CONSTRAINT [FK_VirtualMedicinalProduct_BasisOfName] FOREIGN KEY([BasisofPreviousNameId])
REFERENCES [dmd].[BasisOfName] ([BasisOfNameId])
GO
ALTER TABLE [dmd].[VirtualMedicinalProduct] CHECK CONSTRAINT [FK_VirtualMedicinalProduct_BasisOfName]
GO
ALTER TABLE [dmd].[VirtualMedicinalProduct]  WITH CHECK ADD  CONSTRAINT [FK_VirtualMedicinalProduct_BasisOfPreferredName] FOREIGN KEY([BasisofPreferredNameId])
REFERENCES [dmd].[BasisOfName] ([BasisOfNameId])
GO
ALTER TABLE [dmd].[VirtualMedicinalProduct] CHECK CONSTRAINT [FK_VirtualMedicinalProduct_BasisOfPreferredName]
GO
ALTER TABLE [dmd].[VirtualMedicinalProduct]  WITH CHECK ADD  CONSTRAINT [FK_VirtualMedicinalProduct_CombinationProductIndicator] FOREIGN KEY([CombinationProductIndicatorId])
REFERENCES [dmd].[CombinationProductIndicator] ([CombinationProductIndicatorId])
GO
ALTER TABLE [dmd].[VirtualMedicinalProduct] CHECK CONSTRAINT [FK_VirtualMedicinalProduct_CombinationProductIndicator]
GO
ALTER TABLE [dmd].[VirtualMedicinalProduct]  WITH CHECK ADD  CONSTRAINT [FK_VirtualMedicinalProduct_DoseFormIndicator] FOREIGN KEY([DoseFormIndicatorId])
REFERENCES [dmd].[DoseFormIndicator] ([DoseFormIndicatorId])
GO
ALTER TABLE [dmd].[VirtualMedicinalProduct] CHECK CONSTRAINT [FK_VirtualMedicinalProduct_DoseFormIndicator]
GO
ALTER TABLE [dmd].[VirtualMedicinalProduct]  WITH CHECK ADD  CONSTRAINT [FK_VirtualMedicinalProduct_NonAvailabilityIndicator] FOREIGN KEY([NonAvailabilityIndicatorId])
REFERENCES [dmd].[NonAvailabilityIndicator] ([NonAvailabilityIndicatorId])
GO
ALTER TABLE [dmd].[VirtualMedicinalProduct] CHECK CONSTRAINT [FK_VirtualMedicinalProduct_NonAvailabilityIndicator]
GO
ALTER TABLE [dmd].[VirtualMedicinalProduct]  WITH CHECK ADD  CONSTRAINT [FK_VirtualMedicinalProduct_ReasonForNameChange] FOREIGN KEY([ReasonforNameChangeId])
REFERENCES [dmd].[ReasonForNameChange] ([ReasonforNameChangeId])
GO
ALTER TABLE [dmd].[VirtualMedicinalProduct] CHECK CONSTRAINT [FK_VirtualMedicinalProduct_ReasonForNameChange]
GO
ALTER TABLE [dmd].[VirtualMedicinalProduct]  WITH CHECK ADD  CONSTRAINT [FK_VirtualMedicinalProduct_UnitDoseFormUnits] FOREIGN KEY([UnitDoseFormUnits])
REFERENCES [dmd].[UnitOfMeasurement] ([UnitOfMeasurementId])
GO
ALTER TABLE [dmd].[VirtualMedicinalProduct] CHECK CONSTRAINT [FK_VirtualMedicinalProduct_UnitDoseFormUnits]
GO
ALTER TABLE [dmd].[VirtualMedicinalProduct]  WITH CHECK ADD  CONSTRAINT [FK_VirtualMedicinalProduct_UnitofMeasurement] FOREIGN KEY([UnitDoseFormUnits])
REFERENCES [dmd].[UnitOfMeasurement] ([UnitOfMeasurementId])
GO
ALTER TABLE [dmd].[VirtualMedicinalProduct] CHECK CONSTRAINT [FK_VirtualMedicinalProduct_UnitofMeasurement]
GO
ALTER TABLE [dmd].[VirtualMedicinalProduct]  WITH CHECK ADD  CONSTRAINT [FK_VirtualMedicinalProduct_VirtualMedicinalProductPrescribingStatus] FOREIGN KEY([VirtualMedicinalProductPrescribingStatusId])
REFERENCES [dmd].[VirtualMedicinalProductPrescribingStatus] ([VirtualMedicinalProductPrescribingStatusId])
GO
ALTER TABLE [dmd].[VirtualMedicinalProduct] CHECK CONSTRAINT [FK_VirtualMedicinalProduct_VirtualMedicinalProductPrescribingStatus]
GO
ALTER TABLE [dmd].[VirtualMedicinalProduct]  WITH CHECK ADD  CONSTRAINT [FK_VirtualMedicinalProduct_VirtualTherapeuticMoiety] FOREIGN KEY([VirtualTherapeuticMoietyId])
REFERENCES [dmd].[VirtualTherapeuticMoiety] ([VirtualTherapeuticMoietyId])
GO
ALTER TABLE [dmd].[VirtualMedicinalProduct] CHECK CONSTRAINT [FK_VirtualMedicinalProduct_VirtualTherapeuticMoiety]
GO
ALTER TABLE [dmd].[VirtualMedicinalProductPack]  WITH CHECK ADD  CONSTRAINT [FK_VirtualMedicinalProductPack_CombinationPackIndicator] FOREIGN KEY([CombinationPackIndicatorId])
REFERENCES [dmd].[CombinationPackIndicator] ([CombinationPackIndicatorId])
GO
ALTER TABLE [dmd].[VirtualMedicinalProductPack] CHECK CONSTRAINT [FK_VirtualMedicinalProductPack_CombinationPackIndicator]
GO
ALTER TABLE [dmd].[VirtualMedicinalProductPack]  WITH CHECK ADD  CONSTRAINT [FK_VirtualMedicinalProductPack_UnitofMeasurement] FOREIGN KEY([VirtualMedicinalProductPackUnitOfMeasurementId])
REFERENCES [dmd].[UnitOfMeasurement] ([UnitOfMeasurementId])
GO
ALTER TABLE [dmd].[VirtualMedicinalProductPack] CHECK CONSTRAINT [FK_VirtualMedicinalProductPack_UnitofMeasurement]
GO
ALTER TABLE [dmd].[VirtualMedicinalProductPack]  WITH CHECK ADD  CONSTRAINT [FK_VirtualMedicinalProductPack_VirtualMedicinalProduct] FOREIGN KEY([VirtualMedicinalProductId])
REFERENCES [dmd].[VirtualMedicinalProduct] ([VirtualMedicinalProductId])
GO
ALTER TABLE [dmd].[VirtualMedicinalProductPack] CHECK CONSTRAINT [FK_VirtualMedicinalProductPack_VirtualMedicinalProduct]
GO
ALTER TABLE [dmd].[VirtualProductIngredient]  WITH CHECK ADD  CONSTRAINT [FK_VirtualProductIngredient_BasisOfPharmaceuticalStrength] FOREIGN KEY([BasisofPharmaceuticalStrengthId])
REFERENCES [dmd].[BasisOfPharmaceuticalStrength] ([BasisOfPharmaceuticalStrengthId])
GO
ALTER TABLE [dmd].[VirtualProductIngredient] CHECK CONSTRAINT [FK_VirtualProductIngredient_BasisOfPharmaceuticalStrength]
GO
ALTER TABLE [dmd].[VirtualProductIngredient]  WITH CHECK ADD  CONSTRAINT [FK_VirtualProductIngredient_IngrediantSubstance] FOREIGN KEY([IngredientSubstanceId])
REFERENCES [dmd].[IngredientSubstance] ([IngredientSubstanceId])
GO
ALTER TABLE [dmd].[VirtualProductIngredient] CHECK CONSTRAINT [FK_VirtualProductIngredient_IngrediantSubstance]
GO
ALTER TABLE [dmd].[VirtualProductIngredient]  WITH CHECK ADD  CONSTRAINT [FK_VirtualProductIngredient_StrengthValueDenominatorUnitofMeasurement] FOREIGN KEY([StrengthValueDenominatorUnitOfMeasurementId])
REFERENCES [dmd].[UnitOfMeasurement] ([UnitOfMeasurementId])
GO
ALTER TABLE [dmd].[VirtualProductIngredient] CHECK CONSTRAINT [FK_VirtualProductIngredient_StrengthValueDenominatorUnitofMeasurement]
GO
ALTER TABLE [dmd].[VirtualProductIngredient]  WITH CHECK ADD  CONSTRAINT [FK_VirtualProductIngredient_StrengthValueNumeratorUnitofMeasurement] FOREIGN KEY([StrengthValueNumeratorUnitOfMeasurementId])
REFERENCES [dmd].[UnitOfMeasurement] ([UnitOfMeasurementId])
GO
ALTER TABLE [dmd].[VirtualProductIngredient] CHECK CONSTRAINT [FK_VirtualProductIngredient_StrengthValueNumeratorUnitofMeasurement]
GO
ALTER TABLE [dmd].[VirtualProductIngredient]  WITH CHECK ADD  CONSTRAINT [FK_VirtualProductIngredient_VirtualMedicinalProduct] FOREIGN KEY([VirtualMedicinalProductId])
REFERENCES [dmd].[VirtualMedicinalProduct] ([VirtualMedicinalProductId])
GO
ALTER TABLE [dmd].[VirtualProductIngredient] CHECK CONSTRAINT [FK_VirtualProductIngredient_VirtualMedicinalProduct]
GO
/****** Object:  StoredProcedure [dbo].[AdvisoryNotesReport_Header]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AdvisoryNotesReport_Header]
	@batchID bigint

as

	set nocount on;
	set transaction isolation level read uncommitted;

	select b.BatchID, 
		fp.FinancialYear, fp.FinancialMonth,
		c.Code as ContractorCode, c.Name as ContractorName,
		lpc.Code as LpcCode, lpc.Name as LpcName
	from dbo.Batch b
		inner join dbo.Contractor c on b.ContractorID = c.ContractorID
		inner join dbo.FinancialPeriod fp on b.FinancialPeriodID = fp.FinancialPeriodID
		left join dbo.LocalPharmaceuticalCommittee lpc on b.LocalPharmaceuticalCommitteeID = lpc.LocalPharmaceuticalCommitteeID
	where b.BatchID = @batchID;

GO
/****** Object:  StoredProcedure [dbo].[AdvisoryNotesReport_ItemAdvisories]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AdvisoryNotesReport_ItemAdvisories]
	@batchID bigint

as

	set nocount on;
	set transaction isolation level read uncommitted;

	select ag.DisplaySequence, ag.Header, ag.Description as GroupDescription,
		a.Name, a.Description, a.IsDetailedListRequired, a.ShowAtBottomOfGroup,
		d.DocumentNumber, i.Sequence, i.SubSequence,
		ia.ProductPrescribed, ia.DispenserEndorsements, ia.ProductPaid, ia.Comment
	from dbo.Document d
		inner join dbo.Item i
			inner join dbo.ItemAdvisory ia
				inner join dbo.Advisory a 
					inner join dbo.AdvisoryGroup ag on a.AdvisoryGroupID = ag.AdvisoryGroupID
				on ia.AdvisoryID = a.AdvisoryID
			on i.ItemID = ia.ItemID
			left join dbo.ItemVersion oiv on i.OriginalItemVersionID = oiv.ItemVersionID
			left join dbo.ItemVersion civ on i.CurrentItemVersionID = civ.ItemVersionID
		on d.DocumentID = i.DocumentID
	where d.BatchID = @batchID
	order by ag.DisplaySequence, ag.Header, a.Name, d.DocumentNumber, i.Sequence, i.SubSequence;

GO
/****** Object:  StoredProcedure [dbo].[AuditorAccuracyReport_Details]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AuditorAccuracyReport_Details]
	@frDate date = '01 January 2013',
	@toDate date = '31 December 2013',
	@auditTypes varchar(max) = '5, 7',
	@userProfileID integer = 7

as

	-- See TFS7561

	set nocount on;
	set transaction isolation level read uncommitted;

	declare @commaIndex integer = -1;

	-- Split the audit types into a table...

	declare @auditTypeID integer;
	declare @auditType table (
		AuditTypeID bigint,
		primary key (AuditTypeID)
	);

	set @auditTypes = ltrim(rtrim(@auditTypes));
	while (len(@auditTypes) > 0) begin
		set @commaIndex = charindex(',', @auditTypes)
		if (@commaIndex > 0) begin
			set @auditTypeID = substring(@auditTypes, 1, @commaIndex - 1);
			insert into @auditType (AuditTypeID) values (@auditTypeID);
			set @auditTypes = substring(@auditTypes, @commaIndex + 1, len(@auditTypes) - (@commaIndex));
		end else begin
			set @auditTypeID = @auditTypes;
			insert into @auditType (AuditTypeID) values (@auditTypeID);
			set @auditTypes = '';
		end
		set @auditTypes = ltrim(rtrim(@auditTypes));
	end;

	-- Find all batches that were last "2nd check completed" within the date range...

	declare @batches table (
		BatchID bigint primary key,
		FinancialPeriodID integer,
		ContractorCode varchar(10)
	);

	with secondCheckCompleted as (
		select BatchID, Completed = max(cast(DateTime as date))
		from BatchStatusHistory
		where BatchStatusID = 490
		group by BatchID
	)
	insert into @batches
	select b.BatchID, b.FinancialPeriodID, c.Code
	from secondCheckCompleted s
		inner join dbo.Batch b
			inner join @auditType at on b.AuditTypeID = at.AuditTypeID
			inner join dbo.Contractor c on b.ContractorID = c.ContractorID
		on s.BatchID = b.BatchID
	where b.BatchID >= 490
		and Completed between @frDate and dateadd(day, 1, @toDate);

/* This is where we are going to change it up a bit; first, we need to identify the last action carried out by the user  against each
	 item in the selected batches. Note we are only interested in "New item version created" (13), "Previous item version restored" (15)
	 and "Item deleted" (16) activities... */

	select ua.*
	from (
		select ua.*,
			NumberOfItemsEligibleForRandomAccuracyCheck = ua.NumberOfLines - ua.NumberOfErrorsIdentified,
			NumberOfRandomAccuracyCheckItemsSelected = isnull(rc.NumberOfItemsSelected, 0),
			NumberOfRandomAccuracyCheckItemsChanged = isnull(rc.NumberOfItemsChanged, 0)
		from (
			select b.BatchID, b.FinancialPeriodID, b.ContractorCode,
				NumberOfLines = vi.NumberOfLines,
				-- NumberOfElements = isnull(vi.NumberOfElements, 0),
				NumberOfItemsInspected = vi.NumberOfItemsInspected,
				NumberOfErrorsIdentified = isnull(mi.NumberOfErrorsIdentified, 0),
				NumberOfErrorsChanged = isnull(mi.NumberOfErrorsChanged, 0)
			from @batches b
				inner join (
					select BatchID,
						NumberOfLines = count(*),
						-- NumberOfElements = sum(NumberOfElements),
						NumberOfItemsInspected = sum(NumberOfItemsInspected)
					from (
						select BatchID, DocumentID, Sequence,
							NumberOfElements = count(*),
							NumberOfItemsInspected = max(case when NumberOfTimesDispensed = 0 then 1 else NumberOfTimesDispensed end) + (count(*) - 1)
						from (
							select d.BatchID, d.DocumentID, i.Sequence, i.SubSequence, NumberOfTimesDispensed = max(iv.NumberOfTimesDispensed)
							from @batches b
								inner join dbo.Document d
									inner join dbo.Item i
										inner join dbo.ItemVersion iv
											inner join dbo.UserActivity ua on iv.ItemVersionID = ua.OldItemVersionID and ua.UserProfileID = @userProfileID and ua.ActivityID = 4
										on i.ItemID = iv.ItemID
									on d.DocumentID = i.DocumentID
								on b.BatchID = d.BatchID
							group by d.BatchID, d.DocumentID, i.Sequence, i.SubSequence
						) ua
						group by BatchID, DocumentID, Sequence
					) vi
					group by BatchID
				) vi on b.BatchID = vi.BatchID
				left join (
					select BatchID,
						NumberOfErrorsIdentified = count(*),
						NumberOfErrorsChanged = count(*) - sum(IsCurrentItemVersion)
					from (
						select BatchID, DocumentID, Sequence,
							IsCurrentItemVersion = min(IsCurrentItemVersion),
							IsFormChargeStatusChange = max(IsFormChargeStatusChange)
						from (
							select b.BatchID,
								i.DocumentID, i.Sequence,
								IsCurrentItemVersion = case when isnull(i.CurrentItemVersionID, 0) = isnull(ua.NewItemVersionID, 0) then 1 else 0 end,
								IsFormChargeStatusChange = case when exists (select * from dbo.UserActivity where FormID = d.DocumentID and ActivityID = 18) then 1 else 0 end
							from @batches b
								inner join dbo.Document d
									inner join dbo.Item i
										inner join (
										select *
										from dbo.UserActivity ua1
										where ua1.UserActivityID in (
											select top 1 ua2.UserActivityID
											from dbo.UserActivity ua2
												inner join dbo.Item i2 on ua2.ItemID = i2.ItemID
											where ua2.ItemID = ua1.ItemID
												and (ua2.UserProfileID = @userProfileID)
												and (ua2.ActivityID in (13, 15, 16))
											order by ua2.DateTime desc)
									) ua on i.ItemID = ua.ItemID and isnull(i.OriginalItemVersionID, 0) != isnull(ua.NewItemVersionID, 0)
									on d.DocumentID = i.DocumentID
								on b.BatchID = d.BatchID
						) ua
						group by BatchID, DocumentID, Sequence
					) mi
					group by BatchID
				) mi on b.BatchID = mi.BatchID
			) ua left join (
				select BatchID, 
					NumberOfItemsSelected = count(*),
					NumberOfItemsChanged = sum(case when IsChanged = 1 then 1 else 0 end)
				from (
					select BatchID, DocumentID, Sequence, IsChanged = max(IsChanged)
					from (
						select b.BatchID, d.DocumentID, i.Sequence,
							IsChanged = case when OriginalItemVersionID != CurrentItemVersionID then 1 else 0 end
						from @batches b
							inner join dbo.Document d
								inner join dbo.Item i 
									inner join dbo.ItemVersion iv 
										inner join dbo.UserActivity ua on iv.ItemVersionID = ua.OldItemVersionID and ua.UserProfileID = @userProfileID and ua.ActivityID = 4
									on i.OriginalItemVersionID = iv.ItemVersionID
								on d.DocumentID = i.DocumentID and i.IsRandomAccuracyCheck = 1
							on b.BatchID = d.BatchID
						) rc
						group by BatchID, DocumentID, Sequence
				) rc
				group by BatchID
			) rc on ua.BatchID = rc.BatchID
		) ua
	where NumberOfLines != 0
		-- or NumberOfElements != 0
		or NumberOfItemsInspected != 0
		or NumberOfErrorsIdentified != 0
		or NumberOfRandomAccuracyCheckItemsSelected != 0
	order by FinancialPeriodID, ContractorCode
	option (optimize for (@userProfileID unknown));

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[AuditorFeedbackReport_Header]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AuditorFeedbackReport_Header]
	@batchID bigint,
	@userProfileID bigint = null

as

	set nocount on;
	set transaction isolation level read uncommitted;

	select b.BatchID, 
		fp.FinancialYear, fp.FinancialMonth,
		ContractorCode = c.Code, ContractorName = c.Name,
		AuditorName = 
			case when @userProfileID is not null then (select LoginName from dbo.UserProfile where UserProfileID = @userProfileID)
			else cast ('All checkers' as varchar) end
	from dbo.Batch b
		inner join dbo.Contractor c on b.ContractorID = c.ContractorID
		inner join dbo.FinancialPeriod fp on b.FinancialPeriodID = fp.FinancialPeriodID
	where b.BatchID = @batchID;

GO
/****** Object:  StoredProcedure [dbo].[AuditorFeedbackReport_ItemFeedback]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AuditorFeedbackReport_ItemFeedback]
	@batchID bigint,
	@userProfileID bigint = null

as

	set nocount on;
	set transaction isolation level read uncommitted;

	select
		Form = case
			when d.FragmentTypeCode is null then right('000000' + convert(varchar(6), d.DocumentNumber), 6)
			else d.FragmentTypeCode + right('00000' + convert(varchar(6), d.DocumentNumber), 5) 
			end,
		Item = i.Sequence, 
		Element = i.SubSequence,
		af.ProductPrescribed, 
		af.DispenserEndorsements,
		af.ProductPaid,
		af.ProductChecked, 
		af.Comment
	from dbo.Document d
		inner join dbo.Item i
			inner join dbo.AuditFeedback af on i.ItemID = af.ItemID
		on d.DocumentID = i.DocumentID
	where d.BatchID = @batchID
	  and (@userProfileID is null or af.userProfileID = @userProfileID)
	order by Form, Item, Element;

GO
/****** Object:  StoredProcedure [dbo].[AuditorPerformanceReport_Details]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AuditorPerformanceReport_Details]
	@frDate date = '01 January 2013',
	@toDate date = '31 January 2013',
	@auditTypes varchar(max) = '1, 3',
	@userProfileID integer = 7

as

	-- See TFS6220

	set nocount on;
	set transaction isolation level read uncommitted;

	declare @dateSequence table (
		frDate date,
		toDate date,
		primary key (frDate, toDate)
	);

	if (@toDate <= dateadd(month, 1, @frDate)) begin

		-- If the difference between the from and to dates is a month or less,
		-- we will break the report down by week. A week always starts on a Sunday,
		-- and will (later) be trimmed to the start and end period specified by the user.

		set datefirst 7;
		declare @fr date = @frDate;
		while (datepart(weekday, @fr) != 1)
			set @fr = dateadd(day, -1, @fr);

		with dateSequence (frDate, toDate) as (
			select frDate = @fr, toDate = dateadd(day, 7, @fr)
			union all
			select frDate = dateadd(week, 1, frDate), toDate = dateadd(week, 1, toDate)
			from dateSequence
			where dateadd(week, 1, frDate) <= @toDate
		)
		insert into @dateSequence
		select *
		from dateSequence;

	end else begin

		-- The difference between the from and to dates is more than one month.
		-- We will therefore present the report on a month by month basis, and this
		-- will (later) be trimmed to the start and end period specified by the user.

		with dateSequence (frDate, toDate) as (
			select frDate = @frDate, toDate = dateadd(month, 1, @frDate)
			union all
			select frDate = dateadd(month, 1, frDate), toDate = dateadd(month, 2, frDate)
			from dateSequence
			where dateadd(month, 1, frDate) <= @toDate
		)
		insert into @dateSequence
		select *
		from dateSequence;

	end;

	-- Prevent date overspill; reset the start date of the first period and the
	-- end date of the last period if required.
	update @dateSequence
	set frDate = @frDate
	where frDate < @frDate;

	update @dateSequence
	set toDate = dateadd(day, 1, @toDate)
	where toDate > @toDate;

	declare @commaIndex integer = -1;

	declare @auditTypeID integer;
	declare @auditType table (
		AuditTypeID bigint,
		primary key (AuditTypeID)
	);

	set @auditTypes = ltrim(rtrim(@auditTypes));
	while (len(@auditTypes) > 0) begin
		set @commaIndex = charindex(',', @auditTypes)
		if (@commaIndex > 0) begin
			set @auditTypeID = substring(@auditTypes, 1, @commaIndex - 1);
			insert into @auditType (AuditTypeID) values (@auditTypeID);
			set @auditTypes = substring(@auditTypes, @commaIndex + 1, len(@auditTypes) - (@commaIndex));
		end else begin
			set @auditTypeID = @auditTypes;
			insert into @auditType (AuditTypeID) values (@auditTypeID);
			set @auditTypes = '';
		end
		set @auditTypes = ltrim(rtrim(@auditTypes));
	end;

	-- We are only interested in the first occurance of each activity for each item
	-- this way, we only count each activity once (even across periods), and the totals
	-- balance with the consolidated report for the same period.

	declare @userActivity table (
		DocumentID bigint,
		Sequence integer,
		ActivityID integer,
		DateTime datetime,
		NumberOfElements integer,
		ItemsInspected integer
		primary key (DateTime, ActivityID, DocumentID, Sequence)
	);

	-- We find the first occurance of each activity for each item, we then select those first occurances that were
	-- within the defined time period, and group them by document/Item, counting the number of elements included and
	-- calculating the number of actual items inspected...

	-- ItemsInspected = [Number of times dispensed] + [Number of additional elements on the item]

	with ua as (
		select *
		from (
			select ua.ItemID, ua.ActivityID,
				DateTime = min(ua.DateTime)
			from dbo.UserActivity ua
			where ua.UserProfileID = @userProfileID
				and ua.ActivityID in (4, 6, 13)
			group by ItemID, ActivityID
		) ua
		where DateTime between @frDate and dateadd(day, 1, @toDate)
	)

	insert into @userActivity
	select *
	from (
		select i.DocumentID, i.Sequence, ua.ActivityID,
			DateTime = min(ua.DateTime),
			NumberOfElements = count(*),
			ItemsInspected = max(case when iv.NumberOfTimesDispensed = 0 then 1 else iv.NumberOfTimesDispensed end) + (count(*) - 1)
		from ua
			inner join dbo.Item i 
				inner join dbo.Document d
					inner join dbo.Batch b 
						inner join @auditType at on b.AuditTypeID = at.AuditTypeID
					on d.BatchID = b.BatchID
				on i.DocumentID = d.DocumentID
				inner join dbo.ItemVersion iv on i.CurrentItemVersionID = iv.ItemVersionID
			on ua.ItemID = i.ItemID
		group by i.DocumentID, i.Sequence, ua.ActivityID
	) ua
	order by DateTime, ActivityID, DocumentID, Sequence
	option (optimize for (@frDate unknown, @toDate unknown, @userProfileID unknown));

	-- We now remove any "New item version created" rows for items where the same user has subsequently reversed that
	-- decision and restored to the ORIGINAL (BSA) item version...

	delete ua
	from @userActivity ua
		inner join dbo.Item i 
			inner join (
				select *
				from dbo.UserActivity ua1
				where ua1.UserActivityID in (
					select top 1 ua2.UserActivityID
					from dbo.UserActivity ua2
						inner join dbo.Item i2 on ua2.ItemID = i2.ItemID
					where ua2.ItemID = ua1.ItemID
					  and (ua2.UserProfileID = @userProfileID)
						and (ua2.ActivityID = 13
						 or (ua2.ActivityID = 15 and ua2.NewItemVersionID = i2.OriginalItemVersionID))
					order by ua2.DateTime desc)
			) r on i.ItemID = r.ItemID and r.ActivityID = 15
		on ua.DocumentID = i.DocumentID and ua.Sequence = i.Sequence
	where ua.ActivityID = 13
	option (optimize for (@userProfileID unknown));

	-- We now add in the items affected by form charge status changes.
	-- We are looking for a form charge status activity from the user, combined with a difference in the price
	-- of an item (this way, we can also pick up current/old rate changes)...

	with chargeStatusSwitchingActivities as (
		select *
		from (
			select ua.FormID, ua.ActivityID,
				DateTime = min(ua.DateTime)
			from dbo.UserActivity ua
			where ua.UserProfileID = @userProfileID
				and ua.ActivityID = 18
			group by FormID, ActivityID
		) ua
		where DateTime between @frDate and dateadd(day, 1, @toDate)
	)
		
	insert into @userActivity
	select ua.DocumentID, ua.Sequence, ua.ActivityID,
		DateTime = min(ua.DateTime),
		NumberOfElements = count(*),
		ItemsInspected = max(case when ua.NumberOfTimesDispensed = 0 then 1 else ua.NumberOfTimesDispensed end) + (count(*) - 1)
	from (
		select DocumentID, Sequence, ActivityID, DateTime, NumberOfTimesDispensed,
			IsPatientChargeChange = case when OriginalProductPackPeriodicID is not null and CurrentProductPackPeriodicID is not null then 1 else 0 end,
			OriginalValueOfCharges, CurrentValueOfCharges
		from (
			select i.DocumentID, i.Sequence, chargeStatusSwitchingActivities.ActivityID, chargeStatusSwitchingActivities.DateTime, civ.NumberOfTimesDispensed,
				OriginalProductPackPeriodicID = oiv.ProductPackPeriodicID,
				OriginalValueOfCharges = case
					when d.OriginalDocumentGroupTypeID is null or ogt.IsExemptFromCharges = 1 then 0.00
					else isnull(oiv.NumberOfCharges, 0) * case 
						when op.ProductTypeCode = 'A' then case
							when ogt.IsOldRate = 0 then (
								select jp.ApplianceItemPrescriptionChargeValue 
								from dbo.JurisdictionPeriodic jp 
								where jp.JurisdictionID = c.JurisdictionID and jp.FinancialPeriodID = b.FinancialPeriodID
								)
							else (
								select top 1 jp.ApplianceItemPrescriptionChargeValue 
								from dbo.JurisdictionPeriodic jp 
								where jp.JurisdictionID = c.JurisdictionID and jp.FinancialPeriodID <= b.FinancialPeriodID and jp.ApplianceItemPrescriptionChargeValue != (
									select jp.ApplianceItemPrescriptionChargeValue 
									from dbo.JurisdictionPeriodic jp 
									where jp.JurisdictionID = c.JurisdictionID and jp.FinancialPeriodID = b.FinancialPeriodID
								)
								order by jp.FinancialPeriodID desc) 
							end
						else case
							when ogt.IsOldRate = 0 then (
								select jp.DrugItemPrescriptionChargeValue 
								from dbo.JurisdictionPeriodic jp 
								where jp.JurisdictionID = c.JurisdictionID and jp.FinancialPeriodID = b.FinancialPeriodID
								)
							else (
								select top 1 jp.DrugItemPrescriptionChargeValue 
								from dbo.JurisdictionPeriodic jp 
								where jp.JurisdictionID = c.JurisdictionID and jp.FinancialPeriodID <= b.FinancialPeriodID and jp.DrugItemPrescriptionChargeValue != (
									select jp.DrugItemPrescriptionChargeValue 
									from dbo.JurisdictionPeriodic jp 
									where jp.JurisdictionID = c.JurisdictionID and jp.FinancialPeriodID = b.FinancialPeriodID
								)
								order by jp.FinancialPeriodID desc) 
							end 
						end
					end,
				CurrentProductPackPeriodicID = civ.ProductPackPeriodicID,
				CurrentValueOfCharges = case
					when d.CurrentDocumentGroupTypeID is null or cgt.IsExemptFromCharges = 1 then 0.00
					else isnull(civ.NumberOfCharges, 0) * case 
						when cp.ProductTypeCode = 'A' then case
							when cgt.IsOldRate = 0 then (
								select jp.ApplianceItemPrescriptionChargeValue 
								from dbo.JurisdictionPeriodic jp 
								where jp.JurisdictionID = c.JurisdictionID and jp.FinancialPeriodID = b.FinancialPeriodID
								)
							else (
								select top 1 jp.ApplianceItemPrescriptionChargeValue 
								from dbo.JurisdictionPeriodic jp 
								where jp.JurisdictionID = c.JurisdictionID and jp.FinancialPeriodID <= b.FinancialPeriodID and jp.ApplianceItemPrescriptionChargeValue != (
									select jp.ApplianceItemPrescriptionChargeValue 
									from dbo.JurisdictionPeriodic jp 
									where jp.JurisdictionID = c.JurisdictionID and jp.FinancialPeriodID = b.FinancialPeriodID
								)
								order by jp.FinancialPeriodID desc) 
							end
						else case
							when cgt.IsOldRate = 0 then (
								select jp.DrugItemPrescriptionChargeValue 
								from dbo.JurisdictionPeriodic jp 
								where jp.JurisdictionID = c.JurisdictionID and jp.FinancialPeriodID = b.FinancialPeriodID
								)
							else (
								select top 1 jp.DrugItemPrescriptionChargeValue 
								from dbo.JurisdictionPeriodic jp 
								where jp.JurisdictionID = c.JurisdictionID and jp.FinancialPeriodID <= b.FinancialPeriodID and jp.DrugItemPrescriptionChargeValue != (
									select jp.DrugItemPrescriptionChargeValue 
									from dbo.JurisdictionPeriodic jp 
									where jp.JurisdictionID = c.JurisdictionID and jp.FinancialPeriodID = b.FinancialPeriodID
								)
								order by jp.FinancialPeriodID desc) 
							end 
						end
					end
			from chargeStatusSwitchingActivities
				inner join dbo.Document d
					inner join dbo.Batch b
						inner join @auditType at on b.AuditTypeID = at.AuditTypeID
						inner join dbo.Contractor c on b.ContractorID = c.ContractorID
					on d.BatchID = b.BatchID
					left join dbo.DocumentGroupType odgt 
						inner join dbo.GroupType ogt on odgt.GroupTypeCode = ogt.GroupTypeCode
					on d.OriginalDocumentGroupTypeID = odgt.DocumentGroupTypeID
					inner join dbo.DocumentGroupType cdgt 
						inner join dbo.GroupType cgt on cdgt.GroupTypeCode = cgt.GroupTypeCode
					on d.CurrentDocumentGroupTypeID = cdgt.DocumentGroupTypeID
					inner join dbo.Item i
						left join dbo.ItemVersion oiv
							left join dbo.ProductPackPeriodic oppp 
								inner join dbo.ProductPeriodic opp 
									inner join dbo.Product op on opp.ProductID = op.ProductID
								on oppp.ProductPeriodicID = opp.ProductPeriodicID
							on oiv.ProductPackPeriodicID = oppp.ProductPackPeriodicID
						on i.OriginalItemVersionID = oiv.ItemVersionID
						left join dbo.ItemVersion civ
							left join dbo.ProductPackPeriodic cppp 
								inner join dbo.ProductPeriodic cpp 
									inner join dbo.Product cp on cpp.ProductID = cp.ProductID
								on cppp.ProductPeriodicID = cpp.ProductPeriodicID
							on civ.ProductPackPeriodicID = cppp.ProductPackPeriodicID
						on i.CurrentItemVersionID = civ.ItemVersionID
					on d.DocumentID = i.DocumentID
				on chargeStatusSwitchingActivities.FormID = d.DocumentID
		) ua
	) ua
	where IsPatientChargeChange = 1 and OriginalValueOfCharges != CurrentValueOfCharges
	group by ua.DocumentID, ua.Sequence, ua.ActivityID;

	-- Finally, summarise the results...

	with verifiedItems as (

		select frDate, toDate,
			Lines = count(*),
			ItemsInspected = sum(ItemsInspected)
		from @dateSequence
			inner join @userActivity ua on ua.DateTime between frDate and toDate
		where ua.ActivityID = 4
		group by frDate, toDate

	), errorCount as (

		select frDate, toDate,
			Lines = count(*) -- sum(NumberOfElements) if we want to count each element updated
		from @dateSequence
			inner join (
				select DateTime = min(ua.DateTime),
					NumberOfElements = max(ua.NumberOfElements)
				from @userActivity ua
				where ua.ActivityID in (13, 18)
				group by ua.DocumentID, ua.Sequence
			) ua on ua.DateTime between frDate and toDate
		group by frDate, toDate

	), referredCount as (

		select frDate, toDate,
			Lines = count(*) -- sum(NumberOfElements) if we want to count each element updated
		from @dateSequence
			inner join @userActivity ua on ua.DateTime between frDate and toDate
		where ua.ActivityID = 6
		group by frDate, toDate

	)

	-- Driven from the date sequences, we now tot-up the totals for each period
	-- this way, we include periods where the user may not have done anything (annual leave etc.)
	-- and that stands out.

	select ds.frDate, toDate = dateadd(day, -1, ds.toDate), 
		NumberOfLines = isnull(vi.Lines, 0),
		NumberOfItemsInspected = isnull(vi.ItemsInspected, 0),
		NumberOfErrors = isnull(ec.Lines, 0),
		NumberOfReferrals = isnull(rc.Lines, 0)
	from @dateSequence ds
		left join verifiedItems vi on ds.frDate = vi.frDate and ds.toDate = vi.toDate
		left join errorCount ec on ds.frDate = ec.frDate and ds.toDate = ec.toDate 
		left join referredCount rc on ds.frDate = rc.frDate and ds.toDate = rc.toDate 
	order by ds.frDate;

	return @@error;
GO
/****** Object:  StoredProcedure [dbo].[AuditStatisticsReport_ErrorStatistics]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AuditStatisticsReport_ErrorStatistics]
	@auditTypes varchar(4000),
	@frFinancialPeriodID integer,
	@toFinancialPeriodID integer

as

	set nocount on;
	set transaction isolation level read uncommitted;

	/*
	 * We need to convert the comma separated list of AuditTypeID's into a table containing
	 * each entry. This enables us to then use that table as a filter...
	 */

	declare @auditType table (AuditTypeID integer);
	declare @commaIndex integer = -1, @auditTypeID integer;

	set @auditTypes = ltrim(rtrim(@auditTypes));
	while (len(@auditTypes) > 0) begin
		set @commaIndex = charindex(',', @auditTypes)
		if (@commaIndex > 0) begin
			set @auditTypeID = substring(@auditTypes, 1, @commaIndex - 1);
			insert into @auditType (AuditTypeID) values (@auditTypeID);
			set @auditTypes = substring(@auditTypes, @commaIndex + 1, len(@auditTypes) - (@commaIndex));
		end else begin
			set @auditTypeID = @auditTypes;
			insert into @auditType (AuditTypeID) values (@auditTypeID);
			set @auditTypes = '';
		end
		set @auditTypes = ltrim(rtrim(@auditTypes));
	end

	/*
	 * Now we can get the data...
	 */

	select b.FinancialPeriodID, c.Code,
		bas.NumberOfVerifiedItems,
		bas.TotalNumberOfChanges,
		bas.NumberOfVerifiedItemsWithChanges,
		bas.NumberOfVerifiedItemsWithChangesThatHaveFinancialImpact,
		
		cs.TotalOfAccount,

		NetCashVariance = bas.NetCashVariance
			--+ round(bas.NetProfessionalFeeVariance * case when 201207 <= b.FinancialPeriodID then jp.ValueOfConsumableAllowanceFee else jp.ContainerAllowanceValue end, 2)
			--+ (bas.NetPaymentForContainersVariance * jp.ContainerAllowanceValue)
			--+ (isnull(csco.ContainerAllowanceValue, 0.00) - isnull(osco.ContainerAllowanceValue, 0.00))
			+ (isnull(csfe.RepeatDispensingFee, 0.00) - isnull(osfe.RepeatDispensingFee, 0.00))
			+ (isnull(csfe.PracticePayment, 0.00) - isnull(osfe.PracticePayment, 0.00))
			+ (isnull(csfe.EstablishmentPayment, 0.00) - isnull(osfe.EstablishmentPayment, 0.00))
			+ (isnull(csfe.OtherFeesMedicinalUseReview, 0.00) - isnull(osfe.OtherFeesMedicinalUseReview, 0.00))
			+ (isnull(csfe.OtherFeesNewMedicineServiceConsultation, 0.00) - isnull(osfe.OtherFeesNewMedicineServiceConsultation, 0.00))
			+ (isnull(csfe.OtherFeesApplianceUseReviewHome, 0.00) - isnull(osfe.OtherFeesApplianceUseReviewHome, 0.00))
			+ (isnull(csfe.OtherFeesApplianceUseReviewPremises, 0.00) - isnull(osfe.OtherFeesApplianceUseReviewPremises, 0.00))
			+ (isnull(csfe.OtherFeesStomaCustomisation, 0.00) - isnull(osfe.OtherFeesStomaCustomisation, 0.00))
			+ (isnull(csch.FP57Refunds, 0.00) - isnull(osch.FP57Refunds, 0.00))
			+ (isnull(csco.DiscountValue, 0.00) - isnull(osco.DiscountValue, 0.00))
			+ case when b.FinancialPeriodID < 201207 then (isnull(csco.OutOfPocketExpenses, 0.00) - isnull(osco.OutOfPocketExpenses, 0.00)) else 0.00 end,

		AbsCashVariance = bas.AbsCashVariance
			--+ round(bas.AbsProfessionalFeeVariance * case when 201207 <= b.FinancialPeriodID then jp.ValueOfConsumableAllowanceFee else jp.ContainerAllowanceValue end, 2)
			--+ abs(round(bas.AbsPaymentForContainersVariance * jp.ContainerAllowanceValue, 2))
			--+ abs(isnull(csco.ContainerAllowanceValue, 0.00) - isnull(osco.ContainerAllowanceValue, 0.00))
			+ abs(isnull(csfe.RepeatDispensingFee, 0.00) - isnull(osfe.RepeatDispensingFee, 0.00))
			+ abs(isnull(csfe.PracticePayment, 0.00) - isnull(osfe.PracticePayment, 0.00))
			+ abs(isnull(csfe.EstablishmentPayment, 0.00) - isnull(osfe.EstablishmentPayment, 0.00))
			+ abs(isnull(csfe.OtherFeesMedicinalUseReview, 0.00) - isnull(osfe.OtherFeesMedicinalUseReview, 0.00))
			+ abs(isnull(csfe.OtherFeesNewMedicineServiceConsultation, 0.00) - isnull(osfe.OtherFeesNewMedicineServiceConsultation, 0.00))
			+ abs(isnull(csfe.OtherFeesApplianceUseReviewHome, 0.00) - isnull(osfe.OtherFeesApplianceUseReviewHome, 0.00))
			+ abs(isnull(csfe.OtherFeesApplianceUseReviewPremises, 0.00) - isnull(osfe.OtherFeesApplianceUseReviewPremises, 0.00))
			+ abs(isnull(csfe.OtherFeesStomaCustomisation, 0.00) - isnull(osfe.OtherFeesStomaCustomisation, 0.00))
			+ abs(isnull(csch.FP57Refunds, 0.00) - isnull(osch.FP57Refunds, 0.00))
			+ abs(isnull(csco.DiscountValue, 0.00) - isnull(osco.DiscountValue, 0.00))
			+ case when b.FinancialPeriodID < 201207 then abs(isnull(csco.OutOfPocketExpenses, 0.00) - isnull(osco.OutOfPocketExpenses, 0.00)) else 0.00 end,

		bas.NumberOfMultiLineChanges,
		bas.NumberOfMissedItems,
		bas.NumberOfExtraItems,
		bas.NumberOfProductChanges,
		bas.NumberOfQuantityChanges,
		bas.NumberOfPackChanges,
		bas.NumberOfProfessionalFeeChanges,
		bas.NumberOfPatientChargeChanges,
		bas.NumberOfSurchargeChanges,
		bas.NumberOfInvoicePriceChanges,
		bas.NumberOfOtherChanges
	from dbo.Batch b
		inner join @auditType at on b.AuditTypeID = at.AuditTypeID
		inner join dbo.BatchAuditStatistics bas on b.BatchID = bas.BatchID
		inner join dbo.Contractor c on b.ContractorID = c.ContractorID
		inner join dbo.JurisdictionPeriodic jp on b.FinancialPeriodID = jp.FinancialPeriodID and c.JurisdictionID = jp.JurisdictionID
		inner join dbo.Schedule os
			inner join dbo.ScheduleCharge osch on os.ScheduleID = osch.ScheduleID
			inner join dbo.ScheduleCost osco on os.ScheduleID = osco.ScheduleID
			inner join dbo.ScheduleFee osfe on os.ScheduleID = osfe.ScheduleID
		on b.ScheduleID = os.ScheduleID
		inner join dbo.Schedule cs
			inner join dbo.ScheduleCharge csch on cs.ScheduleID = csch.ScheduleID
			inner join dbo.ScheduleCost csco on cs.ScheduleID = csco.ScheduleID
			inner join dbo.ScheduleFee csfe on cs.ScheduleID = csfe.ScheduleID
		on b.PsncScheduleID = cs.ScheduleID
	where b.FinancialPeriodID between @frFinancialPeriodID and @toFinancialPeriodID
		and b.BatchStatusID = 900
	order by 1, 2
	option (optimize for (@frFinancialPeriodID unknown, @toFinancialPeriodID unknown));

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[AuditSummaryReport_BatchDetails]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AuditSummaryReport_BatchDetails]
	@financialPeriodID integer

as

	set nocount on;
	set transaction isolation level read uncommitted;

	with batchDetails as (
		select RowNumber = row_number() over (order by c.Code),
			Contractor = c.Code,
			ItemsWithAFee  = sum(iv.NumberOfProfessionalFees),
			BSATotalOfAccount = bsa.TotalOfAccount,
			PSNCTotalOfAccount = psnc.TotalOfAccount,
			Underpaid = case when bsa.TotalOfAccount < psnc.TotalOfAccount then psnc.TotalOfAccount - bsa.TotalOfAccount else null end,
			Overpaid = case when psnc.TotalOfAccount < bsa.TotalOfAccount then bsa.TotalOfAccount - psnc.TotalOfAccount else null end
		from dbo.Batch b
			inner join dbo.Contractor c on b.ContractorID = c.ContractorID
			inner join dbo.Document d 
				inner join dbo.Item i
					inner join dbo.ItemVersion iv
						inner join dbo.ProductPackPeriodic ppp on iv.ProductPackPeriodicID = ppp.ProductPackPeriodicID
					on i.CurrentItemVersionID = iv.ItemVersionID
				on d.DocumentID = i.DocumentID
			on b.BatchID = d.BatchID
			inner join dbo.Schedule bsa on b.ScheduleID = bsa.ScheduleID
			inner join dbo.Schedule psnc on b.PsncScheduleID = psnc.ScheduleID
		where b.FinancialPeriodID = @financialPeriodID
			and b.BatchStatusID = 500 /* Checking completed. Awaiting generation of PA correspondence */
		group by c.Code, bsa.TotalOfAccount, psnc.TotalOfAccount

	)
	select *
	from batchDetails
	order by RowNumber

	return;

GO
/****** Object:  StoredProcedure [dbo].[BatchErrorReport_AsCheckedSummary]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BatchErrorReport_AsCheckedSummary]
	@batchID bigint,
	@includeHiddenAdjustments bit = 0

AS 

	set nocount on;
	set transaction isolation level read uncommitted;

	declare @scheduleID bigint,
		@isTransientSchedule bit = 0;

	select @scheduleID = PsncScheduleID
	from dbo.Batch b
	where b.BatchID = @batchID;

	if (@scheduleID is null) begin
		set @isTransientSchedule = 1;
		exec dbo.GenerateCurrentScheduleOfPayments @batchID = @batchID, @scheduleID = @scheduleID output;
	end

	select s.TotalOfAccount,
		sd.ItemsAtStandardDiscountRateForWhichAFeeIsPaidIncludingOxygen,
		sc.TotalOfBasicPricesAtStandardDiscountRate,
		sc.DiscountPercentage,
		sc.DiscountValue,
		sd.ItemsAtZeroDiscountRateForWhichAFeeIsPaid, 
		sc.TotalOfBasicPricesAtZeroDiscount,
		sc.SubTotalOfBasicPrices,
		sc.OutOfPocketExpenses,
		sc.ConsumableAllowanceQuantity,
		sc.ConsumableAllowancePrice,
		sc.ConsumableAllowanceValue,
		sc.ContainerAllowanceQuantity,
		sc.ContainerAllowancePrice,
		sc.ContainerAllowanceValue,
		sc.TotalOfDrugAndApplianceCosts,
		sf.ProfessionalFeeQuantity,
		sf.ProfessionalFeeAmount,
		sf.ProfessionalFeeValue,
		sf.RepeatDispensingFee,
		sd.DispensingStaffNumberOfHoursDeclared,
		sf.PracticePayment,
		sf.AdditionalFees2a,
		sf.AdditionalFees2bMeasuredFitted,
		sf.AdditionalFees2bHomeDelivery,
		sf.AdditionalFees2c,
		sf.AdditionalFees2d,
		sf.AdditionalFeesMethadone,
		sf.AdditionalFees2e,
		sf.AdditionalFees2f,
		sf.ManuallyPriced,
		sf.EstablishmentPayment,
		sf.SubTotalOfPrescriptionFees,
		sf.OtherFeesMedicinalUseReview,
		sf.OtherFeesApplianceUseReviewHome,
		sf.OtherFeesApplianceUseReviewPremises,
		sf.OtherFeesStomaCustomisation,
		isnull(sd.NewMedicineServicesUndertaken, 0) as NewMedicineServicesUndertaken,
		isnull(sd.NewMedicineServiceItems, 0) as NewMedicineServiceItems,
		isnull(sf.OtherFeesNewMedicineServiceConsultation, 0) as OtherFeesNewMedicineServiceConsultation,
		sf.TotalOfAllFees,
		sh.ElasticHoiseryValue + sh.ExclHoiseryR1Value + sh.ExclHoiseryR2Value as PatientCharges,
		sh.FP57Refunds,
		sh.TotalIncludingFP57
	from dbo.Schedule s 
		inner join dbo.ScheduleCost sc on s.ScheduleID = sc.ScheduleID
		inner join dbo.ScheduleCharge sh on s.ScheduleID = sh.ScheduleID
		inner join dbo.ScheduleFee sf on s.ScheduleID = sf.ScheduleID
		inner join dbo.ScheduleDocumentData sd on s.ScheduleID = sd.ScheduleID
	where s.ScheduleID = @scheduleID;

	if (@isTransientSchedule = 1)
		exec dbo.DeleteScheduleOfPayments @scheduleID = @ScheduleID;

	return @@error;
GO
/****** Object:  StoredProcedure [dbo].[BatchErrorReport_AsPricedSummary]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BatchErrorReport_AsPricedSummary]
	@BatchID bigint,
	@includeHiddenAdjustments bit = 0

AS

	set nocount on;
	set transaction isolation level read uncommitted;

	select s.TotalOfAccount,
		isnull(sd.ItemsAtStandardDiscountRateForWhichAFeeIsPaidIncludingOxygen, 0) as ItemsAtStandardDiscountRateForWhichAFeeIsPaidIncludingOxygen,
		isnull(sc.TotalOfBasicPricesAtStandardDiscountRate, 0) as TotalOfBasicPricesAtStandardDiscountRate,
		isnull(sc.DiscountPercentage, 0) as DiscountPercentage,
		isnull(sc.DiscountValue, 0) as DiscountValue,
		isnull(sd.ItemsAtZeroDiscountRateForWhichAFeeIsPaid, 0) as ItemsAtZeroDiscountRateForWhichAFeeIsPaid,
		isnull(sc.TotalOfBasicPricesAtZeroDiscount, 0) as TotalOfBasicPricesAtZeroDiscount,
		isnull(sc.SubTotalOfBasicPrices, 0) as SubTotalOfBasicPrices,
		isnull(sc.OutOfPocketExpenses, 0) as OutOfPocketExpenses,
		isnull(sc.ConsumableAllowanceQuantity, 0) as ConsumableAllowanceQuantity,
		isnull(sc.ConsumableAllowancePrice, 0) as ConsumableAllowancePrice,
		isnull(sc.ConsumableAllowanceValue, 0) as ConsumableAllowanceValue,
		isnull(sc.ContainerAllowanceQuantity, 0) as ContainerAllowanceQuantity,
		isnull(sc.ContainerAllowancePrice, 0) as ContainerAllowancePrice,
		isnull(sc.ContainerAllowanceValue, 0) as ContainerAllowanceValue,
		isnull(sc.TotalOfDrugAndApplianceCosts, 0) as TotalOfDrugAndApplianceCosts,
		isnull(sf.ProfessionalFeeQuantity, 0) as ProfessionalFeeQuantity,
		isnull(sf.ProfessionalFeeAmount, 0) as ProfessionalFeeAmount,
		isnull(sf.ProfessionalFeeValue, 0) as ProfessionalFeeValue,
		isnull(sf.RepeatDispensingFee, 0) as RepeatDispensingFee,
		isnull(sd.DispensingStaffNumberOfHoursDeclared, 0) as DispensingStaffNumberOfHoursDeclared,
		isnull(sf.PracticePayment, 0) as PracticePayment,
		isnull(sf.AdditionalFees2a, 0) as AdditionalFees2a,
		isnull(sf.AdditionalFees2bMeasuredFitted, 0) as AdditionalFees2bMeasuredFitted,
		isnull(sf.AdditionalFees2bHomeDelivery, 0) as AdditionalFees2bHomeDelivery,
		isnull(sf.AdditionalFees2c, 0) as AdditionalFees2c,
		isnull(sf.AdditionalFees2d, 0) as AdditionalFees2d,
		isnull(sf.AdditionalFees2e, 0) as AdditionalFees2e,
		isnull(sf.AdditionalFeesMethadone , 0) as AdditionalFeesMethadone,
		isnull(sf.AdditionalFees2f, 0) as AdditionalFees2f,
		isnull(sf.ManuallyPriced, 0) as ManuallyPriced,
		isnull(sf.EstablishmentPayment, 0) as EstablishmentPayment,
		isnull(sf.SubTotalOfPrescriptionFees, 0) as SubTotalOfPrescriptionFees,
		isnull(sf.OtherFeesMedicinalUseReview, 0) as OtherFeesMedicinalUseReview,
		isnull(sf.OtherFeesApplianceUseReviewHome, 0) as OtherFeesApplianceUseReviewHome,
		isnull(sf.OtherFeesApplianceUseReviewPremises, 0) as OtherFeesApplianceUseReviewPremises,
		isnull(sf.OtherFeesStomaCustomisation, 0) as OtherFeesStomaCustomisation,
		isnull(sd.NewMedicineServicesUndertaken, 0) as NewMedicineServicesUndertaken,
		isnull(sd.NewMedicineServiceItems, 0) as NewMedicineServiceItems,
		isnull(sf.OtherFeesNewMedicineServiceConsultation, 0) as OtherFeesNewMedicineServiceConsultation,
		isnull(sf.TotalOfAllFees, 0) as TotalOfAllFees,
		isnull(sh.ElasticHoiseryValue, 0) + isnull(sh.ExclHoiseryR1Value, 0) + isnull(sh.ExclHoiseryR2Value, 0) as PatientCharges,
		isnull(sh.FP57Refunds, 0) as FP57Refunds,
		isnull(sh.TotalIncludingFP57, 0) as TotalIncludingFP57
	from Batch b 
		inner join Schedule s 
			inner join dbo.ScheduleCost sc on s.ScheduleID = sc.ScheduleID
			inner join dbo.ScheduleCharge sh on s.ScheduleID = sh.ScheduleID
			inner join dbo.ScheduleFee sf on s.ScheduleID = sf.ScheduleID
			inner join dbo.ScheduleDocumentData sd on s.ScheduleID = sd.ScheduleID
		on b.ScheduleID = s.ScheduleID
	where b.BatchID = @BatchID

GO
/****** Object:  StoredProcedure [dbo].[BatchErrorReport_BatchErrors]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BatchErrorReport_BatchErrors]
	@batchID bigint,
	@includeHiddenAdjustments bit = 0

AS

	set nocount on;
	set transaction isolation level read uncommitted;
	
	with itemDifferences as (
		select *
		from dbo.BatchItemDetails(@batchID, @includeHiddenAdjustments)
		where isnull(CurrentItemVersionID, 0) != isnull(OriginalItemVersionID, 0)
		   or CurrentPaymentForContainersIndicator != OriginalPaymentForContainersIndicator
		   or CurrentGroupTypeCode != OriginalGroupTypeCode
	)
	
	select *
	from itemDifferences
	where TotalAdjustment != 0 -- Has financial impact
	   or isnull(OriginalProductPackCode, '') != isnull(CurrentProductPackCode, '') -- Product and/or pack has changed
		 or OriginalQuantity != CurrentQuantity -- Quantity has changed
	order by 1, 2, 3;

	return

GO
/****** Object:  StoredProcedure [dbo].[BatchErrorReport_BatchErrorSummary]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BatchErrorReport_BatchErrorSummary]
	@batchID bigint,
	@includeHiddenAdjustments bit = 0

as

	set nocount on;
	set transaction isolation level read uncommitted;

	-- Get the items that are shown on the BER.
	-- Load them into a temporary table here because we need to scan the list multiple times and if we use a CTE
	-- it will rebuild the list every time we try and scan it...

	select *
	into #itemDifferences
	from (
		select * from dbo.BatchItemDetails(@batchID, @includeHiddenAdjustments)
		where isnull(CurrentItemVersionID, 0) != isnull(OriginalItemVersionID, 0)) batchItemDetails
	where TotalAdjustment != 0 -- Has financial impact
			or isnull(OriginalProductPackCode, '') != isnull(CurrentProductPackCode, '') -- Product and/or pack has changed
			or OriginalQuantity != CurrentQuantity -- Quantity has changed


	-- We now try to identify the various types of error that need to be reported
	-- and build them into a list...

	select ErrorType, sum(Value) as Value
	from (

		select 00 as Sequence, 'Total number of form items with errors' as ErrorType, count(*) as Value
		from #itemDifferences
			
		union
			
		select 01, 'Quantity dispensed changed', count(*)
		from #itemDifferences
		where OriginalQuantity != CurrentQuantity
			
		union
			
		select 02, 'Professional fees changed', count(*)
		from #itemDifferences
		where OriginalNumberOfProfessionalFees != CurrentNumberOfProfessionalFees
			
		union
			
		select 03, 'Product changed', count(*)
		from #itemDifferences
		where OriginalProductDescription != CurrentProductDescription
			
		union
			
		select 04, 'Product pack changed', count(*)
		from #itemDifferences
		where OriginalProductDescription = CurrentProductDescription
			and OriginalProductPackCode != CurrentProductPackCode
			
		union
			
		select 05, 'Charges changed', count(*)
		from #itemDifferences
		where OriginalNumberOfCharges * OriginalChargeValue != CurrentNumberOfCharges * CurrentChargeValue

	) itemDifferences
	where Value != 0
	group by Sequence, ErrorType
	order by Sequence;

	-- Finally, drop the temporary table.
	-- This would happen automatically when the connection running the SP is closed, but in the event
	-- that we are trying to run multiple times from SQLSMS (e.g. testing) that connection may not be closed between runs.

	drop table #itemDifferences;

GO
/****** Object:  StoredProcedure [dbo].[BatchErrorReport_ErrorStatistics]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BatchErrorReport_ErrorStatistics]
	@batchID bigint,
	@includeHiddenAdjustments bit = 0

as

	declare @numberOfVerifiedItems integer,
		@numberOfVerifiedItemsWithErrors integer,
		@numberOfMultiLineChanges integer,
		@numberOfMissedItems integer,
		@numberOfExtraItems integer,
		@numberOfProductChanges integer,
		@numberOfQuantityChanges integer,
		@numberOfPackChanges integer,
		@numberOfProfessionalFeeChanges integer,
		@numberOfPatientChargeChanges integer,
		@numberOfSurchargeChanges integer,
		@numberOfInvoicePriceChanges integer,
		@numberOfOtherChanges integer,
		@numberOfChanges integer,
		@numberOfChangesWithFinacialImpact integer;

	select 
		@numberOfVerifiedItems = NumberOfVerifiedItems,
		@numberOfVerifiedItemsWithErrors = NumberOfVerifiedItemsWithChanges,
		@numberOfMultiLineChanges = NumberOfMultiLineChanges,
		@numberOfMissedItems = NumberOfMissedItems,
		@numberOfExtraItems = NumberOfExtraItems,
		@numberOfProductChanges = NumberOfProductChanges,
		@numberOfQuantityChanges = NumberOfQuantityChanges,
		@numberOfPackChanges = NumberOfPackChanges,
		@numberOfProfessionalFeeChanges = NumberOfProfessionalFeeChanges,
		@numberOfPatientChargeChanges = NumberOfPatientChargeChanges,
		@numberOfSurchargeChanges = NumberOfSurchargeChanges,
		@numberOfInvoicePriceChanges = NumberOfInvoicePriceChanges,
		@numberOfOtherChanges = NumberOfOtherChanges,
		@numberOfChanges = TotalNumberOfChanges,
		@numberOfChangesWithFinacialImpact = NumberOfVerifiedItemsWithChangesThatHaveFinancialImpact
	from dbo.BatchAuditStatistics
	where BatchID = @batchID;

	if (@@rowcount = 0) begin

		with verifiedItems as (

			select b.FinancialPeriodID, d.DocumentID, i.Sequence
			from dbo.Batch b
				inner join dbo.Contractor c on b.ContractorID = c.ContractorID
				inner join dbo.Document d
					inner join dbo.Item i	on d.DocumentID = i.DocumentID
				on b.BatchID = d.BatchID
			where b.BatchID = @batchID
				and (i.OriginalItemVersionID is not null or i.CurrentItemVersionID is not null)
				and not exists (
					select *
					from dbo.ItemVersion iv
					where iv.ItemVersionID = i.CurrentItemVersionID
						and iv.IsVerified = 0
				)
			group by b.FinancialPeriodID, d.DocumentID, i.Sequence

		)

		select
			@numberOfVerifiedItems = count(*),
			@numberOfVerifiedItemsWithErrors = isnull(count(e.DocumentID), 0),
			@numberOfMultiLineChanges = isnull(sum(IsMultiLine), 0),
			@numberOfMissedItems = isnull(sum(IsMissedItem), 0),
			@numberOfExtraItems = isnull(sum(IsExtraItem), 0),
			@numberOfProductChanges = isnull(sum(IsProductChange), 0),
			@numberOfQuantityChanges = isnull(sum(IsQuantityChange), 0),
			@numberOfPackChanges = isnull(sum(IsPackChange), 0),
			@numberOfProfessionalFeeChanges = isnull(sum(IsProfessionalFeeChange), 0),
			@numberOfPatientChargeChanges = isnull(sum(IsPatientChargeChange), 0),
			@numberOfSurchargeChanges = isnull(sum(IsSurchargeChange), 0),
			@numberOfInvoicePriceChanges = isnull(sum(IsInvoicePriceChange), 0),
			@numberOfOtherChanges = isnull(sum(IsOtherChange), 0),
			@numberOfChanges = isnull(sum(TotalNumberOfChanges), 0),
			@numberOfChangesWithFinacialImpact = isnull(sum(HasFinancialImpact), 0)
		from verifiedItems v
			left join dbo.BatchItemErrorAnalysis (@batchID) e on v.DocumentID = e.DocumentID and v.Sequence = e.Sequence
		group by v.FinancialPeriodID
		order by 1
		option (optimize for (@batchID unknown));

	end

	select
		NumberOfVerifiedItems = @numberOfVerifiedItems,
		NumberOfVerifiedItemsWithErrors = @numberOfVerifiedItemsWithErrors,
		NumberOfMultiLineChanges = @numberOfMultiLineChanges,
		NumberOfMissedItems = @numberOfMissedItems,
		NumberOfExtraItems = @numberOfExtraItems,
		NumberOfProductChanges = @numberOfProductChanges,
		NumberOfQuantityChanges = @numberOfQuantityChanges,
		NumberOfPackChanges = @numberOfPackChanges,
		NumberOfProfessionalFeeChanges = @numberOfProfessionalFeeChanges,
		NumberOfPatientChargeChanges = @numberOfPatientChargeChanges,
		NumberOfSurchargeChanges = @numberOfSurchargeChanges,
		NumberOfInvoicePriceChanges = @numberOfInvoicePriceChanges,
		NumberOfOtherChanges = @numberOfOtherChanges,
		NumberOfChanges = @numberOfChanges,
		NumberOfChangesWithFinacialImpact = @numberOfChangesWithFinacialImpact
	from dbo.Batch
	where BatchID = @batchID;

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[BatchErrorReport_Header]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BatchErrorReport_Header]
	@batchID bigint,
	@includeHiddenAdjustments bit = 0

as

	set nocount on;
	set transaction isolation level read uncommitted;

	select b.BatchID, 
		b.FinancialPeriodID, fp.FinancialYear, fp.FinancialMonth,
		c.Code as ContractorCode, c.Name as ContractorName,
		lpc.Code as LpcCode, lpc.Name as LpcName
	from dbo.Batch b
		inner join dbo.Contractor c on b.ContractorID = c.ContractorID
		inner join dbo.FinancialPeriod fp on b.FinancialPeriodID = fp.FinancialPeriodID
		left join LocalPharmaceuticalCommittee lpc on b.LocalPharmaceuticalCommitteeID = lpc.LocalPharmaceuticalCommitteeID
	where b.BatchID = @batchID;

GO
/****** Object:  StoredProcedure [dbo].[BatchItemProductChangeReport_Header]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BatchItemProductChangeReport_Header]
	@batchID bigint,
	@originalProductPackID bigint = null,
	@currentProductPackID bigint = null

as

	set nocount on;
	set transaction isolation level read uncommitted;

	select b.BatchID, 
		fp.FinancialYear, fp.FinancialMonth,
		c.Code as ContractorCode, c.Name as ContractorName,
		lpc.Code as LpcCode, lpc.Name as LpcName
	from dbo.Batch b
		inner join dbo.Contractor c on b.ContractorID = c.ContractorID
		inner join dbo.FinancialPeriod fp on b.FinancialPeriodID = fp.FinancialPeriodID
		left join LocalPharmaceuticalCommittee lpc on b.LocalPharmaceuticalCommitteeID = lpc.LocalPharmaceuticalCommitteeID
	where b.BatchID = @batchID;

GO
/****** Object:  StoredProcedure [dbo].[BatchItemProductChangeReport_Items]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BatchItemProductChangeReport_Items]
	@batchID bigint,
	@originalProductPackID bigint = null,
	@currentProductPackID bigint = null

as

	set nocount on;
	set transaction isolation level read uncommitted;

	select *
	from dbo.BatchItemDetails(@batchID, 1)
	where (@originalProductPackID is not null and isnull(OriginalProductPackID, 0) = @originalProductPackID)
	   or (@currentProductPackID is not null and isnull(CurrentProductPackID, 0) = @currentProductPackID)

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[BatchList_FragmentsReportCallback]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BatchList_FragmentsReportCallback]
	@financialPeriodID integer

as

	declare @minimumBatchStatusID integer = 200; /* Full details received, waiting for FP34C entry */

	with fragments as (
		select b.BatchID, b.ContractorID,
			Form = d.FragmentTypeCode + right('00000' + convert(varchar(6), d.DocumentNumber), 5),
			Elements = count(i.ItemID),
			rowNumber = row_number() over (partition by b.BatchID order by b.BatchID, d.FragmentTypeCode + right('00000' + convert(varchar(6), d.DocumentNumber), 5))
		from dbo.Batch b
			left join dbo.Document d 
				inner join dbo.Item i on d.DocumentID = i.DocumentID
			on b.BatchID = d.BatchID and ((d.FragmentTypeCode between 'N' and 'Y') or (d.FragmentTypeCode != 'Z' and SubmissionGroupCode is null))
		where b.FinancialPeriodID = @financialPeriodID
		  and b.BatchStatusID >= @minimumBatchStatusID
		group by b.BatchID, b.ContractorID, d.DocumentID, d.FragmentTypeCode, d.DocumentNumber
	)

	select Contractor = c.Code, f.Form, f.Elements,
		[Z-Frags] = case 
			when f.rowNumber = 1 then case 
				when exists (select * from dbo.Document where BatchID = f.BatchID and FragmentTypeCode = 'Z') then 'Y'
				else 'N' end
			else null end
	from fragments f
		inner join dbo.Contractor c on f.ContractorID = c.ContractorID
	order by 1, f.rowNumber;

	return @@error;
GO
/****** Object:  StoredProcedure [dbo].[BatchStatisticsViewModel_SelectCallback]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BatchStatisticsViewModel_SelectCallback]
	@batchID bigint

as

	set nocount on;
	set transaction isolation level read uncommitted;

	declare @jurisdictionID integer, @financialPeriodID integer
	declare @applianceChargeAtCurrentRate money, @applianceChargeAtPreviousRate money,
		@drugsChargeAtCurrentRate money, @drugsChargeAtPreviousRate money,
		@professionalFeeAmount money;

	-- Determine the financial period and jurisdiction for the batch...	
	select @financialPeriodID = b.FinancialPeriodID,
		@jurisdictionID = c.JurisdictionID
	from dbo.Batch b
		inner join dbo.Contractor c on b.ContractorID = c.ContractorID
	where b.BatchID = @batchID;

	-- Get the charge rates for the current financial period/jurisdiction combination...
	select @applianceChargeAtCurrentRate = jp.ApplianceItemPrescriptionChargeValue,
		@drugsChargeAtCurrentRate = jp.DrugItemPrescriptionChargeValue,
		@professionalFeeAmount = jp.StandardProfessionalFeeValue
	from dbo.JurisdictionPeriodic jp
	where jp.JurisdictionID = @jurisdictionID and jp.FinancialPeriodID = @financialPeriodID;

	select top 1 @applianceChargeAtPreviousRate = jp.ApplianceItemPrescriptionChargeValue
	from dbo.JurisdictionPeriodic jp
	where jp.JurisdictionID = @jurisdictionID
		and jp.FinancialPeriodID < @financialPeriodID
		and jp.ApplianceItemPrescriptionChargeValue != @applianceChargeAtCurrentRate
	order by jp.FinancialPeriodID desc;

	select top 1 @drugsChargeAtPreviousRate = jp.DrugItemPrescriptionChargeValue
	from dbo.JurisdictionPeriodic jp
	where jp.JurisdictionID = @jurisdictionID
		and jp.FinancialPeriodID < @financialPeriodID
		and jp.DrugItemPrescriptionChargeValue != @drugsChargeAtCurrentRate
	order by jp.FinancialPeriodID desc;



	-- Form charge status statistics
	select sum(case when isnull(cast(cdgt.IsVerified as integer), 0) = 0 and isnull(cast(cdgt.IsReferred as integer), 0) = 0 then 1 else 0 end) as uncheckedFormChargeStatuses,
		sum(case when isnull(cast(cdgt.IsVerified as integer), 0) = 1 and isnull(cast(cdgt.IsConfirmed as integer), 0) = 0 then 1 else 0 end) as verifiedFormChargeStatuses,
		sum(isnull(cast(cdgt.IsReferred as integer), 0)) as referredFormChargeStatuses,
		sum(isnull(cast(cdgt.IsConfirmed as integer), 0)) as confirmedFormChargeStatuses
	from dbo.Document d
		inner join dbo.DocumentGroupType cdgt on d.CurrentDocumentGroupTypeID = cdgt.DocumentGroupTypeID
	where d.BatchID = @batchID;
	
	-- Verified form charge status statistics
	select isnull(sum(case when isnull(odgt.GroupTypeCode, '') != isnull(cdgt.GroupTypeCode, '') then 1 else 0 end), 0) as switchedAndVerifiedForms,
		isnull(sum(case when isnull(odgt.GroupTypeCode, '') = isnull(cdgt.GroupTypeCode, '') then 1 else 0 end), 0) as unswitchedAndVerifiedForms
	from dbo.Document d
		left join dbo.DocumentGroupType odgt on d.OriginalDocumentGroupTypeID = odgt.DocumentGroupTypeID
		left join dbo.DocumentGroupType cdgt on d.CurrentDocumentGroupTypeID = cdgt.DocumentGroupTypeID
	where d.BatchID = @batchID
	  and cdgt.IsVerified = 1;

	-- Item status statistics
	select sum(case when isnull(cast(iv.IsVerified as integer), 0) = 0 and isnull(cast(iv.IsReferred as integer), 0) = 0 then 1 else 0 end) as uncheckedItems,
		sum(case when isnull(cast(iv.IsVerified as integer), 0) = 1 and isnull(cast(iv.IsConfirmed as integer), 0) = 0 then 1 else 0 end) as verifiedItems,
		sum(isnull(cast(iv.IsReferred as integer), 0)) as referredItems,
		sum(isnull(cast(iv.IsConfirmed as integer), 0)) as confirmedItems
	from dbo.Document d
		inner join dbo.Item i
			inner join dbo.ItemVersion iv on i.CurrentItemVersionID = iv.ItemVersionID
		on d.DocumentID = i.DocumentID
	where d.BatchID = @batchID;
	
	-- Verified item status statistics
	with itemsWithDifferences as (
	
		select 
		
			isnull(opp.Code, '') as OriginalProductPackCode,
			isnull(oiv.Quantity, 0) as OriginalQuantity,
			isnull(oiv.BasicPrice, 0) as OriginalBasicPrice,
			isnull(oiv.NumberOfProfessionalFees, 0) * @professionalFeeAmount as OriginalValueOfProfessionalFees,
			isnull(oiv.ValueOfAdditionalFees, 0)as OriginalValueOfAdditionalFees,
			isnull(oiv.ValueOfOutOfPocketExpenses, 0) as OriginalValueofOutOfPocketExpenses,
			case 
				when d.OriginalDocumentGroupTypeID is null or ogt.IsExemptFromCharges = 1 then 0 
				else oiv.NumberOfCharges end as OriginalNumberOfCharges, 
			case 
				when op.ProductTypeCode = 'A' then
					case
						when d.OriginalDocumentGroupTypeID is null or ogt.IsExemptFromCharges = 1 then 0 
						when ogt.IsOldRate = 0 then @applianceChargeAtCurrentRate 
						else @applianceChargeAtPreviousRate end
				else
					case
						when d.OriginalDocumentGroupTypeID is null or ogt.IsExemptFromCharges = 1 then 0 
						when ogt.IsOldRate = 0 then @drugsChargeAtCurrentRate 
						else @drugsChargeAtPreviousRate end
				end as OriginalChargeRate,

			isnull(cpp.Code, '') as CurrentProductPackCode,
			isnull(civ.Quantity, 0) as CurrentQuantity,
			isnull(civ.BasicPrice, 0) as CurrentBasicPrice,
			isnull(civ.NumberOfProfessionalFees, 0) * @professionalFeeAmount as CurrentValueOfProfessionalFees,
			isnull(civ.ValueOfAdditionalFees, 0) as CurrentValueOfAdditionalFees,
			isnull(civ.ValueOfOutOfPocketExpenses, 0) as CurrentValueofOutOfPocketExpenses,
			case 
				when cgt.IsExemptFromCharges = 1 then 0 
				else civ.NumberOfCharges end as CurrentNumberOfCharges, 
			case 
				when cp.ProductTypeCode = 'A' then
					case
						when cgt.IsExemptFromCharges = 1 then 0 
						when cgt.IsOldRate = 0 then @applianceChargeAtCurrentRate 
						else @applianceChargeAtPreviousRate end
				else
					case
						when cgt.IsExemptFromCharges = 1 then 0 
						when cgt.IsOldRate = 0 then @drugsChargeAtCurrentRate 
						else @drugsChargeAtPreviousRate end
				end as CurrentChargeRate
				
		from dbo.Document d
			left join dbo.DocumentGroupType odgt 
				inner join dbo.GroupType ogt on odgt.GroupTypeCode = ogt.GroupTypeCode
			on d.OriginalDocumentGroupTypeID = odgt.DocumentGroupTypeID
			left join dbo.DocumentGroupType cdgt 
				inner join dbo.GroupType cgt on cdgt.GroupTypeCode = cgt.GroupTypeCode
			on d.CurrentDocumentGroupTypeID = cdgt.DocumentGroupTypeID
			inner join dbo.Item i
				left join dbo.ItemVersion oiv 
					left join dbo.ProductPackPeriodic oppp 
						inner join dbo.ProductPack opp 
							inner join dbo.Product op on opp.ProductID = op.ProductID
						on oppp.ProductPackID = opp.ProductPackID
					on oiv.ProductPackPeriodicID = oppp.ProductPackPeriodicID
				on i.OriginalItemVersionID = oiv.ItemVersionID
				left join dbo.ItemVersion civ 
					left join dbo.ProductPackPeriodic cppp 
						inner join dbo.ProductPack cpp 
							inner join dbo.Product cp on cpp.ProductID = cp.ProductID
						on cppp.ProductPackID = cpp.ProductPackID
					on civ.ProductPackPeriodicID = cppp.ProductPackPeriodicID
				on i.CurrentItemVersionID = civ.ItemVersionID
			on d.DocumentID = i.DocumentID
		where d.BatchID = @batchID
		  and civ.IsVerified = 1
	
	), summaryOfItemsWithDifferences as (
	
		select *,
			CurrentBasicPrice - OriginalBasicPrice as BasicPriceDifference,
			(CurrentBasicPrice + CurrentValueOfProfessionalFees + CurrentValueOfAdditionalFees + ROUND(CurrentChargeRate * (0 - CurrentNumberOfCharges), 2)) -
			(OriginalBasicPrice + OriginalValueOfProfessionalFees + OriginalValueOfAdditionalFees + ROUND(OriginalChargeRate * (0 - OriginalNumberOfCharges), 2)) as TotalAdjustment
		from itemsWithDifferences
		
	)
	select isnull(sum(case when OriginalProductPackCode != CurrentProductPackCode or TotalAdjustment != 0 or OriginalQuantity != CurrentQuantity then 1 else 0 end), 0) as itemsVerifiedOrConfirmedWithErrors,
		isnull(sum(case when OriginalProductPackCode != CurrentProductPackCode or TotalAdjustment != 0 or OriginalQuantity != CurrentQuantity then 0 else 1 end), 0) as itemsVerifiedOrConfirmedWithoutErrors
	from summaryOfItemsWithDifferences;
	
	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[BatchView_UserActivityExtractionCallback]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BatchView_UserActivityExtractionCallback]
	@batchID bigint = null

as

	set nocount on;
	set transaction isolation level read uncommitted;

	select ua.DateTime, Activity = a.Description, 
		FormNumber = isnull(d.FragmentTypeCode, '0') + right('00000' + convert(varchar, d.DocumentNumber), 5),
		Item = i.Sequence, Element = i.SubSequence,
		up.LoginName
	from dbo.UserActivity ua
		inner join dbo.Activity a on ua.ActivityID = a.ActivityID
		left join dbo.Batch b on ua.BatchID = b.BatchID
		left join dbo.Document d on ua.FormID = d.DocumentID
		left join dbo.Item i on ua.ItemID = i.ItemID
		left join dbo.UserProfile up on ua.UserProfileID = up.UserProfileID
	where @batchID is null or ua.BatchID = @batchID
	order by ua.DateTime

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[BatchView_UserListCallback]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BatchView_UserListCallback]
	@batchID bigint

as

	set nocount on;
	set transaction isolation level read uncommitted;

	select up.UserProfileID, up.LoginName
	from dbo.UserActivity ua
		inner join dbo.Activity a on ua.ActivityID = a.ActivityID and a.IsUpdate = 1
		inner join dbo.UserProfile up on ua.UserProfileID = up.UserProfileID
	where ua.BatchID = @batchID
	group by up.UserProfileID, up.LoginName
	order by up.LoginName;

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[BatchViewModel_SelectCallback]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BatchViewModel_SelectCallback]
	@batchID bigint,
	@userCan2ndCheck bit = 0,
	@endorsementCode varchar(2) = null,
	@productPackCode varchar(8) = null,
	@includeItemsWithAdvisories bit = 0,
	@includeItemsWithErrors bit = 0,
	@includeItemsWithNotes bit = 0,
	@includeReferredItems bit = 0,
	@includeUncheckedItems bit = 0,
	@includeSwitchedForms bit = 0,
	@includeUnconfirmedSwitch bit = 0,
	@includeUnconfirmedItems bit = 0,
	@includeItemsWithFeedback bit = 0,
	@includeRandomAccuracyCheckItems bit = 0

AS

	set nocount on;
	set transaction isolation level read uncommitted;

	/*
	 * This SP is called from the BatchViewModel.SelectCallback method and provides the data that is
	 * used for custom materialization of the required form list.
	 *
	 */


	with batchItems as (

		-- Why are we using isnull() on the IsVerified, IsReferred and IsConfimred bit fields when they are not nullable?
		-- Similarly, why are we sometimes casting the same to integer, only to check them against 1 or 0 (could this be a left over from a previous way of counting)?
		-- Both of these must be slowing down the selection of the list?

		select d.DocumentID,
			i.ItemID,
			cast (i.IsRandomAccuracyCheck as integer) as IsRandomAccuracyCheck,
			i.CurrentItemVersionID,
			isnull(cast(iv.IsConfirmed as integer), 0) as IsConfirmed,
			isnull(cast(iv.IsReferred as integer), 0) as IsReferred,
			isnull(cast(iv.IsVerified as integer), 0) as IsVerified,
			case when isnull(i.OriginalItemVersionID, 0) != isnull(i.CurrentItemVersionID, 0) then 1 else 0 end as HasError,
			case when isnull(i.OriginalItemVersionID, 0) != isnull(i.CurrentItemVersionID, 0) and isnull(cast(iv.IsConfirmed as integer), 0) = 0 then 1 else 0 end as HasUnconfirmedError,
			case when i.IsRandomAccuracyCheck = 1 and (dgt.IsConfirmed = 0 or iv.IsConfirmed = 0) then 1 else 0 end as HasUnconfirmedRandomAccuracyCheck,
			case when i.ItemID is not null and /* isnull(iv.IsConfirmed, 0) = 0 and isnull(iv.IsReferred, 0) = 0 and */ isnull(iv.IsVerified, 0) = 0 then 1 else 0 end as IsNotVerified,
			case when exists(select * from dbo.ItemAdvisory ia where ia.ItemID = i.ItemID) then 1 else 0 end as HasAdvisories,
			case when exists(select * from dbo.ItemNote ia where ia.ItemID = i.ItemID) then 1 else 0 end as HasNotes,
			cast (case when isnull(d.OriginalDocumentGroupTypeID, 0) != isnull(d.CurrentDocumentGroupTypeID, 0) then 1 else 0 end as bit) as IsSwitched,
			cast (case when isnull(d.OriginalDocumentGroupTypeID, 0) != isnull(d.CurrentDocumentGroupTypeID, 0) and isnull(dgt.IsConfirmed, 0) = 0 then 1 else 0 end as bit) as IsUnconfirmedSwitch,
			cast (isnull(dgt.IsVerified, 0) as bit) as IsChargeStatusVerified,
			cast (isnull(dgt.IsReferred, 0) as bit) as IsChargeStatusReferred,
			case when exists(select * from dbo.ItemVersionEndorsement ive inner join dbo.EndorsementPeriodic ep on ive.EndorsementPeriodicID = ep.EndorsementPeriodicID and ep.EndorsementCode in ('DA', 'RB') where ive.ItemVersionID = i.CurrentItemVersionID) then 1 else 0 end as IsBsaReferred,
			case when exists(select * from dbo.AuditFeedback af where af.ItemID = i.ItemID) then 1 else 0 end as HasAuditFeedback
		from dbo.Document d
			left join dbo.Item i
				left join dbo.ItemVersion iv on i.CurrentItemVersionID = iv.ItemVersionID
			on d.DocumentID = i.DocumentID
			left join dbo.DocumentGroupType dgt on d.CurrentDocumentGroupTypeID = dgt.DocumentGroupTypeID
		where d.BatchID = @batchID

	), summary as (
	
		select d.DocumentID as ID,
			d.DocumentNumber,
			d.FragmentTypeCode,
			d.SubmissionGroupCode,
			count(bi.ItemID) as ItemCount,
			sum(bi.HasAdvisories) as ItemsWithAdvisories,
			sum(bi.HasError) as ItemsWithErrors,
			case when @userCan2ndCheck = 0 then sum(bi.HasUnconfirmedError) else sum(bi.HasUnconfirmedError) + sum(bi.HasUnconfirmedRandomAccuracyCheck) end as ItemsWithUnconfirmedErrors,
			sum(bi.HasNotes) as ItemsWithNotes,
			sum(bi.IsConfirmed) as ConfirmedItemCount,
			sum(bi.IsReferred) as ReferredItemCount,
			sum(bi.IsNotVerified) as UncheckedItemCount,
			sum(bi.IsVerified) as VerifiedItemCount,
			sum(bi.HasAuditFeedback) as ItemsWithFeedback,
			bi.IsSwitched,
			bi.IsUnconfirmedSwitch,
			bi.IsChargeStatusVerified,
			bi.IsChargeStatusReferred,
			sum(isnull(bi.IsRandomAccuracyCheck, 0)) as ItemsSelectedForRandomAccuracyCheck
		from batchItems bi
			inner join dbo.Document d on bi.DocumentID = d.DocumentID
			left join dbo.ItemVersion iv 
				left join dbo.ProductPackPeriodic ppp
					inner join dbo.ProductPack pp on ppp.ProductPackID = pp.ProductPackID
				on iv.ProductPackPeriodicID = ppp.ProductPackPeriodicID
				left join dbo.ItemVersionEndorsement ive
					inner join dbo.EndorsementPeriodic ep on ive.EndorsementPeriodicID = ep.EndorsementPeriodicID and ep.EndorsementCode = @endorsementCode
				on iv.ItemVersionID = ive.ItemVersionID
			on bi.CurrentItemVersionID = iv.ItemVersionID
		where (@productPackCode is null or pp.Code = @productPackCode)
		  and (@endorsementCode is null or ep.EndorsementCode = @endorsementCode)
			and (@includeUncheckedItems = 0 or bi.IsBsaReferred = 0)
		group by d.DocumentID, d.DocumentNumber, d.FragmentTypeCode, d.SubmissionGroupCode, bi.IsSwitched, bi.IsUnconfirmedSwitch, IsChargeStatusVerified, IsChargeStatusReferred
	
	)
	select *
	from summary
	where (@includeItemsWithAdvisories = 0 and @includeItemsWithErrors = 0 and @includeItemsWithNotes = 0 and @includeReferredItems = 0 and @includeUncheckedItems = 0 and @includeSwitchedForms = 0 and @includeUnconfirmedSwitch = 0 and @includeUnconfirmedItems = 0 and @includeItemsWithFeedback = 0 and @includeRandomAccuracyCheckItems = 0)
	   or (@includeItemsWithAdvisories = 1 and summary.ItemsWithAdvisories > 0)
	   or (@includeItemsWithErrors = 1 and summary.ItemsWithErrors > 0)
	   or (@includeItemsWithNotes = 1 and summary.ItemsWithNotes > 0)
	   or (@includeReferredItems = 1 and (summary.ReferredItemCount > 0 or summary.IsChargeStatusReferred = 1))
	   or (@includeUncheckedItems = 1 and (summary.UncheckedItemCount > 0 or summary.IsChargeStatusVerified = 0))
		 or (@includeSwitchedForms = 1 and summary.IsSwitched = 1)
		 or (@includeUnconfirmedSwitch = 1 and summary.IsUnconfirmedSwitch = 1)
		 --or (@includeUnconfirmedItems = 1 and summary.ConfirmedItemCount < summary.ItemsWithErrors)
	   or (@includeUnconfirmedItems = 1 and ItemsWithUnconfirmedErrors > 0)
	   or (@includeItemsWithFeedback = 1 and summary.ItemsWithFeedback > 0)
	   or (@includeRandomAccuracyCheckItems = 1 and summary.ItemsSelectedForRandomAccuracyCheck > 0)
	order by FragmentTypeCode, DocumentNumber
	option (optimize for (@batchID unknown, @productPackCode unknown, @endorsementCode unknown));
		
	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[ConsolidatedAuditorAccuracyReport_Details]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ConsolidatedAuditorAccuracyReport_Details]
	@frDate date = '01 January 2013',
	@toDate date = '31 January 2013',
	@auditTypes varchar(max) = '1, 3',
	@userProfiles varchar(max) = '12, 13, 1, 10, 11, 7, 9, 2'
	
as

	-- See TFS7561

	set nocount on;
	set transaction isolation level read uncommitted;

	declare @commaIndex integer = -1;

	-- Split the audit types into a table...

	declare @auditTypeID integer;
	declare @auditType table (
		AuditTypeID bigint,
		primary key (AuditTypeID)
	);

	set @auditTypes = ltrim(rtrim(@auditTypes));
	while (len(@auditTypes) > 0) begin
		set @commaIndex = charindex(',', @auditTypes)
		if (@commaIndex > 0) begin
			set @auditTypeID = substring(@auditTypes, 1, @commaIndex - 1);
			insert into @auditType (AuditTypeID) values (@auditTypeID);
			set @auditTypes = substring(@auditTypes, @commaIndex + 1, len(@auditTypes) - (@commaIndex));
		end else begin
			set @auditTypeID = @auditTypes;
			insert into @auditType (AuditTypeID) values (@auditTypeID);
			set @auditTypes = '';
		end
		set @auditTypes = ltrim(rtrim(@auditTypes));
	end;

	declare @batches table (
		BatchID bigint primary key,
		FinancialPeriodID integer,
		ContractorCode varchar(10)
	);

	with secondCheckCompleted as (
		select BatchID, Completed = max(cast(DateTime as date))
		from BatchStatusHistory
		where BatchStatusID = 490
		group by BatchID
	)
	insert into @batches
	select b.BatchID, b.FinancialPeriodID, c.Code
	from secondCheckCompleted s
		inner join dbo.Batch b
			inner join @auditType at on b.AuditTypeID = at.AuditTypeID
			inner join dbo.Contractor c on b.ContractorID = c.ContractorID
		on s.BatchID = b.BatchID
	where b.BatchID >= 490
		and Completed between @frDate and dateadd(day, 1, @toDate);

	declare @userProfileID integer;
	declare @userProfile table (
		UserProfileID bigint,
		LoginName nvarchar(128)
		primary key (UserProfileID)
	);

	set @userProfiles = ltrim(rtrim(@userProfiles));
	while (len(@userProfiles) > 0) begin
		set @commaIndex = charindex(',', @userProfiles)
		if (@commaIndex > 0) begin
			set @userProfileID = substring(@userProfiles, 1, @commaIndex - 1);
			insert into @userProfile (UserProfileID) values (@userProfileID);
			set @userProfiles = substring(@userProfiles, @commaIndex + 1, len(@userProfiles) - (@commaIndex));
		end else begin
			set @userProfileID = @userProfiles;
			insert into @userProfile (UserProfileID) values (@userProfileID);
			set @userProfiles = '';
		end
		set @userProfiles = ltrim(rtrim(@userProfiles));
	end;

	update up1
	set LoginName = up2.LoginName
	from @userProfile up1
		inner join dbo.UserProfile up2 on up1.UserProfileID = up2.UserProfileID;


	declare @userActivity table (
		UserProfileID integer,
		BatchID bigint,
		NumberOfLinesVerified integer,
		NumberOfItemsInspected integer,
		NumberOfErrorsIdentified integer,
		NumberOfErrorsChanged integer,
		NumberOfRandomAccuracyCheckItemsSelected integer,
		NumberOfRandomAccuracyCheckItemsChanged integer,
		primary key (UserProfileID, BatchID)
	)

	insert into @userActivity
	select UserProfileID, BatchID,
		NumberOfLinesVerified = sum(NumberOfLinesVerified),
		NumberOfItemsInspected = sum(NumberOfItemsInspected),
		NumberOfErrorsIdentified = sum(NumberOfErrorsIdentified),
		NumberOfErrorsChanged = sum(NumberOfErrorsChanged),
		NumberOfRandomAccuracyCheckItemsSelected = sum(NumberOfRandomAccuracyCheckItemsSelected),
		NumberOfRandomAccuracyCheckItemsChanged = sum(NumberOfRandomAccuracyCheckItemsChanged)
	from (

	/* Items verified */

		select UserProfileID, BatchID,
			NumberOfLinesVerified = count(*),
			-- NumberOfElements = sum(NumberOfElements),
			NumberOfItemsInspected = sum(NumberOfItemsInspected),
			NumberOfErrorsIdentified = 0,
			NumberOfErrorsChanged = 0,
			NumberOfRandomAccuracyCheckItemsSelected = 0,
			NumberOfRandomAccuracyCheckItemsChanged = 0
		from (
			select UserProfileID, BatchID, DocumentID, Sequence,
				NumberOfElements = count(*),
				NumberOfItemsInspected = max(case when NumberOfTimesDispensed = 0 then 1 else NumberOfTimesDispensed end) + (count(*) - 1)
			from (
				select ua.UserProfileID, d.BatchID, d.DocumentID, i.Sequence, i.SubSequence, NumberOfTimesDispensed = max(iv.NumberOfTimesDispensed)
				from @batches b
					inner join dbo.Document d
						inner join dbo.Item i
							inner join dbo.ItemVersion iv
								inner join dbo.UserActivity ua on iv.ItemVersionID = ua.OldItemVersionID and ua.ActivityID = 4
							on i.ItemID = iv.ItemID
						on d.DocumentID = i.DocumentID
					on b.BatchID = d.BatchID
				group by ua.UserProfileID, d.BatchID, d.DocumentID, i.Sequence, i.SubSequence
			) ua
			group by UserProfileID, BatchID, DocumentID, Sequence
		) ua
		group by UserProfileID, BatchID

		union 

	/* Items modified */

		select UserProfileID, BatchID,
			NumberOfElements = 0,
			NumberOfItemsInspected = 0,
			NumberOfErrorsIdentified = count(*),
			NumberOfErrorsChanged = count(*) - sum(IsCurrentItemVersion),
			NumberOfRandomAccuracyCheckItemsSelected = 0,
			NumberOfRandomAccuracyCheckItemsChanged = 0
		from (
			select UserProfileID, BatchID, DocumentID, Sequence,
				IsCurrentItemVersion = min(IsCurrentItemVersion),
				IsFormChargeStatusChange = max(IsFormChargeStatusChange)
			from (
				select UserProfileID, b.BatchID,
					i.DocumentID, i.Sequence,
					IsCurrentItemVersion = case when isnull(i.CurrentItemVersionID, 0) = isnull(ua.NewItemVersionID, 0) then 1 else 0 end,
					IsFormChargeStatusChange = case when exists (select * from dbo.UserActivity where FormID = d.DocumentID and ActivityID = 18) then 1 else 0 end
				from @batches b
					inner join dbo.Document d
						inner join dbo.Item i
							inner join (
								select *
								from dbo.UserActivity ua1
								where ua1.UserActivityID in (
									select top 1 ua2.UserActivityID
									from dbo.UserActivity ua2
										inner join dbo.Item i2 on ua2.ItemID = i2.ItemID 
									where ua2.ItemID = ua1.ItemID
										and ua2.UserProfileID = ua1.UserProfileID
										and (ua2.ActivityID in (13, 15, 16))
									order by ua2.DateTime desc)
							) ua on i.ItemID = ua.ItemID and isnull(i.OriginalItemVersionID, 0) != isnull(ua.NewItemVersionID, 0)
						on d.DocumentID = i.DocumentID
					on b.BatchID = d.BatchID
			) ua
			group by UserProfileID, BatchID, DocumentID, Sequence
		) ua
		group by UserProfileID, BatchID

		union

	/* Random accuracy checks */
		select UserProfileID, BatchID, 
			NumberOfElements = 0,
			NumberOfItemsInspected = 0,
			NumberOfErrorsIdentified = 0,
			NumberOfErrorsChanged = 0,
			NumberOfRandomAccuracyCheckItemsSelected = count(*),
			NumberOfRandomAccuracyCheckItemsChanged = sum(case when IsChanged = 1 then 1 else 0 end)
		from (
			select UserProfileID, BatchID, DocumentID, Sequence, IsChanged = max(IsChanged)
			from (
				select ua.UserProfileID, b.BatchID, d.DocumentID, i.Sequence,
					IsChanged = case when OriginalItemVersionID != CurrentItemVersionID then 1 else 0 end
				from @batches b
					inner join dbo.Document d
						inner join dbo.Item i 
							inner join dbo.UserActivity ua on i.OriginalItemVersionID = ua.OldItemVersionID and ua.ActivityID = 4
						on d.DocumentID = i.DocumentID and i.IsRandomAccuracyCheck = 1
					on b.BatchID = d.BatchID
				) rc
				group by UserProfileID, BatchID, DocumentID, Sequence
		) rc
		group by UserProfileID, BatchID

	) ua 
	group by UserProfileID, BatchID;

	select up.UserProfileID, up.LoginName,
		NumberOfLinesVerified = sum(NumberOfLinesVerified), NumberOfItemsInspected = sum(NumberOfItemsInspected),
		NumberOfErrorsIdentified = sum(NumberOfErrorsIdentified), NumberOfErrorsChanged = sum(NumberOfErrorsChanged),
		NumberOfItemsEligibleForRandomAccuracyCheck = sum(NumberOfLinesVerified - NumberOfErrorsIdentified),
		NumberOfRandomAccuracyCheckItemsSelected = sum(NumberOfRandomAccuracyCheckItemsSelected),
		NumberOfRandomAccuracyCheckItemsChanged = sum(NumberOfRandomAccuracyCheckItemsChanged)

	from (select UserProfileID, BatchID from @batches, @userProfile) x
		inner join @batches b on x.BatchID = b.BatchID
		inner join @userProfile up on x.UserProfileID = up.UserProfileID
		inner join @userActivity ua on x.BatchID = ua.BatchID and x.UserProfileID = ua.UserProfileID

	group by up.UserProfileID, up.LoginName
	order by up.LoginName;

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[ConsolidatedAuditorPerformanceReport_Details]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ConsolidatedAuditorPerformanceReport_Details]
	@frDate date = '01 January 2013',
	@toDate date = '31 January 2013',
	@auditTypes varchar(max) = '1, 3',
	@userProfiles varchar(max) = '12, 13, 1, 10, 11, 7, 9, 2'
	
as

	-- See TFS6220

	set nocount on;
	set transaction isolation level read uncommitted;

	declare @commaIndex integer = -1;

	declare @auditTypeID integer;
	declare @auditType table (
		AuditTypeID bigint,
		primary key (AuditTypeID)
	);

	set @auditTypes = ltrim(rtrim(@auditTypes));
	while (len(@auditTypes) > 0) begin
		set @commaIndex = charindex(',', @auditTypes)
		if (@commaIndex > 0) begin
			set @auditTypeID = substring(@auditTypes, 1, @commaIndex - 1);
			insert into @auditType (AuditTypeID) values (@auditTypeID);
			set @auditTypes = substring(@auditTypes, @commaIndex + 1, len(@auditTypes) - (@commaIndex));
		end else begin
			set @auditTypeID = @auditTypes;
			insert into @auditType (AuditTypeID) values (@auditTypeID);
			set @auditTypes = '';
		end
		set @auditTypes = ltrim(rtrim(@auditTypes));
	end;

	declare @userProfileID integer;
	declare @userProfile table (
		UserProfileID bigint,
		LoginName nvarchar(128)
		primary key (UserProfileID)
	);

	set @userProfiles = ltrim(rtrim(@userProfiles));
	while (len(@userProfiles) > 0) begin
		set @commaIndex = charindex(',', @userProfiles)
		if (@commaIndex > 0) begin
			set @userProfileID = substring(@userProfiles, 1, @commaIndex - 1);
			insert into @userProfile (UserProfileID) values (@userProfileID);
			set @userProfiles = substring(@userProfiles, @commaIndex + 1, len(@userProfiles) - (@commaIndex));
		end else begin
			set @userProfileID = @userProfiles;
			insert into @userProfile (UserProfileID) values (@userProfileID);
			set @userProfiles = '';
		end
		set @userProfiles = ltrim(rtrim(@userProfiles));
	end;

	update up1
	set LoginName = up2.LoginName
	from @userProfile up1
		inner join dbo.UserProfile up2 on up1.UserProfileID = up2.UserProfileID;

	-- We are only interested in the first occurance of each activity for each item
	-- this way, we only count each activity once (even across periods), and the totals
	-- balance with the consolidated report for the same period.

	declare @userActivity table (
		UserProfileID integer,
		DocumentID bigint,
		Sequence integer,
		ActivityID integer,
		NumberOfElements integer,
		ItemsInspected integer
		primary key (ActivityID, UserProfileID, DocumentID, Sequence)
	);

	-- We find the first occurance of each activity for each item per user, we then select those first occurances that were
	-- within the defined time period, and group them by document/Item, counting the number of elements included and
	-- calculating the number of actual items inspected...

	-- ItemsInspected = [Number of times dispensed] + [Number of additional elements on the item]

	with ua as (
		select *
		from (
			select ua.UserProfileID, ua.ItemID, ua.ActivityID,
				DateTime = min(ua.DateTime)
			from @userProfile up 
				inner join dbo.UserActivity ua on up.UserProfileID = ua.UserProfileID
			where ua.ActivityID in (4, 13, 6)
			group by ua.UserProfileID, ItemID, ActivityID
		) ua
		where DateTime between @frDate and dateadd(day, 1, @toDate)
	)

	insert into @userActivity
	select UserProfileID, DocumentID, Sequence, ActivityID,
			NumberOfElements,
			ItemsInspected 
	from (
		select ua.UserProfileID, i.DocumentID, i.Sequence, ua.ActivityID,
			DateTime = min(ua.DateTime),
			NumberOfElements = count(*),
			ItemsInspected = max(case when iv.NumberOfTimesDispensed = 0 then 1 else iv.NumberOfTimesDispensed end) + (count(*) - 1)
		from ua
			inner join dbo.Item i
				inner join dbo.Document d
					inner join dbo.Batch b 
						inner join @auditType at on b.AuditTypeID = at.AuditTypeID
					on d.BatchID = b.BatchID
				on i.DocumentID = d.DocumentID
				inner join dbo.ItemVersion iv on i.CurrentItemVersionID = iv.ItemVersionID
			on ua.ItemID = i.ItemID
		group by ua.UserProfileID, i.DocumentID, i.Sequence, ua.ActivityID
	) ua
	order by UserProfileID, DocumentID, Sequence, ActivityID
	option (optimize for (@frDate unknown, @toDate unknown));

	-- We now remove any "New item version created" rows for items where the same user has subsequently reversed that
	-- decision and restored to the ORIGINAL (BSA) item version...

	delete ua
	from @userActivity ua
		inner join dbo.Item i 
			inner join (
				select *
				from dbo.UserActivity ua1
				where ua1.UserActivityID in (
					select top 1 ua2.UserActivityID
					from dbo.UserActivity ua2
						inner join dbo.Item i2 on ua2.ItemID = i2.ItemID
					where ua2.ItemID = ua1.ItemID
					  and ua2.UserProfileID = ua1.UserProfileID
						and (ua2.ActivityID = 13
						 or (ua2.ActivityID = 15 and ua2.NewItemVersionID = i2.OriginalItemVersionID))
					order by ua2.DateTime desc)
			) r on i.ItemID = r.ItemID
		on ua.UserProfileID = r.UserProfileID and ua.DocumentID = i.DocumentID and ua.Sequence = i.Sequence
	where ua.ActivityID = 13
	  and r.ActivityID = 15;

	-- We now add in the items affected by form charge status changes.
	-- We are looking for a form charge status activity from the user, combined with a difference in the price
	-- of an item (this way, we can also pick up current/old rate changes)...

	with chargeStatusSwitchingActivities as (

		select *
		from (
			select up.UserProfileID, ua.FormID, ua.ActivityID,
				DateTime = min(ua.DateTime)
			from @userProfile up 
				inner join dbo.UserActivity ua on up.UserProfileID = ua.UserProfileID
			where ua.ActivityID = 18
			group by up.UserProfileID, ua.FormID, ua.ActivityID
		) ua
		where DateTime between @frDate and dateadd(day, 1, @toDate)

	)
		
	insert into @userActivity
	select UserProfileID, ua.DocumentID, ua.Sequence, ua.ActivityID,
		NumberOfElements = count(*),
		ItemsInspected = max(case when ua.NumberOfTimesDispensed = 0 then 1 else ua.NumberOfTimesDispensed end) + (count(*) - 1)
	from (
		select UserProfileID, DocumentID, Sequence, ActivityID, DateTime, NumberOfTimesDispensed,
			IsPatientChargeChange = case when OriginalProductPackPeriodicID is not null and CurrentProductPackPeriodicID is not null then 1 else 0 end,
			OriginalValueOfCharges, CurrentValueOfCharges
		from (
			select UserProfileID, i.DocumentID, i.Sequence, chargeStatusSwitchingActivities.ActivityID, chargeStatusSwitchingActivities.DateTime, civ.NumberOfTimesDispensed,
				OriginalProductPackPeriodicID = oiv.ProductPackPeriodicID,
				OriginalValueOfCharges = case
					when d.OriginalDocumentGroupTypeID is null or ogt.IsExemptFromCharges = 1 then 0.00
					else isnull(oiv.NumberOfCharges, 0) * case 
						when op.ProductTypeCode = 'A' then case
							when ogt.IsOldRate = 0 then (
								select jp.ApplianceItemPrescriptionChargeValue 
								from dbo.JurisdictionPeriodic jp 
								where jp.JurisdictionID = c.JurisdictionID and jp.FinancialPeriodID = b.FinancialPeriodID
								)
							else (
								select top 1 jp.ApplianceItemPrescriptionChargeValue 
								from dbo.JurisdictionPeriodic jp 
								where jp.JurisdictionID = c.JurisdictionID and jp.FinancialPeriodID <= b.FinancialPeriodID and jp.ApplianceItemPrescriptionChargeValue != (
									select jp.ApplianceItemPrescriptionChargeValue 
									from dbo.JurisdictionPeriodic jp 
									where jp.JurisdictionID = c.JurisdictionID and jp.FinancialPeriodID = b.FinancialPeriodID
								)
								order by jp.FinancialPeriodID desc) 
							end
						else case
							when ogt.IsOldRate = 0 then (
								select jp.DrugItemPrescriptionChargeValue 
								from dbo.JurisdictionPeriodic jp 
								where jp.JurisdictionID = c.JurisdictionID and jp.FinancialPeriodID = b.FinancialPeriodID
								)
							else (
								select top 1 jp.DrugItemPrescriptionChargeValue 
								from dbo.JurisdictionPeriodic jp 
								where jp.JurisdictionID = c.JurisdictionID and jp.FinancialPeriodID <= b.FinancialPeriodID and jp.DrugItemPrescriptionChargeValue != (
									select jp.DrugItemPrescriptionChargeValue 
									from dbo.JurisdictionPeriodic jp 
									where jp.JurisdictionID = c.JurisdictionID and jp.FinancialPeriodID = b.FinancialPeriodID
								)
								order by jp.FinancialPeriodID desc) 
							end 
						end
					end,
				CurrentProductPackPeriodicID = civ.ProductPackPeriodicID,
				CurrentValueOfCharges = case
					when d.CurrentDocumentGroupTypeID is null or cgt.IsExemptFromCharges = 1 then 0.00
					else isnull(civ.NumberOfCharges, 0) * case 
						when cp.ProductTypeCode = 'A' then case
							when cgt.IsOldRate = 0 then (
								select jp.ApplianceItemPrescriptionChargeValue 
								from dbo.JurisdictionPeriodic jp 
								where jp.JurisdictionID = c.JurisdictionID and jp.FinancialPeriodID = b.FinancialPeriodID
								)
							else (
								select top 1 jp.ApplianceItemPrescriptionChargeValue 
								from dbo.JurisdictionPeriodic jp 
								where jp.JurisdictionID = c.JurisdictionID and jp.FinancialPeriodID <= b.FinancialPeriodID and jp.ApplianceItemPrescriptionChargeValue != (
									select jp.ApplianceItemPrescriptionChargeValue 
									from dbo.JurisdictionPeriodic jp 
									where jp.JurisdictionID = c.JurisdictionID and jp.FinancialPeriodID = b.FinancialPeriodID
								)
								order by jp.FinancialPeriodID desc) 
							end
						else case
							when cgt.IsOldRate = 0 then (
								select jp.DrugItemPrescriptionChargeValue 
								from dbo.JurisdictionPeriodic jp 
								where jp.JurisdictionID = c.JurisdictionID and jp.FinancialPeriodID = b.FinancialPeriodID
								)
							else (
								select top 1 jp.DrugItemPrescriptionChargeValue 
								from dbo.JurisdictionPeriodic jp 
								where jp.JurisdictionID = c.JurisdictionID and jp.FinancialPeriodID <= b.FinancialPeriodID and jp.DrugItemPrescriptionChargeValue != (
									select jp.DrugItemPrescriptionChargeValue 
									from dbo.JurisdictionPeriodic jp 
									where jp.JurisdictionID = c.JurisdictionID and jp.FinancialPeriodID = b.FinancialPeriodID
								)
								order by jp.FinancialPeriodID desc) 
							end 
						end
					end
			from chargeStatusSwitchingActivities
				inner join dbo.Document d
					inner join dbo.Batch b
						inner join @auditType at on b.AuditTypeID = at.AuditTypeID
						inner join dbo.Contractor c on b.ContractorID = c.ContractorID
					on d.BatchID = b.BatchID
					left join dbo.DocumentGroupType odgt 
						inner join dbo.GroupType ogt on odgt.GroupTypeCode = ogt.GroupTypeCode
					on d.OriginalDocumentGroupTypeID = odgt.DocumentGroupTypeID
					inner join dbo.DocumentGroupType cdgt 
						inner join dbo.GroupType cgt on cdgt.GroupTypeCode = cgt.GroupTypeCode
					on d.CurrentDocumentGroupTypeID = cdgt.DocumentGroupTypeID
					inner join dbo.Item i
						left join dbo.ItemVersion oiv
							left join dbo.ProductPackPeriodic oppp 
								inner join dbo.ProductPeriodic opp 
									inner join dbo.Product op on opp.ProductID = op.ProductID
								on oppp.ProductPeriodicID = opp.ProductPeriodicID
							on oiv.ProductPackPeriodicID = oppp.ProductPackPeriodicID
						on i.OriginalItemVersionID = oiv.ItemVersionID
						left join dbo.ItemVersion civ
							left join dbo.ProductPackPeriodic cppp 
								inner join dbo.ProductPeriodic cpp 
									inner join dbo.Product cp on cpp.ProductID = cp.ProductID
								on cppp.ProductPeriodicID = cpp.ProductPeriodicID
							on civ.ProductPackPeriodicID = cppp.ProductPackPeriodicID
						on i.CurrentItemVersionID = civ.ItemVersionID
					on d.DocumentID = i.DocumentID
				on chargeStatusSwitchingActivities.FormID = d.DocumentID
		) ua
	) ua
	where IsPatientChargeChange = 1 and OriginalValueOfCharges != CurrentValueOfCharges
	group by UserProfileID, ua.DocumentID, ua.Sequence, ua.ActivityID;

	-- Finally, summarise the results...

	with verifiedItems as (

		select UserProfileID,
			Lines = count(*),
			ItemsInspected = sum(ItemsInspected)
		from @userActivity ua 
		where ua.ActivityID = 4
		group by UserProfileID

	), errorCount as (

		select UserProfileID,
			Lines = count(*) -- sum(NumberOfElements) if we want to count each element updated
		from (
			select UserProfileID,
				NumberOfElements = max(ua.NumberOfElements)
			from @userActivity ua
			where ua.ActivityID in (13, 18)
			group by UserProfileID, DocumentID, Sequence
		) ua
		group by UserProfileID

	), referredCount as (

		select UserProfileID,
			Lines = count(*) -- sum(NumberOfElements) if we want to count each element updated
		from @userActivity ua
		where ua.ActivityID = 6
		group by UserProfileID

	)

	-- Driven from the date sequences, we now tot-up the totals for each period
	-- this way, we include periods where the user may not have done anything (annual leave etc.)
	-- and that stands out.

	select up.UserProfileID, up.LoginName,
		NumberOfLines = isnull(vi.Lines, 0),
		NumberOfItemsInspected = isnull(vi.ItemsInspected, 0),
		NumberOfErrors = isnull(ec.Lines, 0),
		NumberOfReferrals = isnull(rc.Lines, 0)
	from @userProfile up
		left join verifiedItems vi on up.UserProfileID = vi.UserProfileID
		left join errorCount ec on up.UserProfileID = ec.UserProfileID
		left join referredCount rc on up.UserProfileID = rc.UserProfileID
	order by up.LoginName asc

	return @@error;
GO
/****** Object:  StoredProcedure [dbo].[ContractorEdit_CheckForActiveBatch]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ContractorEdit_CheckForActiveBatch]
	@contractorID bigint,
	@activeBatchCount integer output

as

	set nocount on;
	set transaction isolation level read uncommitted;

	select @activeBatchCount = count(*)
	from dbo.Batch b
	where b.ContractorID = @contractorID
	  and b.BatchStatusID != 100 -- Schedule of Payments loaded
		and b.BatchStatusID != 900 -- Completed

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[ContractorTrendAnalysis_SelectCallback]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ContractorTrendAnalysis_SelectCallback]
	@contractorID bigint,
	@frFinancialPeriodID integer,
	@toFinancialPeriodID integer

as

	set nocount on;
	set transaction isolation level read uncommitted;

	declare @batchFrFinancialPeriodID integer;
	select top 1 @batchFrFinancialPeriodID = FinancialPeriodID
	from dbo.FinancialPeriod
	where FinancialPeriodID < @frFinancialPeriodID
	order by FinancialPeriodID desc;

	with nationalStatistics as (
	
		select b.FinancialPeriodID,
			count(b.BatchID) as NumberOfContractors,
			sum(data.TotalOfItemsForWhichAFeeIsPaid) as TotalOfItemsForWhichAFeeIsPaid,
			sum(data.AverageItemValue) as AverageItemValue
		from dbo.Batch b
			inner join dbo.Schedule s	
				inner join dbo.ScheduleCharge charge on s.ScheduleID = charge.ScheduleID
				inner join dbo.ScheduleCost cost on s.ScheduleID = cost.ScheduleID
				inner join dbo.ScheduleDocumentData data on s.ScheduleID = data.ScheduleID
				inner join dbo.ScheduleFee fee on s.ScheduleID = fee.ScheduleID
			on b.ScheduleID = s.ScheduleID
		where b.FinancialPeriodID between @frFinancialPeriodID and @toFinancialPeriodID
		group by b.FinancialPeriodID
	
	), contractorBatches as (
		
		select row_number() over (order by FinancialPeriodID) as rowNumber,
			b.FinancialPeriodID,
			s.PaymentOnAccountForItems as AdvanceOfItemsForWhichAFeeIsPaid, s.TotalAmountAuthorisedByPPD, s.TotalOfChargesIncludingFP57Refunds, s.TotalOfDrugAndApplianceCostsPlusFees, 
			cost.TotalOfBasicPricesAtStandardDiscountRate, cost.TotalOfBasicPricesAtZeroDiscount, TotalOfBasicPrices = cost.TotalOfBasicPricesAtStandardDiscountRate + cost.TotalOfBasicPricesAtZeroDiscount,
			data.AverageItemValue, data.DispensingStaffNumberOfHoursDeclared, data.ItemsAtZeroDiscountRateForWhichAFeeIsPaid, data.ReferredBackItems, data.TotalOfItemsForWhichAFeeIsPaid, 
			NumberOfItemsAboveLowerLimit = isnull(eis.NumberOfItemsAboveLowerLimit, 0), ValueOfItemsAboveLowerLimit = isnull(eis.ValueOfItemsAboveLowerLimit, 0),
			fee.TotalOfAllFees, fee.OtherFeesMedicinalUseReview, 
			
			round(case when data.TotalOfItemsForWhichAFeeIsPaid != 0 then ((fee.TotalOfAllFees - fee.OtherFeesMedicinalUseReview) / data.TotalOfItemsForWhichAFeeIsPaid) else 0 end, 2) as AverageFeePerItem,
			dbo.CalculateNumberOfHoursRequiredToQualifyForFullPracticePayment(b.FinancialPeriodID, c.JurisdictionID, fee.ProfessionalFeeQuantity) as DispensingStaffNumberOfHoursRequired,
			isnull((select sum(gss.Number) from dbo.ScheduleGroupSwitchSummary gss where b.ScheduleID = gss.ScheduleID), 0) as TotalNumberOfSwitchedItems,
			s.TotalOfDrugAndApplianceCostsPlusFees - s.TotalOfAllFees - isnull(eis.ValueOfItemsAboveLowerLimit, 0) as ValueOfItemsBelowLowerLimit
			
		from dbo.Batch b
			inner join dbo.Contractor c on b.ContractorID = c.ContractorID
			inner join dbo.Schedule s	
				inner join dbo.ScheduleCost cost on s.ScheduleID = cost.ScheduleID
				inner join dbo.ScheduleDocumentData data on s.ScheduleID = data.ScheduleID
				inner join dbo.ScheduleExpensiveItemSummary eis on s.ScheduleID = eis.ScheduleID
				inner join dbo.ScheduleFee fee on s.ScheduleID = fee.ScheduleID
			on b.ScheduleID = s.ScheduleID
		where b.ContractorID = @contractorID
		  and b.FinancialPeriodID between @batchFrFinancialPeriodID and @toFinancialPeriodID
		
	)
	
	select ns.FinancialPeriodID as Period,

		/*
		 * Note: some result columns are duplicated and then commented out below (using "/*" & "*/"); this is deliberate in order to make it easier to identify which
		 * result columns are used in each of the tables (and supporting charts). I know it is additional text, but I believe it makes it easier to read and understand.
		 *
		 * Edit 07/11/2012: Divide by zero errors where a contractor does not have data for all requested periods.
		 * Original code has been commented out (using "--") and the replacement code placed directly underneath in order to provide future comparison in the event that
		 * the replacement code is not producing the same results.
		 *
		 */
	
		currentBatch.TotalOfItemsForWhichAFeeIsPaid,
		ns.TotalOfItemsForWhichAFeeIsPaid as NationalTotalOfItemsForWhichAFeeIsPaid,
		ns.NumberOfContractors,
		(ns.TotalOfItemsForWhichAFeeIsPaid / ns.NumberOfContractors) as NationalAverageOfItemsForWhichAFeeIsPaid,
		
		currentBatch.AverageItemValue,
		round(ns.AverageItemValue / ns.NumberOfContractors, 2) as NationalAverageItemValue,
		
		/* currentBatch.TotalOfItemsForWhichAFeeIsPaid, */
		currentBatch.TotalOfAllFees,
		currentBatch.OtherFeesMedicinalUseReview,
		currentBatch.AverageFeePerItem,
		
		currentBatch.DispensingStaffNumberOfHoursDeclared,
		currentBatch.DispensingStaffNumberOfHoursRequired,
		/* currentBatch.TotalOfItemsForWhichAFeeIsPaid, */
		
		AdvanceOfItemsForWhichAFeeIsPaid = isnull(previousBatch.AdvanceOfItemsForWhichAFeeIsPaid, 0),
		/* currentBatch.TotalOfItemsForWhichAFeeIsPaid, */
		currentBatch.ReferredBackItems,
		--(previousBatch.AdvanceOfItemsForWhichAFeeIsPaid + currentBatch.ReferredBackItems) - currentBatch.TotalOfItemsForWhichAFeeIsPaid as DifferenceInAdvanceOfItemsForWhichAFeeIsPaid,
		DifferenceInAdvanceOfItemsForWhichAFeeIsPaid = (currentBatch.TotalOfItemsForWhichAFeeIsPaid + currentBatch.ReferredBackItems) - isnull(previousBatch.AdvanceOfItemsForWhichAFeeIsPaid, 0),
		
		currentBatch.TotalNumberOfSwitchedItems,
		
		currentBatch.TotalOfDrugAndApplianceCostsPlusFees + currentBatch.TotalOfChargesIncludingFP57Refunds as TotalOfDrugAndApplianceCostsPlusFeesLessCharges,
		currentBatch.TotalAmountAuthorisedByPPD,
		(select sum(b.TotalAmountAuthorisedByPPD - (b.TotalOfDrugAndApplianceCostsPlusFees + b.TotalOfChargesIncludingFP57Refunds)) 
			from contractorBatches b 
			where b.rowNumber <= currentBatch.rowNumber) as AccumulativeDifferenceInPayment,
		
		
		--round((currentBatch.ValueOfItemsAboveLowerLimit / (currentBatch.TotalOfBasicPricesAtStandardDiscountRate + currentBatch.TotalOfBasicPricesAtZeroDiscount)) * 100, 1) as ExpensiveItemsPercentageOfIngredientCost,
		round(case when currentBatch.TotalOfBasicPrices != 0 
			then (currentBatch.ValueOfItemsAboveLowerLimit / currentBatch.TotalOfBasicPrices) * 100 
			else 0 end, 1) as ExpensiveItemsPercentageOfIngredientCost,
		--round((currentBatch.TotalOfBasicPricesAtStandardDiscountRate + currentBatch.TotalOfBasicPricesAtZeroDiscount) / currentBatch.TotalOfItemsForWhichAFeeIsPaid, 2) as AverageIngredientCostPerItem,
		round(case when currentBatch.TotalOfItemsForWhichAFeeIsPaid != 0 
			then (currentBatch.TotalOfBasicPrices / currentBatch.TotalOfItemsForWhichAFeeIsPaid) -- * 100 
			else 0 end, 2) as AverageIngredientCostPerItem,
		case when currentBatch.NumberOfItemsAboveLowerLimit > 0 
			then currentBatch.ValueOfItemsAboveLowerLimit / currentBatch.NumberOfItemsAboveLowerLimit 
			else 0 end as IngredientCostPerExpensiveItem,
		case when (currentBatch.TotalOfItemsForWhichAFeeIsPaid - currentBatch.NumberOfItemsAboveLowerLimit) != 0
			then isnull((currentBatch.TotalOfBasicPrices - currentBatch.ValueOfItemsAboveLowerLimit) / (currentBatch.TotalOfItemsForWhichAFeeIsPaid - currentBatch.NumberOfItemsAboveLowerLimit), 0.00)
			else 0.00 end as IngredientCostPerNonExpensiveItem,
		
		--round((currentBatch.TotalOfBasicPricesAtZeroDiscount / (currentBatch.TotalOfBasicPricesAtStandardDiscountRate + currentBatch.TotalOfBasicPricesAtZeroDiscount)) * 100, 1) as ZeroDiscountPercentageOfIngredientCost,
		round(case when currentBatch.TotalOfBasicPrices != 0 
			then (currentBatch.TotalOfBasicPricesAtZeroDiscount / currentBatch.TotalOfBasicPrices) * 100 
			else 0 end, 1) as ZeroDiscountPercentageOfIngredientCost,
		/* round((currentBatch.TotalOfBasicPricesAtStandardDiscountRate + currentBatch.TotalOfBasicPricesAtZeroDiscount) / currentBatch.TotalOfItemsForWhichAFeeIsPaid, 2) as AverageIngredientCostPerItem, */
		--currentBatch.TotalOfBasicPricesAtZeroDiscount / currentBatch.ItemsAtZeroDiscountRateForWhichAFeeIsPaid as IngredientCostPerZeroDiscountItem,
		case when currentBatch.ItemsAtZeroDiscountRateForWhichAFeeIsPaid != 0 
			then (currentBatch.TotalOfBasicPricesAtZeroDiscount / currentBatch.ItemsAtZeroDiscountRateForWhichAFeeIsPaid) -- * 100 
			else 0 end as IngredientCostPerZeroDiscountItem,
		
		isnull(currentBatch.ValueOfItemsAboveLowerLimit, 0.00) as ValueOfItemsAboveLowerLimit,
		isnull(currentBatch.ValueOfItemsBelowLowerLimit, 0.00) as ValueOfItemsBelowLowerLimit,
		--round((currentBatch.ValueOfItemsAboveLowerLimit / (currentBatch.TotalOfDrugAndApplianceCostsPlusFees - currentBatch.TotalOfAllFees)) * 100, 2) as PercentageOfValueOfItemsAboveLowerLimit,
		round(case when (currentBatch.TotalOfDrugAndApplianceCostsPlusFees - currentBatch.TotalOfAllFees) != 0 
			then (currentBatch.ValueOfItemsAboveLowerLimit / (currentBatch.TotalOfDrugAndApplianceCostsPlusFees - currentBatch.TotalOfAllFees)) * 100 
			else 0 end, 0) as PercentageOfValueOfItemsAboveLowerLimit,
		--round((currentBatch.ValueOfItemsBelowLowerLimit / (currentBatch.TotalOfDrugAndApplianceCostsPlusFees - currentBatch.TotalOfAllFees)) * 100, 2) as PercentageOfValueOfItemsBelowLowerLimit,
		round(case when (currentBatch.TotalOfDrugAndApplianceCostsPlusFees - currentBatch.TotalOfAllFees) != 0 
			then (currentBatch.ValueOfItemsBelowLowerLimit / (currentBatch.TotalOfDrugAndApplianceCostsPlusFees - currentBatch.TotalOfAllFees)) * 100 
			else 0 end, 0) as PercentageOfValueOfItemsBelowLowerLimit,
		
		currentBatch.TotalOfBasicPricesAtZeroDiscount as TotalOfBasicPricesAtZeroDiscountRate,
		currentBatch.TotalOfBasicPricesAtStandardDiscountRate,
		--round((currentBatch.TotalOfBasicPricesAtZeroDiscount / (currentBatch.TotalOfBasicPricesAtStandardDiscountRate + currentBatch.TotalOfBasicPricesAtZeroDiscount)) * 100, 2) as PercentageOfBasicPricesAtZeroDiscountRate,
		round(case when currentBatch.TotalOfBasicPrices != 0 
			then (currentBatch.TotalOfBasicPricesAtZeroDiscount / currentBatch.TotalOfBasicPrices) * 100 
			else 0 end, 0) as PercentageOfBasicPricesAtZeroDiscountRate,
		--round((currentBatch.TotalOfBasicPricesAtStandardDiscountRate / (currentBatch.TotalOfBasicPricesAtStandardDiscountRate + currentBatch.TotalOfBasicPricesAtZeroDiscount)) * 100, 2) as PercentageOfBasicPricesAtStandardDiscountRate
		round(case when currentBatch.TotalOfBasicPrices != 0 
			then (currentBatch.TotalOfBasicPricesAtStandardDiscountRate / currentBatch.TotalOfBasicPrices) * 100 
			else 0 end, 0) as PercentageOfBasicPricesAtStandardDiscountRate
		 
	from nationalStatistics ns
		left join contractorBatches currentBatch
			left join contractorBatches previousBatch on currentBatch.rowNumber = previousBatch.rowNumber + 1
		on ns.FinancialPeriodID = currentBatch.FinancialPeriodID
	order by ns.FinancialPeriodID;
	return @@error;
GO
/****** Object:  StoredProcedure [dbo].[CopyControlledSubstanceIndicatorPeriodicFromPeriodToPeriod]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CopyControlledSubstanceIndicatorPeriodicFromPeriodToPeriod]
	@fromFinancialPeriodID integer = null,
	@toFinancialPeriodID integer,
	@jurisdictionID integer

as

	set nocount on;

	if (@fromFinancialPeriodID is null)
		select @fromFinancialPeriodID = max(FinancialPeriodID) 
		from dbo.FinancialPeriod
		where FinancialPeriodID < @toFinancialPeriodID;

	insert into dbo.ControlledSubstanceIndicatorPeriodic (
		JurisdictionID, FinancialPeriodID, ControlledSubstanceIndicatorCode, AdditionalProfessionalFeeValue)
	select JurisdictionID, @toFinancialPeriodID, ControlledSubstanceIndicatorCode, AdditionalProfessionalFeeValue
	from dbo.ControlledSubstanceIndicatorPeriodic
	where FinancialPeriodID = @fromFinancialPeriodID and JurisdictionID = @jurisdictionID;

	return;

GO
/****** Object:  StoredProcedure [dbo].[CopyControlledSubstanceIndicatorPeriodicFromPeriodToPeriodNoJurisdiction]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CopyControlledSubstanceIndicatorPeriodicFromPeriodToPeriodNoJurisdiction]
	@fromFinancialPeriodID integer = null,
	@toFinancialPeriodID integer

as

	set nocount on;

	if (@fromFinancialPeriodID is null)
		select @fromFinancialPeriodID = max(FinancialPeriodID) 
		from dbo.FinancialPeriod
		where FinancialPeriodID < @toFinancialPeriodID;

	insert into dbo.ControlledSubstanceIndicatorPeriodic (
		JurisdictionID, FinancialPeriodID, ControlledSubstanceIndicatorCode, AdditionalProfessionalFeeValue)
	select JurisdictionID, @toFinancialPeriodID, ControlledSubstanceIndicatorCode, AdditionalProfessionalFeeValue
	from dbo.ControlledSubstanceIndicatorPeriodic
	where FinancialPeriodID = @fromFinancialPeriodID --and JurisdictionID = @jurisdictionID;

	return;

GO
/****** Object:  StoredProcedure [dbo].[CopyEndorsementPeriodicFromPeriodToPeriod]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CopyEndorsementPeriodicFromPeriodToPeriod]
	@fromFinancialPeriodID integer = null,
	@toFinancialPeriodID integer,
	@jurisdictionID integer

as

	set nocount on;

	if (@fromFinancialPeriodID is null)
		select @fromFinancialPeriodID = max(FinancialPeriodID) 
		from dbo.FinancialPeriod
		where FinancialPeriodID < @toFinancialPeriodID;

	insert into dbo.EndorsementPeriodic (
		JurisdictionID, FinancialPeriodID, EndorsementCode, Description, ZeroCharges, SpecialFeeValue, SpecialFeeBasis, IsUserSelectable, DisplaySequence)
	select JurisdictionID, @toFinancialPeriodID, EndorsementCode, Description, ZeroCharges, SpecialFeeValue, SpecialFeeBasis, IsUserSelectable, DisplaySequence
	from dbo.EndorsementPeriodic
	where FinancialPeriodID = @fromFinancialPeriodID and JurisdictionID = @jurisdictionID;

	return;

GO
/****** Object:  StoredProcedure [dbo].[CopyEndorsementPeriodicFromPeriodToPeriodNoJurisdiction]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CopyEndorsementPeriodicFromPeriodToPeriodNoJurisdiction]
	@fromFinancialPeriodID integer = null,
	@toFinancialPeriodID integer

as

	set nocount on;

	if (@fromFinancialPeriodID is null)
		select @fromFinancialPeriodID = max(FinancialPeriodID) 
		from dbo.FinancialPeriod
		where FinancialPeriodID < @toFinancialPeriodID;

	insert into dbo.EndorsementPeriodic (
		JurisdictionID, FinancialPeriodID, EndorsementCode, Description, ZeroCharges, SpecialFeeValue, SpecialFeeBasis, IsUserSelectable, DisplaySequence)
	select JurisdictionID, @toFinancialPeriodID, EndorsementCode, Description, ZeroCharges, SpecialFeeValue, SpecialFeeBasis, IsUserSelectable, DisplaySequence
	from dbo.EndorsementPeriodic
	where FinancialPeriodID = @fromFinancialPeriodID --and JurisdictionID = @jurisdictionID;

	return;

GO
/****** Object:  StoredProcedure [dbo].[CopyJurisdictionPeriodicFromPeriodToPeriod]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CopyJurisdictionPeriodicFromPeriodToPeriod]
	@fromFinancialPeriodID integer = null,
	@toFinancialPeriodID integer

as

	set nocount on;

	if (@fromFinancialPeriodID is null)
		select @fromFinancialPeriodID = max(FinancialPeriodID) 
		from dbo.FinancialPeriod
		where FinancialPeriodID < @toFinancialPeriodID;

	insert into dbo.JurisdictionPeriodic (
		JurisdictionID, FinancialPeriodID, ApplianceItemPrescriptionChargeValue, DrugItemPrescriptionChargeValue, 
		StandardProfessionalFeeValue, ExpensiveItemThreshold, ExpensiveItemFeePercentage, OutOfPocketExpenseDeductionValue, 
		ContainerAllowanceValue, RepeatDispensingFee, MedicinesUseReviewFee, StomaCustomisationFee, ApplianceUseReviewAtPharmacyFee, 
		ApplianceUseReviewAtHomeFee, ContractTransitionPaymentDivisor, ContractTransitionPaymentMultiplier, PracticePaymentDivisor, EstablishmentPaymentDivisor,
		OutOfPocketExpenseMinimumClaimValue, ValueOfSupplementaryControlledDrugFee, ValueOfConsumableAllowanceFee,
		PppaTargetPercentage, NcvLowerTargetPercentage, NcvUpperTargetPercentage, AcvLowerTargetPercentage)
	select JurisdictionID, @toFinancialPeriodID, ApplianceItemPrescriptionChargeValue, DrugItemPrescriptionChargeValue, 
		StandardProfessionalFeeValue, ExpensiveItemThreshold, ExpensiveItemFeePercentage, OutOfPocketExpenseDeductionValue, 
		ContainerAllowanceValue, RepeatDispensingFee, MedicinesUseReviewFee, StomaCustomisationFee, ApplianceUseReviewAtPharmacyFee, 
		ApplianceUseReviewAtHomeFee, ContractTransitionPaymentDivisor, ContractTransitionPaymentMultiplier, PracticePaymentDivisor, EstablishmentPaymentDivisor,
		OutOfPocketExpenseMinimumClaimValue, ValueOfSupplementaryControlledDrugFee, ValueOfConsumableAllowanceFee,
		PppaTargetPercentage, NcvLowerTargetPercentage, NcvUpperTargetPercentage, AcvLowerTargetPercentage
	from dbo.JurisdictionPeriodic 
		where FinancialPeriodID = @fromFinancialPeriodID

	return

GO
/****** Object:  StoredProcedure [dbo].[CopyNewMedicineServiceConsultationFee]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CopyNewMedicineServiceConsultationFee]
	@fromFinancialPeriodID integer = null,
	@toFinancialPeriodID integer

as

	set nocount on;

	if (@fromFinancialPeriodID is null)
		select @fromFinancialPeriodID = max(FinancialPeriodID) 
		from dbo.FinancialPeriod
		where FinancialPeriodID < @toFinancialPeriodID;

	insert into dbo.NewMedicineServiceConsultationFee (
		FinancialPeriodID, PercentageOfTargetConsultationsAcheived, PaymentValuePerConsultation)
	select @toFinancialPeriodID, PercentageOfTargetConsultationsAcheived, PaymentValuePerConsultation
	from dbo.NewMedicineServiceConsultationFee
	where FinancialPeriodID = @fromFinancialPeriodID

	return;

GO
/****** Object:  StoredProcedure [dbo].[CopyNewMedicineServiceTargetBand]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CopyNewMedicineServiceTargetBand]
	@fromFinancialPeriodID integer = null,
	@toFinancialPeriodID integer

as

	set nocount on;

	if (@fromFinancialPeriodID is null)
		select @fromFinancialPeriodID = max(FinancialPeriodID) 
		from dbo.FinancialPeriod
		where FinancialPeriodID < @toFinancialPeriodID;

	insert into dbo.NewMedicineServiceTargetBand (
		FinancialPeriodID, NumberOfQualifyingItemsLowerLimit, NumberOfQualifyingItemsUpperLimit, NumberOfQualifyingItemsIncrementor, MaximumNumberOfItemsPayable, MaximumNumberOfItemsPayableIncrementor)
	select @toFinancialPeriodID, NumberOfQualifyingItemsLowerLimit, NumberOfQualifyingItemsUpperLimit, NumberOfQualifyingItemsIncrementor, MaximumNumberOfItemsPayable, MaximumNumberOfItemsPayableIncrementor
	from dbo.NewMedicineServiceTargetBand
	where FinancialPeriodID = @fromFinancialPeriodID

	return;

GO
/****** Object:  StoredProcedure [dbo].[CopyTariffDiscountDeductionScale]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CopyTariffDiscountDeductionScale]
	@fromFinancialPeriodID int = null,
	@FinancialPeriodID int
AS

if (@fromFinancialPeriodID is null)
	select @fromFinancialPeriodID = max(FinancialPeriodID) 
	from dbo.FinancialPeriod
	where FinancialPeriodID < @FinancialPeriodID;

INSERT INTO TariffDiscountDeductionScale (FinancialPeriodID, [From], [To], DeductionRate)
SELECT @FinancialPeriodID, [From], [To], DeductionRate 
FROM TariffDiscountDeductionScale
WHERE FinancialPeriodID = @FromFinancialPeriodID

GO
/****** Object:  StoredProcedure [dbo].[CopyTariffEstablishmentPaymentScale]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CopyTariffEstablishmentPaymentScale]
	@fromFinancialPeriodID int = null,
	@FinancialPeriodID int,
	@JurisdictionID int
AS

if (@fromFinancialPeriodID is null)
	select @fromFinancialPeriodID = max(FinancialPeriodID) 
	from dbo.FinancialPeriod
	where FinancialPeriodID < @FinancialPeriodID;

INSERT INTO TariffEstablishmentPaymentScale ([FinancialPeriodID],[JurisdictionID],[ItemsFrom],[ItemsTo],[EstablishmentPayment])
SELECT @FinancialPeriodID, [JurisdictionID],[ItemsFrom],[ItemsTo],[EstablishmentPayment]
FROM TariffEstablishmentPaymentScale
WHERE FinancialPeriodID = @FromFinancialPeriodID AND JurisdictionID = @JurisdictionID

GO
/****** Object:  StoredProcedure [dbo].[CopyTariffEstablishmentPaymentScaleNoJurisdiction]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CopyTariffEstablishmentPaymentScaleNoJurisdiction]
	@fromFinancialPeriodID int = null,
	@FinancialPeriodID int
AS

if (@fromFinancialPeriodID is null)
	select @fromFinancialPeriodID = max(FinancialPeriodID) 
	from dbo.FinancialPeriod
	where FinancialPeriodID < @FinancialPeriodID;

INSERT INTO TariffEstablishmentPaymentScale ([FinancialPeriodID],[JurisdictionID],[ItemsFrom],[ItemsTo],[EstablishmentPayment])
SELECT @FinancialPeriodID, [JurisdictionID],[ItemsFrom],[ItemsTo],[EstablishmentPayment]
FROM TariffEstablishmentPaymentScale
WHERE FinancialPeriodID = @FromFinancialPeriodID --AND JurisdictionID = @JurisdictionID

GO
/****** Object:  StoredProcedure [dbo].[CopyTariffPracticePayment]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CopyTariffPracticePayment]
	@fromFinancialPeriodID int = null,
	@FinancialPeriodID int,
	@JurisdictionID int
AS

if (@fromFinancialPeriodID is null)
	select @fromFinancialPeriodID = max(FinancialPeriodID) 
	from dbo.FinancialPeriod
	where FinancialPeriodID < @FinancialPeriodID;

INSERT INTO TariffPracticePayment ([FinancialPeriodID],[JurisdictionID],[ItemsFrom],[ItemsTo],[PracticePaymentFixed],[PracticePaymentPerItem],[ContributionInPracticePaymentFixed],[ContributionInPracticePaymentPerItem])
SELECT @FinancialPeriodID, [JurisdictionID],[ItemsFrom],[ItemsTo],[PracticePaymentFixed],[PracticePaymentPerItem],[ContributionInPracticePaymentFixed],[ContributionInPracticePaymentPerItem]
FROM TariffPracticePayment
WHERE FinancialPeriodID = @FromFinancialPeriodID AND JurisdictionID = @JurisdictionID

GO
/****** Object:  StoredProcedure [dbo].[CopyTariffPracticePaymentItemsDispensed]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CopyTariffPracticePaymentItemsDispensed]
	@fromFinancialPeriodID int = null,
	@FinancialPeriodID int
AS

if (@fromFinancialPeriodID is null)
	select @fromFinancialPeriodID = max(FinancialPeriodID) 
	from dbo.FinancialPeriod
	where FinancialPeriodID < @FinancialPeriodID;

INSERT INTO TariffPracticePaymentItemsDispensed (FinancialPeriodID, [ItemsFrom],[ItemsTo],[HoursPerWeek],[AdditionalHoursPerWeek],[AdditionalItems])
SELECT @FinancialPeriodID, [ItemsFrom],[ItemsTo],[HoursPerWeek],[AdditionalHoursPerWeek],[AdditionalItems]
FROM TariffPracticePaymentItemsDispensed
WHERE FinancialPeriodID = @FromFinancialPeriodID

GO
/****** Object:  StoredProcedure [dbo].[CopyTariffPracticePaymentNoJurisdiction]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CopyTariffPracticePaymentNoJurisdiction]
	@fromFinancialPeriodID int = null,
	@FinancialPeriodID int
AS

if (@fromFinancialPeriodID is null)
	select @fromFinancialPeriodID = max(FinancialPeriodID) 
	from dbo.FinancialPeriod
	where FinancialPeriodID < @FinancialPeriodID;

INSERT INTO TariffPracticePayment ([FinancialPeriodID],[JurisdictionID],[ItemsFrom],[ItemsTo],[PracticePaymentFixed],[PracticePaymentPerItem],[ContributionInPracticePaymentFixed],[ContributionInPracticePaymentPerItem])
SELECT @FinancialPeriodID, [JurisdictionID],[ItemsFrom],[ItemsTo],[PracticePaymentFixed],[PracticePaymentPerItem],[ContributionInPracticePaymentFixed],[ContributionInPracticePaymentPerItem]
FROM TariffPracticePayment
WHERE FinancialPeriodID = @FromFinancialPeriodID --AND JurisdictionID = @JurisdictionID

GO
/****** Object:  StoredProcedure [dbo].[CopyTariffTransitionalPayment]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CopyTariffTransitionalPayment]
	@FinancialPeriodID int,
	@JurisdictionID int
AS

DECLARE @FromFinancialPeriodID int

	if (@fromFinancialPeriodID is null)
		select @fromFinancialPeriodID = max(FinancialPeriodID) 
		from dbo.FinancialPeriod
		where FinancialPeriodID < @FinancialPeriodID;

INSERT INTO TariffTransitionalPayment ([FinancialPeriodID],[JurisdictionID],[ItemsFrom],[ItemsTo],[Payment])
SELECT @FinancialPeriodID, [JurisdictionID],[ItemsFrom],[ItemsTo],[Payment]
FROM TariffTransitionalPayment
WHERE FinancialPeriodID = @FromFinancialPeriodID AND JurisdictionID = @JurisdictionID

GO
/****** Object:  StoredProcedure [dbo].[DashboardBatchStatusChart_SelectCallback]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DashboardBatchStatusChart_SelectCallback]
	@financialPeriodID integer

as

	set nocount on;
	set transaction isolation level read uncommitted;
	
	select bs.BatchStatusID, bs.Description, count(b.BatchID) as Total
	from dbo.BatchStatus bs
		left join dbo.Batch b on bs.BatchStatusID = b.BatchStatusID and b.FinancialPeriodID = @financialPeriodID
	where bs.ShowOnDashboard = 1
	group by bs.BatchStatusID, bs.Description
	order by bs.BatchStatusID;

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[DashboardUserActivityChart_SelectCallback]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DashboardUserActivityChart_SelectCallback]
	@frActivityDate datetime = null,
	@toActivityDate datetime = null

as

	set nocount on;
	set transaction isolation level read uncommitted;

	if (@frActivityDate is null)
		set @frActivityDate = cast(getdate() as date);
	if (@toActivityDate is null)
		set @toActivityDate = dateadd(dd, 1, @frActivityDate);

	with todaysActivity as (
		select *
		from dbo.UserActivity ua
		where cast(ua.DateTime as date) between @frActivityDate and @toActivityDate
	)

	select a.ActivityID, a.Description, count(ua.UserActivityID) as Total
	from dbo.Activity a 
		left join todaysActivity ua on a.ActivityID = ua.ActivityID
	where a.ActivityID in (4, 5, 6, 13, 18, 19, 20, 32, 37)
	group by a.ActivityID, a.Description
	order by a.ActivityID;

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[DeleteBatchAndScheduleOfPaymentsForFinancialPeriod]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DeleteBatchAndScheduleOfPaymentsForFinancialPeriod]
(
	@vc_FinancialPeriod		VARCHAR(6)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	BEGIN TRANSACTION

	BEGIN TRY		
		DECLARE @tbl_Batch TABLE ([BatchID] INTEGER)
		DECLARE @tbl_Schedule TABLE ([ScheduleID] INTEGER)

		-- Get list of batch items to remove.
		INSERT INTO @tbl_Batch
		SELECT [BatchID]
		FROM   [dbo].[Batch]
		WHERE  [FinancialPeriodID] = @vc_FinancialPeriod	

		-- Get list of schedule items to remove.
		INSERT INTO @tbl_Schedule
		SELECT [ScheduleID]
		FROM   [dbo].[Schedule]
		WHERE  [BatchID] IN (SELECT [BatchID] FROM @tbl_Batch)
				
		-- Remove 'link' between tables.
		UPDATE [dbo].[Batch] SET [ScheduleID] = NULL WHERE [BatchID] IN (SELECT [BatchID] FROM @tbl_Batch)
	
		-- Delete the schedule items.
		DELETE FROM [dbo].[ScheduleOtherPayment] WHERE [ScheduleID] IN (SELECT [ScheduleID] FROM @tbl_Schedule)						
		DELETE FROM [dbo].[ScheduleLocalPayment] WHERE [ScheduleID] IN (SELECT [ScheduleID] FROM @tbl_Schedule)			
		DELETE FROM [dbo].[ScheduleGroupSwitchSummary] WHERE [ScheduleID] IN (SELECT [ScheduleID] FROM @tbl_Schedule)			
		DELETE FROM [dbo].[ScheduleFee] WHERE [ScheduleID] IN (SELECT [ScheduleID] FROM @tbl_Schedule)
		DELETE FROM [dbo].[ScheduleExpensiveItemSummary] WHERE [ScheduleID] IN (SELECT [ScheduleID] FROM @tbl_Schedule)
		DELETE FROM [dbo].[ScheduleExpensiveItem] WHERE [ScheduleID] IN (SELECT [ScheduleID] FROM @tbl_Schedule)
		DELETE FROM [dbo].[ScheduleDocumentData] WHERE [ScheduleID] IN (SELECT [ScheduleID] FROM @tbl_Schedule)
		DELETE FROM [dbo].[ScheduleCost] WHERE [ScheduleID] IN (SELECT [ScheduleID] FROM @tbl_Schedule)
		DELETE FROM [dbo].[ScheduleCharge] WHERE [ScheduleID] IN (SELECT [ScheduleID] FROM @tbl_Schedule)
		DELETE FROM [dbo].[Schedule] WHERE [ScheduleID] IN (SELECT [ScheduleID] FROM @tbl_Schedule)		

		-- Delete the batch items.
		DELETE FROM [dbo].[BatchStatusHistory] WHERE [BatchID] IN (SELECT [BatchID] FROM @tbl_Batch)
		DELETE FROM [dbo].[Batch] WHERE [BatchID] IN (SELECT [BatchID] FROM @tbl_Batch)
	END TRY
	
	BEGIN CATCH
		ROLLBACK;
		RETURN;				
	END CATCH

	IF (@@ERROR != 0 )
		BEGIN
			ROLLBACK;
		END
	ELSE
		BEGIN
			COMMIT;
		END
				
	RETURN @@ERROR
END

GO
/****** Object:  StoredProcedure [dbo].[DeleteBatchRequest]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DeleteBatchRequest]
(
	@vc_ContractorCode		VARCHAR(MAX),
	@vc_FinancialPeriod		VARCHAR(6),
	@int_BatchID			INTEGER = NULL
)
AS
BEGIN	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		
	IF ((SELECT COUNT(*) FROM [dbo].[Batch] WHERE [BatchID] = @int_BatchID) = 0)
		BEGIN				
			SET @int_BatchID = (SELECT BatchID
								FROM   [dbo].[Batch] b JOIN [dbo].[Contractor] c ON b.[ContractorID] = c.[ContractorID]
								WHERE  b.[FinancialPeriodID] = @vc_FinancialPeriod
								AND	   c.[Code] = @vc_ContractorCode)
		END

	IF ((SELECT COUNT(*) FROM [dbo].[Batch] WHERE [BatchID] = @int_BatchID) = 0)
		BEGIN
			PRINT 'Invalid BatchID detected'				
			RETURN @@ERROR;
		END

	PRINT 'BatchID is ' + CAST(@int_BatchID as VARCHAR(10))

	BEGIN TRANSACTION

	BEGIN TRY	
		DECLARE @tbl_Document TABLE ([DocumentID] INTEGER)
		DECLARE @tbl_Item TABLE ([ItemID] INTEGER)
		DECLARE @tbl_ItemVersion TABLE ([ItemVersionID] INTEGER)
		DECLARE @tbl_EpsPrescribedItem TABLE ([EpsPrescribedItemID] INTEGER)		
	
		-- Get list of document items to remove.
		INSERT INTO @tbl_Document
		SELECT [DocumentID]
		FROM   [dbo].[Document]
		WHERE  [BatchID] = @int_BatchID	

		-- Get list of item items to remove.
		INSERT INTO @tbl_Item
		SELECT [ItemID]
		FROM   [dbo].[Item]
		WHERE  [DocumentID] IN (SELECT [DocumentID] FROM @tbl_Document)

		-- Get list of item version items to remove.
		INSERT INTO @tbl_ItemVersion
		SELECT [ItemVersionID]
		FROM   [dbo].[ItemVersion]
		WHERE  [ItemID] IN (SELECT [ItemID] FROM @tbl_Item)	
		
		-- Get list of EpsPrescribedItems to remove
		
		INSERT INTO @tbl_EpsPrescribedItem
		Select EpsPrescribedItemID from EpsPrescribedItem where EpsHeaderID in 
		(SELECT [EpsHeaderID] from EPSHeader WHERE [DocumentID] IN (SELECT [DocumentID] FROM @tbl_Document))
		
		
		DELETE FROM [dbo].[UserActivity] WHERE [BatchID] = @int_BatchID
		DELETE FROM [dbo].[SubmissionImage] WHERE [BatchID] = @int_BatchID
		DELETE FROM [dbo].[Submission] WHERE [BatchID] = @int_BatchID
		UPDATE [dbo].[Batch] SET [CurrentFragmentTrackingID] = NULL, [DivisionID] = NULL WHERE [BatchID] = @int_BatchID
		DELETE FROM [dbo].[PaperTracking] WHERE [BatchID] = @int_BatchID	
		DELETE FROM [dbo].[ItemVersionEndorsement] WHERE [ItemVersionID] IN (SELECT [ItemVersionID] FROM @tbl_ItemVersion)
		UPDATE [dbo].[Item] SET [OriginalItemVersionID] = NULL, [CurrentItemVersionID] = NULL WHERE [ItemID] IN (SELECT [ItemID] FROM @tbl_Item)
		DELETE FROM [dbo].[ItemVersion] WHERE [ItemID] IN (SELECT [ItemID] FROM @tbl_Item)
		DELETE FROM [dbo].[Item] WHERE [ItemID] IN (SELECT [ItemID] FROM @tbl_Item)	
		DELETE FROM [dbo].[DocumentImage] WHERE [DocumentID] IN (SELECT [DocumentID] FROM @tbl_Document)
		UPDATE [dbo].[Document] SET [OriginalDocumentGroupTypeID] = NULL, [CurrentDocumentGroupTypeID] = NULL WHERE [DocumentID] IN (SELECT [DocumentID] FROM @tbl_Document)
		DELETE FROM [dbo].[DocumentGroupType] WHERE [DocumentID] IN (SELECT [DocumentID] FROM @tbl_Document)
		
		DELETE FROM [dbo].[EpsDispensedItem]	WHERE EpsPrescribedItemID in (SELECT [EpsPrescribedItemID] FROM @tbl_EpsPrescribedItem)
		DELETE from [dbo].[EpsPrescribedItem]	where EpsPrescribedItemID in (SELECT [EpsPrescribedItemID] FROM @tbl_EpsPrescribedItem)
		DELETE FROM [dbo].[EpsHeader]	WHERE [DocumentID] IN (SELECT [DocumentID] FROM @tbl_Document)
		
		
		DELETE FROM [dbo].[BatchUserProfile] WHERE [BatchID] = @int_BatchID	
		DELETE FROM [dbo].[Document] WHERE [BatchID] = @int_BatchID	
		UPDATE [dbo].[Batch] SET [BatchStatusID] = 100 WHERE [BatchID] = @int_BatchID		
	END TRY
	
	BEGIN CATCH
		SELECT ERROR_NUMBER() AS ErrorNumber,
			   ERROR_SEVERITY() AS ErrorSeverity,
			   ERROR_STATE() AS ErrorState,
			   ERROR_PROCEDURE() AS ErrorProcedure,
			   ERROR_LINE() AS ErrorLine,
			   ERROR_MESSAGE() AS ErrorMessage;

		ROLLBACK;
		RETURN;				
	END CATCH

	IF (@@ERROR != 0 )
		BEGIN
			ROLLBACK;
		END
	ELSE
		BEGIN
			COMMIT;
		END
	
	RETURN @@ERROR
END


GO
/****** Object:  StoredProcedure [dbo].[DeleteBatchRequest_backup]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DeleteBatchRequest_backup]
(
	@vc_ContractorCode		VARCHAR(MAX),
	@vc_FinancialPeriod		VARCHAR(6),
	@int_BatchID			INTEGER = NULL
)
AS
BEGIN	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		
	IF ((SELECT COUNT(*) FROM [dbo].[Batch] WHERE [BatchID] = @int_BatchID) = 0)
		BEGIN				
			SET @int_BatchID = (SELECT BatchID
								FROM   [dbo].[Batch] b JOIN [dbo].[Contractor] c ON b.[ContractorID] = c.[ContractorID]
								WHERE  b.[FinancialPeriodID] = @vc_FinancialPeriod
								AND	   c.[Code] = @vc_ContractorCode)
		END

	IF ((SELECT COUNT(*) FROM [dbo].[Batch] WHERE [BatchID] = @int_BatchID) = 0)
		BEGIN
			PRINT 'Invalid BatchID detected'				
			RETURN @@ERROR;
		END

	PRINT 'BatchID is ' + CAST(@int_BatchID as VARCHAR(10))

	BEGIN TRANSACTION

	BEGIN TRY	
		DECLARE @tbl_Document TABLE ([DocumentID] INTEGER)
		DECLARE @tbl_Item TABLE ([ItemID] INTEGER)
		DECLARE @tbl_ItemVersion TABLE ([ItemVersionID] INTEGER)
	
		-- Get list of document items to remove.
		INSERT INTO @tbl_Document
		SELECT [DocumentID]
		FROM   [dbo].[Document]
		WHERE  [BatchID] = @int_BatchID	

		-- Get list of item items to remove.
		INSERT INTO @tbl_Item
		SELECT [ItemID]
		FROM   [dbo].[Item]
		WHERE  [DocumentID] IN (SELECT [DocumentID] FROM @tbl_Document)

		-- Get list of item version items to remove.
		INSERT INTO @tbl_ItemVersion
		SELECT [ItemVersionID]
		FROM   [dbo].[ItemVersion]
		WHERE  [ItemID] IN (SELECT [ItemID] FROM @tbl_Item)	

		DELETE FROM [dbo].[UserActivity] WHERE [BatchID] = @int_BatchID
		DELETE FROM [dbo].[SubmissionImage] WHERE [BatchID] = @int_BatchID
		DELETE FROM [dbo].[Submission] WHERE [BatchID] = @int_BatchID
		UPDATE [dbo].[Batch] SET [CurrentFragmentTrackingID] = NULL, [DivisionID] = NULL WHERE [BatchID] = @int_BatchID
		DELETE FROM [dbo].[PaperTracking] WHERE [BatchID] = @int_BatchID	
		DELETE FROM [dbo].[ItemVersionEndorsement] WHERE [ItemVersionID] IN (SELECT [ItemVersionID] FROM @tbl_ItemVersion)
		UPDATE [dbo].[Item] SET [OriginalItemVersionID] = NULL, [CurrentItemVersionID] = NULL WHERE [ItemID] IN (SELECT [ItemID] FROM @tbl_Item)
		DELETE FROM [dbo].[ItemVersion] WHERE [ItemID] IN (SELECT [ItemID] FROM @tbl_Item)
		DELETE FROM [dbo].[Item] WHERE [ItemID] IN (SELECT [ItemID] FROM @tbl_Item)	
		DELETE FROM [dbo].[DocumentImage] WHERE [DocumentID] IN (SELECT [DocumentID] FROM @tbl_Document)
		UPDATE [dbo].[Document] SET [OriginalDocumentGroupTypeID] = NULL, [CurrentDocumentGroupTypeID] = NULL WHERE [DocumentID] IN (SELECT [DocumentID] FROM @tbl_Document)
		DELETE FROM [dbo].[DocumentGroupType] WHERE [DocumentID] IN (SELECT [DocumentID] FROM @tbl_Document)
		DELETE FROM [dbo].[Document] WHERE [BatchID] = @int_BatchID	
		UPDATE [dbo].[Batch] SET [BatchStatusID] = 100 WHERE [BatchID] = @int_BatchID		
	END TRY
	
	BEGIN CATCH
		SELECT ERROR_NUMBER() AS ErrorNumber,
			   ERROR_SEVERITY() AS ErrorSeverity,
			   ERROR_STATE() AS ErrorState,
			   ERROR_PROCEDURE() AS ErrorProcedure,
			   ERROR_LINE() AS ErrorLine,
			   ERROR_MESSAGE() AS ErrorMessage;

		ROLLBACK;
		RETURN;				
	END CATCH

	IF (@@ERROR != 0 )
		BEGIN
			ROLLBACK;
		END
	ELSE
		BEGIN
			COMMIT;
		END
	
	RETURN @@ERROR
END


GO
/****** Object:  StoredProcedure [dbo].[DeleteDocumentData]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







CREATE PROCEDURE [dbo].[DeleteDocumentData]
	@periodsToRetain integer = 14,
	@batchSize integer = 500,
	@maxDuration integer = 1 -- RA changed from data type time


as

	set nocount on;
--select getdate() + '60:00'  @maxDuration, 
	declare @stopDeletingAt datetime =  DATEADD(hh,@maxDuration,getdate()), --RA 2014/09/02 updated to dateadd instead of getdate + time due to 24h limitation
		@documentCount integer = -1,
		@scheduleCount integer = -1,
		@submissionImageCount integer = -1;

	declare @minimumFinancialPeriodIDToRetain integer;
	select top 1 @minimumFinancialPeriodIDToRetain = FinancialPeriodID
	from (
		select top (@periodsToRetain) FinancialPeriodID
		from FinancialPeriod
		where FinancialPeriodID < (
			select top 1 FinancialPeriodID
			from dbo.FinancialPeriod
			where IsDefault = 1
			order by FinancialPeriodID
		)
		order by FinancialPeriodID desc
	) fp
	order by FinancialPeriodID;

	--End previous logs
	
	UPDATE LOGGING set EndTime = GETDATE() where endtime is null
	
	--Start of deletions
	INSERT INTO Logging (minimumFinancialPeriodIDToRetain, startTime, stopTime, Type, BatchSize, RowCountRemaining) VALUES ( @minimumFinancialPeriodIDToRetain, getDate(), @stopDeletingAt, 'Main', @batchSize, (select COUNT( DocumentID) from dbo.Batch b inner join dbo.Document d on b.BatchID = d.BatchID where b.FinancialPeriodID = @minimumFinancialPeriodIDToRetain) )


	declare @documents table (
		DocumentID bigint primary key
	);

	while (@documentCount != 0 and getdate() < @stopDeletingAt) begin

		delete from @documents;

		insert into @documents
		select top (@batchSize) DocumentID
		from dbo.Batch b
			inner join dbo.Document d on b.BatchID = d.BatchID
		where b.FinancialPeriodID < @minimumFinancialPeriodIDToRetain
		order by DocumentID
		option (optimize for (@minimumFinancialPeriodIDToRetain unknown));

		set @documentCount = @@rowcount;
		if (@documentCount > 0) begin


		--Start of Document deletion
		INSERT INTO Logging (minimumFinancialPeriodIDToRetain, starttime, type) VALUES ( @minimumFinancialPeriodIDToRetain,GetDate(), 'Documents')



			delete from ua
			from @documents d
				inner join dbo.Item i
					inner join dbo.UserActivity ua on i.ItemID = ua.ItemID
				on d.DocumentID = i.DocumentID;

			delete from af
			from @documents d
				inner join dbo.Item i
					inner join dbo.AuditFeedback af on i.ItemID = af.ItemID
				on d.DocumentID = i.DocumentID;

			update i
			set i.CurrentItemVersionID = null,
				i.OriginalItemVersionID = null
			from @documents d
				inner join dbo.Item i on d.DocumentID = i.DocumentID;

			delete from edi
			from @documents d
				inner join EpsHeader eh 
					inner join EpsPrescribedItem epi 
						inner join dbo.EpsDispensedItem edi on epi.EpsPrescribedItemID = edi.EpsPrescribedItemID
					on eh.EpsHeaderID = epi.EpsHeaderID
				on d.DocumentID = eh.DocumentID;

			delete from epi
			from @documents d
				inner join EpsHeader eh 
					inner join EpsPrescribedItem epi on eh.EpsHeaderID = epi.EpsHeaderID
				on d.DocumentID = eh.DocumentID;

			delete from eh
			from @documents d
				inner join EpsHeader eh on d.DocumentID = eh.DocumentID;

			delete from ive
			from @documents d
				inner join dbo.Item i 
					inner join dbo.ItemVersion iv 
						inner join dbo.ItemVersionEndorsement ive on iv.ItemVersionID = ive.ItemVersionID
					on i.ItemID = iv.ItemID
				on d.DocumentID = i.DocumentID;

			delete from iv
			from @documents d
				inner join dbo.Item i 
					inner join dbo.ItemVersion iv on i.ItemID = iv.ItemID
				on d.DocumentID = i.DocumentID;

			delete from ia
			from @documents d
				inner join dbo.Item i 
					inner join dbo.ItemAdvisory ia on i.ItemID = ia.ItemID
				on d.DocumentID = i.DocumentID;

			delete from ia
			from @documents d
				inner join dbo.Item i 
					inner join dbo.ItemNote ia on i.ItemID = ia.ItemID
				on d.DocumentID = i.DocumentID;

			delete from i
			from @documents d
				inner join dbo.Item i on d.DocumentID = i.DocumentID;

			delete from di
			from @documents d
				inner join dbo.DocumentImage di on d.DocumentID = di.DocumentID;

			delete bup
			from @documents d
				inner join dbo.BatchUserProfile bup on d.DocumentID = bup.LastDocumentID;

			delete from ua
			from @documents d
				inner join dbo.UserActivity ua on d.DocumentID = ua.FormID;

			update doc
			set OriginalDocumentGroupTypeID = null,
				CurrentDocumentGroupTypeID = null
			from @documents d 
				inner join dbo.Document doc on d.DocumentID = doc.DocumentID;

			delete from dgt
			from @documents d 
				inner join dbo.DocumentGroupType dgt on d.DocumentID = dgt.DocumentID;

			delete from doc
			from @documents d 
				inner join dbo.Document doc on d.DocumentID = doc.DocumentID;


		--End of Documents
		Update logging set EndTime = Getdate(), Rowcountremaining = (select COUNT( DocumentID)from dbo.Batch b inner join dbo.Document d on b.BatchID = d.BatchID
		where b.FinancialPeriodID < @minimumFinancialPeriodIDToRetain) where type = 'Documents' and EndTime is null

		end;

	end;




	declare @schedules table (
		ScheduleID bigint primary key
	);

	while (@scheduleCount != 0 and getdate() < @stopDeletingAt) begin

		delete from @schedules;

		insert into @schedules
		select top (@batchSize) s.ScheduleID
		from dbo.Batch b
			inner join dbo.Schedule s on b.BatchID = s.BatchID and b.ScheduleID != s.ScheduleID and b.PsncScheduleID != s.ScheduleID
		where b.FinancialPeriodID < @minimumFinancialPeriodIDToRetain
		order by s.ScheduleID
		option (optimize for (@minimumFinancialPeriodIDToRetain unknown));

		set @scheduleCount = @@rowcount;
		if (@scheduleCount > 0) begin

		--Start of schedules deletion
		INSERT INTO Logging (minimumFinancialPeriodIDToRetain, starttime, type) VALUES ( @minimumFinancialPeriodIDToRetain,GetDate(), 'Schedules')


			delete from charge
			from @schedules s
				inner join dbo.ScheduleCharge charge on s.ScheduleID = charge.ScheduleID;

			delete from cost
			from @schedules s
				inner join dbo.ScheduleCost cost on s.ScheduleID = cost.ScheduleID;

			delete from documentData
			from @schedules s
				inner join dbo.ScheduleDocumentData documentData on s.ScheduleID = documentData.ScheduleID;

			delete from expensiveItem
			from @schedules s
				inner join dbo.ScheduleExpensiveItem expensiveItem on s.ScheduleID = expensiveItem.ScheduleID;

			delete from expensiveItemSummary
			from @schedules s
				inner join dbo.ScheduleExpensiveItemSummary expensiveItemSummary on s.ScheduleID = expensiveItemSummary.ScheduleID;

			delete from fee
			from @schedules s
				inner join dbo.ScheduleFee fee on s.ScheduleID = fee.ScheduleID;

			delete from groupSwitchSummary
			from @schedules s
				inner join dbo.ScheduleGroupSwitchSummary groupSwitchSummary on s.ScheduleID = groupSwitchSummary.ScheduleID;

			delete from localPayment
			from @schedules s
				inner join dbo.ScheduleLocalPayment localPayment on s.ScheduleID = localPayment.ScheduleID;

			delete from otherPayment
			from @schedules s
				inner join dbo.ScheduleOtherPayment otherPayment on s.ScheduleID = otherPayment.ScheduleID;

			delete from schedule
			from @schedules s
				inner join dbo.Schedule schedule on s.ScheduleID = schedule.ScheduleID;

		--End of Schedules
		Update logging set EndTime = Getdate() , rowcountremaining = (
		
		select count(s.ScheduleID)
		from dbo.Batch b
			inner join dbo.Schedule s on b.BatchID = s.BatchID and b.ScheduleID != s.ScheduleID and b.PsncScheduleID != s.ScheduleID
		where b.FinancialPeriodID < @minimumFinancialPeriodIDToRetain)
		
		
		where type = 'Schedules' and EndTime is null

		end

	end

	declare @submissionImages table (
		SubmissionImageID bigint primary key
	);

	while (@submissionImageCount != 0 and getdate() < @stopDeletingAt) begin

		delete from @submissionImages;

		insert into @submissionImages
		select top (@batchSize) si.SubmissionImageID
		from dbo.Batch b
			inner join dbo.SubmissionImage si on b.BatchID = si.BatchID
		where b.FinancialPeriodID < @minimumFinancialPeriodIDToRetain
		order by si.SubmissionImageID
		option (optimize for (@minimumFinancialPeriodIDToRetain unknown));

		set @submissionImageCount = @@rowCount;
		if (@submissionImageCount > 0) begin

		--Start of Submission deletion
		INSERT INTO Logging (minimumFinancialPeriodIDToRetain, starttime, type) VALUES ( @minimumFinancialPeriodIDToRetain,GetDate(), 'Submission')


			delete from si2
			from @submissionImages si
				inner join dbo.SubmissionImage si2 on si.SubmissionImageID = si2.SubmissionImageID


		--End of Submission
		Update logging set EndTime = Getdate(), rowcountremaining = (		select count(si.SubmissionImageID)
		from dbo.Batch b
			inner join dbo.SubmissionImage si on b.BatchID = si.BatchID
		where b.FinancialPeriodID < @minimumFinancialPeriodIDToRetain)  where type = 'Submission' and EndTime is null
		
		end

	end

		Update logging set EndTime = Getdate() where type = 'Main' and EndTime is null

	return @@error;








GO
/****** Object:  StoredProcedure [dbo].[DeleteFinancialPeriodDataFromPeriod]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeleteFinancialPeriodDataFromPeriod]
	@FinancialPeriodID integer
AS

	set nocount on;

if @FinancialPeriodID > 0
BEGIN

	delete from dbo.ControlledSubstanceIndicatorPeriodic 
	where FinancialPeriodID = @FinancialPeriodID 

	delete from dbo.EndorsementPeriodic 
	where FinancialPeriodID = @FinancialPeriodID 

	delete from dbo.JurisdictionPeriodic 
	where FinancialPeriodID = @FinancialPeriodID 

	delete from dbo.NewMedicineServiceConsultationFee 
	where FinancialPeriodID = @FinancialPeriodID 

	delete from dbo.NewMedicineServiceTargetBand 
	where FinancialPeriodID = @FinancialPeriodID 

	delete from dbo.TariffDiscountDeductionScale 
	where FinancialPeriodID = @FinancialPeriodID 

	delete from dbo.TariffEstablishmentPaymentScale 
	where FinancialPeriodID = @FinancialPeriodID 

	delete from dbo.TariffPracticePayment 
	where FinancialPeriodID = @FinancialPeriodID 

	delete from dbo.TariffPracticePaymentItemsDispensed 
	where FinancialPeriodID = @FinancialPeriodID 
END

GO
/****** Object:  StoredProcedure [dbo].[DeleteScheduleOfPayments]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeleteScheduleOfPayments]
	@scheduleID bigint

as

	set nocount on

	begin transaction

	begin try

		delete from ScheduleCharge
		where ScheduleID = @scheduleID;

		delete from ScheduleCost
		where ScheduleID = @scheduleID;

		delete from ScheduleDocumentData
		where ScheduleID = @scheduleID;

		delete from ScheduleExpensiveItem
		where ScheduleID = @scheduleID;

		delete from ScheduleExpensiveItemSummary
		where ScheduleID = @scheduleID;

		delete from ScheduleFee
		where ScheduleID = @scheduleID;

		delete from ScheduleGroupSwitchSummary
		where ScheduleID = @scheduleID;

		delete from ScheduleLocalPayment
		where ScheduleID = @scheduleID;

		delete from ScheduleOtherPayment
		where ScheduleID = @scheduleID;

		delete from Schedule
		where ScheduleID = @scheduleID;

	end try
	begin catch

		if @@trancount > 0
			rollback transaction;

	end catch;

	if @@trancount > 0
		commit transaction;
		
	return @@error

GO
/****** Object:  StoredProcedure [dbo].[DeriveAndApplyPackLevelDentalFormularyIndicators]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeriveAndApplyPackLevelDentalFormularyIndicators]
	@financialPeriodID integer,
	@state integer = 0

as

	set nocount on;

	raiserror('Deriving pack level dental formulary indicators...', 10, @state) with nowait;
	update ppp
	set DentalFormularyIndicatorCode = pp.DentalFormularyIndicatorCode
	from dbo.ProductPeriodic pp
		inner join dbo.ProductPackPeriodic ppp on pp.ProductPeriodicID = ppp.ProductPeriodicID
	where pp.FinancialPeriodID = @financialPeriodID

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[DeriveAndApplyPackLevelDrugTariffCategories]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeriveAndApplyPackLevelDrugTariffCategories]
	@financialPeriodID integer,
	@state integer = 0

as

	set nocount on;

	raiserror('Deriving drug tariff categories (1/3, using StandardDrugClassIndicatorCode)...', 10, @state) with nowait;
	update ppp
	set DrugTariffCategoryCode = dtc.DrugTariffCategoryCode
	from dbo.ProductPeriodic pp
		inner join dbo.DrugTariffCategory dtc on pp.StandardDrugClassIndicatorCode = dtc.StandardDrugClassIndicatorCode
		inner join dbo.ProductPackPeriodic ppp on pp.ProductPeriodicID = ppp.ProductPeriodicID
	where pp.FinancialPeriodID = @financialPeriodID
	  and ppp.PackPrice != 0.00;
	  
	raiserror('Deriving drug tariff categories (2/3, using VirtualMedicinalProductPack)...', 10, @state) with nowait;
	update ppp
	set DrugTariffCategoryCode = dtc.DrugTariffCategoryCode
	from dbo.ProductPeriodic pp 
		inner join dbo.ProductPackPeriodic ppp 
			inner join dmd.VirtualMedicinalProductPack vmpp 
				inner join dmd.DrugTariffCategoryInformation dtci 
					inner join dbo.DrugTariffCategory dtc on dtci.PaymentCategoryId = dtc.PaymentCategoryID
				on vmpp.VirtualMedicinalProductPackId = dtci.VirtualMedicinalProductPackId
			on ppp.VirtualMedicinalProductPackId = vmpp.VirtualMedicinalProductPackId and vmpp.Invalid = 0
		on pp.ProductPeriodicID = ppp.ProductPeriodicID
	where pp.FinancialPeriodID = @financialPeriodID
	  and ppp.DrugTariffCategoryCode is null
	  and ppp.PackPrice != 0.00;

	raiserror('Deriving drug tariff categories (3/3, using ActualMedicinalProductPack)...', 10, @state) with nowait;
	update ppp
	set DrugTariffCategoryCode = dtc.DrugTariffCategoryCode
	from dbo.ProductPeriodic pp 
		inner join dbo.ProductPackPeriodic ppp
			inner join dmd.ActualMedicinalProductPack ampp
				left join dmd.AppliancePackInformation api on ampp.ActualMedicinalProductPackId = api.ActualMedicinalProductPackId and api.ReimbursementStatusID = 1
				inner join dmd.VirtualMedicinalProductPack vmpp 
					inner join dmd.DrugTariffCategoryInformation dtci 
						inner join dbo.DrugTariffCategory dtc on dtci.PaymentCategoryId = dtc.PaymentCategoryID
					on vmpp.VirtualMedicinalProductPackId = dtci.VirtualMedicinalProductPackId
				on ampp.VirtualMedicinalProductPackId = vmpp.VirtualMedicinalProductPackId and vmpp.Invalid = 0
			on ppp.ActualMedicinalProductPackId = ampp.ActualMedicinalProductPackId and ampp.Invalid = 0
		on pp.ProductPeriodicID = ppp.ProductPeriodicID
	where pp.FinancialPeriodID = @financialPeriodID
	  and ppp.DrugTariffCategoryCode is null
	  and ppp.PackPrice != 0.00;

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[DeriveAndApplyPackLevelZeroDiscount]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeriveAndApplyPackLevelZeroDiscount]
	@financialPeriodID integer,
	@state integer = 0

as

	/*
	 * MDR carries ZD indicators at the presentation level, but they are applied within CDR at the pack level.
	 * MH has determined an algorithm that will (hopefully) allow us to determine the correct pack level value based upon
	 * the presentation level indicators of eqivelent class2 and class3 products...
	 *
	 * Modified 30/10/2012: The result of this algorithm was to update one of the class 2 packs incorrectly as there were
	 * two matching class 3 packs with contradicting indicators. We have now modified this script to update a class 2 pack
	 * only where all of the matching class 3 packs have the same indicator value...
	 * 
	 * Updated correctly: Hydrocort/Miconazole (32511011)
	 * Updated incorrectly: Ropinirole (63116132)
	 *
	 */

	set nocount on;

	raiserror('Deriving pack level zero discount indicators...', 10, @state) with nowait;

	update ppp
		set IsEligibleForZeroDiscount = cast (pp.ZeroDiscountIndicator as bit)
	from ProductPeriodic pp
		inner join ProductPackPeriodic ppp on pp.ProductPeriodicID = ppp.ProductPeriodicID
	where pp.FinancialPeriodID = @financialPeriodID;

	with class2packs as (

		select p.Code as Product, ppp.ProductPackPeriodicID, pack.ContainerSize, pack.PackSize, ppp.PackPrice
		from dbo.Product p
			inner join dbo.ProductPeriodic pp 
				inner join dbo.ProductPackPeriodic ppp 
					inner join dbo.ProductPack pack on ppp.ProductPackID = pack.ProductPackID
				on pp.ProductPeriodicID = ppp.ProductPeriodicID
			on p.ProductID = pp.ProductID and pp.FinancialPeriodID = @financialPeriodID
		where pp.PreperationClassIndicatorCode = 2
			and ppp.PackPrice != 0

	), class3packs as (

		select pp.EquivelentProductCode as EquivelentProduct, pack.ContainerSize, pack.PackSize, ppp.PackPrice, ppp.IsEligibleForZeroDiscount
		from dbo.Product p
			inner join dbo.ProductPeriodic pp 
				inner join dbo.ProductPackPeriodic ppp 
					inner join dbo.ProductPack pack on ppp.ProductPackID = pack.ProductPackID
				on pp.ProductPeriodicID = ppp.ProductPeriodicID
			on p.ProductID = pp.ProductID and pp.FinancialPeriodID = @financialPeriodID
		where pp.PreperationClassIndicatorCode = 3
			and ppp.PackPrice != 0
			and pack.PeriodDiscontinued is null

	), equivalents as (

		select c2.ProductPackPeriodicID, cast(max(cast(c3.IsEligibleForZeroDiscount as integer)) as bit) as IsEligibleForZeroDiscount
		from class2packs c2
			inner join class3packs c3 on c2.Product = c3.EquivelentProduct 
				and (c2.ContainerSize = c3.ContainerSize /* and c3.ContainerSize != 0 */)
				and (c2.PackSize = c3.PackSize /* and c3.PackSize != 0 */)
				and (c2.PackPrice = c3.PackPrice)
		group by c2.ProductPackPeriodicID
		having count(*) = 1

	)
	
	update ppp
	set IsEligibleForZeroDiscount = e.IsEligibleForZeroDiscount
	--output pp.Code, deleted.IsEligibleForZeroDiscount, inserted.IsEligibleForZeroDiscount
	from equivalents e
		inner join dbo.ProductPackPeriodic ppp
	--		inner join dbo.ProductPack pp on ppp.ProductPackID = pp.ProductPackID
		on e.ProductPackPeriodicID = ppp.ProductPackPeriodicID
	where ppp.IsEligibleForZeroDiscount != e.IsEligibleForZeroDiscount;
			
	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[DeriveAndApplyReconstitutionIndicator]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeriveAndApplyReconstitutionIndicator]
	@financialPeriodID integer,
	@state integer = 0

as

	set nocount on;

	raiserror('Deriving class 3/5 eligibility for clause 13b reconstitution...', 10, @state) with nowait;
	update c3c5pp
	set IsEligibleForReconstitutionFee = 1
	from dbo.ProductPeriodic c3c5pp
		inner join dbo.Product c3c5p on c3c5pp.ProductID = c3c5p.ProductID
		inner join dbo.Product c1c2p
			inner join dbo.ProductPeriodic c1c2pp on c1c2p.ProductID = c1c2pp.ProductID and c1c2pp.FinancialPeriodID = @financialPeriodID and c1c2pp.IsEligibleForReconstitutionFee = 1 and c1c2pp.PreperationClassIndicatorCode in (1, 2)
		on c3c5pp.EquivelentProductCode = c1c2p.Code
	where c3c5pp.FinancialPeriodID = @financialPeriodID
	  and c3c5pp.PreperationClassIndicatorCode in (3, 5);

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[DeriveAndApplyUnlicensedMedicineIndicator]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeriveAndApplyUnlicensedMedicineIndicator]
	@financialPeriodID integer,
	@state integer = 0

as

	/*
	 * MDR does not have any indicator that can be used to filter products that are subject to the unlicensed medicine rules specified
	 * in Part VIII.B of the drug tariff.
	 * MH has provided a method of identifying these products based upon the supplier name instead (email, Wed 08/08/2012 14:59)...
	 *
	 *   “Special” Listed in Part VIIIB: Supplier will be “Drug Tariff Special Order”
	 *   “Special” covered by but not listed in Part VIIIB: Supplier will be “Special Order”
	 *   “Imported” covered by but not listed in Part VIIIB: Supplier will begin with “Imported”
	 *
	 */

	set nocount on;

	raiserror('Deriving unlicensed medicine indicators...', 10, @state) with nowait;

	update pp
	set IsUnlicensedMedicine = 0
	from dbo.ProductPeriodic pp
	where pp.FinancialPeriodID = @financialPeriodID;

	update pp
	set IsUnlicensedMedicine = 1
	from dbo.ProductPeriodic pp
		inner join dbo.Product p
			inner join dbo.Manufacturer m on p.ManufacturerID = m.ManufacturerID
		on pp.ProductID = p.ProductID
	where pp.FinancialPeriodID = @financialPeriodID
	  and (m.Name = 'Drug Tariff Special Order' or m.Name = 'Special Order' or m.Name like 'Imported%');

	update pp
	set IsDrugTariffSpecialOrder = 1
	from dbo.ProductPeriodic pp
		inner join dbo.Product p
			inner join dbo.Manufacturer m on p.ManufacturerID = m.ManufacturerID
		on pp.ProductID = p.ProductID
	where pp.FinancialPeriodID = @financialPeriodID
	  and (m.Name = 'Drug Tariff Special Order');

	return;

GO
/****** Object:  StoredProcedure [dbo].[DispenserSearchReport_Header]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DispenserSearchReport_Header]
	@batchID bigint

as

	set nocount on;
	set transaction isolation level read uncommitted;

	select b.BatchID, 
		fp.FinancialYear, fp.FinancialMonth,
		c.Code as ContractorCode, c.Name as ContractorName,
		lpc.Code as LpcCode, lpc.Name as LpcName
	from dbo.Batch b
		inner join dbo.Contractor c on b.ContractorID = c.ContractorID
		inner join dbo.FinancialPeriod fp on b.FinancialPeriodID = fp.FinancialPeriodID
		left join LocalPharmaceuticalCommittee lpc on b.LocalPharmaceuticalCommitteeID = lpc.LocalPharmaceuticalCommitteeID
	where b.BatchID = @batchID;

	return @@error

GO
/****** Object:  StoredProcedure [dbo].[DispenserSearchReport_Items]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DispenserSearchReport_Items]
	@batchID bigint

as

	set nocount on;
	set transaction isolation level read uncommitted;

	select 
		i.ItemID,
		FormNumber = case
			when d.FragmentTypeCode is null then right('000000' + convert(varchar(6), d.DocumentNumber), 6)
			else d.FragmentTypeCode + right('00000' + convert(varchar(6), d.DocumentNumber), 5) 
			end,
		i.Sequence,
		i.SubSequence,
		ProductPackCode = pack.Code,
		ProductDescription = p.Description,
		ProductPackDescription = pack.Description,
		SpecialContainerIndicator = sci.Description,
		Quantity = case
			when iv.ProductPackPeriodicID is null then null
			else iv.Quantity
			end,
		BasicPrice = case
			when iv.ProductPackPeriodicID is null then null
			else iv.BasicPrice
			end,
		gt.GroupTypeCode,
		ProductPackPrice = ppp.PackPrice,
		ProductPackSize = case 
			when ppp.SpecialContainerIndicatorCode = 'S' and pack.ContainerSize = 1 and pack.PackSize > 1 and ppp.PackPrice != ppp.UnitPrice then pack.PackSize
			when ppp.SpecialContainerIndicatorCode = 'S' and pack.ContainerSize != 0 then pack.ContainerSize
			when ppp.SpecialContainerIndicatorCode = 'B' and pack.ContainerSize != 0 then pack.ContainerSize
			else pack.PackSize 
			end,
		NumberOfProfessionalFees = case
			when iv.ProductPackPeriodicID is null then null
			else iv.NumberOfProfessionalFees
			end,
		ValueOfAdditionalFees = case
			when iv.ProductPackPeriodicID is null then null
			else iv.ValueOfAdditionalFees
			end,
		NumberOfCharges = case
			when iv.ProductPackPeriodicID is null then null
			else iv.NumberOfCharges
			end,
		ValueOfOutOfPocketExpenses,
		EndorsementCodes = isnull(rtrim((
			select ep.EndorsementCode + ' '
			from dbo.ItemVersionEndorsement ive
				inner join dbo.EndorsementPeriodic ep on ive.EndorsementPeriodicID = ep.EndorsementPeriodicID
			where ive.ItemVersionID = i.OriginalItemVersionID
			order by ep.EndorsementCode
			for xml path('')
	  )), '')

		--EndorsementCodes = dbo.EndorsementCodeList(iv.ItemVersionID)
	from dbo.Document d
		inner join dbo.DocumentGroupType dgt
			inner join dbo.GroupType gt on dgt.GroupTypeCode = gt.GroupTypeCode
		on d.CurrentDocumentGroupTypeID = dgt.DocumentGroupTypeID
		inner join dbo.Item i
			inner join dbo.ItemVersion iv
				left join dbo.ProductPackPeriodic ppp
					inner join dbo.ProductPack pack
						inner join dbo.Product p on pack.ProductID = p.ProductID
					on ppp.ProductPackID = pack.ProductPackID
					left join dbo.SpecialContainerIndicator sci on ppp.SpecialContainerIndicatorCode = sci.SpecialContainerIndicatorCode
				on iv.ProductPackPeriodicID = ppp.ProductPackPeriodicID
			on i.OriginalItemVersionID = iv.ItemVersionID
		on d.DocumentID = i.DocumentID
	where d.batchID = @batchID
	  and i.OriginalItemVersionID is not null
	order by FormNumber, Sequence, SubSequence;

	return @@error
GO
/****** Object:  StoredProcedure [dbo].[FinancialPeriodSummary]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[FinancialPeriodSummary]
	@FinancialPeriodID int
AS

--DECLARE @FinancialPeriodID int = 201112

SELECT 
(SELECT COUNT(*) 
	FROM ProductPeriodic 
	WHERE FinancialPeriodID=@FinancialPeriodID) AS ProductCount,
(SELECT COUNT(*) 
	FROM ProductPackPeriodic 
		INNER JOIN ProductPeriodic ON ProductPeriodic.ProductPeriodicID = ProductPackPeriodic.ProductPeriodicID 
		WHERE ProductPeriodic.FinancialPeriodID=@FinancialPeriodID
	) AS ProductPackCount,
(SELECT COUNT(*) 
	FROM ControlledSubstanceIndicator 
	WHERE ControlledSubstanceIndicatorCode NOT IN (
		SELECT ControlledSubstanceIndicatorCode 
		FROM ControlledSubstanceIndicatorPeriodic 
		WHERE FinancialPeriodID=@FinancialPeriodID)
	) AS ControlledSubstanceIndicatorCountMissing,
(SELECT COUNT(*) 
	FROM Jurisdiction 
	WHERE JurisdictionID NOT IN (
		SELECT JurisdictionID 
		FROM JurisdictionPeriodic 
		WHERE FinancialPeriodID=@FinancialPeriodID)
	) AS JurisdictionCountMissing,
(SELECT COUNT(*) 
	FROM ControlledSubstanceIndicator 
	WHERE ControlledSubstanceIndicatorCode IN (
		SELECT ControlledSubstanceIndicatorCode 
		FROM ControlledSubstanceIndicatorPeriodic 
		WHERE FinancialPeriodID=@FinancialPeriodID)
	) AS ControlledSubstanceIndicatorCount,
(SELECT COUNT(*) 
	FROM EndorsementPeriodic
	WHERE FinancialPeriodID=@FinancialPeriodID) AS EndorsementCount,
(SELECT COUNT(*) 
	FROM Jurisdiction 
	WHERE JurisdictionID IN (
		SELECT JurisdictionID 
		FROM JurisdictionPeriodic 
		WHERE FinancialPeriodID=@FinancialPeriodID)
	) AS JurisdictionCount,

(select count(*)
	from dbo.NewMedicineServiceConsultationFee
	where FinancialPeriodID = @financialPeriodID) as NewMedicineServiceConsultationFeeCount,

(select count(*)
	from dbo.NewMedicineServiceTargetBand
	where FinancialPeriodID = @financialPeriodID) as NewMedicineServiceTargetBandCount,

(SELECT COUNT(*) 
	FROM TariffDiscountDeductionScale 
	WHERE FinancialPeriodID=@FinancialPeriodID) AS TariffDiscountDeductionScaleCount,
(SELECT COUNT(*) 
	FROM TariffEstablishmentPaymentScale 
	WHERE FinancialPeriodID=@FinancialPeriodID) AS TariffEstablishmentPaymentScaleCount,
(SELECT COUNT(*) 
	FROM TariffPracticePayment 
	WHERE FinancialPeriodID=@FinancialPeriodID) AS TariffPracticePaymentCount,
(SELECT COUNT(*) 
	FROM TariffPracticePaymentItemsDispensed 
	WHERE FinancialPeriodID=@FinancialPeriodID) AS TariffPracticePaymentItemsDispensedCount--,
--(SELECT COUNT(*) FROM TariffTransitionalPayment WHERE FinancialPeriodID=@FinancialPeriodID) AS TariffTransitionalPaymentCount

GO
/****** Object:  StoredProcedure [dbo].[FormViewModel_SelectCallback]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[FormViewModel_SelectCallback]
	@documentID bigint

as

	set nocount on;
	set transaction isolation level read uncommitted;

	/*
	 * This SP is called from the FormViewModel.SelectCallback method and provides the data that is
	 * used for custom materialization of the required document object graph.
	 *
	 * IT IS ESSENTIAL THAT THE ORDER IN WHICH THE DATA IS SELECTED IN THIS SP MATCHES THE ORDER IN 
	 * WHICH THE RESULTS ARE MATERIALISED IN THE ABOVE METHOD.
	 *
	 * Note also the "...ID as ID" in the select statements. This is because in the EF Model I have renamed
	 * each of the "...ID" properties to "ID" and the current version of the Materializer<T> class does not appear
	 * to use the model for translation, but instead simply matches column names in the DbDataReader to property 
	 * names on the object being materialised.
	 *
	 */

	select d.DocumentID as ID, d.* 
	from dbo.Document d
	where d.DocumentID = @documentID;

	select b.BatchID as ID, b.*
	from dbo.Document d
		inner join dbo.Batch b on d.BatchID = b.BatchID
	where d.DocumentID = @documentID;

	select fp.FinancialPeriodID as ID, fp.*
	from dbo.Document d
		inner join dbo.Batch b 
			inner join dbo.FinancialPeriod fp on b.FinancialPeriodID = fp.FinancialPeriodID
		on d.BatchID = b.BatchID
	where d.DocumentID = @documentID;

	select c.ContractorID as ID, c.*
	from dbo.Document d
		inner join dbo.Batch b 
			inner join dbo.Contractor c on b.ContractorID = c.ContractorID
		on d.BatchID = b.BatchID
	where d.DocumentID = @documentID;

	select dgt.DocumentGroupTypeID as ID, dgt.*
	from dbo.DocumentGroupType dgt
	where dgt.DocumentID = @documentID
	order by dgt.InsertedOn;

	select gt.GroupTypeCode as Code, gt.*
	from dbo.GroupType gt
	order by gt.GroupTypeCode;

	select i.ItemID as ID, i.*
	from dbo.Item i
	where i.DocumentID = @documentID
	order by i.Sequence, i.SubSequence;

	select iv.ItemVersionID as ID, iv.*
	from dbo.Item i
		inner join dbo.ItemVersion iv on i.ItemID = iv.ItemID
	where i.DocumentID = @documentID
	order by i.Sequence, iv.InsertedOn;

	select ppp.ProductPackPeriodicID as ID, ppp.*
	from dbo.Item i
		inner join dbo.ItemVersion iv 
			inner join dbo.ProductPackPeriodic ppp on iv.ProductPackPeriodicID = ppp.ProductPackPeriodicID
		on i.ItemID = iv.ItemID
	where i.DocumentID = @documentID;

	select pp.ProductPackID as ID, pp.*
	from dbo.Item i
		inner join dbo.ItemVersion iv 
			inner join dbo.ProductPackPeriodic ppp 
				inner join dbo.ProductPack pp on ppp.ProductPackID = pp.ProductPackID
			on iv.ProductPackPeriodicID = ppp.ProductPackPeriodicID
		on i.ItemID = iv.ItemID
	where i.DocumentID = @documentID;

	select p.ProductID as ID, p.*
	from dbo.Item i
		inner join dbo.ItemVersion iv 
			inner join dbo.ProductPackPeriodic ppp 
				inner join dbo.ProductPack pp 
					inner join dbo.Product p on pp.ProductID = p.ProductID
				on ppp.ProductPackID = pp.ProductPackID
			on iv.ProductPackPeriodicID = ppp.ProductPackPeriodicID
		on i.ItemID = iv.ItemID
	where i.DocumentID = @documentID;

	select m.ManufacturerID as ID, m.*
	from dbo.Item i
		inner join dbo.ItemVersion iv 
			inner join dbo.ProductPackPeriodic ppp 
				inner join dbo.ProductPack pp 
					inner join dbo.Product p 
						inner join dbo.Manufacturer m on p.ManufacturerID = m.ManufacturerID
					on pp.ProductID = p.ProductID
				on ppp.ProductPackID = pp.ProductPackID
			on iv.ProductPackPeriodicID = ppp.ProductPackPeriodicID
		on i.ItemID = iv.ItemID
	where i.DocumentID = @documentID;

	select pp.ProductPeriodicID as ID, pp.*
	from dbo.Item i
		inner join dbo.ItemVersion iv 
			inner join dbo.ProductPackPeriodic ppp 
				inner join dbo.ProductPeriodic pp on ppp.ProductPeriodicID = pp.ProductPeriodicID
			on iv.ProductPackPeriodicID = ppp.ProductPackPeriodicID
		on i.ItemID = iv.ItemID
	where i.DocumentID = @documentID;

	select sci.SpecialContainerIndicatorCode as Code, sci.*
	from dbo.SpecialContainerIndicator sci
	order by sci.SpecialContainerIndicatorCode;

	select dtc.DrugTariffCategoryCode as Code, dtc.*
	from dbo.DrugTariffCategory dtc
	order by dtc.DrugTariffCategoryCode;

	select ive.*
	from dbo.Item i
		inner join dbo.ItemVersion iv 
			inner join dbo.ItemVersionEndorsement ive on iv.ItemVersionID = ive.ItemVersionID
		on i.ItemID = iv.ItemID
	where i.DocumentID = @documentID
	order by i.ItemID, ive.ItemVersionID;

	select ep.EndorsementPeriodicID as ID, ep.EndorsementCode as Code, ep.*
	from dbo.Item i
		inner join dbo.ItemVersion iv 
			inner join dbo.ItemVersionEndorsement ive
				inner join dbo.EndorsementPeriodic ep on ive.EndorsementPeriodicID = ep.EndorsementPeriodicID
			on iv.ItemVersionID = ive.ItemVersionID
		on i.ItemID = iv.ItemID
	where i.DocumentID = @documentID;

	select ia.ItemAdvisoryID as ID, ia.*
	from dbo.Item i
		inner join dbo.ItemAdvisory ia on i.ItemID = ia.ItemID
	where i.DocumentID = @documentID;

	select note.ItemNoteID as ID, note.*
	from dbo.Item i
		inner join dbo.ItemNote note on i.ItemID = note.ItemID
	where i.DocumentID = @documentID;

	select af.*
	from dbo.Item i
		inner join dbo.AuditFeedback af on i.ItemID = af.ItemID
	where i.DocumentID = @documentID;

	select di.DocumentImageID as ID, di.*
	from dbo.DocumentImage di
		inner join dbo.DocumentImageType dit on di.DocumentImageTypeID = dit.DocumentImageTypeID
	where di.DocumentID = @documentID
	order by dit.DisplaySequence;

	select dit.DocumentImageTypeID as ID, dit.*
	from dbo.DocumentImageType dit;


	return

GO
/****** Object:  StoredProcedure [dbo].[FreeTextContractorSearch]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[FreeTextContractorSearch]
	@likePredicate varchar(4000),
	@freeTextPredicate varchar(4000),
	@potentiallyClosedOnly bit = 0,
	@startRowIndex integer = null,
	@maximumRows integer = null,
	@sortExpression sysname,
	@sortDirection sysname = null,
	@totalRowCount integer = null output

as

	set nocount on;
	set transaction isolation level read uncommitted;

	return 0;

GO
/****** Object:  StoredProcedure [dbo].[FreeTextGuidanceSearch]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[FreeTextGuidanceSearch]
	@freeTextPredicate varchar(4000),
	@startRowIndex integer = null,
	@maximumRows integer = null,
	@totalRowCount integer = null output

as

	return 0;
GO
/****** Object:  StoredProcedure [dbo].[FreeTextManufacturerSearch]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[FreeTextManufacturerSearch]
	@likePredicate varchar(4000),
	@freeTextPredicate varchar(4000),
	@startRowIndex integer = null,
	@maximumRows integer = null,
	@sortExpression sysname,
	@sortDirection sysname = null,
	@totalRowCount integer = null output

as

	set nocount on;
	set transaction isolation level read uncommitted;

	return 0;

GO
/****** Object:  StoredProcedure [dbo].[FreeTextProductSearch]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[FreeTextProductSearch]
	@likePredicate varchar(4000),
	@freeTextPredicate varchar(4000),
	@manufacturerId bigint = null,
	@startRowIndex integer = null,
	@maximumRows integer = null,
	@sortExpression sysname,
	@sortDirection sysname = null,
	@totalRowCount integer = null output

as

	set nocount on;
	set transaction isolation level read uncommitted;

	return 0;

GO
/****** Object:  StoredProcedure [dbo].[FreeTextProductSearchForAudit]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[FreeTextProductSearchForAudit] 
	@likePredicate varchar(4000),
	@freeTextPredicate varchar(4000),
	@financialPeriodID integer = null

as

	set nocount on;
	set transaction isolation level read uncommitted;
	
	return 0;

GO
/****** Object:  StoredProcedure [dbo].[GenerateBatchAuditStatistics]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GenerateBatchAuditStatistics]
	@batchID bigint

as

	set nocount on;
	set ansi_warnings off;

	delete from dbo.BatchAuditStatistics
	where BatchID = @batchID;

	declare @financialPeriodID integer,
		@valueOfPaymentForContainersFee money,
		@valueOfPaymentForConsumableFee money;
	select @financialPeriodID = b.FinancialPeriodID,
		@valueOfPaymentForContainersFee = jp.ContainerAllowanceValue,
		@valueOfPaymentForConsumableFee = jp.ValueOfConsumableAllowanceFee
	from dbo.Batch b
		inner join dbo.Contractor c on b.ContractorID = c.ContractorID
		inner join dbo.JurisdictionPeriodic jp on b.FinancialPeriodID = jp.FinancialPeriodID and c.JurisdictionID = jp.JurisdictionID
	where b.BatchID = @batchID;

	with verifiedItems as (

		select b.FinancialPeriodID, d.DocumentID, i.Sequence
		from dbo.Batch b
			inner join dbo.Document d
				inner join dbo.Item i	on d.DocumentID = i.DocumentID
			on b.BatchID = d.BatchID
		where b.BatchID = @batchID
		  and (i.OriginalItemVersionID is not null or i.CurrentItemVersionID is not null)
			and not exists (
				select *
				from dbo.ItemVersion iv
				where iv.ItemVersionID = i.CurrentItemVersionID
				  and iv.IsVerified = 0
			)
		group by b.FinancialPeriodID, d.DocumentID, i.Sequence

	)
	
	insert into dbo.BatchAuditStatistics (BatchID,
		NumberOfVerifiedItems, 
		NumberOfMultiLineChanges,
		NumberOfMissedItems,
		NumberOfExtraItems,
		NumberOfProductChanges,
		NumberOfQuantityChanges,
		NumberOfPackChanges,
		NumberOfProfessionalFeeChanges,
		NumberOfPatientChargeChanges,
		NumberOfSurchargeChanges,
		NumberOfInvoicePriceChanges,
		NumberOfOtherChanges,
		TotalNumberOfChanges,
		NumberOfVerifiedItemsWithChanges,
		NumberOfVerifiedItemsWithChangesThatHaveFinancialImpact,
		NetCashVariance,
		AbsCashVariance,
		NetProfessionalFeeVariance,
		AbsProfessionalFeeVariance,
		NetPaymentForContainersVariance,
		AbsPaymentForContainersVariance
	)
	
	select @batchID,
		isnull(count(*), 0),
		sum(isnull(IsMultiLine, 0)),
		sum(isnull(IsMissedItem, 0)),
		sum(isnull(IsExtraItem, 0)),
		sum(isnull(IsProductChange, 0)),
		sum(isnull(IsQuantityChange, 0)),
		sum(isnull(IsPackChange, 0)),
		sum(isnull(IsProfessionalFeeChange, 0)),
		sum(isnull(IsPatientChargeChange, 0)),
		sum(isnull(IsSurchargeChange, 0)),
		sum(isnull(IsInvoicePriceChange, 0)),
		sum(isnull(IsOtherChange, 0)),
		sum(isnull(TotalNumberOfChanges, 0)),
		isnull(count(e.DocumentID), 0),
		sum(isnull(HasFinancialImpact, 0)),

		--sum(isnull(NetCashVariance, 0)),
		sum(isnull(NetCashVariance, 0)
			+ (isnull(NetProfessionalFeeVariance, 0) * case when 201207 <= @financialPeriodID then @valueOfPaymentForConsumableFee else @valueOfPaymentForContainersFee end)
			+ (isnull(NetPaymentForContainersVariance, 0) * case when @financialPeriodID < 201207 then 0.00 else @valueOfPaymentForContainersFee end)
			+ (isnull(NetNonLiquidMethadoneContainerPaymentsVariance, 0) * case when @financialPeriodID < 201312 then 0.00 else @valueOfPaymentForContainersFee end)) AS 'NetCashVariance',
		--sum(abs(isnull(NetCashVariance, 0))),
		sum(ABS(isnull(NetCashVariance, 0))
			+ ((CASE WHEN isnull(NetProfessionalFeeVariance, 0) < 0 THEN ABS(isnull(NetProfessionalFeeVariance, 0)) ELSE isnull(NetProfessionalFeeVariance, 0) END)			
					* CASE WHEN 201207 <= @financialPeriodID THEN @valueOfPaymentForConsumableFee ELSE @valueOfPaymentForContainersFee END)
			+ ((CASE WHEN isnull(NetPaymentForContainersVariance, 0) < 0 THEN ABS(isnull(NetPaymentForContainersVariance, 0)) ELSE isnull(NetPaymentForContainersVariance, 0) END)
					* CASE WHEN @financialPeriodID < 201207 THEN 0.00 ELSE @valueOfPaymentForContainersFee END)
			+ ((CASE WHEN isnull(NetNonLiquidMethadoneContainerPaymentsVariance, 0) < 0 THEN ABS(isnull(NetNonLiquidMethadoneContainerPaymentsVariance, 0)) ELSE isnull(NetNonLiquidMethadoneContainerPaymentsVariance, 0) END)
					* CASE WHEN @financialPeriodID < 201312 THEN 0.00 ELSE @valueOfPaymentForContainersFee END)
		) AS 'AbsCashVariance',


		sum(isnull(NetProfessionalFeeVariance, 0)),
		sum(abs(isnull(NetProfessionalFeeVariance, 0))),
		sum(isnull(NetPaymentForContainersVariance, 0)),
		sum(abs(isnull(NetPaymentForContainersVariance, 0)))
	from verifiedItems v
		left join dbo.BatchItemErrorAnalysis (@batchID) e on v.DocumentID = e.DocumentID and v.Sequence = e.Sequence
	group by v.FinancialPeriodID
	order by 1
	option (optimize for (@batchID unknown));

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[GenerateCurrentScheduleOfPayments]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[GenerateCurrentScheduleOfPayments]
	@batchID bigint,
	@scheduleID bigint = null output

as

	set nocount on;
	set transaction isolation level read uncommitted;

	/*
	 * There is a lot of functionality in here that you may argue is duplicated and could all be rolled up
	 * together in order to reduce the number of database reads/writes. You are correct. It is however deliberate
	 * at this time that the functionality is duplicated in order to ease the development of the SP by breaking down
	 * each individual requirement into its own chunk rather than having massive select statements that sum and count
	 * all values at the same time.
	 *
	 * Once the functionality has been proven, time can be taken to review this SP and merge the various sums and counts
	 * into a significantly smaller number of selects. Of course, this will also be seen by the end user as a performance
	 * increase which cannot hurt either!
	 *
	 * Note: I now think that most of the "Costs" and "Charges" sections are now fairly optimal.
	 *       There may be some additional improvements that can be applied to the fees and document data still though.
	 *
	 */

	declare @financialPeriodID integer, 
			@originalScheduleID bigint,
			@contractorIsRegisteredForApplianceUseReview bit, @contractorRegisteredForApplianceUseReviewSince integer,
			@contractorIsRegisteredForStomaCustomisation bit, @contractorRegisteredForStomaCustomisationSince integer,
			@jurisdictionID integer,
			@applianceChargeAtCurrentRate money, 
			@applianceChargeAtPreviousRate money,
			@applianceUseReviewAtHomeFee money,
			@applianceUseReviewAtPharmacyFee money,
			@contractTransitionPaymentDivisor integer, 
			@contractTransitionPaymentMultiplier money,
			@drugsChargeAtCurrentRate money, @drugsChargeAtPreviousRate money,
			@expensiveItemThreshold money, @expensiveItemFeePercentage numeric (7, 2),
			@establishmentPaymentDivisor integer,
			@medicineUseReviewFee money,
			@outOfPocketExpenseDeductionPerItem money, 
			@outOfPocketExpenseMinimumClaimValue money = 0.51, -- Correct as at July 2012, retrieved from JurisdictionPeriodic
			@professionalFee money,
			@repeatDispensingFee money,
			@stomaCustomisationFee money,
			@valueOfConsumableAllowanceFee money = 0.00,
			@valueOfContainerAllowanceFee money = 0.00,		
			@numberOfApplianceUseReviewsAtHomeDeclaredOnFP34c integer, 
			@numberOfApplianceUseReviewsAtPharmacyDeclaredOnFP34c integer,
			@numberOfFp57RefundsDeclaredOnFP34c integer,
			@numberOfMedicineUseReviewsDeclaredOnFP34c integer, 
			@numberOfOutOfPocketExpenseItemsDeclaredOnFP34c integer,  
			@numberOfSupportHoursDeclaredOnFP34c integer,
			@valueOfFp57RefundsDeclaredOnFP34c money,
			@valueOfOutOfPocketExpensesDeclaredOnFP34c money,
			@valueOfSupplementaryControlledDrugFee money,
			@adjustment money;

		-- Load up what we need to know about the batch (including the submission) and the contractor, as well as
		-- details specific to the financial period and jurisdiction...
		select @financialPeriodID = b.FinancialPeriodID,
			   @originalScheduleID = b.ScheduleID,
			   @contractorIsRegisteredForApplianceUseReview = c.IsRegisteredForApplianceUseReview,
			   @contractorRegisteredForApplianceUseReviewSince = c.RegisteredForApplianceUseReviewSince,
			   @contractorIsRegisteredForStomaCustomisation = c.IsRegisteredForStomaCustomisation,
			   @contractorRegisteredForStomaCustomisationSince = c.RegisteredForStomaCustomisationSince,
			   @jurisdictionID = c.JurisdictionID,	
			   @applianceChargeAtCurrentRate = jp.ApplianceItemPrescriptionChargeValue,
			   @applianceUseReviewAtHomeFee = jp.ApplianceUseReviewAtHomeFee,
			   @applianceUseReviewAtPharmacyFee = jp.ApplianceUseReviewAtPharmacyFee,
			   @contractTransitionPaymentDivisor = jp.ContractTransitionPaymentDivisor,
			   @contractTransitionPaymentMultiplier = jp.ContractTransitionPaymentMultiplier,
			   @drugsChargeAtCurrentRate = jp.DrugItemPrescriptionChargeValue,
			   @establishmentPaymentDivisor = jp.EstablishmentPaymentDivisor,
			   @expensiveItemThreshold = jp.ExpensiveItemThreshold,
			   @expensiveItemFeePercentage = jp.ExpensiveItemFeePercentage,
			   @medicineUseReviewFee = jp.MedicinesUseReviewFee,
			   @outOfPocketExpenseDeductionPerItem = jp.OutOfPocketExpenseDeductionValue,
			   @outOfPocketExpenseMinimumClaimValue = jp.OutOfPocketExpenseMinimumClaimValue,
			   @professionalFee = jp.StandardProfessionalFeeValue,
			   @repeatDispensingFee = jp.RepeatDispensingFee,
			   @stomaCustomisationFee = jp.StomaCustomisationFee,
			   @valueOfConsumableAllowanceFee = jp.ValueOfConsumableAllowanceFee,
			   @valueOfContainerAllowanceFee = jp.ContainerAllowanceValue, 
			   @numberOfFp57RefundsDeclaredOnFP34c = s.FP57Count,
			   @numberOfApplianceUseReviewsAtHomeDeclaredOnFP34c = s.ApplianceUseReviewsHome,
			   @numberOfApplianceUseReviewsAtPharmacyDeclaredOnFP34c = s.ApplianceUseReviews,
			   @numberOfMedicineUseReviewsDeclaredOnFP34c = s.MedicineUseReviews,
			   @numberOfOutOfPocketExpenseItemsDeclaredOnFP34c = s.OutOfPocketExpenseItems,
			   @numberOfSupportHoursDeclaredOnFP34c = s.SupportHours,
			   @valueOfFp57RefundsDeclaredOnFP34c = s.FP57TotalRefund,
			   @valueOfOutOfPocketExpensesDeclaredOnFP34c = s.OutOfPocketExpenses,
			   @valueOfSupplementaryControlledDrugFee = jp.ValueOfSupplementaryControlledDrugFee
		from dbo.Batch b
			   inner join dbo.Contractor c on b.ContractorID = c.ContractorID
			   inner join dbo.JurisdictionPeriodic jp on c.JurisdictionID = jp.JurisdictionID and b.FinancialPeriodID = jp.FinancialPeriodID
			   inner join dbo.Schedule ss on b.ScheduleID = ss.ScheduleID
			   inner join dbo.Submission s on b.BatchID = s.BatchID
		where b.BatchID = @batchID;

		if (@@rowcount = 1) 
			begin
				-- We have current period details, we also need to know a few details from the previous period...
				select top 1 @applianceChargeAtPreviousRate = jp.ApplianceItemPrescriptionChargeValue
				from		 dbo.JurisdictionPeriodic jp
				where		 jp.JurisdictionID = @jurisdictionID
				and			 jp.FinancialPeriodID < @financialPeriodID
				and			 jp.ApplianceItemPrescriptionChargeValue != @applianceChargeAtCurrentRate
				order by	 jp.FinancialPeriodID desc;

				if (@@rowcount = 1) 
					begin
						select top 1 @drugsChargeAtPreviousRate = jp.DrugItemPrescriptionChargeValue
						from		 dbo.JurisdictionPeriodic jp
						where		 jp.JurisdictionID = @jurisdictionID
						and			 jp.FinancialPeriodID < @financialPeriodID
						and			 jp.DrugItemPrescriptionChargeValue != @drugsChargeAtCurrentRate
						order by	 jp.FinancialPeriodID desc;
					end
			end
	
		-- We only continue if the last queries returned a single row (each).
		-- Otherwise, something is wrong - either the batchID specified does not exist (bad) or (worse) the links resulted in
		-- multiple rows!
		if (@@rowcount = 1) 
			begin
				/*
				* DRUG & APPLIANCE COSTS...
				*
				*/

				-- Totals:
				-- total number/value of items/basic prices at SDR & ZDR (also separating the basic prices of oxygen (Part X) related products)...
				declare @numberOfMDAItems integer = 0,
						@numberOfItemsPaidAtStandardDiscountRate integer = 0, 
						@totalValueOfBasicPriceAtStandardDiscountRate money = 0.00, 
						@totalValueOfBasicPriceForPartX money = 0.00,
						@numberOfItemsPaidAtZeroDiscountRate integer = 0, 
						@totalValueOfBasicPriceAtZeroDiscountRate money = 0.00, 
						@totalValueOfOutOfPocketExpenseExpenseClaims money = 0.00,
						@totalNumberOfContainerAllowanceEndorsements integer = 0, 
						@totalNonLiquidMethadoneContainerPayments integer = 0;					
				with itemBasicPrices as (		
					select d.FragmentTypeCode, 
						   d.FormTypeCode, 
						   d.DocumentNumber, 
						   i.Sequence,
						   BasicPrice = sum(iv.BasicPrice),
						   BasicPriceForPartX = sum(case when ppp.IsEligibleForZeroDiscount = 1 and dtc.Category = 10 then BasicPrice else 0.00 end),
						   NumberOfNonMDAProfessionalFees = sum(CASE WHEN d.FormTypeCode IN ('D','T','S') THEN 0 ELSE iv.NumberOfProfessionalFees END),
						   NumberOfProfessionalFees = sum(iv.NumberOfProfessionalFees),
						   IsEligibleForZeroDiscount = cast(max(cast (ppp.IsEligibleForZeroDiscount as integer)) as bit),
						   ValueOfOutOfPocketExpenses = sum(iv.ValueOfOutOfPocketExpenses),
						   NumberOfContainerAllowanceEndorsements = count(ep.EndorsementPeriodicID),
						   HasPackagedDoseEndorsement = case when count(eep.EndorsementPeriodicID) > 0 then 1 else 0 end						 	 
					from dbo.Document d
						inner join dbo.Item i
							inner join dbo.ItemVersion iv
							inner join dbo.ProductPackPeriodic ppp left join dbo.DrugTariffCategory dtc on ppp.DrugTariffCategoryCode = dtc.DrugTariffCategoryCode
								on iv.ProductPackPeriodicID = ppp.ProductPackPeriodicID
									left join dbo.ItemVersionEndorsement ive
										inner join dbo.EndorsementPeriodic ep on ive.EndorsementPeriodicID = ep.EndorsementPeriodicID and ep.EndorsementCode = 'PK'						
								on iv.ItemVersionID = ive.ItemVersionID						
									left join dbo.ItemVersionEndorsement iive
										inner join dbo.EndorsementPeriodic eep on iive.EndorsementPeriodicID = eep.EndorsementPeriodicID and (eep.EndorsementCode = 'CS')
								on iv.ItemVersionID = iive.ItemVersionID	
							on i.CurrentItemVersionID = iv.ItemVersionID
						on d.DocumentID = i.DocumentID
					where d.BatchID = @batchID
					group by d.FragmentTypeCode, d.DocumentNumber, d.FormTypeCode, i.Sequence			
					)
				select @numberOfMDAItems = sum(case when IsEligibleForZeroDiscount = 0 then NumberOfProfessionalFees else 0.00 end) - sum(case when IsEligibleForZeroDiscount = 0 then NumberOfNonMDAProfessionalFees else 0.00 end),
					   @numberOfItemsPaidAtStandardDiscountRate = sum(case when IsEligibleForZeroDiscount = 0 then NumberOfProfessionalFees else 0.00 end),	
					   @totalValueOfBasicPriceAtStandardDiscountRate = sum(case when IsEligibleForZeroDiscount = 0 then BasicPrice else 0.00 end),
					   @totalValueOfBasicPriceForPartX = sum(BasicPriceForPartX),
					   @numberOfItemsPaidAtZeroDiscountRate = sum(case when IsEligibleForZeroDiscount = 1 then NumberOfProfessionalFees else 0.00 end),
					   @totalValueOfBasicPriceAtZeroDiscountRate = sum(case when IsEligibleForZeroDiscount = 1 then BasicPrice else 0.00 end),
					   @totalValueOfOutOfPocketExpenseExpenseClaims = sum(case when ValueOfOutOfPocketExpenses >= @outOfPocketExpenseMinimumClaimValue then ValueOfOutOfPocketExpenses else 0.00 end),
					   @totalNumberOfContainerAllowanceEndorsements = sum(NumberOfContainerAllowanceEndorsements),
					   @totalNonLiquidMethadoneContainerPayments = sum(case when NumberOfContainerAllowanceEndorsements > 0 AND HasPackagedDoseEndorsement = 0 AND NumberOfProfessionalFees > 1 AND (FormTypeCode = 'S' OR FormTypeCode = 'T' OR FormTypeCode = 'D') then (NumberOfProfessionalFees - 1) else 0 end)					 
				from itemBasicPrices;				

				declare @totalNumberOfProfessionalFees integer = @numberOfItemsPaidAtStandardDiscountRate + @numberOfItemsPaidAtZeroDiscountRate;

				-- Discount:
				-- determine the discount rate % from the tariff Part V based upon the sub total of all basic prices at standard discount rate less the value of the 
				-- of oxygen (Part X) related products
				declare @totalValueOfBasicPriceAtStandardDiscountRateLessPartX money = @totalValueOfBasicPriceAtStandardDiscountRate - @totalValueOfBasicPriceForPartX,
						@standardDiscountDeductionPercentage money = 0.00;
		
				select @standardDiscountDeductionPercentage = dds.DeductionRate
				from   dbo.TariffDiscountDeductionScale dds
				where  dds.FinancialPeriodID = @financialPeriodID
				and	   floor(@totalValueOfBasicPriceAtStandardDiscountRateLessPartX) between dds.[From] and dds.[To];
		
				-- Using the discount rate determined above, calculate the discount against all basic prices at standard discount rate less the value of the 
				-- of oxygen (Part X) related products
				declare @valueOfStandardDiscount money = round(@totalValueOfBasicPriceAtStandardDiscountRateLessPartX * (@standardDiscountDeductionPercentage / 100), 2);

				-- Subtotal of basic prices:
				-- the sum of the three above totals...
				declare @subTotalOfBasicPrice money = (@totalValueOfBasicPriceAtStandardDiscountRate - @valueOfStandardDiscount) + @totalValueOfBasicPriceAtZeroDiscountRate;

				-- Out of pocket expenses:
				-- This has already been totalled from the individual item versions, but if we are prior to July 2012 then any claims entered agains those individual
				-- item versions are ignored and overriden by the value of the out of pocket expenses from the FP34C submission document,
				-- less the number of out of pocket expense items multiplied by the deduction value specified in tariff part II.12...
				if (@financialPeriodID < 201207) 
					begin
						set @totalValueOfOutOfPocketExpenseExpenseClaims = @valueOfOutOfPocketExpensesDeclaredOnFP34c - round(@numberOfOutOfPocketExpenseItemsDeclaredOnFP34c * @outOfPocketExpenseDeductionPerItem, 2);
					end

				-- Consumable/Container allowances:
				declare @totalNumberOfContainerAllowanceFees integer = 0, @totalNumberOfConsumableAllowanceFees integer = 0,
						@totalValueOfContainerAllowanceFees money = 0.00, @totalValueOfConsumableAllowanceFees money = 0.00;
				
				if (@financialPeriodID < 201207) 
					begin
						-- Prior to July 2012, the total number of professional fees paid, multiplied by the container allowance value specified in tariff part IV...
						set @totalNumberOfContainerAllowanceFees = @totalNumberOfProfessionalFees;
						set @totalValueOfContainerAllowanceFees = round(@totalNumberOfContainerAllowanceFees * @valueOfContainerAllowanceFee, 2);
					end 
				else 
					begin
						-- Since July 2012 (Simplification)...
						set @totalNumberOfConsumableAllowanceFees = @totalNumberOfProfessionalFees;
						set @totalValueOfConsumableAllowanceFees = round(@totalNumberOfProfessionalFees * @valueOfConsumableAllowanceFee, 2);
						select @adjustment = sc.adjustment
						from   dbo.ScheduleCost sc
						where  sc.ScheduleID = @originalScheduleID;

						if (@financialPeriodID between 201207 and 201209) 
							begin
								-- Between July 2012 and September 2012 (inclusive), BSA were applying the PK endorsement incorrectly (misinterpretation of the tariff rules).
								-- As such, we will simply use the "Payment for containers" figures provided on the BSA schedule...
								select @totalNumberOfContainerAllowanceFees = sc.ContainerAllowanceQuantity,
									   @valueOfContainerAllowanceFee = sc.ContainerAllowancePrice,
									   @totalValueOfContainerAllowanceFees = sc.ContainerAllowanceValue
								from   dbo.ScheduleCost sc
								where  sc.ScheduleID = @originalScheduleID;
							end 
						else 
							begin
								-- From October 2012 onward, we should be calculating "Payment for containers" in the same manner
								-- as the BSA are calculating it (though this remains to be seen as I know further changes are being requested).
								set @totalNumberOfContainerAllowanceFees = @totalNumberOfContainerAllowanceEndorsements;
								set @totalValueOfContainerAllowanceFees = round(@totalNumberOfContainerAllowanceFees * @valueOfContainerAllowanceFee, 2);
				
								if (@financialPeriodID >= 201312) 
									begin
										-- From December 2013 onwards we need to add a payment for container fee per interaction on eligible non-liquid 
										-- methadone products. 
										set @totalValueOfContainerAllowanceFees = @totalValueOfContainerAllowanceFees + round(@totalNonLiquidMethadoneContainerPayments * @valueOfContainerAllowanceFee, 2);
									end				
							end
					end			

				-- Total of drug and appliance costs:
				declare @totalOfDrugAndApplianceCosts money = @subTotalOfBasicPrice + @totalValueOfOutOfPocketExpenseExpenseClaims + @totalValueOfConsumableAllowanceFees + @totalValueOfContainerAllowanceFees + @adjustment;

				/*
				 * PRESCRIPTION FEES...
				 *
				 */

				declare @valueOfAdditionalFees2a money = 0.00, 
						@valueOfAdditionalFees2bHomeDelivery money = 0.00, 
						@valueOfAdditionalFees2bMeasuredFitted money = 0.00, 
						@valueOfAdditionalFees2c money = 0.00, 
						@valueOfAdditionalFees2d money = 0.00, 
						@valueOfAdditionalFees2e money = 0.00,
						@valueOfAdditionalFeesMethadone money = 0.00,
						@additionaFees2fCount integer = 0, @valueOfAdditionalFees2f money = 0.00,
						@valueOfEstablishmentPayment money = 0.00,
						@valueOfMedicineUseReviews money = 0.00,
						@valueOfPharmacyContractTransitionPayment money = 0.00,
						@valueOfPracticePayment money = 0.00,
						@totalValueOfProfessionalFees money = 0.00,
						@subTotalOfFees money = 0.00,
						@totalOfAllFees money = 0.00,
						@valueOfStandardFees money = 0.00,
						@numberOfNewMedicineServiceItems integer = 0,
						@numberOfNewMedicineServicesUndertaken integer = 0,
						@valueOfNewMedicineServiceConsultationFees money = 0.00,
						@valueOfApplianceUseReviewAtHomeFees money = 0.00,
						@valueOfApplianceUseReviewAtPharmacyFees money = 0.00,
						@ProtectedAdditionalPayment money = 0.00,
						@DisposalOfUnwantedMedicinesAndSignposting money = 0.00,
						@PromotionOfHealthyLifestyleAndSupportForSelfCare money = 0.00,
						@ClinicalGovernance money = 0.00,
						@SupportForPeopleWithDisabilities money = 0.00;

				-- Professional fee:
				-- the total number of professional fees paid multiplied by the value specified in tariff part IIIA.1

				select @totalValueOfProfessionalFees = round(@professionalFee * @totalNumberOfProfessionalFees, 2);

				-- Pharmacy contract transition payment (only applicable in England to batches prior to April 2011):
				-- the total number of professional fees paid divided by 500, rounded up to the next whole number, multiplied by £7.40. 
				-- For all batches from April 2011 onwards, this value must be 0 (zero) - except in Wales.
				set @valueOfPharmacyContractTransitionPayment = 0;

				if (@contractTransitionPaymentDivisor is not null and @contractTransitionPaymentDivisor != 0 and @contractTransitionPaymentMultiplier is not null and @contractTransitionPaymentMultiplier != 0) 
					begin
						declare @pctpNumberOfStandardFees float = @totalNumberOfProfessionalFees,
								@pctpDivisor float = @contractTransitionPaymentDivisor;
					
						set @valueOfPharmacyContractTransitionPayment = round(ceiling(@pctpNumberOfStandardFees / @pctpDivisor) * @contractTransitionPaymentMultiplier, 2);
					end

				-- Practice Payment (now moved out to a function)...
				set @valueOfPracticePayment = dbo.CalculateValueOfPracticePayment(@financialPeriodID, @jurisdictionID, @totalNumberOfProfessionalFees, @numberOfSupportHoursDeclaredOnFP34c, @originalScheduleID);

				-- The following fees can all be calculated at the same time as the methodology is the same (we simply need split the results)...
				--   Additional Fees 2A: sum of the value of all “LB”, “LC”, “LE” & “LF” endorsements...
				--   Additional Fees 2C: sum of the value of all “AA” endorsements...
				--   Additional Fees 2D: sum of the value of all “AB” endorsements...
				with endorsements as (
					select d.DocumentID, 
						   i.Sequence, 
						   NumberOfProfessionalFees = (select sum(iv2.NumberOfProfessionalFees)
													   from   dbo.Item i2 inner join dbo.ItemVersion iv2 on i2.CurrentItemVersionID = iv2.ItemVersionID
													   where  i2.DocumentID = d.DocumentID and i2.Sequence = i.Sequence),
						   ep.EndorsementCode, 
						   ep.SpecialFeeBasis, 
						   ep.SpecialFeeValue, 
						   ive.Quantity
					from   dbo.Document d
						   inner join dbo.Item i
								inner join dbo.ItemVersion iv
									inner join dbo.ItemVersionEndorsement ive
										inner join dbo.EndorsementPeriodic ep 
										on ive.EndorsementPeriodicID = ep.EndorsementPeriodicID
									on iv.ItemVersionID = ive.ItemVersionID
								on i.CurrentItemVersionID = iv.ItemVersionID
							on d.DocumentID = i.DocumentID
					where  d.BatchID = @batchID
				), 
				summary as (
					select EndorsementCode, 
						   ValueOfEndorsements = isnull(sum(round(isnull(case SpecialFeeBasis when '1' then SpecialFeeValue
																							  when 'S' then NumberOfProfessionalFees * SpecialFeeValue
																							  when 'Q' then Quantity * SpecialFeeValue
																							  else 0 end, 0), 2)), 0)
					from endorsements
					group by EndorsementCode
				)				
				select @valueOfAdditionalFees2a = case when EndorsementCode in ('LB', 'LC', 'LE', 'LF') then @valueOfAdditionalFees2a + ValueOfEndorsements else @valueOfAdditionalFees2a end,
					   @valueOfAdditionalFees2c = case when EndorsementCode = 'AA' then ValueOfEndorsements else @valueOfAdditionalFees2c end,
					   @valueOfAdditionalFees2d = case when EndorsementCode = 'AB' then ValueOfEndorsements else @valueOfAdditionalFees2d end,
					   @valueOfAdditionalFeesMethadone = case when EndorsementCode = 'CS' then ValueOfEndorsements else @valueOfAdditionalFeesMethadone end
				from summary;
		
				-- Additional Fees 2B Home Delivery:
				-- For each prescription item where the product(s) are eligible for home delivery, we pay a single home delivery fee which is the higher of
				-- the fees payable for the individual products. (If for example we have a multi-line item and one product attracts the standard home delivery
				-- fee but another attracts the higher home delivery fee, pay only a single instance of the higher fee). Note that the fees themselves are
				-- stored as EndorsementPeriodic rows where the endorsement code is "H" + the home delivery indicator code ("H1" & "H2" at the time of writing)...
				with homeDeliveryFees as (
					select d.DocumentID, 
						   i.Sequence, 
						   isnull(ep.SpecialFeeValue, 0) as Value
					from   dbo.Document d
						   inner join dbo.Item i
								inner join dbo.ItemVersion iv
									inner join dbo.ProductPackPeriodic ppp 
										inner join dbo.HomeDeliveryIndicator hdi
											inner join dbo.EndorsementPeriodic ep 
											on 'H' + hdi.HomeDeliveryIndicatorCode = ep.EndorsementCode	and ep.FinancialPeriodID = @financialPeriodID and ep.JurisdictionID = @jurisdictionID
										on ppp.HomeDeliveryIndicatorCode = hdi.HomeDeliveryIndicatorCode
		 								inner join dbo.DrugTariffCategory dtc 
										on ppp.DrugTariffCategoryCode = dtc.DrugTariffCategoryCode and dtc.Category = 9
									on iv.ProductPackPeriodicID = ppp.ProductPackPeriodicID
								on i.CurrentItemVersionID = iv.ItemVersionID
						   on d.DocumentID = i.DocumentID
					where  d.BatchID = @batchID
				), 
				maxHomeDeliveryFees as (
					select DocumentID, 
						   Sequence, 
						   max(Value) as Value
					from   homeDeliveryFees
					group by DocumentID, Sequence
				)
		
				select @valueOfAdditionalFees2bHomeDelivery = isnull(sum(Value), cast (0.00 as money))
				from maxHomeDeliveryFees;

				-- Additional Fees 2B Measured and Fitted:
				-- sum of the value of all “MF” endorsements applied to the item versions where the product is an appliance that is indicated as being measured and fitted...
				with endorsements as (
					select d.DocumentID, 
						   i.Sequence, 
						   NumberOfProfessionalFees = (select sum(iv2.NumberOfProfessionalFees)
													   from   dbo.Item i2 inner join dbo.ItemVersion iv2 on i2.CurrentItemVersionID = iv2.ItemVersionID
													   where  i2.DocumentID = d.DocumentID and i2.Sequence = i.Sequence),
						   ep.SpecialFeeBasis, 
						   ep.SpecialFeeValue, 
						   ive.Quantity
					from   dbo.Document d
						   inner join dbo.Item i
								inner join dbo.ItemVersion iv
									inner join dbo.ItemVersionEndorsement ive
										inner join dbo.EndorsementPeriodic ep 
										on ive.EndorsementPeriodicID = ep.EndorsementPeriodicID and ep.EndorsementCode = 'MF'
									on iv.ItemVersionID = ive.ItemVersionID
									inner join dbo.ProductPackPeriodic ppp
										inner join dbo.ProductPeriodic pp 
											inner join dbo.ApplianceTypeIndicator ati 
											on pp.ApplianceTypeIndicatorCode = ati.ApplianceTypeIndicatorCode and ati.IsMeasuredAndFitted = 1
										on ppp.ProductPeriodicID = pp.ProductPeriodicID
									on iv.ProductPackPeriodicID = ppp.ProductPackPeriodicID
								on i.CurrentItemVersionID = iv.ItemVersionID
							on d.DocumentID = i.DocumentID
					where  d.BatchID = @batchID
				)
				select @valueOfAdditionalFees2bMeasuredFitted = isnull(sum(round(isnull(case SpecialFeeBasis when '1' then SpecialFeeValue
																											 when 'S' then NumberOfProfessionalFees * SpecialFeeValue
																											 when 'Q' then Quantity * SpecialFeeValue
																											 else 0 end, 0), 2)), 0)
				from   endorsements;

				-- Additional Fees 2E:
				-- sum of all controlled drug fees...
				-- For each prescription item where the product(s) are eligible for a controlled substance fee, we pay a single fee which is the higher of
				-- the fees payable for the individual products. (If for example we have a multi-line item and one product attracts a lower controlled substance
				-- fee but another attracts a higher controlled substance fee, pay only a single instance of the higher fee)...
				with controlledSubstanceIndicators as (
					select d.DocumentID, 
						   i.Sequence,
						   NumberOfProfessionalFees = (select sum(iv2.NumberOfProfessionalFees)
													   from   dbo.Item i2 inner join dbo.ItemVersion iv2 on i2.CurrentItemVersionID = iv2.ItemVersionID
													   where  i2.DocumentID = d.DocumentID and i2.Sequence = i.Sequence),
						   AdditionalFeeValue = isnull(csip.AdditionalProfessionalFeeValue, 0),
						   NumberOfSupplementaryControlledDrugFees = cast(pp.IsEligibleForSupplementaryControlledDrugFee as integer)
					from   dbo.Document d
								inner join dbo.Item i
									inner join dbo.ItemVersion iv
										inner join dbo.ProductPackPeriodic ppp 
											inner join dbo.ProductPeriodic pp 
												inner join dbo.ControlledSubstanceIndicatorPeriodic csip 
												on pp.ControlledSubstanceIndicatorCode = csip.ControlledSubstanceIndicatorCode and csip.FinancialPeriodID = @financialPeriodID and csip.JurisdictionID = @jurisdictionID
											on ppp.ProductPeriodicID = pp.ProductPeriodicID
										on iv.ProductPackPeriodicID = ppp.ProductPackPeriodicID
									on i.CurrentItemVersionID = iv.ItemVersionID
								on d.DocumentID = i.DocumentID
					where  d.BatchID = @batchID
				), 
				controlledSubstanceFees as (
					select DocumentID, 
						   Sequence, 
						   NumberOfProfessionalFees * AdditionalFeeValue as AdditionalFeeValue,
						   NumberOfSupplementaryControlledDrugFees * @valueOfSupplementaryControlledDrugFee as SupplementaryControlledDrugFeeValue
					from   controlledSubstanceIndicators		
				), 
				maxControlledSubstanceFees as (
					select DocumentID, 
						   Sequence, 
						   max(AdditionalFeeValue) as AdditionalFeeValue, 
						   sum(SupplementaryControlledDrugFeeValue) as SupplementaryControlledDrugFeeValue
					from   controlledSubstanceFees
					group by DocumentID, Sequence
				)	
				select @valueOfAdditionalFees2e = isnull(sum(AdditionalFeeValue), 0), 
					   @valueOfAdditionalFeesMethadone = isnull(sum(SupplementaryControlledDrugFeeValue), 0) + @valueOfAdditionalFeesMethadone
				from   maxControlledSubstanceFees;					

				-- Additional Fees 2F:
				-- Number of items where the basic price exceeds the value specified in tariff part IIIA.2.F; 
				-- and the value of the sum of the basic price of those items multiplied by the fee %age specified in the same rule.
				-- Note that it is necessary to recalculate the basic prices at an item level (rolling up the individual products) as
				--  (a) the fee is payable against the sum of all products dispensed against an individual prescription item
				--  (b) the basic price specified against each product may have been adjusted for broken bulk whereas the fee is payable against the cost of the quanity dispensed
				with recalculatedPrices as (
					select BasicPrice = round(sum(cast(case when ep.EndorsementCode is not null or ppp.PackPrice = 0.00 then iv.BasicPrice
															else round(case	when ppp.SpecialContainerIndicatorCode = 'S' then 
															case when pp.ContainerSize = 1 and pp.PackSize > 1 and ppp.PackPrice != ppp.UnitPrice then ((ppp.PackPrice * 100) / pp.PackSize) * iv.Quantity
																 when pp.ContainerSize != 0 then (ppp.PackPrice * 100) * (iv.Quantity / pp.ContainerSize) end
															when ppp.SpecialContainerIndicatorCode = 'B' and pp.ContainerSize != 0 then ((ppp.PackPrice * 100) * iv.Quantity) / pp.ContainerSize
															when pp.PackSize != 0 then ((ppp.PackPrice * 100) * iv.Quantity) / pp.PackSize
															else 0.00 end, 0) / 100	end as money)), 2)
					from   dbo.Document d
								inner join dbo.Item i 
									inner join dbo.ItemVersion iv
										inner join dbo.ProductPackPeriodic ppp 
											inner join dbo.ProductPack pp 
											on ppp.ProductPackID = pp.ProductPackID
										on iv.ProductPackPeriodicID = ppp.ProductPackPeriodicID
										left join dbo.ItemVersionEndorsement ive 
											inner join dbo.EndorsementPeriodic ep 
											on ive.EndorsementPeriodicID = ep.EndorsementPeriodicID and ep.EndorsementCode = 'IP'
										on iv.ItemVersionID = ive.ItemVersionID
									on i.CurrentItemVersionID = iv.ItemVersionID
								on d.DocumentID = i.DocumentID
					where  d.BatchID = @batchID
					group by i.DocumentID, i.Sequence
				)

				select @additionaFees2fCount = isnull(count(*), 0),
					   @valueOfAdditionalFees2f = isnull(sum(round(isnull(BasicPrice, 0) * (@expensiveItemFeePercentage / 100), 2)), 0)
				from   recalculatedPrices
				where  BasicPrice >= @expensiveItemThreshold;

				-- Establishment payment:
				-- lookup the number of professional fees against the table in Part VIA.1 and pay the value specified...
				set @valueOfEstablishmentPayment = 0;
				
				select @valueOfEstablishmentPayment = round(eps.EstablishmentPayment / @establishmentPaymentDivisor, 2)
				from   dbo.TariffEstablishmentPaymentScale eps
				where  eps.FinancialPeriodID = @financialPeriodID 
				and    eps.JurisdictionID = @jurisdictionID
				and    @totalNumberOfProfessionalFees between eps.ItemsFrom and eps.ItemsTo;

				-- Subtotal of prescription fees:
				-- = the sum of the values calculated so far...
				set @subTotalOfFees = @totalValueOfProfessionalFees
									+ @valueOfPharmacyContractTransitionPayment
									+ (@repeatDispensingFee / 12)
									+ @valueOfPracticePayment
									+ @valueOfAdditionalFees2a
									+ @valueOfAdditionalFees2bHomeDelivery 
									+ @valueOfAdditionalFees2bMeasuredFitted
									+ @valueOfAdditionalFees2c
									+ @valueOfAdditionalFees2d
									+ @valueOfAdditionalFees2e 
									+ @valueOfAdditionalFeesMethadone
									+ @valueOfAdditionalFees2f
									+ @valueOfEstablishmentPayment;

				-- Other fees, Medicines use reviews:
				-- the number of Medicines Use Reviews undertaken from the FP34C declaration multiplied by the value specified in tariff part VIC.2...
				-- Note, the number of reviews and the fee for each review are actually loaded at the start of the SP
				set @valueOfMedicineUseReviews = round(@medicineUseReviewFee * @numberOfMedicineUseReviewsDeclaredOnFP34c, 2);

				-- Other fees, Appliance use reviews carried out at patients home:
				-- the number of Appliance Use reviews conducted at the users home from the FP34C declaration multiplied by the value specified 
				-- in Part VIE for a “first” review conducted at the users home...
				-- Note, the number of reviews and the fee for each review are actually loaded at the start of the SP
				if (@contractorIsRegisteredForApplianceUseReview = 1 and @contractorRegisteredForApplianceUseReviewSince <= @financialPeriodID)
					set @valueOfApplianceUseReviewAtHomeFees = round(@applianceUseReviewAtHomeFee * @numberOfApplianceUseReviewsAtHomeDeclaredOnFP34c, 2);

				-- Other fees, Appliance use reviews carried out at premises:
				-- the number of Appliance Use Reviews carried out at premises or subsequent reviews for users living at the same location within a 24 hour period
				-- from the FP34C declaration multiplied by the value specified in tariff part VIE for a “subsequent” review conducted at the users home...
				-- Note, the number of reviews and the fee for each review are actually loaded at the start of the SP
				if (@contractorIsRegisteredForApplianceUseReview = 1 and @contractorRegisteredForApplianceUseReviewSince <= @financialPeriodID)
					set @valueOfApplianceUseReviewAtPharmacyFees = round(@applianceUseReviewAtPharmacyFee * @numberOfApplianceUseReviewsAtPharmacyDeclaredOnFP34c, 2);

				/* TODO: Determine if Stoma Customisation is offerred, and if so, how many items qualify and calculate the fees payable */
				declare @numberOfStomaCustomisations integer = 0, 
						@valueOfStomaCustomisationFees money;

				if (@contractorIsRegisteredForStomaCustomisation = 1 and @contractorRegisteredForStomaCustomisationSince <= @financialPeriodID) 
					begin
						select @numberOfStomaCustomisations = count(*)
						from   dbo.Document d
									inner join dbo.Item i
										inner join dbo.ItemVersion iv
											inner join dbo.ProductPackPeriodic ppp
												inner join dbo.ProductPeriodic pp
													inner join dbo.Product p 
													on pp.ProductID = p.ProductID and p.IsEligibleForStomaCustomisationFee = 1
												on ppp.ProductPeriodicID = pp.ProductPeriodicID
											inner join dbo.DrugTariffCategory dtc on ppp.DrugTariffCategoryCode = dtc.DrugTariffCategoryCode and dtc.Category = 9
											on iv.ProductPackPeriodicID = ppp.ProductPackPeriodicID
										on i.CurrentItemVersionID = iv.ItemVersionID
									on d.DocumentID = i.DocumentID
						where  d.BatchID = @batchID
					end
		
				set @valueOfStomaCustomisationFees = round(@numberOfStomaCustomisations * @stomaCustomisationFee, 2);

				-- Other fees, New Medicine Service...
				if (@financialPeriodID <= 201204) 
					begin
						select @numberOfNewMedicineServicesUndertaken = isnull(NewMedicineServicesUndertaken, @numberOfNewMedicineServicesUndertaken)
						from   dbo.ScheduleDocumentData sd
						where  sd.ScheduleID = @originalScheduleID
					end 
				else 
					begin
						select @numberOfNewMedicineServicesUndertaken = isnull(NewMedicineServiceConsultations, @numberOfNewMedicineServicesUndertaken)
						from   dbo.Submission s
						where  s.BatchID = @batchID
					end
		
				set @numberOfNewMedicineServiceItems = dbo.CalculateNumberOfNewMedicineServiceItems(@batchID, @financialPeriodID);
				set @valueOfNewMedicineServiceConsultationFees = dbo.CalculateValueOfNewMedicineServiceFee(@batchID, @financialPeriodID, @numberOfNewMedicineServiceItems, @numberOfNewMedicineServicesUndertaken);

				-- Total of all fees:
				-- the “subtotal of prescription fees” + the sum of the “Other fees”...
				set @totalOfAllFees = @subTotalOfFees
									+ @valueOfMedicineUseReviews
									+ @valueOfApplianceUseReviewAtHomeFees
									+ @valueOfApplianceUseReviewAtPharmacyFees
									+ @valueOfStomaCustomisationFees
									+ @valueOfNewMedicineServiceConsultationFees;

				/*
				* Charges...
				*
				*/

				declare @numberOfChargesForApplianceItemsAtCurrentRate integer = 0, 
						@valueOfChargesForApplianceItemsAtCurrentRate money = 0.00,
						@numberOfChargesForApplianceItemsAtPreviousRate integer = 0, 
						@valueOfChargesForApplianceItemsAtPreviousRate money = 0.00,
						@numberOfChargesForDrugItemsAtCurrentRate integer = 0, 
						@valueOfChargesForDrugItemsAtCurrentRate money = 0.00,
						@numberOfChargesForDrugItemsAtPreviousRate integer = 0, 
						@valueOfChargesForDrugItemsAtPreviousRate money = 0.00,
						@totalvalueOfCharges money = 0.00;

				declare @numberOfChargesForApplianceItemsExcludingHoiseryAtCurrentRate integer = 0, 
						@valueOfChargesForApplianceItemsExcludingHoiseryAtCurrentRate money = 0.00, 
						@numberOfChargesForApplianceItemsExcludingHoiseryAtPreviousRate integer = 0, 
						@valueOfChargesForApplianceItemsExcludingHoiseryAtPreviousRate money = 0.00,
						@numberOfChargesForHoiseryApplianceItemsAtCurrentRate integer = 0, 
						@valueOfChargesForHoiseryApplianceItemsAtCurrentRate money = 0.00, 
						@numberOfChargesForHoiseryApplianceItemsAtPreviousRate integer = 0, 
						@valueOfChargesForHoiseryApplianceItemsAtPreviousRate money = 0.00;

				with charges as (
					select dgt.GroupTypeCode,
						   p.ProductTypeCode,
						   IsHosiery = isnull(ati.IsHosiery, 0),
						   iv.NumberOfCharges
					from   dbo.Document d
								inner join dbo.Item i
									inner join dbo.ItemVersion iv
										inner join dbo.ProductPackPeriodic ppp 
											inner join dbo.ProductPeriodic pp 
												inner join dbo.Product p 
												on pp.ProductID = p.ProductID
													left join dbo.ApplianceTypeIndicator ati 
													on pp.ApplianceTypeIndicatorCode = ati.ApplianceTypeIndicatorCode
												on ppp.ProductPeriodicID = pp.ProductPeriodicID
											on iv.ProductPackPeriodicID = ppp.ProductPackPeriodicID
										on i.CurrentItemVersionID = iv.ItemVersionID
									on d.DocumentID = i.DocumentID
								inner join dbo.DocumentGroupType dgt 
								on d.CurrentDocumentGroupTypeID = dgt.DocumentGroupTypeID
					where  d.BatchID = @batchID				
				)
				select @numberOfChargesForApplianceItemsAtCurrentRate = isnull(sum(
							case when GroupTypeCode = '2' and ProductTypeCode = 'A' then NumberOfCharges else 0 end), cast (0.00 as money)),
					   @numberOfChargesForDrugItemsAtCurrentRate = isnull(sum(
							case when GroupTypeCode = '2' and ProductTypeCode = 'D' then NumberOfCharges else 0 end), cast (0.00 as money)),
					   @numberOfChargesForApplianceItemsAtPreviousRate = isnull(sum(
							case when GroupTypeCode = '3' and ProductTypeCode = 'A' then NumberOfCharges else 0 end), cast (0.00 as money)),
					   @numberOfChargesForDrugItemsAtPreviousRate = isnull(sum(
							case when GroupTypeCode = '3' and ProductTypeCode = 'D' then NumberOfCharges else 0 end), cast (0.00 as money)),							
					   @numberOfChargesForApplianceItemsExcludingHoiseryAtCurrentRate = isnull(sum(
							case when GroupTypeCode = '2' and IsHosiery = 0 then NumberOfCharges else 0 end), cast (0.00 as money)),
					   @numberOfChargesForApplianceItemsExcludingHoiseryAtPreviousRate = isnull(sum(
							case when GroupTypeCode = '3' and IsHosiery = 0 then NumberOfCharges else 0 end), cast (0.00 as money)),
				       @numberOfChargesForHoiseryApplianceItemsAtCurrentRate = isnull(sum(
							case when GroupTypeCode = '2' and IsHosiery = 1 then NumberOfCharges else 0 end), cast (0.00 as money)),
					   @numberOfChargesForHoiseryApplianceItemsAtPreviousRate = isnull(sum(
							case when GroupTypeCode = '3' and IsHosiery = 1 then NumberOfCharges else 0 end), cast (0.00 as money))					
				from   charges;
			
				set @valueOfChargesForApplianceItemsAtCurrentRate = round(@numberOfChargesForApplianceItemsAtCurrentRate * @applianceChargeAtCurrentRate, 2);
				set @valueOfChargesForDrugItemsAtCurrentRate = round(@numberOfChargesForDrugItemsAtCurrentRate * @drugsChargeAtCurrentRate, 2);
				set @valueOfChargesForApplianceItemsAtPreviousRate = round(@numberOfChargesForApplianceItemsAtPreviousRate * @applianceChargeAtPreviousRate, 2);
				set @valueOfChargesForDrugItemsAtPreviousRate = round(@numberOfChargesForDrugItemsAtPreviousRate * @drugsChargeAtPreviousRate, 2);
				set @valueOfChargesForApplianceItemsExcludingHoiseryAtCurrentRate = round(@numberOfChargesForApplianceItemsExcludingHoiseryAtCurrentRate * @applianceChargeAtCurrentRate, 2);
				set @valueOfChargesForApplianceItemsExcludingHoiseryAtPreviousRate = round(@numberOfChargesForApplianceItemsExcludingHoiseryAtPreviousRate * @applianceChargeAtPreviousRate, 2);
				set @valueOfChargesForHoiseryApplianceItemsAtCurrentRate = round(@numberOfChargesForHoiseryApplianceItemsAtCurrentRate * @applianceChargeAtCurrentRate, 2);
				set @valueOfChargesForHoiseryApplianceItemsAtPreviousRate = round(@numberOfChargesForHoiseryApplianceItemsAtPreviousRate * @applianceChargeAtPreviousRate, 2);		
				set @totalValueOfCharges = @valueOfChargesForApplianceItemsAtCurrentRate + @valueOfChargesForApplianceItemsAtPreviousRate + @valueOfChargesForDrugItemsAtCurrentRate + @valueOfChargesForDrugItemsAtPreviousRate - @valueOfFp57RefundsDeclaredOnFP34c

				/*
				 * PRESCRIPTION DATA...
				*
				*/

				declare @averageItemValue money,
						@numberOfEpsFormsReceived integer = 0, 
						@numberOfEpsItemsReceived integer = 0,
						@numberOfItemsReferredBackToContractor integer = 0, 
						@numberOfFormsReferredBackToContractor integer = 0, 
						@numberOfItemsDisallowed integer = 0, 
						@numberOfFormsDisallowed integer = 0, 
						@totalNumerOfFormsReceived integer = 0;

				-- Total forms received (including electronic prescriptions):
				-- total number of forms processed in the batch...
				select @totalNumerOfFormsReceived = count(d.DocumentID)
				from   dbo.Document d
				where  d.BatchID = @batchID;
		
				select @numberOfEpsFormsReceived = count(d.DocumentID)
				from   dbo.Document d
				where  d.BatchID = @batchID and d.FragmentTypeCode = 'Z';
		
				select @numberOfEpsItemsReceived = count(i.ItemID)
				from   dbo.Document d inner join dbo.Item i on d.DocumentID = i.DocumentID
				where  d.BatchID = @batchID and d.FragmentTypeCode = 'Z' and i.CurrentItemVersionID is not null;
		
				-- Number of items at SDR & ZDR are already counted above.
				-- Total of items, for which a fee is paid: = @totalNumberOfProfessionalFees

				-- Average item value:
				-- (“DRUG AND APPLIANCE COSTS : Total of drug and appliance costs” + “PRESCRIPTION FEES : Sub total of prescription fees”) / total “professional” fees
				set @averageItemValue = round((@totalOfDrugAndApplianceCosts + @subTotalOfFees) / @totalNumberOfProfessionalFees, 2);

				select @numberOfFormsReferredBackToContractor = count(distinct d.DocumentID),
					   @numberOfItemsReferredBackToContractor = count(i.ItemID)
				from   dbo.Document d
							inner join dbo.Item i
								inner join dbo.ItemVersion iv
									inner join dbo.ItemVersionEndorsement ive
										inner join dbo.EndorsementPeriodic ep 
										on ive.EndorsementPeriodicID = ep.EndorsementPeriodicID and ep.EndorsementCode = 'RB'
									on iv.ItemVersionID = ive.ItemVersionID
								on i.CurrentItemVersionID = iv.ItemVersionID
							on d.DocumentID = i.DocumentID
				where  d.BatchID = @batchID;

				select @numberOfFormsDisallowed = count(distinct d.DocumentID),
					   @numberOfItemsDisallowed = count(i.ItemID)
				from   dbo.Document d
							inner join dbo.Item i
								inner join dbo.ItemVersion iv
									inner join dbo.ItemVersionEndorsement ive
										inner join dbo.EndorsementPeriodic ep 
										on ive.EndorsementPeriodicID = ep.EndorsementPeriodicID and ep.EndorsementCode = 'DA'
									on iv.ItemVersionID = ive.ItemVersionID
								on i.CurrentItemVersionID = iv.ItemVersionID
							on d.DocumentID = i.DocumentID
				where  d.BatchID = @batchID;

				-- New Medicine Service, number of services undertaken/number of applicable items is calculated above (when the fee is calculated.

				/*
				* Now that we have calcuated everything, it is time to write it to the database...
				*
				*/

				begin transaction

				insert into dbo.Schedule (BatchID, 
										  TotalOfDrugAndApplianceCosts, 
										  TotalOfAllFees, 
										  TotalOfDrugAndApplianceCostsPlusFees, 
										  TotalOfChargesIncludingFP57Refunds, 
										  TotalOfAccount, 
										  RecoveryOfAdvancePayment, 
										  RecoveryOfAdvancePaymentInRespectOfALateRegisteredBatch, 
										  RecoveryOfAdditionalAdvancePayment,
										  RecoveryOfCredit,
										  BalanceDueInRespectOfValue, 
										  PaymentOnAccountForFinancialPeriodID, 
										  PaymentOnAccountForItems, 
										  PaymentOnAccountForCharges, 
										  PaymentOnAccountForValue, 
										  AdvancePaymentInRespectOfALateRegisteredBatch, 
										  AdditionalAdvancePayment,
										  TotalAmountAuthorisedByPPD, 
										  TotalAmountAuthorisedByPCT, 
										  TotalAmountAuthorisedByOther, 
										  NetPaymentMadeByPPD, 
										  DateNetPaymentMadeByPPD, 
										  GeneratedBy, 
										  GeneratedOn)
				select @batchID,
				       @totalOfDrugAndApplianceCosts,
					   @totalOfAllFees,
					   @totalOfDrugAndApplianceCosts + @totalOfAllFees,
					   (0 - @totalValueOfCharges),
					   (@totalOfDrugAndApplianceCosts + @totalOfAllFees - @totalValueOfCharges),
					   s.RecoveryOfAdvancePayment,
					   s.RecoveryOfAdvancePaymentInRespectOfALateRegisteredBatch,
					   s.RecoveryOfAdditionalAdvancePayment,
					   s.RecoveryOfCredit,
					   (@totalOfDrugAndApplianceCosts + @totalOfAllFees - @totalValueOfCharges) + (isnull(s.RecoveryOfAdvancePayment, 0) + isnull(s.RecoveryOfAdvancePaymentInRespectOfALateRegisteredBatch, 0) + isnull(s.RecoveryOfAdditionalAdvancePayment, 0) + isnull(s.RecoveryOfCredit, 0)),
					   s.PaymentOnAccountForFinancialPeriodID,
					   s.PaymentOnAccountForItems,
					   s.PaymentOnAccountForCharges,
					   s.PaymentOnAccountForValue,
					   s.AdvancePaymentInRespectOfALateRegisteredBatch,
					   s.AdditionalAdvancePayment,
					   s.TotalAmountAuthorisedByPPD, -- TotalAmountAuthorisedByPPD
					   s.TotalAmountAuthorisedByPCT,
					   s.TotalAmountAuthorisedByOther,
					   s.NetPaymentMadeByPPD, -- NetPaymentMadeByPPD
					   s.DateNetPaymentMadeByPPD, -- DateNetPaymentMadeByPPD
					   'PSNC', 
					   getdate()
				from   dbo.Schedule s
				where  s.ScheduleID = @originalScheduleID;

				set @scheduleID = scope_identity();

		
				insert into dbo.ScheduleCharge (ScheduleID, 
												DrugsR1Quantity, 
												DrugsR1Price, 
												DrugsR1Value, 
												AppliancesR1Quantity, 
												AppliancesR1Price, 
												AppliancesR1Value, 
												DrugsR2Quantity, 
												DrugsR2Price, 
												DrugsR2Value, 
												AppliancesR2Quantity, 
												AppliancesR2Price, 
												AppliancesR2Value, 
												ExclHoiseryR1Quantity, 
												ExclHoiseryR1Price, 
												ExclHoiseryR1Value, 
												ExclHoiseryR2Quantity, 
												ExclHoiseryR2Price, 
												ExclHoiseryR2Value, 
												ElasticHoiseryValue, 
												FP57Refunds, 
												TotalIncludingFP57)
				values (@scheduleID,
						@numberOfChargesForDrugItemsAtCurrentRate, 
						@drugsChargeAtCurrentRate, 
						(0 - @valueOfChargesForDrugItemsAtCurrentRate),
						@numberOfChargesForApplianceItemsAtCurrentRate, 
						@applianceChargeAtCurrentRate, 
						(0 - @valueOfChargesForApplianceItemsAtCurrentRate),
						@numberOfChargesForDrugItemsAtPreviousRate, 
						@drugsChargeAtPreviousRate, 
						(0- @valueOfChargesForDrugItemsAtPreviousRate),
						@numberOfChargesForApplianceItemsAtPreviousRate, 
						@applianceChargeAtPreviousRate, 
						(0 - @valueOfChargesForApplianceItemsAtPreviousRate),
						@numberOfChargesForApplianceItemsExcludingHoiseryAtCurrentRate, 
						@applianceChargeAtCurrentRate, 
						(0 - @valueOfChargesForApplianceItemsExcludingHoiseryAtCurrentRate),
						@numberOfChargesForApplianceItemsExcludingHoiseryAtPreviousRate, 
						@applianceChargeAtPreviousRate, 
						(0 - @valueOfChargesForApplianceItemsExcludingHoiseryAtPreviousRate),
						(0 - (@valueOfChargesForHoiseryApplianceItemsAtCurrentRate + @valueOfChargesForHoiseryApplianceItemsAtPreviousRate)),
						@valueOfFp57RefundsDeclaredOnFP34c,
						(0 - @totalValueOfCharges)
				);
		
				insert into dbo.ScheduleCost (ScheduleID, 
											  TotalOfBasicPricesAtStandardDiscountRate, 
											  DiscountPercentage, 
											  DiscountValue, 
											  TotalOfBasicPricesAtZeroDiscount, 
											  SubTotalOfBasicPrices, 
											  OutOfPocketExpenses, 
											  ConsumableAllowanceQuantity, 
											  ConsumableAllowancePrice, 
											  ConsumableAllowanceValue, 
											  ContainerAllowanceQuantity, 
											  ContainerAllowancePrice, 
											  ContainerAllowanceValue, 
											  TotalOfDrugAndApplianceCosts, 
											  Adjustment)
				select @scheduleID,
					   @totalValueOfBasicPriceAtStandardDiscountRate,
					   @standardDiscountDeductionPercentage, 
					   0 - @valueOfStandardDiscount,
					   @totalValueOfBasicPriceAtZeroDiscountRate,
					   @subTotalOfBasicPrice,
		  			   @totalValueOfOutOfPocketExpenseExpenseClaims,
					   @totalNumberOfConsumableAllowanceFees, 
					   @valueOfConsumableAllowanceFee, 
					   @totalValueOfConsumableAllowanceFees,
					   @totalNumberOfContainerAllowanceFees, 
					   @valueOfContainerAllowanceFee, 
					   @totalValueOfContainerAllowanceFees,
					   @totalOfDrugAndApplianceCosts,
					   sc.Adjustment
				from   dbo.ScheduleCost sc
				where  sc.ScheduleID = @originalScheduleID;
		
				insert into dbo.ScheduleDocumentData (ScheduleID, 
													  TotalFormsReceived, 
													  TotalEpsFormsReceived, 
													  TotalEpsItemsReceived, 
													  ItemsAtZeroDiscountRateForWhichAFeeIsPaid, 
													  ItemsAtStandardDiscountRateForWhichAFeeIsPaidIncludingOxygen, 
													  TotalOfItemsForWhichAFeeIsPaid, 
													  AverageItemValue, 
													  ReferredBackItems, 
													  ReferredBackForms, 
													  DisallowedItems,
													  DisallowedForms,
													  MedicineUseReviewsDeclared, 
													  DispensingStaffNumberOfHoursDeclared, 
													  FP57FormsDeclared, 
													  ApplianceUseReviewsCarriedOutAtPatientsHomeDeclared, 
													  ApplianceUseReviewsCarriedOutAtPremisesDeclared, 
													  NewMedicineServicesUndertaken, 
													  NewMedicineServiceItems)
				values (@scheduleID,
						@totalNumerOfFormsReceived,
						@numberOfEpsFormsReceived, 
						@numberOfEpsItemsReceived,
						@numberOfItemsPaidAtZeroDiscountRate,
						@numberOfItemsPaidAtStandardDiscountRate,
						@totalNumberOfProfessionalFees,
						@averageItemValue,
						@numberOfItemsReferredBackToContractor,
						@numberOfFormsReferredBackToContractor,
						@numberOfItemsDisallowed,
						@numberOfFormsDisallowed,
						@numberOfMedicineUseReviewsDeclaredOnFP34c,
						@numberOfSupportHoursDeclaredOnFP34c,
						@numberOfFp57RefundsDeclaredOnFP34c,
						@numberOfApplianceUseReviewsAtHomeDeclaredOnFP34c,
						@numberOfApplianceUseReviewsAtPharmacyDeclaredOnFP34c,
						@numberOfNewMedicineServicesUndertaken,
						@numberOfNewMedicineServiceItems
				);
		
				-- ToDo: Get lower and upper limits from the database...
				declare @valueOfExpensiveItemLowerLimit money = 100.00, 
						@valueOfExpensiveItemUpperLimit money = 300.00;

				with recalculatedPrices as (
					select d.DocumentID, 
						   d.FragmentTypeCode, 
						   d.DocumentNumber, 
						   i.Sequence, 
						   sum(dbo.CalculateBasicPrice(i.CurrentItemVersionID)) as BasicPrice, 
						   count(i.CurrentItemVersionID) as NumberOfElements
					from   dbo.Document d
								inner join dbo.Item i 
								on d.DocumentID = i.DocumentID
					where  d.BatchID = @batchID
					group by d.DocumentID, d.FragmentTypeCode, d.DocumentNumber, i.Sequence
				), 
				expensiveItems as (
					select *
					from   recalculatedPrices
					where  BasicPrice >= @valueOfExpensiveItemLowerLimit
				),
				expensiveItemDetails as (
					select isnull(ei.FragmentTypeCode, '0') + right(N'00000' + convert(varchar(6), ei.DocumentNumber), 5) as FormNumber,
						   ei.Sequence as ItemNumber, 
						   ltrim(rtrim(p.Description)) + ' ' + ltrim(rtrim(pp.Description)) as Description,
						   pp.PackSize, 
						   iv.Quantity,
						   ei.BasicPrice
					from   expensiveItems ei
								inner join dbo.Item i
									inner join dbo.ItemVersion iv 
										inner join dbo.ProductPackPeriodic ppp 
											inner join dbo.ProductPack pp 
												inner join dbo.Product p 
												on pp.ProductID = p.ProductID
											on ppp.ProductPackID = pp.ProductPackID
										on iv.ProductPackPeriodicID = ppp.ProductPackPeriodicID
									on i.CurrentItemVersionID = iv.ItemVersionID
								on ei.DocumentID = i.DocumentID and ei.Sequence = i.Sequence
					where  ei.NumberOfElements = 1
					union
					select isnull(ei.FragmentTypeCode, '0') + right(N'00000' + convert(varchar(6), ei.DocumentNumber), 5) as FormNumber,
						   ei.Sequence as ItemNumber, 
						   'MULTIPLE PRODUCTS AND/OR PACKS' as Description, 0 as PackSize,  0 as Quantity, 
						   ei.BasicPrice
					from   expensiveItems ei
					where  ei.NumberOfElements > 1
				)
				insert into dbo.ScheduleExpensiveItem (ScheduleID, 
													   FormNumber, 
													   ItemNumber, 
													   Description, 
													   PackSize, 
													   Quantity, 
													   BasicPrice)
				select @scheduleID, 
					   FormNumber, 
					   ItemNumber, 
					   Description, 
					   PackSize, 
					   Quantity, 
					   BasicPrice
				from   expensiveItemDetails	
				order by BasicPrice desc, 2, 3;		
		
				insert into dbo.ScheduleExpensiveItemSummary (ScheduleID, 
															  LowerLimitValue, 
															  UpperLimitValue, 
															  NumberOfItemsBetweenLowerAndUpperLimit, 
															  ValueOfItemsBetweenLowerAndUpperLimit, 
															  NumberOfItemsAboveUpperLimit, 
															  ValueOfItemsAboveUpperLimit, 
															  NumberOfItemsAboveLowerLimit, 
															  ValueOfItemsAboveLowerLimit)
				select @scheduleID, 
					   @valueOfExpensiveItemLowerLimit, 
					   @valueOfExpensiveItemUpperLimit, 
					   count(*),
					   sum(BasicPrice),
					   sum(case when BasicPrice <= @valueOfExpensiveItemUpperLimit then 1 else 0 end),
					   sum(case when BasicPrice <= @valueOfExpensiveItemUpperLimit then BasicPrice else 0 end),
					   sum(case when BasicPrice > @valueOfExpensiveItemUpperLimit then 1 else 0 end),
					   sum(case when BasicPrice > @valueOfExpensiveItemUpperLimit then BasicPrice else 0 end)
				from   dbo.ScheduleExpensiveItem
				where  ScheduleID = @scheduleID;
		
				select @ProtectedAdditionalPayment = ProtectedAdditionalPayment,
						@DisposalOfUnwantedMedicinesAndSignposting = DisposalOfUnwantedMedicinesAndSignposting,
						@PromotionOfHealthyLifestyleAndSupportForSelfCare = PromotionOfHealthyLifestyleAndSupportForSelfCare,
						@ClinicalGovernance = ClinicalGovernance,
						@SupportForPeopleWithDisabilities = SupportForPeopleWithDisabilities
				from	dbo.ScheduleFee
				where	ScheduleId = @originalScheduleID;

				insert into dbo.ScheduleFee (ScheduleID, 
											 ProfessionalFeeQuantity, 
											 ProfessionalFeeAmount, 
											 ProfessionalFeeValue, 
											 PharmacyContractTransitionPayment, 
											 RepeatDispensingFee, 
											 PracticePayment, 
											 AdditionalFees2a, 
											 AdditionalFees2bMeasuredFitted, 
											 AdditionalFees2bHomeDelivery, 
											 AdditionalFees2c, 
											 AdditionalFees2d, 
											 AdditionalFees2e, 
											 AdditionalFeesMethadone,
											 AdditionalFees2fCount, 
											 AdditionalFees2f, 
											 EstablishmentPayment, 
											 ManuallyPriced, 
											 SubTotalOfPrescriptionFees, 
											 OtherFeesMedicinalUseReview, 
											 OtherFeesApplianceUseReviewHome, 
											 OtherFeesApplianceUseReviewPremises, 
											 OtherFeesStomaCustomisation, 
											 TotalOfAllFees, 
											 OtherFeesNewMedicineServiceConsultation,
											 ProtectedAdditionalPayment,
											 DisposalOfUnwantedMedicinesAndSignposting,
											 PromotionOfHealthyLifestyleAndSupportForSelfCare,
											 ClinicalGovernance,
											 SupportForPeopleWithDisabilities)
				values (@scheduleID,
						@totalNumberOfProfessionalFees,
						@professionalFee,
						@totalValueOfProfessionalFees,
						@valueOfPharmacyContractTransitionPayment,
						@repeatDispensingFee / 12,
						@valueOfPracticePayment,
						@valueOfAdditionalFees2a,
						@valueOfAdditionalFees2bMeasuredFitted,
						@valueOfAdditionalFees2bHomeDelivery,
						@valueOfAdditionalFees2c,
						@valueOfAdditionalFees2d,
						@valueOfAdditionalFees2e,
						@valueOfAdditionalFeesMethadone,
						@additionaFees2fCount,
						@valueOfAdditionalFees2f,
						@valueOfEstablishmentPayment,
						0, -- ManuallyPriced - no longer used according to Michael Hamilton.
						@subTotalOfFees,
						@valueOfMedicineUseReviews,
						@valueOfApplianceUseReviewAtHomeFees,
						@valueOfApplianceUseReviewAtPharmacyFees,
						@valueOfStomaCustomisationFees,
						@totalOfAllFees,
						@valueOfNewMedicineServiceConsultationFees,
						@ProtectedAdditionalPayment,
						@DisposalOfUnwantedMedicinesAndSignposting,
						@PromotionOfHealthyLifestyleAndSupportForSelfCare,
						@ClinicalGovernance,
						@SupportForPeopleWithDisabilities
				);
		
				insert into dbo.ScheduleGroupSwitchSummary (ScheduleID, 
															ScheduleGroupSwitchDirectionID, 
															Reason, 
															Number, 
															OldRate)
				values (@scheduleID,
						1, -- Exempt to chargeable
						'',
						(select count(d.DocumentID)
						 from   dbo.Document d
							inner join dbo.SubmissionGroup sg on d.SubmissionGroupCode = sg.SubmissionGroupCode
							inner join dbo.DocumentGroupType cdgt on d.CurrentDocumentGroupTypeID = cdgt.DocumentGroupTypeID
						 where d.BatchID = @batchID
						 and sg.IsChargeable = 0 and cdgt.GroupTypeCode = '2'),
						0 -- OldRate
				);

				insert into dbo.ScheduleGroupSwitchSummary (ScheduleID, 
															ScheduleGroupSwitchDirectionID, 
															Reason, 
															Number, 
															OldRate)
				values (@scheduleID,
						2, -- Exempt to chargeable (old rate)
						'',
						(select count(d.DocumentID)
						 from dbo.Document d
						 inner join dbo.SubmissionGroup sg on d.SubmissionGroupCode = sg.SubmissionGroupCode
						 inner join dbo.DocumentGroupType cdgt on d.CurrentDocumentGroupTypeID = cdgt.DocumentGroupTypeID
						 where d.BatchID = @batchID
						 and sg.IsChargeable = 0 and cdgt.GroupTypeCode = '3'),
					    1 -- OldRate
				);

				insert into dbo.ScheduleGroupSwitchSummary (ScheduleID, 
															ScheduleGroupSwitchDirectionID, 
															Reason, 
															Number, 
															OldRate)
				values (@scheduleID,
						3, -- Chargeable to exempt
						'',
						(select count(d.DocumentID)
						 from dbo.Document d
						 inner join dbo.SubmissionGroup sg on d.SubmissionGroupCode = sg.SubmissionGroupCode
						 inner join dbo.DocumentGroupType cdgt on d.CurrentDocumentGroupTypeID = cdgt.DocumentGroupTypeID
						 where d.BatchID = @batchID
						 and sg.IsChargeable = 1 and sg.IsOldRate = 0 and cdgt.GroupTypeCode = '1'),
						0 -- OldRate
				);

				insert into dbo.ScheduleGroupSwitchSummary (ScheduleID, 
															ScheduleGroupSwitchDirectionID, 
															Reason, 
															Number, 
															OldRate)
				values (@scheduleID,
						4, -- Chargeable (old rate) to exempt
						'',
						(select count(d.DocumentID)
						 from dbo.Document d
						 inner join dbo.SubmissionGroup sg on d.SubmissionGroupCode = sg.SubmissionGroupCode
						 inner join dbo.DocumentGroupType cdgt on d.CurrentDocumentGroupTypeID = cdgt.DocumentGroupTypeID
						 where d.BatchID = @batchID
						 and sg.IsChargeable = 1 and sg.IsOldRate = 1 and cdgt.GroupTypeCode = '1'),
						0 -- OldRate
				);
		
				insert into dbo.ScheduleLocalPayment (ScheduleID, 
													  AuthorisedBy, 
													  Description, 
													  Value)
				select @scheduleID,
					   slp.AuthorisedBy, 
					   slp.Description, 
					   slp.Value
				from   dbo.ScheduleLocalPayment slp
				where  slp.ScheduleID = @originalScheduleID;
		
				insert into dbo.ScheduleOtherPayment (ScheduleID, 
													  Description, 
													  Value)
				select @scheduleID,
					   sop.Description, 
					   sop.Value
				from dbo.ScheduleOtherPayment sop
				where sop.ScheduleID = @originalScheduleID;

				insert into dbo.ScheduleTop10ZD    (ScheduleID,
													Description,
													Quantity,
													BasicPrice)
				select  @scheduleID,
						Description,
						Quantity,
						BasicPrice
				from ScheduleTop10ZD
				where scheduleid = @originalScheduleID

				insert into dbo.ScheduleTop10CatM  (ScheduleID,
													Description,
													Quantity,
													BasicPrice)
				select @scheduleID,
						Description,
						Quantity,
						BasicPrice
				from ScheduleTop10CatM
				where scheduleid = @originalScheduleID

				insert into dbo.ScheduleSpecialItem(ScheduleID,
													Description,
													Quantity,
													BasicPrice)
				select @scheduleID,
						Description,
						Quantity,
						BasicPrice 
				from ScheduleSpecialItem
				where scheduleid = @originalScheduleID

				commit transaction;
		end

	return


GO
/****** Object:  StoredProcedure [dbo].[GenerateOriginalScheduleOfPayments]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GenerateOriginalScheduleOfPayments]
	@batchID bigint,
	@scheduleID bigint = null output

as

	set nocount on;
	set transaction isolation level read uncommitted;

	/*
	 * There is a lot of functionality in here that you may argue is duplicated and could all be rolled up
	 * together in order to reduce the number of database reads/writes. You are correct. It is however deliberate
	 * at this time that the functionality is duplicated in order to ease the development of the SP by breaking down
	 * each individual requirement into its own chunk rather than having massive select statements that sum and count
	 * all values at the same time.
	 *
	 * Once the functionality has been proven, time can be taken to review this SP and merge the various sums and counts
	 * into a significantly smaller number of selects. Of course, this will also be seen by the end user as a performance
	 * increase which cannot hurt either!
	 *
	 * Note: I now think that most of the "Costs" and "Charges" sections are now fairly optimal.
	 *       There may be some additional improvements that can be applied to the fees and document data still though.
	 *
	 */

	declare @financialPeriodID integer, 
		@originalScheduleID bigint,

		@contractorIsRegisteredForApplianceUseReview bit, @contractorRegisteredForApplianceUseReviewSince integer,
		@contractorIsRegisteredForStomaCustomisation bit, @contractorRegisteredForStomaCustomisationSince integer,
		@jurisdictionID integer,

		@applianceChargeAtOriginalRate money, 
		@applianceChargeAtPreviousRate money,
		@applianceUseReviewAtHomeFee money,
		@applianceUseReviewAtPharmacyFee money,
		@contractTransitionPaymentDivisor integer, 
		@contractTransitionPaymentMultiplier money,
		@drugsChargeAtOriginalRate money, @drugsChargeAtPreviousRate money,
		@expensiveItemThreshold money, @expensiveItemFeePercentage numeric (7, 2),
		@establishmentPaymentDivisor integer,
		@medicineUseReviewFee money,
		@outOfPocketExpenseDeductionPerItem money, 
		@outOfPocketExpenseMinimumClaimValue money = 0.51, -- Correct as at July 2012, retrieved from JurisdictionPeriodic
		@professionalFee money,
		@repeatDispensingFee money,
		@stomaCustomisationFee money,
		@valueOfConsumableAllowanceFee money = 0.00,
		@valueOfContainerAllowanceFee money = 0.00,
		
		@numberOfApplianceUseReviewsAtHomeDeclaredOnFP34c integer, 
		@numberOfApplianceUseReviewsAtPharmacyDeclaredOnFP34c integer,
		@numberOfFp57RefundsDeclaredOnFP34c integer,
		@numberOfMedicineUseReviewsDeclaredOnFP34c integer, 
		@numberOfOutOfPocketExpenseItemsDeclaredOnFP34c integer,  
		@numberOfSupportHoursDeclaredOnFP34c integer,
		@valueOfFp57RefundsDeclaredOnFP34c money,
		@valueOfOutOfPocketExpensesDeclaredOnFP34c money,
		@valueOfSupplementaryControlledDrugFee money;

	-- Load up what we need to know about the batch (including the submission) and the contractor, as well as
	-- details specific to the financial period and jurisdiction...
	select @financialPeriodID = b.FinancialPeriodID,
		@originalScheduleID = b.ScheduleID,

		@contractorIsRegisteredForApplianceUseReview = c.IsRegisteredForApplianceUseReview,
		@contractorRegisteredForApplianceUseReviewSince = c.RegisteredForApplianceUseReviewSince,
		@contractorIsRegisteredForStomaCustomisation = c.IsRegisteredForStomaCustomisation,
		@contractorRegisteredForStomaCustomisationSince = c.RegisteredForStomaCustomisationSince,
		@jurisdictionID = c.JurisdictionID,
	
		@applianceChargeAtOriginalRate = jp.ApplianceItemPrescriptionChargeValue,
		@applianceUseReviewAtHomeFee = jp.ApplianceUseReviewAtHomeFee,
		@applianceUseReviewAtPharmacyFee = jp.ApplianceUseReviewAtPharmacyFee,
		@contractTransitionPaymentDivisor = jp.ContractTransitionPaymentDivisor,
		@contractTransitionPaymentMultiplier = jp.ContractTransitionPaymentMultiplier,
		@drugsChargeAtOriginalRate = jp.DrugItemPrescriptionChargeValue,
		@establishmentPaymentDivisor = jp.EstablishmentPaymentDivisor,
		@expensiveItemThreshold = jp.ExpensiveItemThreshold,
		@expensiveItemFeePercentage = jp.ExpensiveItemFeePercentage,
		@medicineUseReviewFee = jp.MedicinesUseReviewFee,
		@outOfPocketExpenseDeductionPerItem = jp.OutOfPocketExpenseDeductionValue,
		@outOfPocketExpenseMinimumClaimValue = jp.OutOfPocketExpenseMinimumClaimValue,
		@professionalFee = jp.StandardProfessionalFeeValue,
		@repeatDispensingFee = jp.RepeatDispensingFee,
		@stomaCustomisationFee = jp.StomaCustomisationFee,
		@valueOfConsumableAllowanceFee = jp.ValueOfConsumableAllowanceFee,
		@valueOfContainerAllowanceFee = jp.ContainerAllowanceValue, 

		@numberOfFp57RefundsDeclaredOnFP34c = s.FP57Count,
		@numberOfApplianceUseReviewsAtHomeDeclaredOnFP34c = s.ApplianceUseReviewsHome,
		@numberOfApplianceUseReviewsAtPharmacyDeclaredOnFP34c = s.ApplianceUseReviews,
		@numberOfMedicineUseReviewsDeclaredOnFP34c = s.MedicineUseReviews,
		@numberOfOutOfPocketExpenseItemsDeclaredOnFP34c = s.OutOfPocketExpenseItems,
		@numberOfSupportHoursDeclaredOnFP34c = s.SupportHours,
		@valueOfFp57RefundsDeclaredOnFP34c = s.FP57TotalRefund,
		@valueOfOutOfPocketExpensesDeclaredOnFP34c = s.OutOfPocketExpenses,
		@valueOfSupplementaryControlledDrugFee = jp.ValueOfSupplementaryControlledDrugFee
	from dbo.Batch b
		inner join dbo.Contractor c on b.ContractorID = c.ContractorID
		inner join dbo.JurisdictionPeriodic jp on c.JurisdictionID = jp.JurisdictionID and b.FinancialPeriodID = jp.FinancialPeriodID
		inner join dbo.Submission s on b.BatchID = s.BatchID
	where b.BatchID = @batchID;

	if (@@rowcount = 1) begin

		-- We have Original period details, we also need to know a few details from the previous period...

		select top 1 @applianceChargeAtPreviousRate = jp.ApplianceItemPrescriptionChargeValue
		from dbo.JurisdictionPeriodic jp
		where jp.JurisdictionID = @jurisdictionID
		  and jp.FinancialPeriodID < @financialPeriodID
			and jp.ApplianceItemPrescriptionChargeValue != @applianceChargeAtOriginalRate
		order by jp.FinancialPeriodID desc;

		if (@@rowcount = 1) begin

			select top 1 @drugsChargeAtPreviousRate = jp.DrugItemPrescriptionChargeValue
			from dbo.JurisdictionPeriodic jp
			where jp.JurisdictionID = @jurisdictionID
				and jp.FinancialPeriodID < @financialPeriodID
				and jp.DrugItemPrescriptionChargeValue != @drugsChargeAtOriginalRate
			order by jp.FinancialPeriodID desc;

		end

	end
	
	-- We only continue if the last queries returned a single row (each).
	-- Otherwise, something is wrong - either the batchID specified does not exist (bad) or (worse) the links resulted in
	-- multiple rows!
	if (@@rowcount = 1) begin

		/*
		 * DRUG & APPLIANCE COSTS...
		 *
		 */

		-- Totals:
		-- total number/value of items/basic prices at SDR & ZDR (also separating the basic prices of oxygen (Part X) related products)...
		declare @numberOfItemsPaidAtStandardDiscountRate integer = 0, @totalValueOfBasicPriceAtStandardDiscountRate money = 0.00, @totalValueOfBasicPriceForPartX money = 0.00,
			@numberOfItemsPaidAtZeroDiscountRate integer = 0, @totalValueOfBasicPriceAtZeroDiscountRate money = 0.00, @totalValueOfOutOfPocketExpenseExpenseClaims money = 0.00,
			@totalNumberOfContainerAllowanceEndorsements integer = 0, @totalNumberOfMdaPatientInteractions integer = 0;
		with itemBasicPrices as (
		
			select 
				d.FragmentTypeCode, d.DocumentNumber, i.Sequence,
				BasicPrice = sum(iv.BasicPrice),
				BasicPriceForPartX = sum(case when ppp.IsEligibleForZeroDiscount = 1 and dtc.Category = 10 then BasicPrice else 0.00 end),
				NumberOfProfessionalFees = sum(iv.NumberOfProfessionalFees),
				IsEligibleForZeroDiscount = cast(max(cast (ppp.IsEligibleForZeroDiscount as integer)) as bit),
				ValueOfOutOfPocketExpenses = sum(iv.ValueOfOutOfPocketExpenses),
				NumberOfContainerAllowanceEndorsements = count(ep.EndorsementPeriodicID),
				NumberOfPatientInteractions = max(iv.NumberOfTimesDispensed)
			from dbo.Document d
				inner join dbo.Item i
					inner join dbo.ItemVersion iv
						inner join dbo.ProductPackPeriodic ppp 
							left join dbo.DrugTariffCategory dtc on ppp.DrugTariffCategoryCode = dtc.DrugTariffCategoryCode
						on iv.ProductPackPeriodicID = ppp.ProductPackPeriodicID
						left join dbo.ItemVersionEndorsement ive
							inner join dbo.EndorsementPeriodic ep on ive.EndorsementPeriodicID = ep.EndorsementPeriodicID and ep.EndorsementCode = 'PK'
						on iv.ItemVersionID = ive.ItemVersionID
					on i.OriginalItemVersionID = iv.ItemVersionID
				on d.DocumentID = i.DocumentID
			where d.BatchID = @batchID
			group by d.FragmentTypeCode, d.DocumentNumber, i.Sequence
			
		)
		select
			@numberOfItemsPaidAtStandardDiscountRate = sum(case when IsEligibleForZeroDiscount = 0 then NumberOfProfessionalFees else 0.00 end),	
			@totalValueOfBasicPriceAtStandardDiscountRate = sum(case when IsEligibleForZeroDiscount = 0 then BasicPrice else 0.00 end),
			@totalValueOfBasicPriceForPartX = sum(BasicPriceForPartX),
			@numberOfItemsPaidAtZeroDiscountRate = sum(case when IsEligibleForZeroDiscount = 1 then NumberOfProfessionalFees else 0.00 end),
			@totalValueOfBasicPriceAtZeroDiscountRate = sum(case when IsEligibleForZeroDiscount = 1 then BasicPrice else 0.00 end),
			@totalValueOfOutOfPocketExpenseExpenseClaims = sum(case when ValueOfOutOfPocketExpenses >= @outOfPocketExpenseMinimumClaimValue then ValueOfOutOfPocketExpenses else 0.00 end),
			@totalNumberOfContainerAllowanceEndorsements = sum(case when NumberOfPatientInteractions = 1 then NumberOfContainerAllowanceEndorsements else 0 end),
			@totalNumberOfMdaPatientInteractions = sum(case when NumberOfPatientInteractions > 1 then NumberOfPatientInteractions else 0 end)
		from itemBasicPrices;

		declare @totalNumberOfProfessionalFees integer = @numberOfItemsPaidAtStandardDiscountRate + @numberOfItemsPaidAtZeroDiscountRate;

		-- Discount:
		-- determine the discount rate % from the tariff Part V based upon the sub total of all basic prices at standard discount rate less the value of the 
		-- of oxygen (Part X) related products
		declare @totalValueOfBasicPriceAtStandardDiscountRateLessPartX money = @totalValueOfBasicPriceAtStandardDiscountRate - @totalValueOfBasicPriceForPartX,
			@standardDiscountDeductionPercentage money = 0.00;
		select @standardDiscountDeductionPercentage = dds.DeductionRate
		from dbo.TariffDiscountDeductionScale dds
		where dds.FinancialPeriodID = @financialPeriodID
		  and floor(@totalValueOfBasicPriceAtStandardDiscountRateLessPartX) between dds.[From] and dds.[To];
		-- Using the discount rate determined above, calculate the discount against all basic prices at standard discount rate less the value of the 
		-- of oxygen (Part X) related products
		declare @valueOfStandardDiscount money = round(@totalValueOfBasicPriceAtStandardDiscountRateLessPartX * (@standardDiscountDeductionPercentage / 100), 2);

		-- Subtotal of basic prices:
		-- the sum of the three above totals...
		declare @subTotalOfBasicPrice money = (@totalValueOfBasicPriceAtStandardDiscountRate - @valueOfStandardDiscount) + @totalValueOfBasicPriceAtZeroDiscountRate;

		-- Out of pocket expenses:
		-- This has already been totalled from the individual item versions, but if we are prior to July 2012 then any claims entered agains those individual
		-- item versions are ignored and overriden by the value of the out of pocket expenses from the FP34C submission document,
		-- less the number of out of pocket expense items multiplied by the deduction value specified in tariff part II.12...
		if (@financialPeriodID < 201207) begin
			set @totalValueOfOutOfPocketExpenseExpenseClaims = @valueOfOutOfPocketExpensesDeclaredOnFP34c - round(@numberOfOutOfPocketExpenseItemsDeclaredOnFP34c * @outOfPocketExpenseDeductionPerItem, 2);
		end

		-- Consumable/Container allowances:
		declare @totalNumberOfContainerAllowanceFees integer = 0, @totalNumberOfConsumableAllowanceFees integer = 0,
			@totalValueOfContainerAllowanceFees money = 0.00, @totalValueOfConsumableAllowanceFees money = 0.00;
		if (@financialPeriodID < 201207) begin

			-- Prior to July 2012, the total number of professional fees paid, multiplied by the container allowance value specified in tariff part IV...
			set @totalNumberOfContainerAllowanceFees = @totalNumberOfProfessionalFees;
			set @totalValueOfContainerAllowanceFees = round(@totalNumberOfContainerAllowanceFees * @valueOfContainerAllowanceFee, 2);

		end else begin

			-- Since July 2012 (Simplification)...

			set @totalNumberOfConsumableAllowanceFees = @totalNumberOfProfessionalFees;
			set @totalValueOfConsumableAllowanceFees = round(@totalNumberOfProfessionalFees * @valueOfConsumableAllowanceFee, 2);

			if (@financialPeriodID < 201210) begin

				-- Between July 2012 and September 2012 (inclusive), BSA were applying the PK endorsement incorrectly (misinterpretation of the tariff rules).
				-- As such, we will simply use the "Payment for containers" figures provided on the BSA schedule...
				select @totalNumberOfContainerAllowanceFees = sc.ContainerAllowanceQuantity,
					@valueOfContainerAllowanceFee = sc.ContainerAllowancePrice,
					@totalValueOfContainerAllowanceFees = sc.ContainerAllowanceValue
				from dbo.ScheduleCost sc
				where sc.ScheduleID = @originalScheduleID;

			end else begin

				-- From October 2012 onward, we should be calculating "Payment for containers" in the same manner
				-- as the BSA are calculating it (though this remains to be seen as I know further changes are being requested).
				set @totalNumberOfContainerAllowanceFees = @totalNumberOfContainerAllowanceEndorsements + @totalNumberOfMdaPatientInteractions;
				set @totalValueOfContainerAllowanceFees = round(@totalNumberOfContainerAllowanceFees * @valueOfContainerAllowanceFee, 2);

			end

		end

		-- Total of drug and appliance costs:
		declare @totalOfDrugAndApplianceCosts money = @subTotalOfBasicPrice + @totalValueOfOutOfPocketExpenseExpenseClaims + @totalValueOfConsumableAllowanceFees + @totalValueOfContainerAllowanceFees;

		/*
		 * PRESCRIPTION FEES...
		 *
		 */

		declare @valueOfAdditionalFees2a money = 0.00, 
			@valueOfAdditionalFees2bHomeDelivery money = 0.00, 
			@valueOfAdditionalFees2bMeasuredFitted money = 0.00, 
			@valueOfAdditionalFees2c money = 0.00, 
			@valueOfAdditionalFees2d money = 0.00, 
			@valueOfAdditionalFees2e money = 0.00,
			@valueOfAdditionalFeesMethadone money = 0.00,
			@additionaFees2fCount integer = 0, @valueOfAdditionalFees2f money = 0.00,
			@valueOfEstablishmentPayment money = 0.00,
			@valueOfMedicineUseReviews money = 0.00,
			@valueOfPharmacyContractTransitionPayment money = 0.00,
			@valueOfPracticePayment money = 0.00,
			@totalValueOfProfessionalFees money = 0.00,
			@subTotalOfFees money = 0.00,
			@totalOfAllFees money = 0.00,
			@valueOfStandardFees money = 0.00,
			@numberOfNewMedicineServiceItems integer = 0,
			@numberOfNewMedicineServicesUndertaken integer = 0,
			@valueOfNewMedicineServiceConsultationFees money = 0.00,
			@valueOfApplianceUseReviewAtHomeFees money = 0.00,
			@valueOfApplianceUseReviewAtPharmacyFees money = 0.00;

		-- Professional fee:
		-- the total number of professional fees paid multiplied by the value specified in tariff part IIIA.1

		select @totalValueOfProfessionalFees = round(@professionalFee * @totalNumberOfProfessionalFees, 2);

		-- Pharmacy contract transition payment (only applicable in England to batches prior to April 2011):
		-- the total number of professional fees paid divided by 500, rounded up to the next whole number, multiplied by £7.40. 
		-- For all batches from April 2011 onwards, this value must be 0 (zero) - except in Wales.
		set @valueOfPharmacyContractTransitionPayment = 0;
		if (@contractTransitionPaymentDivisor is not null and @contractTransitionPaymentDivisor != 0 
		and @contractTransitionPaymentMultiplier is not null and @contractTransitionPaymentMultiplier != 0) begin
			declare @pctpNumberOfStandardFees float = @totalNumberOfProfessionalFees,
				@pctpDivisor float = @contractTransitionPaymentDivisor;
			set @valueOfPharmacyContractTransitionPayment = round(ceiling(@pctpNumberOfStandardFees / @pctpDivisor) * @contractTransitionPaymentMultiplier, 2);
		end

		-- Practice Payment (now moved out to a function)...
		set @valueOfPracticePayment = dbo.CalculateValueOfPracticePayment(@financialPeriodID, @jurisdictionID, @totalNumberOfProfessionalFees, @numberOfSupportHoursDeclaredOnFP34c, @originalScheduleID);

		-- The following fees can all be calculated at the same time as the methodology is the same (we simply need split the results)...
		--   Additional Fees 2A: sum of the value of all “LB”, “LC”, “LE” & “LF” endorsements...
		--   Additional Fees 2C: sum of the value of all “AA” endorsements...
		--   Additional Fees 2D: sum of the value of all “AB” endorsements...
		with endorsements as (
			select d.DocumentID, i.Sequence, NumberOfProfessionalFees = (
					select sum(iv2.NumberOfProfessionalFees)
					from dbo.Item i2
						inner join dbo.ItemVersion iv2 on i2.OriginalItemVersionID = iv2.ItemVersionID
					where i2.DocumentID = d.DocumentID and i2.Sequence = i.Sequence),
				ep.EndorsementCode, ep.SpecialFeeBasis, ep.SpecialFeeValue, ive.Quantity
			from dbo.Document d
				inner join dbo.Item i
					inner join dbo.ItemVersion iv
						inner join dbo.ItemVersionEndorsement ive
							inner join dbo.EndorsementPeriodic ep on ive.EndorsementPeriodicID = ep.EndorsementPeriodicID
						on iv.ItemVersionID = ive.ItemVersionID
					on i.OriginalItemVersionID = iv.ItemVersionID
				on d.DocumentID = i.DocumentID
			where d.BatchID = @batchID
		), summary as (
			select EndorsementCode, ValueOfEndorsements = isnull(sum(round(isnull(case SpecialFeeBasis
				when '1' then SpecialFeeValue
				when 'S' then NumberOfProfessionalFees * SpecialFeeValue
				when 'Q' then Quantity * SpecialFeeValue
				else 0 end, 0), 2)), 0)
			from endorsements
			group by EndorsementCode
		)
		select
			@valueOfAdditionalFees2a = case when EndorsementCode in ('LB', 'LC', 'LE', 'LF') then @valueOfAdditionalFees2a + ValueOfEndorsements else @valueOfAdditionalFees2a end,
			@valueOfAdditionalFees2c = case when EndorsementCode = 'AA' then ValueOfEndorsements else @valueOfAdditionalFees2c end,
			@valueOfAdditionalFees2d = case when EndorsementCode = 'AB' then ValueOfEndorsements else @valueOfAdditionalFees2d end
		from summary;
		
		-- Additional Fees 2B Home Delivery:
		-- For each prescription item where the product(s) are eligible for home delivery, we pay a single home delivery fee which is the higher of
		-- the fees payable for the individual products. (If for example we have a multi-line item and one product attracts the standard home delivery
		-- fee but another attracts the higher home delivery fee, pay only a single instance of the higher fee). Note that the fees themselves are
		-- stored as EndorsementPeriodic rows where the endorsement code is "H" + the home delivery indicator code ("H1" & "H2" at the time of writing)...
		with homeDeliveryFees as (

			select d.DocumentID, i.Sequence, isnull(ep.SpecialFeeValue, 0) as Value
			from dbo.Document d
				inner join dbo.Item i
					inner join dbo.ItemVersion iv
						inner join dbo.ProductPackPeriodic ppp 
							inner join dbo.HomeDeliveryIndicator hdi
								inner join dbo.EndorsementPeriodic ep on 'H' + hdi.HomeDeliveryIndicatorCode = ep.EndorsementCode
									and ep.FinancialPeriodID = @financialPeriodID and ep.JurisdictionID = @jurisdictionID
							on ppp.HomeDeliveryIndicatorCode = hdi.HomeDeliveryIndicatorCode
		 					inner join dbo.DrugTariffCategory dtc on ppp.DrugTariffCategoryCode = dtc.DrugTariffCategoryCode and dtc.Category = 9
						on iv.ProductPackPeriodicID = ppp.ProductPackPeriodicID
					on i.OriginalItemVersionID = iv.ItemVersionID
				on d.DocumentID = i.DocumentID
			where d.BatchID = @batchID

		), maxHomeDeliveryFees as (

			select DocumentID, Sequence, max(Value) as Value
			from homeDeliveryFees
			group by DocumentID, Sequence

		)
		
		select @valueOfAdditionalFees2bHomeDelivery = isnull(sum(Value), cast (0.00 as money))
		from maxHomeDeliveryFees;

		-- Additional Fees 2B Measured and Fitted:
		-- sum of the value of all “MF” endorsements applied to the item versions where the product is an appliance that is indicated as being measured and fitted...
		with endorsements as (
			select d.DocumentID, i.Sequence, NumberOfProfessionalFees = (
					select sum(iv2.NumberOfProfessionalFees)
					from dbo.Item i2
						inner join dbo.ItemVersion iv2 on i2.OriginalItemVersionID = iv2.ItemVersionID
					where i2.DocumentID = d.DocumentID and i2.Sequence = i.Sequence),
				ep.SpecialFeeBasis, ep.SpecialFeeValue, ive.Quantity
			from dbo.Document d
				inner join dbo.Item i
					inner join dbo.ItemVersion iv
						inner join dbo.ItemVersionEndorsement ive
							inner join dbo.EndorsementPeriodic ep on ive.EndorsementPeriodicID = ep.EndorsementPeriodicID and ep.EndorsementCode = 'MF'
						on iv.ItemVersionID = ive.ItemVersionID
						inner join dbo.ProductPackPeriodic ppp
							inner join dbo.ProductPeriodic pp 
								inner join dbo.ApplianceTypeIndicator ati on pp.ApplianceTypeIndicatorCode = ati.ApplianceTypeIndicatorCode and ati.IsMeasuredAndFitted = 1
							on ppp.ProductPeriodicID = pp.ProductPeriodicID
						on iv.ProductPackPeriodicID = ppp.ProductPackPeriodicID
					on i.OriginalItemVersionID = iv.ItemVersionID
				on d.DocumentID = i.DocumentID
			where d.BatchID = @batchID
		)
		select @valueOfAdditionalFees2bMeasuredFitted = isnull(sum(round(isnull(case SpecialFeeBasis
			when '1' then SpecialFeeValue
			when 'S' then NumberOfProfessionalFees * SpecialFeeValue
			when 'Q' then Quantity * SpecialFeeValue
			else 0 end, 0), 2)), 0)
		from endorsements;

		-- Additional Fees 2E:
		-- sum of all controlled drug fees...
		-- For each prescription item where the product(s) are eligible for a controlled substance fee, we pay a single fee which is the higher of
		-- the fees payable for the individual products. (If for example we have a multi-line item and one product attracts a lower controlled substance
		-- fee but another attracts a higher controlled substance fee, pay only a single instance of the higher fee)...
		with controlledSubstanceIndicators as (

			select d.DocumentID, i.Sequence,
				NumberOfProfessionalFees = (
					select sum(iv2.NumberOfProfessionalFees)
					from dbo.Item i2
						inner join dbo.ItemVersion iv2 on i2.OriginalItemVersionID = iv2.ItemVersionID
					where i2.DocumentID = d.DocumentID and i2.Sequence = i.Sequence),
				AdditionalFeeValue = isnull(csip.AdditionalProfessionalFeeValue, 0),
				NumberOfSupplementaryControlledDrugFees = cast(pp.IsEligibleForSupplementaryControlledDrugFee as integer)
			from dbo.Document d
				inner join dbo.Item i
					inner join dbo.ItemVersion iv
						inner join dbo.ProductPackPeriodic ppp 
							inner join dbo.ProductPeriodic pp 
								inner join dbo.ControlledSubstanceIndicatorPeriodic csip on pp.ControlledSubstanceIndicatorCode = csip.ControlledSubstanceIndicatorCode
									and csip.FinancialPeriodID = @financialPeriodID and csip.JurisdictionID = @jurisdictionID
							on ppp.ProductPeriodicID = pp.ProductPeriodicID
						on iv.ProductPackPeriodicID = ppp.ProductPackPeriodicID
					on i.OriginalItemVersionID = iv.ItemVersionID
				on d.DocumentID = i.DocumentID
			where d.BatchID = @batchID

		), controlledSubstanceFees as (
		
			select DocumentID, Sequence, 
				NumberOfProfessionalFees * AdditionalFeeValue as AdditionalFeeValue,
				NumberOfSupplementaryControlledDrugFees * @valueOfSupplementaryControlledDrugFee as SupplementaryControlledDrugFeeValue
			from controlledSubstanceIndicators
		
		), maxControlledSubstanceFees as (

			select DocumentID, Sequence, max(AdditionalFeeValue) as AdditionalFeeValue, sum(SupplementaryControlledDrugFeeValue) as SupplementaryControlledDrugFeeValue
			from controlledSubstanceFees
			group by DocumentID, Sequence

		)
	
		select @valueOfAdditionalFees2e = isnull(sum(AdditionalFeeValue), 0), 
			@valueOfAdditionalFeesMethadone = isnull(sum(SupplementaryControlledDrugFeeValue), 0)
		from maxControlledSubstanceFees;

		-- Additional Fees 2F:
		-- Number of items where the basic price exceeds the value specified in tariff part IIIA.2.F; 
		-- and the value of the sum of the basic price of those items multiplied by the fee %age specified in the same rule.
		-- Note that it is necessary to recalculate the basic prices at an item level (rolling up the individual products) as
		--  (a) the fee is payable against the sum of all products dispensed against an individual prescription item
		--  (b) the basic price specified against each product may have been adjusted for broken bulk whereas the fee is payable against the cost of the quanity dispensed
		with recalculatedPrices as (
			select BasicPrice = round(sum(cast(
				case 
					when ep.EndorsementCode is not null or ppp.PackPrice = 0.00 then iv.BasicPrice
					else round(case
						when ppp.SpecialContainerIndicatorCode = 'S' then case
							when pp.ContainerSize = 1 and pp.PackSize > 1 and ppp.PackPrice != ppp.UnitPrice then ((ppp.PackPrice * 100) / pp.PackSize) * iv.Quantity
							when pp.ContainerSize != 0 then (ppp.PackPrice * 100) * (iv.Quantity / pp.ContainerSize)
							end
						when ppp.SpecialContainerIndicatorCode = 'B' and pp.ContainerSize != 0 then ((ppp.PackPrice * 100) * iv.Quantity) / pp.ContainerSize
						when pp.PackSize != 0 then ((ppp.PackPrice * 100) * iv.Quantity) / pp.PackSize
						else 0.00
						end, 0) / 100
					end as money)), 2)
			from dbo.Document d
				inner join dbo.Item i 
					inner join dbo.ItemVersion iv
						inner join dbo.ProductPackPeriodic ppp 
							inner join dbo.ProductPack pp on ppp.ProductPackID = pp.ProductPackID
						on iv.ProductPackPeriodicID = ppp.ProductPackPeriodicID
						left join dbo.ItemVersionEndorsement ive 
							inner join dbo.EndorsementPeriodic ep on ive.EndorsementPeriodicID = ep.EndorsementPeriodicID and ep.EndorsementCode = 'IP'
						on iv.ItemVersionID = ive.ItemVersionID
					on i.OriginalItemVersionID = iv.ItemVersionID
				on d.DocumentID = i.DocumentID
			where d.BatchID = @batchID
			group by i.DocumentID, i.Sequence
		)
		select @additionaFees2fCount = isnull(count(*), 0),
			@valueOfAdditionalFees2f = isnull(sum(round(isnull(BasicPrice, 0) * (@expensiveItemFeePercentage / 100), 2)), 0)
		from recalculatedPrices
		where BasicPrice >= @expensiveItemThreshold;

		-- Establishment payment:
		-- lookup the number of professional fees against the table in Part VIA.1 and pay the value specified...
		set @valueOfEstablishmentPayment = 0;
		select @valueOfEstablishmentPayment = round(eps.EstablishmentPayment / @establishmentPaymentDivisor, 2)
		from dbo.TariffEstablishmentPaymentScale eps
		where eps.FinancialPeriodID = @financialPeriodID and eps.JurisdictionID = @jurisdictionID
		  and @totalNumberOfProfessionalFees between eps.ItemsFrom and eps.ItemsTo;

		-- Subtotal of prescription fees:
		-- = the sum of the values calculated so far...
		set @subTotalOfFees = @totalValueOfProfessionalFees
			+ @valueOfPharmacyContractTransitionPayment
			+ (@repeatDispensingFee / 12)
			+ @valueOfPracticePayment
			+ @valueOfAdditionalFees2a
			+ @valueOfAdditionalFees2bHomeDelivery + @valueOfAdditionalFees2bMeasuredFitted
			+ @valueOfAdditionalFees2c
			+ @valueOfAdditionalFees2d
			+ @valueOfAdditionalFees2e + @valueOfAdditionalFeesMethadone
			+ @valueOfAdditionalFees2f
			+ @valueOfEstablishmentPayment;

		-- Other fees, Medicines use reviews:
		-- the number of Medicines Use Reviews undertaken from the FP34C declaration multiplied by the value specified in tariff part VIC.2...
		-- Note, the number of reviews and the fee for each review are actually loaded at the start of the SP
		set @valueOfMedicineUseReviews = round(@medicineUseReviewFee * @numberOfMedicineUseReviewsDeclaredOnFP34c, 2);

		-- Other fees, Appliance use reviews carried out at patients home:
		-- the number of Appliance Use reviews conducted at the users home from the FP34C declaration multiplied by the value specified 
		-- in Part VIE for a “first” review conducted at the users home...
		-- Note, the number of reviews and the fee for each review are actually loaded at the start of the SP
		if (@contractorIsRegisteredForApplianceUseReview = 1 and @contractorRegisteredForApplianceUseReviewSince <= @financialPeriodID)
			set @valueOfApplianceUseReviewAtHomeFees = round(@applianceUseReviewAtHomeFee * @numberOfApplianceUseReviewsAtHomeDeclaredOnFP34c, 2);

		-- Other fees, Appliance use reviews carried out at premises:
		-- the number of Appliance Use Reviews carried out at premises or subsequent reviews for users living at the same location within a 24 hour period
		-- from the FP34C declaration multiplied by the value specified in tariff part VIE for a “subsequent” review conducted at the users home...
		-- Note, the number of reviews and the fee for each review are actually loaded at the start of the SP
		if (@contractorIsRegisteredForApplianceUseReview = 1 and @contractorRegisteredForApplianceUseReviewSince <= @financialPeriodID)
			set @valueOfApplianceUseReviewAtPharmacyFees = round(@applianceUseReviewAtPharmacyFee * @numberOfApplianceUseReviewsAtPharmacyDeclaredOnFP34c, 2);

		/* TODO: Determine if Stoma Customisation is offerred, and if so, how many items qualify and calculate the fees payable */
		declare @numberOfStomaCustomisations integer = 0, @valueOfStomaCustomisationFees money;

		if (@contractorIsRegisteredForStomaCustomisation = 1 and @contractorRegisteredForStomaCustomisationSince <= @financialPeriodID) begin
			select @numberOfStomaCustomisations = count(*)
			from dbo.Document d
				inner join dbo.Item i
					inner join dbo.ItemVersion iv
						inner join dbo.ProductPackPeriodic ppp
							inner join dbo.ProductPeriodic pp
								inner join dbo.Product p on pp.ProductID = p.ProductID and p.IsEligibleForStomaCustomisationFee = 1
							on ppp.ProductPeriodicID = pp.ProductPeriodicID
							inner join dbo.DrugTariffCategory dtc on ppp.DrugTariffCategoryCode = dtc.DrugTariffCategoryCode and dtc.Category = 9
						on iv.ProductPackPeriodicID = ppp.ProductPackPeriodicID
					on i.OriginalItemVersionID = iv.ItemVersionID
				on d.DocumentID = i.DocumentID
			where d.BatchID = @batchID
		end
		set @valueOfStomaCustomisationFees = round(@numberOfStomaCustomisations * @stomaCustomisationFee, 2);

		-- Other fees, New Medicine Service...
		if (@financialPeriodID <= 201204) begin

			select @numberOfNewMedicineServicesUndertaken = isnull(NewMedicineServicesUndertaken, @numberOfNewMedicineServicesUndertaken)
			from dbo.ScheduleDocumentData sd
			where sd.ScheduleID = @originalScheduleID

		end else begin

			select @numberOfNewMedicineServicesUndertaken = isnull(NewMedicineServiceConsultations, @numberOfNewMedicineServicesUndertaken)
			from dbo.Submission s
			where s.BatchID = @batchID

		end
		
		set @numberOfNewMedicineServiceItems = dbo.CalculateNumberOfNewMedicineServiceItems(@batchID, @financialPeriodID);
		set @valueOfNewMedicineServiceConsultationFees = dbo.CalculateValueOfNewMedicineServiceFee(@batchID, @financialPeriodID, @numberOfNewMedicineServiceItems, @numberOfNewMedicineServicesUndertaken);

		-- Total of all fees:
		-- the “subtotal of prescription fees” + the sum of the “Other fees”...
		set @totalOfAllFees = @subTotalOfFees
			+ @valueOfMedicineUseReviews
			+ @valueOfApplianceUseReviewAtHomeFees
			+ @valueOfApplianceUseReviewAtPharmacyFees
			+ @valueOfStomaCustomisationFees
			+ @valueOfNewMedicineServiceConsultationFees;

		/*
		 * Charges...
		 *
		 */

		declare @numberOfChargesForApplianceItemsAtOriginalRate integer = 0, @valueOfChargesForApplianceItemsAtOriginalRate money = 0.00,
			@numberOfChargesForApplianceItemsAtPreviousRate integer = 0, @valueOfChargesForApplianceItemsAtPreviousRate money = 0.00,
			@numberOfChargesForDrugItemsAtOriginalRate integer = 0, @valueOfChargesForDrugItemsAtOriginalRate money = 0.00,
			@numberOfChargesForDrugItemsAtPreviousRate integer = 0, @valueOfChargesForDrugItemsAtPreviousRate money = 0.00,
			@totalvalueOfCharges money = 0.00;

		declare @numberOfChargesForApplianceItemsExcludingHoiseryAtOriginalRate integer = 0, @valueOfChargesForApplianceItemsExcludingHoiseryAtOriginalRate money = 0.00, 
			@numberOfChargesForApplianceItemsExcludingHoiseryAtPreviousRate integer = 0, @valueOfChargesForApplianceItemsExcludingHoiseryAtPreviousRate money = 0.00,
			@numberOfChargesForHoiseryApplianceItemsAtOriginalRate integer = 0, @valueOfChargesForHoiseryApplianceItemsAtOriginalRate money = 0.00, 
			@numberOfChargesForHoiseryApplianceItemsAtPreviousRate integer = 0, @valueOfChargesForHoiseryApplianceItemsAtPreviousRate money = 0.00;

		with charges as (
			
			select dgt.GroupTypeCode,
				p.ProductTypeCode,
				IsHosiery = isnull(ati.IsHosiery, 0),
				iv.NumberOfCharges
			from dbo.Document d
				inner join dbo.Item i
					inner join dbo.ItemVersion iv
						inner join dbo.ProductPackPeriodic ppp 
							inner join dbo.ProductPeriodic pp 
								inner join dbo.Product p on pp.ProductID = p.ProductID
								left join dbo.ApplianceTypeIndicator ati on pp.ApplianceTypeIndicatorCode = ati.ApplianceTypeIndicatorCode
							on ppp.ProductPeriodicID = pp.ProductPeriodicID
						on iv.ProductPackPeriodicID = ppp.ProductPackPeriodicID
					on i.OriginalItemVersionID = iv.ItemVersionID
				on d.DocumentID = i.DocumentID
				inner join dbo.DocumentGroupType dgt on d.OriginalDocumentGroupTypeID = dgt.DocumentGroupTypeID
			where d.BatchID = @batchID
				
		)
		select
			
			@numberOfChargesForApplianceItemsAtOriginalRate = isnull(sum(
				case when GroupTypeCode = '2' and ProductTypeCode = 'A' then NumberOfCharges else 0 end), cast (0.00 as money)),
			@numberOfChargesForDrugItemsAtOriginalRate = isnull(sum(
				case when GroupTypeCode = '2' and ProductTypeCode = 'D' then NumberOfCharges else 0 end), cast (0.00 as money)),
			@numberOfChargesForApplianceItemsAtPreviousRate = isnull(sum(
				case when GroupTypeCode = '3' and ProductTypeCode = 'A' then NumberOfCharges else 0 end), cast (0.00 as money)),
			@numberOfChargesForDrugItemsAtPreviousRate = isnull(sum(
				case when GroupTypeCode = '3' and ProductTypeCode = 'D' then NumberOfCharges else 0 end), cast (0.00 as money)),
					
			@numberOfChargesForApplianceItemsExcludingHoiseryAtOriginalRate = isnull(sum(
				case when GroupTypeCode = '2' and IsHosiery = 0 then NumberOfCharges else 0 end), cast (0.00 as money)),
			@numberOfChargesForApplianceItemsExcludingHoiseryAtPreviousRate = isnull(sum(
				case when GroupTypeCode = '3' and IsHosiery = 0 then NumberOfCharges else 0 end), cast (0.00 as money)),
			@numberOfChargesForHoiseryApplianceItemsAtOriginalRate = isnull(sum(
				case when GroupTypeCode = '2' and IsHosiery = 1 then NumberOfCharges else 0 end), cast (0.00 as money)),
			@numberOfChargesForHoiseryApplianceItemsAtPreviousRate = isnull(sum(
				case when GroupTypeCode = '3' and IsHosiery = 1 then NumberOfCharges else 0 end), cast (0.00 as money))
					
		from charges;
			
		set @valueOfChargesForApplianceItemsAtOriginalRate = round(@numberOfChargesForApplianceItemsAtOriginalRate * @applianceChargeAtOriginalRate, 2);
		set @valueOfChargesForDrugItemsAtOriginalRate = round(@numberOfChargesForDrugItemsAtOriginalRate * @drugsChargeAtOriginalRate, 2);
		set @valueOfChargesForApplianceItemsAtPreviousRate = round(@numberOfChargesForApplianceItemsAtPreviousRate * @applianceChargeAtPreviousRate, 2);
		set @valueOfChargesForDrugItemsAtPreviousRate = round(@numberOfChargesForDrugItemsAtPreviousRate * @drugsChargeAtPreviousRate, 2);

		set @valueOfChargesForApplianceItemsExcludingHoiseryAtOriginalRate = round(@numberOfChargesForApplianceItemsExcludingHoiseryAtOriginalRate * @applianceChargeAtOriginalRate, 2);
		set @valueOfChargesForApplianceItemsExcludingHoiseryAtPreviousRate = round(@numberOfChargesForApplianceItemsExcludingHoiseryAtPreviousRate * @applianceChargeAtPreviousRate, 2);
		set @valueOfChargesForHoiseryApplianceItemsAtOriginalRate = round(@numberOfChargesForHoiseryApplianceItemsAtOriginalRate * @applianceChargeAtOriginalRate, 2);
		set @valueOfChargesForHoiseryApplianceItemsAtPreviousRate = round(@numberOfChargesForHoiseryApplianceItemsAtPreviousRate * @applianceChargeAtPreviousRate, 2);
					
		set @totalValueOfCharges = @valueOfChargesForApplianceItemsAtOriginalRate + @valueOfChargesForApplianceItemsAtPreviousRate 
			+ @valueOfChargesForDrugItemsAtOriginalRate + @valueOfChargesForDrugItemsAtPreviousRate
			- @valueOfFp57RefundsDeclaredOnFP34c

		/*
		 * PRESCRIPTION DATA...
		 *
		 */

		declare @averageItemValue money,
			@numberOfEpsFormsReceived integer = 0, @numberOfEpsItemsReceived integer = 0,
			@numberOfItemsReferredBackToContractor integer = 0, @numberOfFormsReferredBackToContractor integer = 0, 
			@numberOfItemsDisallowed integer = 0, @numberOfFormsDisallowed integer = 0, 
			@totalNumerOfFormsReceived integer = 0;

		-- Total forms received (including electronic prescriptions):
		-- total number of forms processed in the batch...
		select @totalNumerOfFormsReceived = count(d.DocumentID)
		from dbo.Document d
		where d.BatchID = @batchID;
		
		select @numberOfEpsFormsReceived = count(d.DocumentID)
		from dbo.Document d
		where d.BatchID = @batchID
		  and d.FragmentTypeCode = 'Z';
		
		select @numberOfEpsItemsReceived = count(i.ItemID)
		from dbo.Document d
			inner join dbo.Item i on d.DocumentID = i.DocumentID
		where d.BatchID = @batchID
		  and d.FragmentTypeCode = 'Z'
		  and i.OriginalItemVersionID is not null;
		
		-- Number of items at SDR & ZDR are already counted above.

		-- Total of items, for which a fee is paid: = @totalNumberOfProfessionalFees

		-- Average item value:
		-- (“DRUG AND APPLIANCE COSTS : Total of drug and appliance costs” + “PRESCRIPTION FEES : Sub total of prescription fees”) / total “professional” fees
		set @averageItemValue = round((@totalOfDrugAndApplianceCosts + @subTotalOfFees) / @totalNumberOfProfessionalFees, 2);

		select @numberOfFormsReferredBackToContractor = count(distinct d.DocumentID),
			@numberOfItemsReferredBackToContractor = count(i.ItemID)
		from dbo.Document d
			inner join dbo.Item i
				inner join dbo.ItemVersion iv
					inner join dbo.ItemVersionEndorsement ive
						inner join dbo.EndorsementPeriodic ep on ive.EndorsementPeriodicID = ep.EndorsementPeriodicID and ep.EndorsementCode = 'RB'
					on iv.ItemVersionID = ive.ItemVersionID
				on i.OriginalItemVersionID = iv.ItemVersionID
			on d.DocumentID = i.DocumentID
		where d.BatchID = @batchID;

		select @numberOfFormsDisallowed = count(distinct d.DocumentID),
			@numberOfItemsDisallowed = count(i.ItemID)
		from dbo.Document d
			inner join dbo.Item i
				inner join dbo.ItemVersion iv
					inner join dbo.ItemVersionEndorsement ive
						inner join dbo.EndorsementPeriodic ep on ive.EndorsementPeriodicID = ep.EndorsementPeriodicID and ep.EndorsementCode = 'DA'
					on iv.ItemVersionID = ive.ItemVersionID
				on i.OriginalItemVersionID = iv.ItemVersionID
			on d.DocumentID = i.DocumentID
		where d.BatchID = @batchID;

		-- New Medicine Service, number of services undertaken/number of applicable items is calculated above (when the fee is calculated.

		/*
		 * Now that we have calcuated everything, it is time to write it to the database...
		 *
		 */

		begin transaction

		insert into dbo.Schedule (
			BatchID, 
			TotalOfDrugAndApplianceCosts, 
			TotalOfAllFees, 
			TotalOfDrugAndApplianceCostsPlusFees, 
			TotalOfChargesIncludingFP57Refunds, 
			TotalOfAccount, 
			RecoveryOfAdvancePayment, 
			RecoveryOfAdvancePaymentInRespectOfALateRegisteredBatch, 
			BalanceDueInRespectOfValue, 
			PaymentOnAccountForFinancialPeriodID, 
			PaymentOnAccountForItems, 
			PaymentOnAccountForCharges, 
			PaymentOnAccountForValue, 
			AdvancePaymentInRespectOfALateRegisteredBatch, 
			TotalAmountAuthorisedByPPD, 
			TotalAmountAuthorisedByPCT, 
			TotalAmountAuthorisedByOther, 
			NetPaymentMadeByPPD, 
			DateNetPaymentMadeByPPD, 
			GeneratedBy, 
			GeneratedOn)
		select
			@batchID,
			@totalOfDrugAndApplianceCosts,
			@totalOfAllFees,
			@totalOfDrugAndApplianceCosts + @totalOfAllFees,
			(0 - @totalValueOfCharges),
			@totalOfDrugAndApplianceCosts + @totalOfAllFees - @totalValueOfCharges,
			s.RecoveryOfAdvancePayment,
			s.RecoveryOfAdvancePaymentInRespectOfALateRegisteredBatch,
			s.BalanceDueInRespectOfValue,
			s.PaymentOnAccountForFinancialPeriodID,
			s.PaymentOnAccountForItems,
			s.PaymentOnAccountForCharges,
			s.PaymentOnAccountForValue,
			s.AdvancePaymentInRespectOfALateRegisteredBatch,
			0, -- TotalAmountAuthorisedByPPD
			s.TotalAmountAuthorisedByPCT,
			s.TotalAmountAuthorisedByOther,
			null, -- NetPaymentMadeByPPD
			null, -- DateNetPaymentMadeByPPD
			'PSNC', 
			getdate()
		from dbo.Schedule s
		where s.ScheduleID = @originalScheduleID;

		set @scheduleID = scope_identity();
		
		insert into dbo.ScheduleCharge (
			ScheduleID, 
			DrugsR1Quantity, DrugsR1Price, DrugsR1Value, 
			AppliancesR1Quantity, AppliancesR1Price, AppliancesR1Value, 
			DrugsR2Quantity, DrugsR2Price, DrugsR2Value, 
			AppliancesR2Quantity, AppliancesR2Price, AppliancesR2Value, 
			ExclHoiseryR1Quantity, ExclHoiseryR1Price, ExclHoiseryR1Value, 
			ExclHoiseryR2Quantity, ExclHoiseryR2Price, ExclHoiseryR2Value, 
			ElasticHoiseryValue, 
			FP57Refunds, 
			TotalIncludingFP57
		)
		values (
			@scheduleID,
			@numberOfChargesForDrugItemsAtOriginalRate, @drugsChargeAtOriginalRate, (0 - @valueOfChargesForDrugItemsAtOriginalRate),
			@numberOfChargesForApplianceItemsAtOriginalRate, @applianceChargeAtOriginalRate, (0 - @valueOfChargesForApplianceItemsAtOriginalRate),
			@numberOfChargesForDrugItemsAtPreviousRate, @drugsChargeAtPreviousRate, (0- @valueOfChargesForDrugItemsAtPreviousRate),
			@numberOfChargesForApplianceItemsAtPreviousRate, @applianceChargeAtPreviousRate, (0 - @valueOfChargesForApplianceItemsAtPreviousRate),
			@numberOfChargesForApplianceItemsExcludingHoiseryAtOriginalRate, @applianceChargeAtOriginalRate, (0 - @valueOfChargesForApplianceItemsExcludingHoiseryAtOriginalRate),
			@numberOfChargesForApplianceItemsExcludingHoiseryAtPreviousRate, @applianceChargeAtPreviousRate, (0 - @valueOfChargesForApplianceItemsExcludingHoiseryAtPreviousRate),
			(0 - (@valueOfChargesForHoiseryApplianceItemsAtOriginalRate + @valueOfChargesForHoiseryApplianceItemsAtPreviousRate)),
			@valueOfFp57RefundsDeclaredOnFP34c,
			(0 - @totalValueOfCharges)
		);
		
		insert into dbo.ScheduleCost (
			ScheduleID, 
			TotalOfBasicPricesAtStandardDiscountRate, 
			DiscountPercentage, DiscountValue, 
			TotalOfBasicPricesAtZeroDiscount, 
			SubTotalOfBasicPrices, 
			OutOfPocketExpenses, 
			ConsumableAllowanceQuantity, ConsumableAllowancePrice, ConsumableAllowanceValue, 
			ContainerAllowanceQuantity, ContainerAllowancePrice, ContainerAllowanceValue, 
			TotalOfDrugAndApplianceCosts, 
			Adjustment
		)
		select @scheduleID,
			@totalValueOfBasicPriceAtStandardDiscountRate,
			@standardDiscountDeductionPercentage, 0 - @valueOfStandardDiscount,
			@totalValueOfBasicPriceAtZeroDiscountRate,
			@subTotalOfBasicPrice,
			@totalValueOfOutOfPocketExpenseExpenseClaims,
			@totalNumberOfConsumableAllowanceFees, @valueOfConsumableAllowanceFee, @totalValueOfConsumableAllowanceFees,
			@totalNumberOfContainerAllowanceFees, @valueOfContainerAllowanceFee, @totalValueOfContainerAllowanceFees,
			@totalOfDrugAndApplianceCosts,
			sc.Adjustment
		from dbo.ScheduleCost sc
		where sc.ScheduleID = @originalScheduleID;
		
		insert into dbo.ScheduleDocumentData (
			ScheduleID, 
			TotalFormsReceived, 
			TotalEpsFormsReceived, TotalEpsItemsReceived, 
			ItemsAtZeroDiscountRateForWhichAFeeIsPaid, 
			ItemsAtStandardDiscountRateForWhichAFeeIsPaidIncludingOxygen, 
			TotalOfItemsForWhichAFeeIsPaid, 
			AverageItemValue, 
			ReferredBackItems, 
			ReferredBackForms, 
			DisallowedItems,
			DisallowedForms,
			MedicineUseReviewsDeclared, 
			DispensingStaffNumberOfHoursDeclared, 
			FP57FormsDeclared, 
			ApplianceUseReviewsCarriedOutAtPatientsHomeDeclared, 
			ApplianceUseReviewsCarriedOutAtPremisesDeclared, 
			NewMedicineServicesUndertaken, 
			NewMedicineServiceItems)
		values (
			@scheduleID,
			@totalNumerOfFormsReceived,
			@numberOfEpsFormsReceived, @numberOfEpsItemsReceived,
			@numberOfItemsPaidAtZeroDiscountRate,
			@numberOfItemsPaidAtStandardDiscountRate,
			@totalNumberOfProfessionalFees,
			@averageItemValue,
			@numberOfItemsReferredBackToContractor,
			@numberOfFormsReferredBackToContractor,
			@numberOfItemsDisallowed,
			@numberOfFormsDisallowed,
			@numberOfMedicineUseReviewsDeclaredOnFP34c,
			@numberOfSupportHoursDeclaredOnFP34c,
			@numberOfFp57RefundsDeclaredOnFP34c,
			@numberOfApplianceUseReviewsAtHomeDeclaredOnFP34c,
			@numberOfApplianceUseReviewsAtPharmacyDeclaredOnFP34c,
			@numberOfNewMedicineServicesUndertaken,
			@numberOfNewMedicineServiceItems
		);
		
		-- ToDo: Get lower and upper limits from the database...
		declare @valueOfExpensiveItemLowerLimit money = 100.00, @valueOfExpensiveItemUpperLimit money = 300.00;

		with recalculatedPrices as (
			select d.DocumentID, d.FragmentTypeCode, d.DocumentNumber, i.Sequence, sum(dbo.CalculateBasicPrice(i.OriginalItemVersionID)) as BasicPrice, count(i.OriginalItemVersionID) as NumberOfElements
			from dbo.Document d
				inner join dbo.Item i on d.DocumentID = i.DocumentID
			where d.BatchID = @batchID
			group by d.DocumentID, d.FragmentTypeCode, d.DocumentNumber, i.Sequence
		), expensiveItems as (
			select *
			from recalculatedPrices
			where BasicPrice >= @valueOfExpensiveItemLowerLimit
		),
		expensiveItemDetails as (
			select isnull(ei.FragmentTypeCode, '0') + right(N'00000' + convert(varchar(6), ei.DocumentNumber), 5) as FormNumber,
				ei.Sequence as ItemNumber, 
				ltrim(rtrim(p.Description)) + ' ' + ltrim(rtrim(pp.Description)) as Description,
				pp.PackSize, 
				iv.Quantity,
				ei.BasicPrice
			from expensiveItems ei
				inner join dbo.Item i
					inner join dbo.ItemVersion iv 
						inner join dbo.ProductPackPeriodic ppp 
							inner join dbo.ProductPack pp 
								inner join dbo.Product p on pp.ProductID = p.ProductID
							on ppp.ProductPackID = pp.ProductPackID
						on iv.ProductPackPeriodicID = ppp.ProductPackPeriodicID
					on i.OriginalItemVersionID = iv.ItemVersionID
				on ei.DocumentID = i.DocumentID and ei.Sequence = i.Sequence
			where ei.NumberOfElements = 1
			union
			select isnull(ei.FragmentTypeCode, '0') + right(N'00000' + convert(varchar(6), ei.DocumentNumber), 5) as FormNumber,
				ei.Sequence as ItemNumber, 
				'MULTIPLE PRODUCTS AND/OR PACKS' as Description, 0 as PackSize,  0 as Quantity, 
				ei.BasicPrice
			from expensiveItems ei
			where ei.NumberOfElements > 1
		)
		insert into dbo.ScheduleExpensiveItem (
			ScheduleID, FormNumber, ItemNumber, Description, PackSize, Quantity, BasicPrice)
		select @scheduleID, FormNumber, ItemNumber, Description, PackSize, Quantity, BasicPrice
		from expensiveItemDetails	
		order by BasicPrice desc, 2, 3;
		
		insert into dbo.ScheduleExpensiveItemSummary (
			ScheduleID, LowerLimitValue, UpperLimitValue, 
			NumberOfItemsBetweenLowerAndUpperLimit, 
			ValueOfItemsBetweenLowerAndUpperLimit, 
			NumberOfItemsAboveUpperLimit, 
			ValueOfItemsAboveUpperLimit, 
			NumberOfItemsAboveLowerLimit, 
			ValueOfItemsAboveLowerLimit)
		select @scheduleID, @valueOfExpensiveItemLowerLimit, @valueOfExpensiveItemUpperLimit, 
			count(*),
			sum(BasicPrice),
			sum(case when BasicPrice <= @valueOfExpensiveItemUpperLimit then 1 else 0 end),
			sum(case when BasicPrice <= @valueOfExpensiveItemUpperLimit then BasicPrice else 0 end),
			sum(case when BasicPrice > @valueOfExpensiveItemUpperLimit then 1 else 0 end),
			sum(case when BasicPrice > @valueOfExpensiveItemUpperLimit then BasicPrice else 0 end)
		from dbo.ScheduleExpensiveItem
		where ScheduleID = @scheduleID;
		
		insert into dbo.ScheduleFee (
			ScheduleID, 
			ProfessionalFeeQuantity, 
			ProfessionalFeeAmount, 
			ProfessionalFeeValue, 
			PharmacyContractTransitionPayment, 
			RepeatDispensingFee, 
			PracticePayment, 
			AdditionalFees2a, 
			AdditionalFees2bMeasuredFitted, 
			AdditionalFees2bHomeDelivery, 
			AdditionalFees2c, 
			AdditionalFees2d, 
			AdditionalFees2e, 
			AdditionalFeesMethadone,
			AdditionalFees2fCount, 
			AdditionalFees2f, 
			EstablishmentPayment, 
			ManuallyPriced, 
			SubTotalOfPrescriptionFees, 
			OtherFeesMedicinalUseReview, 
			OtherFeesApplianceUseReviewHome, 
			OtherFeesApplianceUseReviewPremises, 
			OtherFeesStomaCustomisation, 
			TotalOfAllFees, 
			OtherFeesNewMedicineServiceConsultation)
		values (
			@scheduleID,
			@totalNumberOfProfessionalFees,
			@professionalFee,
			@totalValueOfProfessionalFees,
			@valueOfPharmacyContractTransitionPayment,
			@repeatDispensingFee / 12,
			@valueOfPracticePayment,
			@valueOfAdditionalFees2a,
			@valueOfAdditionalFees2bMeasuredFitted,
			@valueOfAdditionalFees2bHomeDelivery,
			@valueOfAdditionalFees2c,
			@valueOfAdditionalFees2d,
			@valueOfAdditionalFees2e,
			@valueOfAdditionalFeesMethadone,
			@additionaFees2fCount,
			@valueOfAdditionalFees2f,
			@valueOfEstablishmentPayment,
			0, -- ManuallyPriced - no longer used according to Michael Hamilton.
			@subTotalOfFees,
			@valueOfMedicineUseReviews,
			@valueOfApplianceUseReviewAtHomeFees,
			@valueOfApplianceUseReviewAtPharmacyFees,
			@valueOfStomaCustomisationFees,
			@totalOfAllFees,
			@valueOfNewMedicineServiceConsultationFees
		);
		
		insert into dbo.ScheduleGroupSwitchSummary (
			ScheduleID, 
			ScheduleGroupSwitchDirectionID, 
			Reason, 
			Number, 
			OldRate)
		values (
			@scheduleID,
			1, -- Exempt to chargeable
			'',
			(select count(d.DocumentID)
				from dbo.Document d
					inner join dbo.SubmissionGroup sg on d.SubmissionGroupCode = sg.SubmissionGroupCode
					inner join dbo.DocumentGroupType cdgt on d.OriginalDocumentGroupTypeID = cdgt.DocumentGroupTypeID
				where d.BatchID = @batchID
					and sg.IsChargeable = 0 and cdgt.GroupTypeCode = '2'),
			0 -- OldRate
		);

		insert into dbo.ScheduleGroupSwitchSummary (
			ScheduleID, 
			ScheduleGroupSwitchDirectionID, 
			Reason, 
			Number, 
			OldRate)
		values (
			@scheduleID,
			2, -- Exempt to chargeable (old rate)
			'',
			(select count(d.DocumentID)
				from dbo.Document d
					inner join dbo.SubmissionGroup sg on d.SubmissionGroupCode = sg.SubmissionGroupCode
					inner join dbo.DocumentGroupType cdgt on d.OriginalDocumentGroupTypeID = cdgt.DocumentGroupTypeID
				where d.BatchID = @batchID
					and sg.IsChargeable = 0 and cdgt.GroupTypeCode = '3'),
			1 -- OldRate
		);

		insert into dbo.ScheduleGroupSwitchSummary (
			ScheduleID, 
			ScheduleGroupSwitchDirectionID, 
			Reason, 
			Number, 
			OldRate)
		values (
			@scheduleID,
			3, -- Chargeable to exempt
			'',
			(select count(d.DocumentID)
				from dbo.Document d
					inner join dbo.SubmissionGroup sg on d.SubmissionGroupCode = sg.SubmissionGroupCode
					inner join dbo.DocumentGroupType cdgt on d.OriginalDocumentGroupTypeID = cdgt.DocumentGroupTypeID
				where d.BatchID = @batchID
					and sg.IsChargeable = 1 and sg.IsOldRate = 0 and cdgt.GroupTypeCode = '1'),
			0 -- OldRate
		);

		insert into dbo.ScheduleGroupSwitchSummary (
			ScheduleID, 
			ScheduleGroupSwitchDirectionID, 
			Reason, 
			Number, 
			OldRate)
		values (
			@scheduleID,
			4, -- Chargeable (old rate) to exempt
			'',
			(select count(d.DocumentID)
				from dbo.Document d
					inner join dbo.SubmissionGroup sg on d.SubmissionGroupCode = sg.SubmissionGroupCode
					inner join dbo.DocumentGroupType cdgt on d.OriginalDocumentGroupTypeID = cdgt.DocumentGroupTypeID
				where d.BatchID = @batchID
					and sg.IsChargeable = 1 and sg.IsOldRate = 1 and cdgt.GroupTypeCode = '1'),
			0 -- OldRate
		);
		
		insert into dbo.ScheduleLocalPayment (
			ScheduleID, 
			AuthorisedBy, 
			Description, 
			Value)
		select @scheduleID,
			slp.AuthorisedBy, 
			slp.Description, 
			slp.Value
		from dbo.ScheduleLocalPayment slp
		where slp.ScheduleID = @originalScheduleID;
		
		insert into dbo.ScheduleOtherPayment (
			ScheduleID, 
			Description, 
			Value)
		select @scheduleID,
			sop.Description, 
			sop.Value
		from dbo.ScheduleOtherPayment sop
		where sop.ScheduleID = @originalScheduleID;

		commit transaction;

	end

	return

GO
/****** Object:  StoredProcedure [dbo].[InsertUserActivity]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsertUserActivity]
	@dateTime datetime = null,
	@userProfileID bigint = null,
	@activityID integer,
	@batchID bigint = null,
	@formID bigint = null,
	@itemID bigint = null,
	@oldItemVersionID bigint = null,
	@newItemVersionID bigint = null

as

	set nocount on;

	if (@dateTime is null)
		set @dateTime = getdate();

	if (@userProfileID is null) begin

		select @userProfileID = UserProfileID
		from dbo.UserProfile
		where LoginName = original_login();

		if (@userProfileID is null) begin

			insert into dbo.UserProfile (
				LoginName, Can1stCheck)
			values (original_login(), 1);

			set @userProfileID = scope_identity();

		end

	end

	insert into dbo.UserActivity (
		DateTime, UserProfileID, ActivityID, BatchID, FormID, ItemID, OldItemVersionID, NewItemVersionID)
	values (
		@dateTime, @userProfileID, @activityID, @batchID, @formID, @itemID, @oldItemVersionID, @newItemVersionID)

	return;

GO
/****** Object:  StoredProcedure [dbo].[MergeControlledSubstanceIndicatorPeriodic]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MergeControlledSubstanceIndicatorPeriodic]
	@toFinancialPeriodID integer,
	@frFinancialPeriodID integer = null

as

	set nocount on;

	if (@frFinancialPeriodID is null)
		select @frFinancialPeriodID = max(FinancialPeriodID) 
		from dbo.FinancialPeriod
		where FinancialPeriodID < @toFinancialPeriodID;

	with s as (
		select *
		from dbo.ControlledSubstanceIndicatorPeriodic
		where FinancialPeriodID = @frFinancialPeriodID),
	t as (
		select *
		from dbo.ControlledSubstanceIndicatorPeriodic
		where FinancialPeriodID = @toFinancialPeriodID)
	merge t
	using s on s.JurisdictionID = t.JurisdictionID and s.ControlledSubstanceIndicatorCode = t.ControlledSubstanceIndicatorCode
	when matched then update
		set t.AdditionalProfessionalFeeValue = s.AdditionalProfessionalFeeValue
	when not matched by target then 
		insert (FinancialPeriodID, JurisdictionID, ControlledSubstanceIndicatorCode, AdditionalProfessionalFeeValue)
		values (@toFinancialPeriodID, s.JurisdictionID, s.ControlledSubstanceIndicatorCode, s.AdditionalProfessionalFeeValue)
	when not matched by source then delete;

	return @@error;
GO
/****** Object:  StoredProcedure [dbo].[MergeEndorsementPeriodic]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MergeEndorsementPeriodic]
	@toFinancialPeriodID integer,
	@frFinancialPeriodID integer = null

as

	set nocount on;

	if (@frFinancialPeriodID is null)
		select @frFinancialPeriodID = max(FinancialPeriodID) 
		from dbo.FinancialPeriod
		where FinancialPeriodID < @toFinancialPeriodID;

	with s as (
		select *
		from dbo.EndorsementPeriodic
		where FinancialPeriodID = @frFinancialPeriodID),
	t as (
		select *
		from dbo.EndorsementPeriodic
		where FinancialPeriodID = @toFinancialPeriodID)
	merge t using s on s.JurisdictionID = t.JurisdictionID and s.EndorsementCode = t.EndorsementCode
	when matched then update
		set t.Description = s.Description,
			t.ZeroCharges = s.ZeroCharges,
			t.SpecialFeeValue = s.SpecialFeeValue, 
			t.SpecialFeebasis = s.SpecialFeebasis,
			t.IsUserSelectable = s.IsUserSelectable,
			t.DisplaySequence = s.DisplaySequence
	when not matched by target then 
		insert (JurisdictionID, FinancialPeriodID, EndorsementCode, Description, ZeroCharges, SpecialFeeValue, SpecialFeebasis, IsUserSelectable, DisplaySequence)
		values (s.JurisdictionID, @toFinancialPeriodID, EndorsementCode, s.Description, s.ZeroCharges, s.SpecialFeeValue, s.SpecialFeebasis, s.IsUserSelectable, s.DisplaySequence)
	when not matched by source then delete;

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[MergeJurisdictionPeriodic]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MergeJurisdictionPeriodic]
	@toFinancialPeriodID integer,
	@frFinancialPeriodID integer = null

as

	set nocount on;

	if (@frFinancialPeriodID is null)
		select @frFinancialPeriodID = max(FinancialPeriodID) 
		from dbo.FinancialPeriod
		where FinancialPeriodID < @toFinancialPeriodID;

	with s as (
		select *
		from dbo.JurisdictionPeriodic
		where FinancialPeriodID = @frFinancialPeriodID),
	t as (
		select *
		from dbo.JurisdictionPeriodic
		where FinancialPeriodID = @toFinancialPeriodID)
	merge t
	using s on s.JurisdictionID = t.JurisdictionID
	when matched then update
		set t.ApplianceItemPrescriptionChargeValue = s.ApplianceItemPrescriptionChargeValue, 
			t.DrugItemPrescriptionChargeValue = s.DrugItemPrescriptionChargeValue, 
			t.StandardProfessionalFeeValue = s.StandardProfessionalFeeValue, 
			t.ExpensiveItemThreshold = s.ExpensiveItemThreshold, 
			t.ExpensiveItemFeePercentage = s.ExpensiveItemFeePercentage, 
			t.OutOfPocketExpenseDeductionValue = s.OutOfPocketExpenseDeductionValue, 
			t.ContainerAllowanceValue = s.ContainerAllowanceValue, 
			t.RepeatDispensingFee = s.RepeatDispensingFee, 
			t.MedicinesUseReviewFee = s.MedicinesUseReviewFee, 
			t.StomaCustomisationFee = s.StomaCustomisationFee, 
			t.ApplianceUseReviewAtPharmacyFee = s.ApplianceUseReviewAtPharmacyFee, 
			t.ApplianceUseReviewAtHomeFee = s.ApplianceUseReviewAtHomeFee, 
			t.ContractTransitionPaymentDivisor = s.ContractTransitionPaymentDivisor, 
			t.ContractTransitionPaymentMultiplier = s.ContractTransitionPaymentMultiplier, 
			t.PracticePaymentDivisor = s.PracticePaymentDivisor,
			t.EstablishmentPaymentDivisor = s.EstablishmentPaymentDivisor,
			t.PppaTargetPercentage = s.PppaTargetPercentage,
			t.NcvLowerTargetPercentage = s.NcvLowerTargetPercentage,
			t.NcvUpperTargetPercentage = s.NcvUpperTargetPercentage,
			t.AcvLowerTargetPercentage = s.AcvLowerTargetPercentage
	when not matched by target then 
		insert (FinancialPeriodID, JurisdictionID, 
			ApplianceItemPrescriptionChargeValue,
			DrugItemPrescriptionChargeValue,
			StandardProfessionalFeeValue,
			ExpensiveItemThreshold,
			ExpensiveItemFeePercentage,
			OutOfPocketExpenseDeductionValue,
			ContainerAllowanceValue,
			RepeatDispensingFee,
			MedicinesUseReviewFee,
			StomaCustomisationFee,
			ApplianceUseReviewAtPharmacyFee,
			ApplianceUseReviewAtHomeFee,
			ContractTransitionPaymentDivisor,
			ContractTransitionPaymentMultiplier,
			PracticePaymentDivisor,
			EstablishmentPaymentDivisor,
			PppaTargetPercentage,
			NcvLowerTargetPercentage,
			NcvUpperTargetPercentage,
			AcvLowerTargetPercentage)
		values (@toFinancialPeriodID, s.JurisdictionID, 
			s.ApplianceItemPrescriptionChargeValue,
			s.DrugItemPrescriptionChargeValue,
			s.StandardProfessionalFeeValue,
			s.ExpensiveItemThreshold,
			s.ExpensiveItemFeePercentage,
			s.OutOfPocketExpenseDeductionValue,
			s.ContainerAllowanceValue,
			s.RepeatDispensingFee,
			s.MedicinesUseReviewFee,
			s.StomaCustomisationFee,
			s.ApplianceUseReviewAtPharmacyFee,
			s.ApplianceUseReviewAtHomeFee,
			s.ContractTransitionPaymentDivisor,
			s.ContractTransitionPaymentMultiplier,
			s.PracticePaymentDivisor,
			s.EstablishmentPaymentDivisor,
			s.PppaTargetPercentage,
			s.NcvLowerTargetPercentage,
			s.NcvUpperTargetPercentage,
			s.AcvLowerTargetPercentage)
	when not matched by source then delete;

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[oldDeleteDocumentData]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[oldDeleteDocumentData]
	@periodsToRetain integer = 14,
	@batchSize integer = 250,
	@maxDuration time = '00:30'

as

	set nocount on;

	declare @stopDeletingAt datetime = getdate() + @maxDuration, 
		@documentCount integer = -1,
		@scheduleCount integer = -1,
		@submissionImageCount integer = -1;

	declare @minimumFinancialPeriodIDToRetain integer;
	select top 1 @minimumFinancialPeriodIDToRetain = FinancialPeriodID
	from (
		select top (@periodsToRetain) FinancialPeriodID
		from FinancialPeriod
		where FinancialPeriodID < (
			select top 1 FinancialPeriodID
			from dbo.FinancialPeriod
			where IsDefault = 1
			order by FinancialPeriodID
		)
		order by FinancialPeriodID desc
	) fp
	order by FinancialPeriodID;


	declare @documents table (
		DocumentID bigint primary key
	);

	while (@documentCount != 0 and getdate() < @stopDeletingAt) begin

		delete from @documents;

		insert into @documents
		select top (@batchSize) DocumentID
		from dbo.Batch b
			inner join dbo.Document d on b.BatchID = d.BatchID
		where b.FinancialPeriodID < @minimumFinancialPeriodIDToRetain
		order by DocumentID
		option (optimize for (@minimumFinancialPeriodIDToRetain unknown));

		set @documentCount = @@rowcount;
		if (@documentCount > 0) begin

			delete from ua
			from @documents d
				inner join dbo.Item i
					inner join dbo.UserActivity ua on i.ItemID = ua.ItemID
				on d.DocumentID = i.DocumentID;

			delete from af
			from @documents d
				inner join dbo.Item i
					inner join dbo.AuditFeedback af on i.ItemID = af.ItemID
				on d.DocumentID = i.DocumentID;

			update i
			set i.CurrentItemVersionID = null,
				i.OriginalItemVersionID = null
			from @documents d
				inner join dbo.Item i on d.DocumentID = i.DocumentID;

			delete from edi
			from @documents d
				inner join EpsHeader eh 
					inner join EpsPrescribedItem epi 
						inner join dbo.EpsDispensedItem edi on epi.EpsPrescribedItemID = edi.EpsPrescribedItemID
					on eh.EpsHeaderID = epi.EpsHeaderID
				on d.DocumentID = eh.DocumentID;

			delete from epi
			from @documents d
				inner join EpsHeader eh 
					inner join EpsPrescribedItem epi on eh.EpsHeaderID = epi.EpsHeaderID
				on d.DocumentID = eh.DocumentID;

			delete from eh
			from @documents d
				inner join EpsHeader eh on d.DocumentID = eh.DocumentID;

			delete from ive
			from @documents d
				inner join dbo.Item i 
					inner join dbo.ItemVersion iv 
						inner join dbo.ItemVersionEndorsement ive on iv.ItemVersionID = ive.ItemVersionID
					on i.ItemID = iv.ItemID
				on d.DocumentID = i.DocumentID;

			delete from iv
			from @documents d
				inner join dbo.Item i 
					inner join dbo.ItemVersion iv on i.ItemID = iv.ItemID
				on d.DocumentID = i.DocumentID;

			delete from ia
			from @documents d
				inner join dbo.Item i 
					inner join dbo.ItemAdvisory ia on i.ItemID = ia.ItemID
				on d.DocumentID = i.DocumentID;

			delete from ia
			from @documents d
				inner join dbo.Item i 
					inner join dbo.ItemNote ia on i.ItemID = ia.ItemID
				on d.DocumentID = i.DocumentID;

			delete from i
			from @documents d
				inner join dbo.Item i on d.DocumentID = i.DocumentID;

			delete from di
			from @documents d
				inner join dbo.DocumentImage di on d.DocumentID = di.DocumentID;

			delete bup
			from @documents d
				inner join dbo.BatchUserProfile bup on d.DocumentID = bup.LastDocumentID;

			delete from ua
			from @documents d
				inner join dbo.UserActivity ua on d.DocumentID = ua.FormID;

			update doc
			set OriginalDocumentGroupTypeID = null,
				CurrentDocumentGroupTypeID = null
			from @documents d 
				inner join dbo.Document doc on d.DocumentID = doc.DocumentID;

			delete from dgt
			from @documents d 
				inner join dbo.DocumentGroupType dgt on d.DocumentID = dgt.DocumentID;

			delete from doc
			from @documents d 
				inner join dbo.Document doc on d.DocumentID = doc.DocumentID;

		end;

	end;

	declare @schedules table (
		ScheduleID bigint primary key
	);

	while (@scheduleCount != 0 and getdate() < @stopDeletingAt) begin

		delete from @schedules;

		insert into @schedules
		select top (@batchSize) s.ScheduleID
		from dbo.Batch b
			inner join dbo.Schedule s on b.BatchID = s.BatchID and b.ScheduleID != s.ScheduleID and b.PsncScheduleID != s.ScheduleID
		where b.FinancialPeriodID < @minimumFinancialPeriodIDToRetain
		order by s.ScheduleID
		option (optimize for (@minimumFinancialPeriodIDToRetain unknown));

		set @scheduleCount = @@rowcount;
		if (@scheduleCount > 0) begin

			delete from charge
			from @schedules s
				inner join dbo.ScheduleCharge charge on s.ScheduleID = charge.ScheduleID;

			delete from cost
			from @schedules s
				inner join dbo.ScheduleCost cost on s.ScheduleID = cost.ScheduleID;

			delete from documentData
			from @schedules s
				inner join dbo.ScheduleDocumentData documentData on s.ScheduleID = documentData.ScheduleID;

			delete from expensiveItem
			from @schedules s
				inner join dbo.ScheduleExpensiveItem expensiveItem on s.ScheduleID = expensiveItem.ScheduleID;

			delete from expensiveItemSummary
			from @schedules s
				inner join dbo.ScheduleExpensiveItemSummary expensiveItemSummary on s.ScheduleID = expensiveItemSummary.ScheduleID;

			delete from fee
			from @schedules s
				inner join dbo.ScheduleFee fee on s.ScheduleID = fee.ScheduleID;

			delete from groupSwitchSummary
			from @schedules s
				inner join dbo.ScheduleGroupSwitchSummary groupSwitchSummary on s.ScheduleID = groupSwitchSummary.ScheduleID;

			delete from localPayment
			from @schedules s
				inner join dbo.ScheduleLocalPayment localPayment on s.ScheduleID = localPayment.ScheduleID;

			delete from otherPayment
			from @schedules s
				inner join dbo.ScheduleOtherPayment otherPayment on s.ScheduleID = otherPayment.ScheduleID;

			delete from schedule
			from @schedules s
				inner join dbo.Schedule schedule on s.ScheduleID = schedule.ScheduleID;

		end

	end

	declare @submissionImages table (
		SubmissionImageID bigint primary key
	);

	while (@submissionImageCount != 0 and getdate() < @stopDeletingAt) begin

		delete from @submissionImages;

		insert into @submissionImages
		select top (@batchSize) si.SubmissionImageID
		from dbo.Batch b
			inner join dbo.SubmissionImage si on b.BatchID = si.BatchID
		where b.FinancialPeriodID < @minimumFinancialPeriodIDToRetain
		order by si.SubmissionImageID
		option (optimize for (@minimumFinancialPeriodIDToRetain unknown));

		set @submissionImageCount = @@rowCount;
		if (@submissionImageCount > 0) begin

			delete from si2
			from @submissionImages si
				inner join dbo.SubmissionImage si2 on si.SubmissionImageID = si2.SubmissionImageID

		end

	end

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[OnDispenserSearchImported]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OnDispenserSearchImported]
	@batchID bigint

as

	set nocount on;
	set transaction isolation level read uncommitted;

	declare @missingImageCount integer = 0,
		@fragmentCount integer = 0,
		@paperTrackingID bigint;
	
	with imageCount as (
		select d.DocumentID, count(di.DocumentImageID) as Images
		from dbo.Document d
			left join dbo.DocumentImage di on d.DocumentID = di.DocumentID
		where d.BatchID = @batchID
		  and d.FragmentTypeCode = 'A'
		group by d.DocumentID
	)
	select @missingImageCount = count(*)
	from imageCount 
	where Images = 0;

	select @fragmentCount = count(*)
	from dbo.Document d 
	where d.BatchID = @batchID
	  and ((FragmentTypeCode != 'Z' and SubmissionGroupCode is null)
	   or d.FragmentTypeCode between 'N' and 'Y');

	if (@fragmentCount = 0) begin

		insert into dbo.PaperTracking (BatchID,
			DateTime, PaperTrackingStatusID, PaperTypeID)
		values (@batchID,
			getdate(), 99 /* None */, 2 /* Fragments */);
		set @paperTrackingID = scope_identity();

		update dbo.Batch
		set CurrentFragmentTrackingID = @paperTrackingID
		where BatchID = @batchID;

	end

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[OnScheduleOfPaymentsImported]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OnScheduleOfPaymentsImported]
	@batchID bigint

as

	set nocount on;
	set transaction isolation level read uncommitted;

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[OpenBatchViewModel_SelectCallback]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OpenBatchViewModel_SelectCallback]
	@financialPeriodID integer,
	@userCan2ndCheck bit

as

	set nocount on;
	set transaction isolation level read uncommitted;

	declare @batches table (
		ID bigint primary key,
		ContractorCode varchar(7), ContractorName varchar(128), ContractorNotes varchar(max),
		AuditType varchar(128), AuditTypeDisplaySequence integer,
		NumberOfForms integer, NumberOfItems integer,
		NumberOfItemsWithErrors integer, NumberOfUncheckedItems integer, NumberOfVerifiedItems integer, NumberOfReferredItems integer,
		BatchStatusID integer, BatchStatus varchar(128), BundleTrackingStatusID integer, BundleTrackingStatus varchar(128), FragmentTrackingStatusID integer, FragmentTrackingStatus varchar(128), LastUser nvarchar(128)
	);

	insert into @batches
	select b.BatchID as ID, 
		c.Code as ContractorCode, isnull(b.Name, c.Name) as ContractorName, c.Notes as ContractorNotes,
		at.Description as AuditType, at.DisplaySequence as AuditTypeDisplaySequence,
		0 as NumberOfForms, 0 as NumberOfItems,
		0 as NumberOfItemsWithErrors, 0 as NumberOfUncheckedItems, 0 as NumberOfVerifiedItems, 0 as NumberOfReferredItems,
		b.BatchStatusID, bs.Description as BatchStatus, 
		BundleTrackingStatusID = bpts.PaperTrackingStatusID, BundleTrackingStatus = bpts.Description,
		FragmentTrackingStatusID = fpts.PaperTrackingStatusID, FragmentTrackingStatus = fpts.Description,
		up.LoginName as LastUser
	from dbo.Batch b
		left join dbo.AuditType at on isnull(b.AuditTypeID, 1) = at.AuditTypeID
		inner join dbo.BatchStatus bs on b.BatchStatusID = bs.BatchStatusID
		inner join dbo.Contractor c on b.ContractorID = c.ContractorID
		left join dbo.PaperTracking bpt
			inner join dbo.PaperTrackingStatus bpts on bpt.PaperTrackingStatusID = bpts.PaperTrackingStatusID
		on b.CurrentBundleTrackingID = bpt.PaperTrackingID
		left join dbo.PaperTracking fpt
			inner join dbo.PaperTrackingStatus fpts on fpt.PaperTrackingStatusID = fpts.PaperTrackingStatusID
		on b.CurrentFragmentTrackingID = fpt.PaperTrackingID
		left join dbo.UserProfile up on b.LastStatusChangeByUserProfileID = up.UserProfileID
	where b.FinancialPeriodID = @financialPeriodID
	  and (bs.IsVisibleInAuditClientTo1stChecker = 1 or (@userCan2ndCheck = 1 and IsVisibleInAuditClientTo2ndChecker = 1))
	option (optimize for (@financialPeriodID unknown, @userCan2ndCheck unknown));

	-- Forms
	update b
	set NumberOfForms = f.NumberOfForms
	from @batches b
		inner join (
			select BatchID, NumberOfForms = count(*)
			from (
				select d.BatchID, d.DocumentID
				from @batches b
					inner join dbo.Document d
						inner join dbo.Item i on d.DocumentID = i.DocumentID
					on b.ID = d.BatchID
				group by d.BatchID, d.DocumentID) f
			group by BatchID) f on b.ID = f.BatchID;

	-- Items
	update b
	set NumberOfItems = i.NumberOfItems,
		NumberOfItemsWithErrors = i.NumberOfItemsWithError,
		NumberOfUncheckedItems = i.NumberOfItemsUnchecked,
		NumberOfVerifiedItems = i.NumberOfItemsVerified,
		NumberOfReferredItems = i.NumberOfItemsReferred
	from @batches b
		inner join (
			select BatchID, 
				NumberOfItems = count(*),
				NumberOfItemsWithError = sum(HasError),
				NumberOfItemsVerified = sum(IsVerified),
				NumberOfItemsUnchecked = sum(IsUnchecked),
				NumberOfItemsReferred = sum(IsReferred)
			from (
				select d.BatchID, i.ItemID,
				HasError = case when isnull(i.OriginalItemVersionID, 0) != isnull(i.CurrentItemVersionID, 0) then 1 else 0 end,
				IsVerified = cast(isnull(iv.IsVerified, 0) as integer),
				IsUnchecked = case when isnull(iv.IsVerified, 0) = 0 and isnull(iv.IsReferred, 0) = 0 then 1 else 0 end,
				IsReferred = cast(isnull(iv.IsReferred, 0) as integer)
				from @batches b
					inner join dbo.Document d
						inner join dbo.Item i 
							left join dbo.ItemVersion iv on i.CurrentItemVersionID = iv.ItemVersionID
						on d.DocumentID = i.DocumentID
					on b.ID = d.BatchID
				group by d.BatchID, i.ItemID, i.OriginalItemVersionID, i.CurrentItemVersionID, iv.IsVerified, iv.IsReferred
				having count(i.ItemID) > 0) i
		group by BatchID) i on b.ID = i.BatchID;

	select *
	from @batches
	order by ContractorCode;
	
	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[OperatorIndecisionReport_Header]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OperatorIndecisionReport_Header]
	@batchID bigint

as

	set nocount on;
	set transaction isolation level read uncommitted;

	select c.Code, c.Name, fp.FinancialYear, fp.FinancialMonth
	from dbo.Batch b
		inner join dbo.Contractor c on b.ContractorID = c.ContractorID
		inner join dbo.FinancialPeriod fp on b.FinancialPeriodID = fp.FinancialPeriodID
	where b.BatchID = @batchID

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[OperatorIndecisionReport_ItemDetails]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OperatorIndecisionReport_ItemDetails]
	@batchID bigint

as

	set nocount on;
	set transaction isolation level read uncommitted;

	with indecisionItems as (
	
		select
			b.FinancialPeriodID, b.BatchID, b.ContractorID,
			d.DocumentID, FormNumber = case
					when d.FragmentTypeCode is null then right('000000' + convert(varchar(6), d.DocumentNumber), 6)
					else d.FragmentTypeCode + right('00000' + convert(varchar(6), d.DocumentNumber), 5) 
					end,
			i.ItemID, ItemNumber = i.Sequence, ElementNumber = i.SubSequence, i.OriginalItemVersionID,
			NumberOfElements = (select count(*) from dbo.Item where Item.DocumentID = d.DocumentID and Item.Sequence = i.Sequence and Item.CurrentItemVersionID is not null),
			NumberOfVersions = count(iv.ItemVersionID)
		from dbo.Batch b
			inner join dbo.Contractor c on b.ContractorID = c.ContractorID
			inner join dbo.Document d 
				inner join dbo.Item i 
					inner join dbo.ItemVersion iv on i.ItemID = iv.ItemID
				on d.DocumentID = i.DocumentID
			on b.BatchID = d.BatchID
		where b.BatchID = @batchID
		group by b.FinancialPeriodID, b.BatchID, b.ContractorID, d.DocumentID, d.FragmentTypeCode, d.DocumentNumber,
			i.ItemID, i.Sequence, i.SubSequence, i.OriginalItemVersionID, i.CurrentItemVersionID
		having count(iv.ItemVersionID) > 2 or (count(iv.ItemVersionID) > 1 and isnull(i.CurrentItemVersionID, 0) = isnull(i.OriginalItemVersionID, 0))
	
	)

	select ii.FinancialPeriodID, c.Code, ii.FormNumber, ii.ItemNumber, ii.ElementNumber, ii.NumberOfElements, ii.NumberOfVersions,
		OrgPackCode = opp.Code, OrgProductDescription = op.Description, OrgPackDescription = opp.Description, OrgQuantity = oiv.Quantity, 
		OrgPackSize = case 
			when oppp.SpecialContainerIndicatorCode = 'C' and not (opp.ContainerSize = 0 and opp.PackSize > 1 and oppp.PackPrice != oppp.UnitPrice) then opp.ContainerSize
			when oppp.SpecialContainerIndicatorCode = 'B' and opp.ContainerSize != 0 then opp.ContainerSize
			else opp.PackSize end,
		OrgBasicPrice = oiv.BasicPrice, OrgValueOfExpensiveItemFee = oiv.ValueOfExpensiveItemFee, OrgValueOfAdditionalFees = oiv.ValueOfAdditionalFees - oiv.ValueOfExpensiveItemFee, OrgEndorsements = dbo.EndorsementCodeList(oiv.ItemVersionID),
		ua.DateTime, up.LoginName, Description = case
			when ua.ActivityID = 13 then 'New'
			when ua.ActivityID = 15 then 'Restore'
			else '' end,
		NewPackCode = npp.Code, NewProductDescription = np.Description, NewPackDescription = npp.Description, NewQuantity = niv.Quantity, 
		NewPackSize = case 
			when nppp.SpecialContainerIndicatorCode = 'C' and not (npp.ContainerSize = 0 and npp.PackSize > 1 and nppp.PackPrice != nppp.UnitPrice) then npp.ContainerSize
			when nppp.SpecialContainerIndicatorCode = 'B' and npp.ContainerSize != 0 then npp.ContainerSize
			else npp.PackSize end,
		NewBasicPrice = niv.BasicPrice, NewValueOfExpensiveItemFee = niv.ValueOfExpensiveItemFee, NewValueOfAdditionalFees = niv.ValueOfAdditionalFees - niv.ValueOfExpensiveItemFee, NewEndorsements = dbo.EndorsementCodeList(niv.ItemVersionID),
		Note = case when ii.OriginalItemVersionID = niv.ItemVersionID then 'BSA' else null end
	from indecisionItems ii
		inner join dbo.Contractor c on ii.ContractorID = c.ContractorID
		inner join dbo.ItemVersion oiv 
			left join dbo.ProductPackPeriodic oppp
				inner join dbo.ProductPack opp
					inner join dbo.Product op on opp.ProductID = op.ProductID
				on oppp.ProductPackID = opp.ProductPackID
			on oiv.ProductPackPeriodicID = oppp.ProductPackPeriodicID
		on ii.OriginalItemVersionID = oiv.ItemVersionID
		inner join dbo.UserActivity ua 
			inner join dbo.ItemVersion niv 
				left join dbo.ProductPackPeriodic nppp
					inner join dbo.ProductPack npp
						inner join dbo.Product np on npp.ProductID = np.ProductID
					on nppp.ProductPackID = npp.ProductPackID
				on niv.ProductPackPeriodicID = nppp.ProductPackPeriodicID
			on ua.NewItemVersionID = niv.ItemVersionID
			inner join dbo.UserProfile up on ua.UserProfileID = up.UserProfileID
		on ii.ItemID = ua.ItemID and ua.ActivityID in (13, 15)
	order by ii.FinancialPeriodID, c.Code, ii.FormNumber, ii.ItemNumber, ii.ElementNumber,
		ua.DateTime;

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[OperatorMonthlyPerformanceReport_Details]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[OperatorMonthlyPerformanceReport_Details]
	@userProfileID integer = null,
	@frDate datetime, 
	@toDate dateTime

as

	set nocount on;
	set transaction isolation level read uncommitted;

	with months (FrDate, ToDate) as (
	
		select cast(@frDate as date) as FrDate, cast(dateadd(day, -1, dateadd(month, 1, @frDate)) as date) as ToDate
		union all
		select dateadd(month, 1, FrDate) as FrDate, cast(dateadd(day, -1, dateadd(month, 2, FrDate)) as date) as ToDate
		from months
		where ToDate < @toDate
	
	), totalUserActivityByMonth as (
	
		select FrDate, 
			ToDate,
			TotalNumberOfItemsInspected = (
				select count(*)
				from (
					select ItemID
					from dbo.UserActivity ua
					where cast(ua.DateTime as date) between FrDate and ToDate
						and ua.ItemID is not null
						and ua.ActivityID in (4, 6, 8, 13) -- Verified, Referred, Confirmed or New Item Version Added
					group by ItemID) i
				), 
			TotalNumberOfItemsCorrected = (
				select count(*)
				from (
					select ItemID
					from dbo.UserActivity ua
					where cast(ua.DateTime as date) between FrDate and ToDate
						and ua.ItemID is not null
						and ua.ActivityID = 13 -- New Item Version Added
					group by ItemID) i
				), 
			TotalNumberOfItemsVerified = (
				select count(*)
				from (
					select ItemID
					from dbo.UserActivity ua
					where cast(ua.DateTime as date) between FrDate and ToDate
						and ua.ItemID is not null
						and ua.ActivityID = 4 -- Item Version Verified
					group by ItemID) i
				), 
			TotalNumberOfItemsReferred = (
				select count(*)
				from (
					select ItemID
					from dbo.UserActivity ua
					where cast(ua.DateTime as date) between FrDate and ToDate
						and ua.ItemID is not null
						and ua.ActivityID = 6 -- Item Version Referred
					group by ItemID) i
				), 
			TotalNumberOfItemsConfirmed = (
				select count(*)
				from (
					select ItemID
					from dbo.UserActivity ua
					where cast(ua.DateTime as date) between FrDate and ToDate
						and ua.ItemID is not null
						and ua.ActivityID = 8 -- Item Version Confirmed
					group by ItemID) i
				),
			TotalNumberOfHoursDeclared = cast(0.00 as money)
		from months
		
	), individualUsersActivityByMonth as (

		select up.UserProfileID,
			up.LoginName,
			FrDate,
			ToDate, 
			TotalNumberOfItemsInspectedByUser = (
				select count(*)
				from (
					select ItemID
					from dbo.UserActivity ua
					where ua.UserProfileID = up.UserProfileID
						and cast(ua.DateTime as date) between FrDate and ToDate
						and ua.ItemID is not null
						and ua.ActivityID in (4, 6, 8, 13) -- Verified, Referred, Confirmed or New Item Version Added
					group by ItemID) i
				),
			TotalNumberOfItemsCorrectedByUser = (
				select count(*)
				from (
					select ItemID
					from dbo.UserActivity ua
					where ua.UserProfileID = up.UserProfileID
						and cast(ua.DateTime as date) between FrDate and ToDate
						and ua.ItemID is not null
						and ua.ActivityID = 13 -- New Item Version Added
					group by ItemID) i
				), 
			TotalNumberOfItemsVerifiedByUser = (
				select count(*)
				from (
					select ItemID
					from dbo.UserActivity ua
					where ua.UserProfileID = up.UserProfileID
						and cast(ua.DateTime as date) between FrDate and ToDate
						and ua.ItemID is not null
						and ua.ActivityID = 4 -- Item Version Verified
					group by ItemID) i
				), 
			TotalNumberOfItemsReferredByUser = (
				select count(*)
				from (
					select ItemID
					from dbo.UserActivity ua
					where ua.UserProfileID = up.UserProfileID
						and cast(ua.DateTime as date) between FrDate and ToDate
						and ua.ItemID is not null
						and ua.ActivityID = 6 -- Item Version Referred
					group by ItemID) i
				), 
			TotalNumberOfItemsConfirmedByUser = (
				select count(*)
				from (
					select ItemID
					from dbo.UserActivity ua
					where ua.UserProfileID = up.UserProfileID
						and cast(ua.DateTime as date) between FrDate and ToDate
						and ua.ItemID is not null
						and ua.ActivityID = 8 -- Item Version Confirmed
					group by ItemID) i
				),
			TotalNumberOfHoursDeclaredByUser = cast(0.00 as money),
			TotalNumberOfItemsInspected, 
			TotalNumberOfItemsCorrected,
			TotalNumberOfItemsVerified,
			TotalNumberOfItemsReferred,
			TotalNumberOfItemsConfirmed,
			TotalNumberOfHoursDeclared
		from dbo.UserProfile up
			cross join totalUserActivityByMonth
		where (@userProfileID is null or up.UserProfileID = @userProfileID)
	
	)
	
	select FrDate, ToDate,
		UserProfileID, LoginName,
		
		TotalNumberOfItemsInspectedByUser, TotalNumberOfItemsCorrectedByUser, 
		PercentageOfItemsCorrectedByUser = case when TotalNumberOfItemsInspectedByUser = 0 then 0 else (cast(TotalNumberOfItemsCorrectedByUser as money) / cast(TotalNumberOfItemsInspectedByUser as money)) * 100 end,
		TotalNumberOfItemsVerifiedByUser, TotalNumberOfItemsReferredByUser,
		PercentageOfItemsReferredByUser = case when TotalNumberOfItemsInspectedByUser = 0 then 0 else (cast(TotalNumberOfItemsReferredByUser as money) / cast(TotalNumberOfItemsInspectedByUser as money)) * 100 end,
		TotalNumberOfItemsConfirmedByUser, TotalNumberOfHoursDeclaredByUser,
		AverageNumberOfItemsPerHourByUser = case when TotalNumberOfHoursDeclaredByUser = 0 then 0 else (cast(TotalNumberOfItemsInspectedByUser as money) / cast(TotalNumberOfHoursDeclaredByUser as money)) * 100 end,
		
		TotalNumberOfItemsInspected, TotalNumberOfItemsCorrected, 
		PercentageOfItemsCorrected = case when TotalNumberOfItemsInspected = 0 then 0 else (cast(TotalNumberOfItemsCorrected as money) / cast(TotalNumberOfItemsInspected as money)) * 100 end,
		TotalNumberOfItemsVerified, TotalNumberOfItemsReferred,
		PercentageOfItemsReferred = case when TotalNumberOfItemsInspected = 0 then 0 else (cast(TotalNumberOfItemsReferred as money) / cast(TotalNumberOfItemsInspected as money)) * 100 end,
		TotalNumberOfItemsConfirmed, TotalNumberOfHoursDeclared,
		AverageNumberOfItemsPerHour = case when TotalNumberOfHoursDeclared = 0 then 0 else (cast(TotalNumberOfItemsInspected as money) / cast(TotalNumberOfHoursDeclared as money)) * 100 end
		
	from individualUsersActivityByMonth
	order by FrDate, LoginName;

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[PotentialAnomaliesReport_AdditionalFees]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PotentialAnomaliesReport_AdditionalFees]
	@batchID bigint = null,
	@financialPeriodID integer = null

as

	set nocount on;
	set transaction isolation level read uncommitted;

	declare @batches table (BatchID bigint not null primary key);
	insert into @batches
	select b.BatchID
	from dbo.Batch b
	where (@financialPeriodID is null or b.FinancialPeriodID = @financialPeriodID)
		and (@batchID is null or b.BatchID = @batchID)
	option (optimize for (@financialPeriodID unknown, @batchID unknown));

	declare @basicPrices table (
		FinancialPeriodID integer, Contractor varchar(7), JurisdictionID integer,
		IsRegisteredForStomaCustomisation bit,
		DocumentID bigint, Form varchar(6),
		Item integer,
		NumberOfProfessionalFees integer,
		ValueOfBasicPriceOnPx money, ValueOfPrismBasicPrice money, ValueOfAdditionalFeesOnPx money, 
		ValueOfPrismControlledSubstanceFee money, ValueOfPrismEndorsementFees money, ValueOfPrismHomeDeliveryFee money, ValueOfPrismExpensiveItemFee money,
		TotalValueOfPrismCalculatedAdditionalFees money,
		Product varchar(60),
		Pack varchar(60),
		EndorsementCodes varchar(MAX),
		Code varchar(60)
	)
	
	insert into @basicPrices
	select b.FinancialPeriodID, c.Code as Contractor, c.JurisdictionID,
		IsRegisteredForStomaCustomisation = case
			when c.IsRegisteredForStomaCustomisation = 1 and c.RegisteredForStomaCustomisationSince <= b.FinancialPeriodID then 1
			else 0 end,
		d.DocumentID, Form = case
			when d.FragmentTypeCode is null then right('000000' + convert(varchar(6), d.DocumentNumber), 6)
			else d.FragmentTypeCode + right('00000' + convert(varchar(6), d.DocumentNumber), 5) 
			end, 
		Item = i.Sequence,
		NumberOfProfessionalFees = sum(iv.NumberOfProfessionalFees),
		ValueOfBasicPriceOnPx = sum(iv.BasicPrice),
		ValueOfPrismBasicPrice = round(sum(cast(
			case 
				when ep.EndorsementCode is not null or ppp.PackPrice = 0.00 then iv.BasicPrice
				else round(case
					when ppp.SpecialContainerIndicatorCode = 'S' then case
						when pp.ContainerSize = 1 and pp.PackSize > 1 and ppp.PackPrice != ppp.UnitPrice then ((ppp.PackPrice * 100) / pp.PackSize) * iv.Quantity
						when pp.ContainerSize != 0 then (ppp.PackPrice * 100) * (iv.Quantity / pp.ContainerSize)
						end
					when ppp.SpecialContainerIndicatorCode = 'B' and pp.ContainerSize != 0 then ((ppp.PackPrice * 100) * iv.Quantity) / pp.ContainerSize
					when pp.PackSize != 0 then ((ppp.PackPrice * 100) * iv.Quantity) / pp.PackSize
					else 0.00
					end, 0) / 100
				end as money)), 2),
		ValueOfAdditionalFeesOnPx = sum(iv.ValueOfAdditionalFees),
		ValueOfPrismControlledSubstanceFee = 0.00,
		ValueOfPrismEndorsementFees = 0.00,
		ValueOfPrismHomeDeliveryFee = 0.00,
		ValueOfPrismExpensiveItemFee = 0.00,
		TotalValueOfPrismCalculatedAdditionalFees = 0.00,
		p.[Description],
		MAX(pp.[Description]),
		NULL,
		p.Code
	from @batches sb
		inner join dbo.Batch b 
			inner join dbo.Contractor c on b.ContractorID = c.ContractorID
			inner join dbo.Document d
				inner join dbo.Item i
					inner join dbo.ItemVersion iv
						inner join dbo.ProductPackPeriodic ppp 
							inner join dbo.ProductPack pp on ppp.ProductPackID = pp.ProductPackID
							inner join dbo.Product p on p.ProductID = pp.ProductID
						on iv.ProductPackPeriodicID = ppp.ProductPackPeriodicID
						left join dbo.ItemVersionEndorsement ive 
							inner join dbo.EndorsementPeriodic ep on ive.EndorsementPeriodicID = ep.EndorsementPeriodicID and ep.EndorsementCode IN ('IP')
						on iv.ItemVersionID = ive.ItemVersionID
					on i.OriginalItemVersionID = iv.ItemVersionID
				on d.DocumentID = i.DocumentID
			on b.BatchID = d.batchID
		on sb.BatchID = b.BatchID
			inner join dbo.JurisdictionPeriodic jp on c.JurisdictionID = jp.JurisdictionID and b.FinancialPeriodID = jp.FinancialPeriodID
	group by b.FinancialPeriodID, c.Code, c.JurisdictionID, c.IsRegisteredForStomaCustomisation, c.RegisteredForStomaCustomisationSince,
		d.DocumentID, d.FragmentTypeCode, d.DocumentNumber, i.Sequence, p.[description], p.Code, ive.ItemVersionID;
	
	update bp
	set 
		ValueOfPrismControlledSubstanceFee = (
			select isnull(max(controlledSubstanceFee), 0.00)
			from (
				select controlledSubstanceFee = isnull(bp.NumberOfProfessionalFees * isnull(csip.AdditionalProfessionalFeeValue, 0), 0.00)
				from dbo.Item i
					inner join dbo.ItemVersion iv
						inner join dbo.ProductPackPeriodic ppp 
							inner join dbo.ProductPeriodic pp 
								inner join dbo.ControlledSubstanceIndicatorPeriodic csip on pp.ControlledSubstanceIndicatorCode = csip.ControlledSubstanceIndicatorCode
									and csip.FinancialPeriodID = @financialPeriodID and csip.JurisdictionID = bp.JurisdictionID
								on ppp.ProductPeriodicID = pp.ProductPeriodicID
							on iv.ProductPackPeriodicID = ppp.ProductPackPeriodicID
						on i.OriginalItemVersionID = iv.ItemVersionID
				where i.DocumentID = bp.DocumentID
					and i.Sequence = bp.Item
				) controlledSubstanceFees),			
		ValueOfPrismEndorsementFees = (
			select isnull(sum(endorsementFee), 0.00)
			from (
				select endorsementFee = isnull(case ep.SpecialFeeBasis
					when '1' then ep.SpecialFeeValue
					when 'S' then bp.NumberOfProfessionalFees * ep.SpecialFeeValue
					when 'Q' then ive.Quantity * ep.SpecialFeeValue
					else 0 end, 0)
				from dbo.Item i
					inner join dbo.ItemVersion iv
						inner join dbo.ItemVersionEndorsement ive
							inner join dbo.EndorsementPeriodic ep on ive.EndorsementPeriodicID = ep.EndorsementPeriodicID
						on iv.ItemVersionID = ive.ItemVersionID
					on i.OriginalItemVersionID = iv.ItemVersionID
				where i.DocumentID = bp.DocumentID
					and i.Sequence = bp.Item
					and ep.EndorsementCode not in ('H1', 'H2')
			) endorsementFees) + (
			select isnull(count(*) * jp.StomaCustomisationFee, 0.00)
			from (
				select iv.ItemVersionID
				from dbo.Item i
					inner join dbo.ItemVersion iv
						inner join dbo.ProductPackPeriodic ppp
							inner join dbo.ProductPeriodic pp
								inner join dbo.Product p on pp.ProductID = p.ProductID and p.IsEligibleForStomaCustomisationFee = 1
							on ppp.ProductPeriodicID = pp.ProductPeriodicID
							inner join dbo.DrugTariffCategory dtc on ppp.DrugTariffCategoryCode = dtc.DrugTariffCategoryCode and dtc.Category = 9
						on iv.ProductPackPeriodicID = ppp.ProductPackPeriodicID
					on i.OriginalItemVersionID = iv.ItemVersionID
				where bp.IsRegisteredForStomaCustomisation = 1
				  and i.DocumentID = bp.DocumentID
					and i.Sequence = bp.Item
			) stomafees) + (
			select isnull(max(supplementaryControlledDrugFee), 0.00)
			from (
				select supplementaryControlledDrugFee = isnull(jp.ValueOfSupplementaryControlledDrugFee, 0.00)
				from dbo.Item i
					inner join dbo.ItemVersion iv
						inner join dbo.ProductPackPeriodic ppp
							inner join dbo.ProductPeriodic pp on ppp.ProductPeriodicID = pp.ProductPeriodicID
						on iv.ProductPackPeriodicID = ppp.ProductPackPeriodicID
					on i.OriginalItemVersionID = iv.ItemVersionID
				where i.DocumentID = bp.DocumentID
					and i.Sequence = bp.Item
					and pp.IsEligibleForSupplementaryControlledDrugFee = 1 
			) supplementaryControlledDrugFees),
		ValueOfPrismHomeDeliveryFee = (
			select isnull(max(homeDeliveryFee), 0.00)
			from (
				select homeDeliveryFee = isnull(ep.SpecialFeeValue, 0.00)
				from dbo.Item i
					inner join dbo.ItemVersion iv
					inner join dbo.ProductPackPeriodic ppp 
						inner join dbo.HomeDeliveryIndicator hdi
							inner join dbo.EndorsementPeriodic ep on 'H' + hdi.HomeDeliveryIndicatorCode = ep.EndorsementCode
								and ep.FinancialPeriodID = @financialPeriodID and ep.JurisdictionID = bp.JurisdictionID
						on ppp.HomeDeliveryIndicatorCode = hdi.HomeDeliveryIndicatorCode
	 					inner join dbo.DrugTariffCategory dtc on ppp.DrugTariffCategoryCode = dtc.DrugTariffCategoryCode and dtc.Category = 9
					on iv.ProductPackPeriodicID = ppp.ProductPackPeriodicID
					on i.OriginalItemVersionID = iv.ItemVersionID
				where i.DocumentID = bp.DocumentID
					and i.Sequence = bp.Item
			) homeDeliveryFees),
		ValueOfPrismExpensiveItemFee = round(cast(case 
			when bp.ValueOfPrismBasicPrice >= jp.ExpensiveItemThreshold then bp.ValueOfPrismBasicPrice * (jp.ExpensiveItemFeePercentage / 100) 
			else 0.00 
			end as money), 2)
	from @basicPrices bp
		inner join dbo.JurisdictionPeriodic jp on bp.JurisdictionID = jp.JurisdictionID and jp.FinancialPeriodID = bp.FinancialPeriodID		
	option (optimize for (@financialPeriodID unknown));
	
	update bp
	set bp.EndorsementCodes = STUFF(
            (SELECT CAST(', ' + EndorsementCode AS VARCHAR(MAX)) 
             FROM EndorsementPeriodic as A				
			 WHERE a.EndorsementPeriodicID IN (SELECT EndorsementPeriodicID FROM ItemVersionEndorsement ive2 WHERE ive2.ItemVersionID = ive.ItemVersionID and EndorsementCode IN ('NB', 'BB', 'DB', 'IP'))
			 FOR XML PATH ('')), 1, 2, '')
	FROM @basicPrices as bp
		inner join dbo.Item i
			inner join dbo.ItemVersion iv
				left join dbo.ItemVersionEndorsement ive 
					inner join dbo.EndorsementPeriodic ep on ive.EndorsementPeriodicID = ep.EndorsementPeriodicID and ep.EndorsementCode IN ('NB', 'BB', 'DB', 'IP')
				on iv.ItemVersionID = ive.ItemVersionID
			on i.OriginalItemVersionID = iv.ItemVersionID
		on bp.DocumentID = i.DocumentID and bp.item = i.[Sequence]

	update @basicPrices
	set TotalValueOfPrismCalculatedAdditionalFees = round(ValueOfPrismControlledSubstanceFee + ValueOfPrismHomeDeliveryFee + ValueOfPrismEndorsementFees + ValueOfPrismExpensiveItemFee, 2);

	select * from @basicPrices
		where ValueOfAdditionalFeesOnPx != TotalValueOfPrismCalculatedAdditionalFees
	   or ValueOfBasicPriceOnPx != ValueOfPrismBasicPrice
	order by 2, 6, 7;

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[PotentialAnomaliesReport_AdditionalFees_Live]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PotentialAnomaliesReport_AdditionalFees_Live]
	@batchID bigint = null,
	@financialPeriodID integer = null

as

	set nocount on;
	set transaction isolation level read uncommitted;

	declare @batches table (BatchID bigint not null primary key);
	insert into @batches
	select b.BatchID
	from dbo.Batch b
	where (@financialPeriodID is null or b.FinancialPeriodID = @financialPeriodID)
		and (@batchID is null or b.BatchID = @batchID)
	option (optimize for (@financialPeriodID unknown, @batchID unknown));

	declare @basicPrices table (
		FinancialPeriodID integer, Contractor varchar(7), JurisdictionID integer,
		IsRegisteredForStomaCustomisation bit,
		DocumentID bigint, Form varchar(6),
		Item integer,
		NumberOfProfessionalFees integer,
		ValueOfBasicPriceOnPx money, ValueOfPrismBasicPrice money, ValueOfAdditionalFeesOnPx money, 
		ValueOfPrismControlledSubstanceFee money, ValueOfPrismEndorsementFees money, ValueOfPrismHomeDeliveryFee money, ValueOfPrismExpensiveItemFee money,
		TotalValueOfPrismCalculatedAdditionalFees money
	)
	
	insert into @basicPrices
	select b.FinancialPeriodID, c.Code as Contractor, c.JurisdictionID,
		IsRegisteredForStomaCustomisation = case
			when c.IsRegisteredForStomaCustomisation = 1 and c.RegisteredForStomaCustomisationSince <= b.FinancialPeriodID then 1
			else 0 end,
		d.DocumentID, Form = case
			when d.FragmentTypeCode is null then right('000000' + convert(varchar(6), d.DocumentNumber), 6)
			else d.FragmentTypeCode + right('00000' + convert(varchar(6), d.DocumentNumber), 5) 
			end, 
		Item = i.Sequence,
		NumberOfProfessionalFees = sum(iv.NumberOfProfessionalFees),
		ValueOfBasicPriceOnPx = sum(iv.BasicPrice),
		ValueOfPrismBasicPrice = round(sum(cast(
			case 
				when ep.EndorsementCode is not null or ppp.PackPrice = 0.00 then iv.BasicPrice
				else round(case
					when ppp.SpecialContainerIndicatorCode = 'S' then case
						when pp.ContainerSize = 1 and pp.PackSize > 1 and ppp.PackPrice != ppp.UnitPrice then ((ppp.PackPrice * 100) / pp.PackSize) * iv.Quantity
						when pp.ContainerSize != 0 then (ppp.PackPrice * 100) * (iv.Quantity / pp.ContainerSize)
						end
					when ppp.SpecialContainerIndicatorCode = 'B' and pp.ContainerSize != 0 then ((ppp.PackPrice * 100) * iv.Quantity) / pp.ContainerSize
					when pp.PackSize != 0 then ((ppp.PackPrice * 100) * iv.Quantity) / pp.PackSize
					else 0.00
					end, 0) / 100
				end as money)), 2),
		ValueOfAdditionalFeesOnPx = sum(iv.ValueOfAdditionalFees),
		ValueOfPrismControlledSubstanceFee = 0.00,
		ValueOfPrismEndorsementFees = 0.00,
		ValueOfPrismHomeDeliveryFee = 0.00,
		ValueOfPrismExpensiveItemFee = 0.00,
		TotalValueOfPrismCalculatedAdditionalFees = 0.00
	from @batches sb
		inner join dbo.Batch b 
			inner join dbo.Contractor c on b.ContractorID = c.ContractorID
			inner join dbo.Document d
				inner join dbo.Item i
					inner join dbo.ItemVersion iv
						inner join dbo.ProductPackPeriodic ppp 
							inner join dbo.ProductPack pp on ppp.ProductPackID = pp.ProductPackID
						on iv.ProductPackPeriodicID = ppp.ProductPackPeriodicID
						left join dbo.ItemVersionEndorsement ive 
							inner join dbo.EndorsementPeriodic ep on ive.EndorsementPeriodicID = ep.EndorsementPeriodicID and ep.EndorsementCode = 'IP'
						on iv.ItemVersionID = ive.ItemVersionID
					on i.OriginalItemVersionID = iv.ItemVersionID
				on d.DocumentID = i.DocumentID
			on b.BatchID = d.batchID
			inner join dbo.JurisdictionPeriodic jp on c.JurisdictionID = jp.JurisdictionID and b.FinancialPeriodID = jp.FinancialPeriodID
		on sb.BatchID = b.BatchID
	group by b.FinancialPeriodID, c.Code, c.JurisdictionID, c.IsRegisteredForStomaCustomisation, c.RegisteredForStomaCustomisationSince,
		d.DocumentID, d.FragmentTypeCode, d.DocumentNumber, i.Sequence;
	
	update bp
	set 
		ValueOfPrismControlledSubstanceFee = (
			select isnull(max(controlledSubstanceFee), 0.00)
			from (
				select controlledSubstanceFee = isnull(bp.NumberOfProfessionalFees * isnull(csip.AdditionalProfessionalFeeValue, 0), 0.00)
				from dbo.Item i
					inner join dbo.ItemVersion iv
						inner join dbo.ProductPackPeriodic ppp 
							inner join dbo.ProductPeriodic pp 
								inner join dbo.ControlledSubstanceIndicatorPeriodic csip on pp.ControlledSubstanceIndicatorCode = csip.ControlledSubstanceIndicatorCode
									and csip.FinancialPeriodID = @financialPeriodID and csip.JurisdictionID = bp.JurisdictionID
								on ppp.ProductPeriodicID = pp.ProductPeriodicID
							on iv.ProductPackPeriodicID = ppp.ProductPackPeriodicID
						on i.OriginalItemVersionID = iv.ItemVersionID
				where i.DocumentID = bp.DocumentID
					and i.Sequence = bp.Item
				) controlledSubstanceFees),			
		ValueOfPrismEndorsementFees = (
			select isnull(sum(endorsementFee), 0.00)
			from (
				select endorsementFee = isnull(case ep.SpecialFeeBasis
					when '1' then ep.SpecialFeeValue
					when 'S' then bp.NumberOfProfessionalFees * ep.SpecialFeeValue
					when 'Q' then ive.Quantity * ep.SpecialFeeValue
					else 0 end, 0)
				from dbo.Item i
					inner join dbo.ItemVersion iv
						inner join dbo.ItemVersionEndorsement ive
							inner join dbo.EndorsementPeriodic ep on ive.EndorsementPeriodicID = ep.EndorsementPeriodicID
						on iv.ItemVersionID = ive.ItemVersionID
					on i.OriginalItemVersionID = iv.ItemVersionID
				where i.DocumentID = bp.DocumentID
					and i.Sequence = bp.Item
					and ep.EndorsementCode not in ('H1', 'H2')
			) endorsementFees) + (
			select isnull(count(*) * jp.StomaCustomisationFee, 0.00)
			from (
				select iv.ItemVersionID
				from dbo.Item i
					inner join dbo.ItemVersion iv
						inner join dbo.ProductPackPeriodic ppp
							inner join dbo.ProductPeriodic pp
								inner join dbo.Product p on pp.ProductID = p.ProductID and p.IsEligibleForStomaCustomisationFee = 1
							on ppp.ProductPeriodicID = pp.ProductPeriodicID
							inner join dbo.DrugTariffCategory dtc on ppp.DrugTariffCategoryCode = dtc.DrugTariffCategoryCode and dtc.Category = 9
						on iv.ProductPackPeriodicID = ppp.ProductPackPeriodicID
					on i.OriginalItemVersionID = iv.ItemVersionID
				where bp.IsRegisteredForStomaCustomisation = 1
				  and i.DocumentID = bp.DocumentID
					and i.Sequence = bp.Item
			) stomafees) + (
			select isnull(max(supplementaryControlledDrugFee), 0.00)
			from (
				select supplementaryControlledDrugFee = isnull(jp.ValueOfSupplementaryControlledDrugFee, 0.00)
				from dbo.Item i
					inner join dbo.ItemVersion iv
						inner join dbo.ProductPackPeriodic ppp
							inner join dbo.ProductPeriodic pp on ppp.ProductPeriodicID = pp.ProductPeriodicID
						on iv.ProductPackPeriodicID = ppp.ProductPackPeriodicID
					on i.OriginalItemVersionID = iv.ItemVersionID
				where i.DocumentID = bp.DocumentID
					and i.Sequence = bp.Item
					and pp.IsEligibleForSupplementaryControlledDrugFee = 1 
			) supplementaryControlledDrugFees),
		ValueOfPrismHomeDeliveryFee = (
			select isnull(max(homeDeliveryFee), 0.00)
			from (
				select homeDeliveryFee = isnull(ep.SpecialFeeValue, 0.00)
				from dbo.Item i
					inner join dbo.ItemVersion iv
					inner join dbo.ProductPackPeriodic ppp 
						inner join dbo.HomeDeliveryIndicator hdi
							inner join dbo.EndorsementPeriodic ep on 'H' + hdi.HomeDeliveryIndicatorCode = ep.EndorsementCode
								and ep.FinancialPeriodID = @financialPeriodID and ep.JurisdictionID = bp.JurisdictionID
						on ppp.HomeDeliveryIndicatorCode = hdi.HomeDeliveryIndicatorCode
	 					inner join dbo.DrugTariffCategory dtc on ppp.DrugTariffCategoryCode = dtc.DrugTariffCategoryCode and dtc.Category = 9
					on iv.ProductPackPeriodicID = ppp.ProductPackPeriodicID
					on i.OriginalItemVersionID = iv.ItemVersionID
				where i.DocumentID = bp.DocumentID
					and i.Sequence = bp.Item
			) homeDeliveryFees),
		ValueOfPrismExpensiveItemFee = round(cast(case 
			when bp.ValueOfPrismBasicPrice >= jp.ExpensiveItemThreshold then bp.ValueOfPrismBasicPrice * (jp.ExpensiveItemFeePercentage / 100) 
			else 0.00 
			end as money), 2)
	from @basicPrices bp
		inner join dbo.JurisdictionPeriodic jp on bp.JurisdictionID = jp.JurisdictionID and jp.FinancialPeriodID = bp.FinancialPeriodID		
	option (optimize for (@financialPeriodID unknown));
	
	update @basicPrices
	set TotalValueOfPrismCalculatedAdditionalFees = round(ValueOfPrismControlledSubstanceFee + ValueOfPrismHomeDeliveryFee + ValueOfPrismEndorsementFees + ValueOfPrismExpensiveItemFee, 2);

	select * from @basicPrices
		where ValueOfAdditionalFeesOnPx != TotalValueOfPrismCalculatedAdditionalFees
	   or ValueOfBasicPriceOnPx != ValueOfPrismBasicPrice
	order by 2, 6, 7;

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[PotentialAnomaliesReport_OutOfPocketExpenses]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PotentialAnomaliesReport_OutOfPocketExpenses]
	@batchID bigint = null,
	@financialPeriodID integer = null

as

	set nocount on;
	set transaction isolation level read uncommitted;

	declare @batches table (BatchID bigint not null primary key);
	insert into @batches
	select b.BatchID
	from dbo.Batch b
	where (@financialPeriodID is null or b.FinancialPeriodID = @financialPeriodID)
		and (@batchID is null or b.BatchID = @batchID)
	option (optimize for (@financialPeriodID unknown, @batchID unknown));

	with outOfPocketExpenseClaims as (

		select 
			Contractor = c.Code, 
			Form = case
				when d.FragmentTypeCode is null then right('000000' + convert(varchar(6), d.DocumentNumber), 6)
				else d.FragmentTypeCode + right('00000' + convert(varchar(6), d.DocumentNumber), 5) 
				end, 
			Item = i.Sequence, 
			Element = i.SubSequence,
			pack.Code, 
			Product = p.Description, 
			Pack = pack.Description,
			IsEligibleForOutOfPocketExpenses = cast(case
				when p.Description like 'Oxygen%' and pp.PreperationClassIndicatorCode = '4' then 1
				else case
					when ppp.DrugTariffCategoryCode in ('08A', '08M') then 0 -- Part VIIIA/Categories 'A' or 'M'
					when ppp.DrugTariffCategoryCode in ('09A', '09R') then 0 -- Part IXA or IXR
					when pp.IsUnlicensedMedicine = 1 then 0 -- Part VIIIB
					else 1 end 
				end as bit),
			iv.ValueOfOutOfPocketExpenses
		from @batches sb
			inner join dbo.Batch b
				inner join dbo.Contractor c on b.ContractorID = c.ContractorID
				inner join dbo.Document d
					inner join Item i
						inner join ItemVersion iv
							inner join dbo.ProductPackPeriodic ppp 
								inner join dbo.ProductPeriodic pp 
									inner join dbo.Product p on pp.ProductID = p.ProductID
								on ppp.ProductPeriodicID = pp.ProductPeriodicID
								inner join dbo.ProductPack pack on ppp.ProductPackID = pack.ProductPackID
							on iv.ProductPackPeriodicID = ppp.ProductPackPeriodicID
						on i.OriginalItemVersionID = iv.ItemVersionID
					on d.DocumentID = i.DocumentID
				on b.BatchID = d.batchID
			on sb.BatchID = b.BatchID
		where iv.ValueOfOutOfPocketExpenses != 0.00

	)
	
	select *
	from outOfPocketExpenseClaims
		where IsEligibleForOutOfPocketExpenses = 0
	order by 1, 2, 3, 4;

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[PotentialAnomaliesReport_PaymentForContainers_NonPartVIIIA]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PotentialAnomaliesReport_PaymentForContainers_NonPartVIIIA]
	@batchID bigint = null,
	@financialPeriodID integer = null

as

	set nocount on;
	set transaction isolation level read uncommitted;

	declare @batches table (BatchID bigint not null primary key);
	insert into @batches
	select b.BatchID
	from dbo.Batch b
	where (@financialPeriodID is null or b.FinancialPeriodID = @financialPeriodID)
		and (@batchID is null or b.BatchID = @batchID)
		and (201210 <= b.BatchID)
	option (optimize for (@financialPeriodID unknown, @batchID unknown));

	with dispensedNonPartVIIIA as (

		select i.DocumentID, i.Sequence, iv.Quantity,
		ComparisonSize = case
			when ppp.SpecialContainerIndicatorCode = 'B' and pack.PackSize > 1 and pack.ContainerSize > 1 then pack.ContainerSize
			when ppp.SpecialContainerIndicatorCode = 'S' and pack.PackSize > 1 and pack.ContainerSize = 1 then pack.PackSize
			when ppp.SpecialContainerIndicatorCode = 'S' and pack.ContainerSize != 0 then pack.ContainerSize
			else pack.PackSize end,
		Endorsements = (
				select count(*)
				from dbo.Item i2
					inner join dbo.ItemVersion iv2
						inner join dbo.ItemVersionEndorsement ive2
							inner join dbo.EndorsementPeriodic ep2 on ive2.EndorsementPeriodicID = ep2.EndorsementPeriodicID and ep2.EndorsementCode = 'PK'
						on iv2.ItemVersionID = ive2.ItemVersionID
					on i2.OriginalItemVersionID = iv2.ItemVersionID
				where i.DocumentID = i2.DocumentID and i.Sequence = i2.Sequence
			)
		from @batches sb
			inner join dbo.Batch b
				inner join dbo.Document d
					inner join dbo.Item i
						inner join dbo.ItemVersion iv
							inner join dbo.ProductPackPeriodic ppp 
								left join dbo.DrugTariffCategory dtc on ppp.DrugTariffCategoryCode = dtc.DrugTariffCategoryCode 
								inner join dbo.ProductPeriodic pp 
								on ppp.ProductPeriodicID = pp.ProductPeriodicID
								inner join dbo.ProductPack pack on ppp.ProductPackID = pack.ProductPackID
							on iv.ProductPackPeriodicID = ppp.ProductPackPeriodicID
						on i.OriginalItemVersionID = iv.ItemVersionID
					on d.DocumentID = i.DocumentID
				on b.BatchID = d.BatchID
			on sb.BatchID = b.BatchID

		where isnull(dtc.Category, 0) != 8

	), anomaliesNonPartVIIIA as (

		select *
		from dispensedNonPartVIIIA
		where ComparisonSize != 0
			and (((Quantity % ComparisonSize) = 0 and Endorsements > 0) 
			 or ((Quantity % ComparisonSize) != 0 and Endorsements < 1))

	)

	select Contractor = c.Code, 
		Form = case
			when d.FragmentTypeCode is null then right('000000' + convert(varchar(6), d.DocumentNumber), 6)
			else d.FragmentTypeCode + right('00000' + convert(varchar(6), d.DocumentNumber), 5) 
			end, Item = i.Sequence, Element = i.SubSequence, NumberOfElements = (select count(*) from dbo.Item i2 where i.DocumentID = i2.DocumentID and i.Sequence = i2.Sequence),
		pack.PpCode,
		iv.Quantity,	
		Endorsements = case when ep.EndorsementCode is not null then 1 else 0 end,
		pack.Code, Product = p.Description, Pack = pack.Description, pack.PackSize, pack.ContainerSize, ppp.SpecialContainerIndicatorCode, 
		ComparisonSize = case
			when ppp.SpecialContainerIndicatorCode = 'B' and pack.PackSize > 1 and pack.ContainerSize > 1 then pack.ContainerSize
			when ppp.SpecialContainerIndicatorCode = 'S' and pack.PackSize > 1 and pack.ContainerSize = 1 then pack.PackSize
			when ppp.SpecialContainerIndicatorCode = 'S' and pack.ContainerSize != 0 then pack.ContainerSize
			else pack.PackSize end
	
	from anomaliesNonPartVIIIA
		inner join dbo.Item i
			inner join dbo.ItemVersion iv
				inner join dbo.ProductPackPeriodic ppp 
					left join dbo.DrugTariffCategory dtc on ppp.DrugTariffCategoryCode = dtc.DrugTariffCategoryCode 
					inner join dbo.ProductPeriodic pp 
						inner join dbo.Product p on pp.ProductID = p.ProductID
					on ppp.ProductPeriodicID = pp.ProductPeriodicID
					inner join dbo.ProductPack pack on ppp.ProductPackID = pack.ProductPackID
				on iv.ProductPackPeriodicID = ppp.ProductPackPeriodicID
				left join dbo.ItemVersionEndorsement ive
					inner join dbo.EndorsementPeriodic ep on ive.EndorsementPeriodicID = ep.EndorsementPeriodicID and ep.EndorsementCode = 'PK'
				on iv.ItemVersionID = ive.ItemVersionID
			on i.OriginalItemVersionID = iv.ItemVersionID
			inner join dbo.Document d
				inner join dbo.Batch b
					inner join dbo.Contractor c on b.ContractorID = c.ContractorID
				on d.BatchID = b.BatchID
			on i.DocumentID = d.DocumentID
		on anomaliesNonPartVIIIA.DocumentID = i.DocumentID and anomaliesNonPartVIIIA.Sequence = i.Sequence

	order by 1, 2, 3, 4, 5, 6;

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[PotentialAnomaliesReport_PaymentForContainers_PartVIIIA]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PotentialAnomaliesReport_PaymentForContainers_PartVIIIA]
	@batchID bigint = null,
	@financialPeriodID integer = null

as

	set nocount on;
	set transaction isolation level read uncommitted;

	/*
	 * Create a table containing the available PartVIIIA product pack sizes for the period.
	 *
	 */
	 
	declare @availablePartVIIIA table (
		ProductPackID bigint primary key,
		PpCode integer,
		ComparisonSize dbo.Size);

	-- Pack size selection rules provided by MH@BSA by email on 20/02/2013...
	--   NOTE: Rule #2 (ppp.SpecialContainerIndicatorCode = 'S' and pack.PackSize > 1 and pack.ContainerSize > 1 then pack.PackSize) has been modified
	--   by me in order to reduce the number of anomalies detected. I am waiting for confirmation from PSNC/BSA as to whether this modification is valid.
	insert into @availablePartVIIIA
	select pack.ProductPackID, pack.PpCode, 
		ComparisonSize = case
			when ppp.SpecialContainerIndicatorCode = 'B' and pack.PackSize > 1 and pack.ContainerSize > 1 then pack.ContainerSize
			when ppp.SpecialContainerIndicatorCode = 'S' and pack.PackSize > 1 and pack.ContainerSize = 1 then pack.PackSize
			when ppp.SpecialContainerIndicatorCode = 'S' and pack.ContainerSize != 0 then pack.ContainerSize
			else pack.PackSize end
	from dbo.ProductPack pack
		inner join dbo.ProductPackPeriodic ppp 
			inner join dbo.ProductPeriodic pp on ppp.ProductPeriodicID = pp.ProductPeriodicID and pp.FinancialPeriodID = @financialPeriodID
			inner join dbo.DrugTariffCategory dtc on ppp.DrugTariffCategoryCode = dtc.DrugTariffCategoryCode and dtc.Category = 8
		on pack.ProductPackID = ppp.ProductPackID and ppp.IsEnabled = 1
	where 201210 <= @financialPeriodID
	  and pack.PpCode is not null;
	
	-- Remove anything for which we could not determine a pack size
	delete from @availablePartVIIIA
	where ComparisonSize = 0;

	/*
	 * Now try and identify any form items where the rules implemented by PRISM result in a difference in eligibility
	 * when compared with the presence of "PK" endorsements on the PX provided by the BSA.
	 *
	 */

	with dispensedPartVIIIA as (

		-- Roll up any multi-pack items by PpCode. According to MH, PK is determined on the total quantity of each product dispensed/paid
		-- vs the pack sizes available for that period.

		select d.DocumentID, 
			FormNumber = case
				when d.FragmentTypeCode is null then right('000000' + convert(varchar(6), d.DocumentNumber), 6)
				else d.FragmentTypeCode + right('00000' + convert(varchar(6), d.DocumentNumber), 5) 
				end,
			i.Sequence, pack.PpCode, Category = max(dtc.Category), Quantity = sum(iv.Quantity),
			Endorsements = count(ep.EndorsementPeriodicID)
		from dbo.Batch b
			inner join dbo.Document d
				inner join dbo.Item i
					inner join dbo.ItemVersion iv
						inner join dbo.ProductPackPeriodic ppp
							inner join dbo.ProductPack pack on ppp.ProductPackID = pack.ProductPackID
								inner join dbo.DrugTariffCategory dtc on ppp.DrugTariffCategoryCode = dtc.DrugTariffCategoryCode and dtc.Category = 8
						on iv.ProductPackPeriodicID = ppp.ProductPackPeriodicID
						left join dbo.ItemVersionEndorsement ive
							inner join dbo.EndorsementPeriodic ep on ive.EndorsementPeriodicID = ep.EndorsementPeriodicID and ep.EndorsementCode = 'PK'
						on iv.ItemVersionID = ive.ItemVersionID
					on i.OriginalItemVersionID = iv.ItemVersionID
				on d.DocumentID = i.DocumentID
			on b.BatchID = d.BatchID

		where 201210 <= @financialPeriodID
		  and b.FinancialPeriodID = @financialPeriodID
			and (@batchID is null or b.BatchID = @batchID)
			and (@financialPeriodID is null or b.FinancialPeriodID = @financialPeriodID)

		group by d.DocumentID, d.FragmentTypeCode, d.DocumentNumber, i.Sequence, pack.PpCode

	), availablePartVIIIA as (

		-- For Tariff Part VIIIA products, we look for any pack size the multiple of which would satisfy the
		-- quantity dispensed/paid.

		select dp.*,
			AvailableProductPackID = (
				select top 1 ap.ProductPackID
				from @availablePartVIIIA ap 
				where ap.PpCode = dp.PpCode
				  and (dp.Quantity % ap.ComparisonSize) = 0
				order by ap.ComparisonSize desc
			)
		from dispensedPartVIIIA dp

	), anomalies as (

		-- Part VIIIA anomalies are products where either...
		--   The total quantity dispensed/paid CAN be satisfied as a multiple of a single pack size, but BSA have endorsed "PK"
		--   or the total quantity dispensed/paid CANNOT be satisfied as a multiple of a single pack size but BSA have NOT paid a "PK"

		select Contractor = c.Code, Form = a.FormNumber, Item = a.Sequence, 
			a.PpCode,
			a.Quantity, a.Endorsements,
			apack.Code, Product = ap.Description, Pack = apack.Description, apack.PackSize, apack.ContainerSize, appp.SpecialContainerIndicatorCode, 
			ComparisonSize = case
				when appp.SpecialContainerIndicatorCode = 'B' and apack.PackSize > 1 and apack.ContainerSize > 1 then apack.ContainerSize
				when appp.SpecialContainerIndicatorCode = 'S' and apack.PackSize > 1 and apack.ContainerSize = 1 then apack.PackSize
				when appp.SpecialContainerIndicatorCode = 'S' and apack.ContainerSize != 0 then apack.ContainerSize
				else apack.PackSize end
		from availablePartVIIIA a
			inner join dbo.Document d
				inner join dbo.Batch b
					inner join dbo.Contractor c on b.ContractorID = c.ContractorID
				on d.BatchID = b.BatchID
			on a.DocumentID = d.DocumentID
			left join dbo.ProductPack apack
				inner join dbo.ProductPackPeriodic appp
					inner join dbo.ProductPeriodic app
						inner join dbo.Product ap on app.ProductID = ap.ProductID
					on appp.ProductPeriodicID = app.ProductPeriodicID and app.FinancialPeriodID = @financialPeriodID
				on apack.ProductPackID = appp.ProductPackID
			on a.AvailableProductPackID = apack.ProductPackID
		where ((Endorsements < 1 and AvailableProductPackID is null)
			 or  (Endorsements > 0 and AvailableProductPackID is not null))
	
	)

	select *
	from anomalies a
	order by 1, 2, 3, 4, 5;

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[PotentialAnomaliesReport_ZeroDiscount]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PotentialAnomaliesReport_ZeroDiscount]
	@batchID bigint = null,
	@financialPeriodID integer = null

as

	set nocount on;
	set transaction isolation level read uncommitted;

	declare @batches table (BatchID bigint not null primary key);
	insert into @batches
	select b.BatchID
	from dbo.Batch b
	where (@financialPeriodID is null or b.FinancialPeriodID = @financialPeriodID)
		and (@batchID is null or b.BatchID = @batchID)
		and (201210 <= b.BatchID)
	option (optimize for (@financialPeriodID unknown, @batchID unknown));

	select pack.Code, 
		p.Description as Product, 
		pack.Description as Pack, 
		case 
			when ppp.IsEligibleForZeroDiscount = 1 then 'Yes' 
			else 'No' 
			end as IsZD,
		c.Code as Contractor, 
		case
			when d.FragmentTypeCode is null then right('000000' + convert(varchar(6), d.DocumentNumber), 6)
			else d.FragmentTypeCode + right('00000' + convert(varchar(6), d.DocumentNumber), 5) 
			end as Form, 
		i.Sequence as Item, 
		i.SubSequence as Element,
		(select count(*) from dbo.Item where Item.DocumentID = d.DocumentID and Item.Sequence = i.Sequence and Item.CurrentItemVersionID is not null) as NumberOfElements,
		case 
			when ive.EndorsementPeriodicID is not null then 'Yes' 
			else 'No' 
			end as HasZD,
		case
			when ppp.IsEligibleForZeroDiscount = 0 and ive.EndorsementPeriodicID is not null then 0.00 - iv.BasicPrice
			else iv.BasicPrice
			end as DifferenceToZdr

	from @batches sb
		inner join dbo.Batch b
			inner join dbo.Contractor c on b.ContractorID = c.ContractorID
			inner join dbo.Document d
				inner join Item i
					inner join ItemVersion iv
						inner join dbo.ProductPackPeriodic ppp 
							inner join dbo.ProductPeriodic pp 
								inner join dbo.Product p on pp.ProductID = p.ProductID
							on ppp.ProductPeriodicID = pp.ProductPeriodicID
							inner join dbo.ProductPack pack on ppp.ProductPackID = pack.ProductPackID
						on iv.ProductPackPeriodicID = ppp.ProductPackPeriodicID
						left join dbo.ItemVersionEndorsement ive 
							inner join dbo.EndorsementPeriodic ep on ive.EndorsementPeriodicID = ep.EndorsementPeriodicID and ep.EndorsementCode = 'ZD'
						on iv.ItemVersionID = ive.ItemVersionID
					on i.OriginalItemVersionID = iv.ItemVersionID
				on d.DocumentID = i.DocumentID
			on b.BatchID = d.batchID
		on sb.BatchID = b.BatchID

	where ((ppp.IsEligibleForZeroDiscount = 0 and ive.EndorsementPeriodicID is not null)
		 or  (ppp.IsEligibleForZeroDiscount = 1 and ive.EndorsementPeriodicID is null))

	order by 5, 6, 7, 8, 1	;

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[PricingAccuracyReport_BatchBasedStatistics]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PricingAccuracyReport_BatchBasedStatistics]
	@auditTypes varchar(4000),
	@frFinancialPeriodID integer,
	@toFinancialPeriodID integer,
	@jurisdictionID integer

as

	set nocount on;
	set transaction isolation level read uncommitted;

	/*
	 * We need to convert the comma separated list of AuditTypeID's into a table containing
	 * each entry. This enables us to then use that table as a filter...
	 */

	declare @auditType table (AuditTypeID integer);
	declare @commaIndex integer = -1, @auditTypeID integer;

	set @auditTypes = ltrim(rtrim(@auditTypes));
	while (len(@auditTypes) > 0) begin
		set @commaIndex = charindex(',', @auditTypes)
		if (@commaIndex > 0) begin
			set @auditTypeID = substring(@auditTypes, 1, @commaIndex - 1);
			insert into @auditType (AuditTypeID) values (@auditTypeID);
			set @auditTypes = substring(@auditTypes, @commaIndex + 1, len(@auditTypes) - (@commaIndex));
		end else begin
			set @auditTypeID = @auditTypes;
			insert into @auditType (AuditTypeID) values (@auditTypeID);
			set @auditTypes = '';
		end
		set @auditTypes = ltrim(rtrim(@auditTypes));
	end

	/*
	 * Now we can get the data...
	 */

	select FinancialPeriodID, BatchesCompleted,
		BsaTotalOfAccount, PsncTotalOfAccount,
		NumberOfOverpayments, TotalValueOfOverpayments,
		NumberOfUnderpayments, TotalValueOfUnderpayments,
		NetCashVariance, --PercentageNetCashVariance = cast(round(cast(BsaTotalOfAccount as decimal(18,4)) / cast(PsncTotalOfAccount as decimal(18,4)), 4) as money),
		NcvLowerTargetPercentage, NcvUpperTargetPercentage,
		AbsCashVariance, --PercentageAbsCashVariance = (1 - cast(round(cast(AbsCashVariance as decimal(18,4)) / cast(PsncTotalOfAccount  as decimal(18,4)), 4) as money)),
		AcvLowerTargetPercentage

	from (

		select b.FinancialPeriodID, BatchesCompleted = count(*),
		
			NumberOfOverpayments = sum(case when cs.TotalOfAccount < os.TotalOfAccount then 1 else 0 end),
			TotalValueOfOverpayments = sum(case when cs.TotalOfAccount < os.TotalOfAccount then cs.TotalOfAccount - os.TotalOfAccount else cast(0.00 as money) end),
		
			NumberOfUnderpayments = sum(case when cs.TotalOfAccount > os.TotalOfAccount then 1 else 0 end),
			TotalValueOfUnderpayments = sum(case when cs.TotalOfAccount > os.TotalOfAccount then cs.TotalOfAccount - os.TotalOfAccount else cast(0.00 as money) end),

			NetCashVariance = sum(cs.TotalOfAccount - os.TotalOfAccount), 
			jp.NcvLowerTargetPercentage, jp.NcvUpperTargetPercentage,

			AbsCashVariance = sum(bas.AbsCashVariance
				/* + round(bas.AbsProfessionalFeeVariance * case when 201207 <= b.FinancialPeriodID then jp.ValueOfConsumableAllowanceFee else jp.ContainerAllowanceValue end, 2)
				+ round(bas.AbsPaymentForContainersVariance * jp.ContainerAllowanceValue, 2) */
				+ abs(isnull(csfe.RepeatDispensingFee, 0.00) - isnull(osfe.RepeatDispensingFee, 0.00))
				+ abs(isnull(csfe.PracticePayment, 0.00) - isnull(osfe.PracticePayment, 0.00))
				+ abs(isnull(csfe.EstablishmentPayment, 0.00) - isnull(osfe.EstablishmentPayment, 0.00))
				+ abs(isnull(csfe.OtherFeesMedicinalUseReview, 0.00) - isnull(osfe.OtherFeesMedicinalUseReview, 0.00))
				+ abs(isnull(csfe.OtherFeesNewMedicineServiceConsultation, 0.00) - isnull(osfe.OtherFeesNewMedicineServiceConsultation, 0.00))
				+ abs(isnull(csfe.OtherFeesApplianceUseReviewHome, 0.00) - isnull(osfe.OtherFeesApplianceUseReviewHome, 0.00))
				+ abs(isnull(csfe.OtherFeesApplianceUseReviewPremises, 0.00) - isnull(osfe.OtherFeesApplianceUseReviewPremises, 0.00))
				+ abs(isnull(csfe.OtherFeesStomaCustomisation, 0.00) - isnull(osfe.OtherFeesStomaCustomisation, 0.00))
				+ abs(isnull(csch.FP57Refunds, 0.00) - isnull(osch.FP57Refunds, 0.00))
				+ abs(isnull(csco.DiscountValue, 0.00) - isnull(osco.DiscountValue, 0.00))), 
			jp.AcvLowerTargetPercentage,

			BsaTotalOfAccount = sum(os.TotalOfAccount),

			PsncTotalOfAccount = sum(cs.TotalOfAccount)

		from dbo.Batch b
			inner join @auditType at on b.AuditTypeID = at.AuditTypeID
			inner join dbo.BatchAuditStatistics bas on b.BatchID = bas.BatchID
			inner join dbo.Contractor c on b.ContractorID = c.ContractorID
			inner join dbo.JurisdictionPeriodic jp on b.FinancialPeriodID = jp.FinancialPeriodID and c.JurisdictionID = jp.JurisdictionID
			inner join dbo.Schedule os
				inner join dbo.ScheduleCharge osch on os.ScheduleID = osch.ScheduleID
				inner join dbo.ScheduleCost osco on os.ScheduleID = osco.ScheduleID
				inner join dbo.ScheduleFee osfe on os.ScheduleID = osfe.ScheduleID
			on b.ScheduleID = os.ScheduleID
			inner join dbo.Schedule cs
				inner join dbo.ScheduleCharge csch on cs.ScheduleID = csch.ScheduleID
				inner join dbo.ScheduleCost csco on cs.ScheduleID = csco.ScheduleID
				inner join dbo.ScheduleFee csfe on cs.ScheduleID = csfe.ScheduleID
			on b.PsncScheduleID = cs.ScheduleID
		where b.FinancialPeriodID between @frFinancialPeriodID and @toFinancialPeriodID
			and c.JurisdictionID = @jurisdictionID
			and b.BatchStatusID = 900 -- completed
		group by b.FinancialPeriodID, jp.NcvLowerTargetPercentage, jp.NcvUpperTargetPercentage, jp.AcvLowerTargetPercentage

	) s
	order by 1
	option (optimize for (@frFinancialPeriodID unknown, @toFinancialPeriodID unknown));

	return @@error;
GO
/****** Object:  StoredProcedure [dbo].[PricingAccuracyReport_ItemBasedStatistics]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PricingAccuracyReport_ItemBasedStatistics]
	@auditTypes varchar(4000),
	@frFinancialPeriodID integer,
	@toFinancialPeriodID integer,
	@jurisdictionID integer

as

	set nocount on;
	set transaction isolation level read uncommitted;

	/*
	 * We need to convert the comma separated list of AuditTypeID's into a table containing
	 * each entry. This enables us to then use that table as a filter...
	 */

	declare @auditType table (AuditTypeID integer);
	declare @commaIndex integer = -1, @auditTypeID integer;

	set @auditTypes = ltrim(rtrim(@auditTypes));
	while (len(@auditTypes) > 0) begin
		set @commaIndex = charindex(',', @auditTypes)
		if (@commaIndex > 0) begin
			set @auditTypeID = substring(@auditTypes, 1, @commaIndex - 1);
			insert into @auditType (AuditTypeID) values (@auditTypeID);
			set @auditTypes = substring(@auditTypes, @commaIndex + 1, len(@auditTypes) - (@commaIndex));
		end else begin
			set @auditTypeID = @auditTypes;
			insert into @auditType (AuditTypeID) values (@auditTypeID);
			set @auditTypes = '';
		end
		set @auditTypes = ltrim(rtrim(@auditTypes));
	end;

	/*
	 * Now we can get the data...
	 */

	with itemStatistics as (

		select b.FinancialPeriodID,

			TotalNumberOfVerifiedItems = cast(sum(NumberOfVerifiedItems) as money),
			TotalNumberOfVerifiedItemsWithErrors = cast(sum(NumberOfVerifiedItemsWithChanges) as money),
			TotalNumberOfVerifiedItemsWithErrorsThatHaveFinancialImpact = cast(sum(NumberOfVerifiedItemsWithChangesThatHaveFinancialImpact) as money),

			OriginalTotalOfAccounts = sum(
				csco.TotalOfBasicPricesAtStandardDiscountRate
				+ csco.TotalOfBasicPricesAtZeroDiscount
				+ csco.OutOfPocketExpenses
				+ csco.ConsumableAllowanceValue
				+ csco.ContainerAllowanceValue
				+ csfe.ProfessionalFeeValue
				+ csfe.AdditionalFees2a + csfe.AdditionalFees2bMeasuredFitted + csfe.AdditionalFees2bHomeDelivery + csfe.AdditionalFees2c + csfe.AdditionalFees2d + csfe.AdditionalFeesMethadone + csfe.AdditionalFees2e
				+ csfe.AdditionalFees2f
				+ csch.ElasticHoiseryValue + csch.ExclHoiseryR1Value + csch.ExclHoiseryR2Value),

			NetCashVariance = sum(bas.NetCashVariance
				/* + round(bas.NetProfessionalFeeVariance * case when 201207 <= b.FinancialPeriodID then jp.ValueOfConsumableAllowanceFee else jp.ContainerAllowanceValue end, 2)
					+ round(bas.NetPaymentForContainersVariance * jp.ContainerAllowanceValue, 2) */),

			AbsCashVariance = sum(bas.AbsCashVariance
				/* + round(bas.AbsProfessionalFeeVariance * case when 201207 <= b.FinancialPeriodID then jp.ValueOfConsumableAllowanceFee else jp.ContainerAllowanceValue end, 2)
					+ round(bas.AbsPaymentForContainersVariance * jp.ContainerAllowanceValue, 2) */)

		from dbo.Batch b
			inner join @auditType at on b.AuditTypeID = at.AuditTypeID
			inner join dbo.BatchAuditStatistics bas on b.BatchID = bas.BatchID
			inner join dbo.Contractor c on b.ContractorID = c.ContractorID
		--	inner join dbo.JurisdictionPeriodic jp on b.FinancialPeriodID = jp.FinancialPeriodID and c.JurisdictionID = jp.JurisdictionID
		--	inner join dbo.Schedule os
		--		inner join dbo.ScheduleCharge osch on os.ScheduleID = osch.ScheduleID
		--		inner join dbo.ScheduleCost osco on os.ScheduleID = osco.ScheduleID
		--		inner join dbo.ScheduleFee osfe on os.ScheduleID = osfe.ScheduleID
		--	on b.ScheduleID = os.ScheduleID
			inner join dbo.Schedule cs
				inner join dbo.ScheduleCharge csch on cs.ScheduleID = csch.ScheduleID
				inner join dbo.ScheduleCost csco on cs.ScheduleID = csco.ScheduleID
				inner join dbo.ScheduleFee csfe on cs.ScheduleID = csfe.ScheduleID
			on b.PsncScheduleID = cs.ScheduleID
		where b.FinancialPeriodID between @frFinancialPeriodID and @toFinancialPeriodID
			and b.BatchStatusID = 900 -- completed
			and c.JurisdictionID = @jurisdictionID
		group by b.FinancialPeriodID

	)

	select itemStatistics.FinancialPeriodID,
		TotalNumberOfVerifiedItems,
		TotalNumberOfVerifiedItemsWithErrors,
		TotalNumberOfVerifiedItemsWithErrorsThatHaveFinancialImpact,
		OriginalTotalOfAccounts,
		NetCashVariance,
		AbsCashVariance,
		jp.PppaTargetPercentage,
		jp.NcvLowerTargetPercentage, jp.NcvUpperTargetPercentage, 
		jp.AcvLowerTargetPercentage

		--PrescriptionPricingInformationAccuracy = round((TotalNumberOfVerifiedItems - TotalNumberOfVerifiedItemsWithErrors) / TotalNumberOfVerifiedItems, 4),
		--PrescriptionPricingPaymentAccuracy = round((TotalNumberOfVerifiedItems - TotalNumberOfVerifiedItemsWithErrorsThatHaveFinancialImpact) / TotalNumberOfVerifiedItems, 4),
		--jp.PppaTargetPercentage,
		--NetCashVariance = round(1 - (itemStatistics.NetCashVariance / OriginalTotalOfAccounts), 4),
		--jp.NcvLowerTargetPercentage, jp.NcvUpperTargetPercentage, 
		--AbsCashVariance = round(1 - (itemStatistics.AbsCashVariance / OriginalTotalOfAccounts), 4),
		--jp.AcvLowerTargetPercentage
	from itemStatistics
		inner join dbo.JurisdictionPeriodic jp on jp.JurisdictionID = @jurisdictionID and itemStatistics.FinancialPeriodID = jp.FinancialPeriodID
	order by 1
	option (optimize for (@frFinancialPeriodID unknown, @toFinancialPeriodID unknown, @jurisdictionID unknown));

	return @@error;
GO
/****** Object:  StoredProcedure [dbo].[ProductChangesReport_Items]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProductChangesReport_Items]
	@frFinancialPeriodID integer,
	@toFinancialPeriodID integer,
	@minimumNumberOfOccurrences integer = 1
	
as

	set nocount on;
	set transaction isolation level read uncommitted;

	with summary as (
	
		select OriginalProductPackPeriodicID = oiv.ProductPackPeriodicID, CurrentProductPackPeriodicID = civ.ProductPackPeriodicID, NumberOfOccurences = count(i.ItemID)
		from dbo.Batch b
			inner join dbo.Document d
				inner join dbo.Item i
					inner join dbo.ItemVersion oiv on i.OriginalItemVersionID = oiv.ItemVersionID
					inner join dbo.ItemVersion civ on i.CurrentItemVersionID = civ.ItemVersionID
				on d.DocumentID = i.DocumentID
			on b.BatchID = d.BatchID
		where b.FinancialPeriodID between @frFinancialPeriodID and @toFinancialPeriodID
		  and isnull(oiv.ProductPackPeriodicID, 0) != isnull(civ.ProductPackPeriodicID, 0)
		group by oiv.ProductPackPeriodicID, civ.ProductPackPeriodicID
	
	), detail as (
	
		select d.*
		from summary s
			inner join (
				select OriginalProductPackPeriodicID = oppp.ProductPackPeriodicID, CurrentProductPackPeriodicID = cppp.ProductPackPeriodicID, 
					OriginalProductPackCode = opp.Code, OriginalProductDescription = op.Description, OriginalProductPackDescription = opp.Description, OriginalManufacturerName = om.Name,
					CurrentProductPackCode = cpp.Code, CurrentProductDescription = cp.Description, CurrentProductPackDescription = cpp.Description, CurrentManufacturerName = cm.Name,
					b.FinancialPeriodID, fp.FinancialYear, fp.FinancialMonth,
					c.Code, 
					Form = case
						when d.FragmentTypeCode is null then right('000000' + convert(varchar(6), d.DocumentNumber), 6)
						else d.FragmentTypeCode + right('00000' + convert(varchar(6), d.DocumentNumber), 5) 
						end, i.ItemID,
					Item = i.Sequence
				from dbo.Batch b
					inner join dbo.Contractor c on b.ContractorID = c.ContractorID
					inner join dbo.FinancialPeriod fp on b.FinancialPeriodID = fp.FinancialPeriodID
					inner join dbo.Document d
						inner join dbo.Item i
							inner join dbo.ItemVersion oiv 
								left join dbo.ProductPackPeriodic oppp
									inner join dbo.ProductPack opp
										inner join dbo.Product op 
											left join dbo.Manufacturer om on op.ManufacturerID = om.ManufacturerID
										on opp.ProductID = op.ProductID
									on oppp.ProductPackID = opp.ProductPackID
								on oiv.ProductPackPeriodicID = oppp.ProductPackPeriodicID
							on i.OriginalItemVersionID = oiv.ItemVersionID
							inner join dbo.ItemVersion civ 
								left join dbo.ProductPackPeriodic cppp
									inner join dbo.ProductPack cpp
										inner join dbo.Product cp 
											left join dbo.Manufacturer cm on cp.ManufacturerID = cm.ManufacturerID
										on cpp.ProductID = cp.ProductID
									on cppp.ProductPackID = cpp.ProductPackID
								on civ.ProductPackPeriodicID = cppp.ProductPackPeriodicID
							on i.CurrentItemVersionID = civ.ItemVersionID
						on d.DocumentID = i.DocumentID
					on b.BatchID = d.BatchID
				where b.FinancialPeriodID between @frFinancialPeriodID and @toFinancialPeriodID
					and isnull(oiv.ProductPackPeriodicID, 0) != isnull(civ.ProductPackPeriodicID, 0)
			) d on s.OriginalProductPackPeriodicID = d.OriginalProductPackPeriodicID and s.CurrentProductPackPeriodicID = d.CurrentProductPackPeriodicID
		where s.NumberOfOccurences >= @minimumNumberOfOccurrences
	
	)
	
	select d.OriginalProductPackCode, d.OriginalProductDescription, d.OriginalProductPackDescription, d.OriginalManufacturerName,
		d.CurrentProductPackCode, d.CurrentProductDescription, d.CurrentProductPackDescription, d.CurrentManufacturerName,
		d.FinancialPeriodID, d.FinancialYear, d.FinancialMonth, d.Code, d.Form, d.Item
	from detail d
	order by OriginalProductDescription, OriginalProductPackDescription, FinancialPeriodID, Code, Form, Item;

GO
/****** Object:  StoredProcedure [dbo].[ProductPackUsageExtraction]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ProductPackUsageExtraction]
	@productPackPeriodicID bigint

as

	set nocount on;
	set transaction isolation level read uncommitted;

	with selectedItems as (
	
		select i.DocumentID, i.ItemID
		from dbo.Item i
			inner join dbo.ItemVersion iv on i.OriginalItemVersionID = iv.ItemVersionID and iv.ProductPackPeriodicID = @productPackPeriodicID
			
		union

		select i.DocumentID, i.ItemID
		from dbo.Item i
			inner join dbo.ItemVersion iv on i.CurrentItemVersionID = iv.ItemVersionID and iv.ProductPackPeriodicID = @productPackPeriodicID
		
	)
	
	select b.FinancialPeriodID, c.Code as Contractor, d.DocumentNumber as Form, i.Sequence, i.SubSequence as Element,
		case when oiv.ProductPackPeriodicID = @productPackPeriodicID then oiv.Quantity else null end as OriginalQuantity,
		case when civ.ProductPackPeriodicID = @productPackPeriodicID then civ.Quantity else null end as CurrentQuantity,
		case when isnull(i.OriginalItemVersionID, 0) = isnull(i.CurrentItemVersionID, 0) then '' else 'Yes' end as ItemHasChanged
	from dbo.Batch b
		inner join dbo.Contractor c on b.ContractorID = c.ContractorID
		inner join dbo.Document d
			inner join selectedItems si
				inner join dbo.Item i 
					left join dbo.ItemVersion oiv on i.OriginalItemVersionID = oiv.ItemVersionID
					left join dbo.ItemVersion civ on i.CurrentItemVersionID = civ.ItemVersionID
				on si.ItemID = i.ItemID
			on d.DocumentID = si.DocumentID
		on b.BatchID = d.BatchID
	order by 1, 2, 3, 4, 5;

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[ReferralsViewModel_SelectCallback]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ReferralsViewModel_SelectCallback]

as

	set nocount on;
	set transaction isolation level read uncommitted;

	/*
	 * This SP is called from the ReferralsViewModel.SelectCallback method and provides the data that is
	 * used for custom materialization of the required form list.
	 *
	 */

	declare @minFinancialPeriodID integer,
		@maxFinancialPeriodID integer;

	with batchItems as (

		select d.DocumentID,
			i.ItemID,
			i.CurrentItemVersionID,
			isnull(cast(iv.IsReferred as integer), 0) as IsReferred,
			isnull(cast(iv.IsVerified as integer), 0) as IsVerified,
			cast (isnull(dgt.IsReferred, 0) as bit) as IsChargeStatusReferred
		from dbo.Document d
			left join dbo.Item i
				left join dbo.ItemVersion iv on i.CurrentItemVersionID = iv.ItemVersionID
			on d.DocumentID = i.DocumentID
			left join dbo.DocumentGroupType dgt on d.CurrentDocumentGroupTypeID = dgt.DocumentGroupTypeID

	), batchItemSummary as (
	
		select d.DocumentID as ID,
			b.FinancialPeriodID,
			datename(month, convert(datetime, convert(varchar, (fp.FinancialYear * 10000) + (fp.FinancialMonth * 100) + 01), 112)) + ' ' + convert(varchar, fp.FinancialYear) as FinancialPeriod,
			c.Code as ContractorCode,
			d.DocumentNumber,
			d.FragmentTypeCode,
			d.SubmissionGroupCode,
			count(bi.ItemID) as ItemCount,
			sum(bi.IsReferred) as ReferredItemCount,
			sum(bi.IsVerified) as VerifiedItemCount,
			bi.IsChargeStatusReferred
		from batchItems bi
			inner join dbo.Document d 
				inner join dbo.Batch b 
					inner join dbo.Contractor c on b.ContractorID = c.ContractorID
					inner join dbo.FinancialPeriod fp on b.FinancialPeriodID = fp.FinancialPeriodID
				on d.BatchID = b.BatchID
			on bi.DocumentID = d.DocumentID
		group by d.DocumentID, b.FinancialPeriodID, fp.FinancialYear, fp.FinancialMonth, c.Code, d.DocumentNumber, d.FragmentTypeCode, d.SubmissionGroupCode, bi.IsChargeStatusReferred
	
	)
	
	select *
	from batchItemSummary bis
	where (bis.ReferredItemCount > 0 or bis.IsChargeStatusReferred = 1)
	order by FinancialPeriodID desc, ContractorCode, FragmentTypeCode, DocumentNumber;

	return @@error;
GO
/****** Object:  StoredProcedure [dbo].[ScheduleOfPaymentsReport_Charges]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ScheduleOfPaymentsReport_Charges]
	@scheduleID bigint

as

	set nocount on;
	set transaction isolation level read uncommitted;

	select *
	from dbo.ScheduleCharge sc
	where sc.ScheduleID = @scheduleID;

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[ScheduleOfPaymentsReport_DrugAndApplianceCosts]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ScheduleOfPaymentsReport_DrugAndApplianceCosts]
	@scheduleID bigint

as

	set nocount on;
	set transaction isolation level read uncommitted;

	select *
	from dbo.ScheduleCost sc
	where sc.ScheduleID = @scheduleID;

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[ScheduleOfPaymentsReport_ExpensiveItems]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ScheduleOfPaymentsReport_ExpensiveItems]
	@scheduleID bigint

as

	set nocount on;
	set transaction isolation level read uncommitted;

	select *
	from dbo.ScheduleExpensiveItem sei
	where sei.ScheduleID = @scheduleID
	order by sei.BasicPrice desc;

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[ScheduleOfPaymentsReport_ExpensiveItemSummary]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ScheduleOfPaymentsReport_ExpensiveItemSummary]
	@scheduleID bigint

as

	set nocount on;
	set transaction isolation level read uncommitted;

	select *
	from dbo.ScheduleExpensiveItemSummary seis
	where seis.ScheduleID = @scheduleID;

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[ScheduleOfPaymentsReport_Header]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ScheduleOfPaymentsReport_Header]
	@scheduleID bigint

as

	set nocount on;
	set transaction isolation level read uncommitted;

	select b.batchID,
		c.Code as ContractorCode, 
		isnull(b.Name, c.Name) as ContractorName, 
		isnull(b.TradingAs, c.TradingAs) as ContractorTradingAs, 
		isnull(b.Address, c.Address) as ContractorAddress, 
		isnull(b.Postcode, c.Postcode) as ContractorPostcode,
		lpc.Code as LpcCode, lpc.Name as LpcName,
		b.FinancialPeriodID, fp.FinancialMonth, fp.FinancialYear,
		s.GeneratedOn, s.GeneratedBy
	from dbo.Schedule s
		inner join dbo.Batch b 
			inner join dbo.Contractor c on b.ContractorID = c.ContractorID
			inner join dbo.FinancialPeriod fp on b.FinancialPeriodID = fp.FinancialPeriodID
			left join LocalPharmaceuticalCommittee lpc on b.LocalPharmaceuticalCommitteeID = lpc.LocalPharmaceuticalCommitteeID
		on s.BatchID = b.BatchID
	where s.ScheduleID = @scheduleID;

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[ScheduleOfPaymentsReport_LocalPayments]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ScheduleOfPaymentsReport_LocalPayments]
	@scheduleID bigint

as

	set nocount on;
	set transaction isolation level read uncommitted;

	select *
	from dbo.ScheduleLocalPayment slp
	where slp.ScheduleID = @scheduleID;

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[ScheduleOfPaymentsReport_OtherPayments]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ScheduleOfPaymentsReport_OtherPayments]
	@scheduleID bigint

as

	set nocount on;
	set transaction isolation level read uncommitted;

	select *
	from dbo.ScheduleOtherPayment sop
	where sop.ScheduleID = @scheduleID;

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[ScheduleOfPaymentsReport_PrescriptionData]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ScheduleOfPaymentsReport_PrescriptionData]
	@scheduleID bigint

as

	set nocount on;
	set transaction isolation level read uncommitted;

	select *
	from dbo.ScheduleDocumentData sdd
	where sdd.ScheduleID = @scheduleID;

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[ScheduleOfPaymentsReport_PrescriptionFees]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ScheduleOfPaymentsReport_PrescriptionFees]
	@scheduleID bigint

as

	set nocount on;
	set transaction isolation level read uncommitted;

	select *
	from dbo.ScheduleFee sf
	where sf.ScheduleID = @scheduleID;

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[ScheduleOfPaymentsReport_Specials]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[ScheduleOfPaymentsReport_Specials]
	@scheduleID bigint

as

	set nocount on;
	set transaction isolation level read uncommitted;

	select c.JurisdictionID, ssi.*
	from dbo.[ScheduleSpecialItem] ssi
	JOIN dbo.Schedule s on s.ScheduleID = ssi.ScheduleID
	JOIN dbo.Batch b on b.BatchID = s.BatchID
	JOIN dbo.Contractor c on c.ContractorID = b.ContractorID
	where ssi.ScheduleID = @scheduleID;

	return @@error;



GO
/****** Object:  StoredProcedure [dbo].[ScheduleOfPaymentsReport_SummaryOfPaymentAmounts]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ScheduleOfPaymentsReport_SummaryOfPaymentAmounts]
	@scheduleID bigint

as

	set nocount on;
	set transaction isolation level read uncommitted;

	select s.*,
		fp.FinancialMonth as BalanceDueInRespectOfFinancialMonth, fp.FinancialYear as BalanceDueInRespectOfFinancialYear,
		poafp.FinancialMonth as PaymentOnAccountForFinancialMonth
	from dbo.Schedule s
		inner join Batch b
			inner join dbo.FinancialPeriod fp on b.FinancialPeriodID = fp.FinancialPeriodID
		on s.BatchID = b.BatchID
		inner join dbo.FinancialPeriod poafp on s.PaymentOnAccountForFinancialPeriodID = poafp.FinancialPeriodID
	where s.ScheduleID = @scheduleID;

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[ScheduleOfPaymentsReport_Switching]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ScheduleOfPaymentsReport_Switching]
	@scheduleID bigint

as

	set nocount on;
	set transaction isolation level read uncommitted;

	select sgsd.Description, 
		sgss.Reason, isnull(sgss.Number, 0) as Number, sgss.OldRate
	from dbo.ScheduleGroupSwitchDirection sgsd 
		left join dbo.ScheduleGroupSwitchSummary sgss on sgsd.ScheduleGroupSwitchDirectionID = sgss.ScheduleGroupSwitchDirectionID and sgss.ScheduleID = @scheduleID
	order by sgsd.DisplaySequence;

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[ScheduleOfPaymentsReport_Top10CatM]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ScheduleOfPaymentsReport_Top10CatM]
	@scheduleID bigint

as

	set nocount on;
	set transaction isolation level read uncommitted;

	select c.JurisdictionID, stcm.*
	from dbo.ScheduleTop10CatM stcm
	JOIN dbo.Schedule s on s.ScheduleID = stcm.ScheduleID
	JOIN dbo.Batch b on b.BatchID = s.BatchID
	JOIN dbo.Contractor c on c.ContractorID = b.ContractorID
	where stcm.ScheduleID = @scheduleID;

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[ScheduleOfPaymentsReport_Top10ZD]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ScheduleOfPaymentsReport_Top10ZD]
	@scheduleID bigint

as

	set nocount on;
	set transaction isolation level read uncommitted;

	select c.JurisdictionID, stzd.*
	from dbo.ScheduleTop10ZD stzd
	JOIN dbo.Schedule s on s.ScheduleID = stzd.ScheduleID
	JOIN dbo.Batch b on b.BatchID = s.BatchID
	JOIN dbo.Contractor c on c.ContractorID = b.ContractorID
	where stzd.ScheduleID = @scheduleID;

	return @@error;


GO
/****** Object:  StoredProcedure [dbo].[SelectAndMarkRandomAccuracyChecks]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SelectAndMarkRandomAccuracyChecks]
	@batchID bigint,
	@samplePercentage decimal(5, 3) = 1.5

as

	set nocount on;

	-- raiserror('Start', 10, 1) with nowait;

	if not exists (
		select * 
		from dbo.Document d
			inner join dbo.Item i on d.DocumentID = i.DocumentID
		where d.BatchID = @batchID
		  and i.IsRandomAccuracyCheck = 1) begin

		set @samplePercentage /= 100;

		-- raiserror('itemVersionsVerifiedByUser', 10, 1) with nowait;

		declare @itemVersionsVerifiedByUser table (
			VerifiedBy varchar (128),
			NumberOfItems integer,
			primary key (VerifiedBy)
		);

		insert into @itemVersionsVerifiedByUser
		select iv.VerifiedBy, NumberOfItems = count(*)
		from dbo.Document d
			inner join dbo.Item i
				inner join dbo.ItemVersion iv  
					left join dbo.ItemVersionEndorsement ive
						inner join dbo.EndorsementPeriodic ep on ive.EndorsementPeriodicID = ep.EndorsementPeriodicID and ep.EndorsementCode in ('DA', 'RB')
					on iv.ItemVersionID = ive.ItemVersionID
				on i.CurrentItemVersionID = iv.ItemVersionID
			on d.DocumentID = i.DocumentID
		where d.BatchID = @batchID
			and iv.IsVerified = 1
			and ive.ItemVersionID is null
		group by iv.VerifiedBy;

		-- raiserror('unchangedItemVersions', 10, 1) with nowait;

		declare @unchangedItemVersions table (
			ItemID bigint,
			VerifiedBy varchar(128),
			RandomSequence integer,
			primary key(VerifiedBy, ItemID)
		);

		insert into @unchangedItemVersions
		select iv.ItemID, iv.VerifiedBy,
			RandomSequence = rank() over (partition by iv.VerifiedBy order by newid())
		from dbo.Document d
			inner join dbo.Item i
				inner join dbo.ItemVersion iv  
					left join dbo.ItemVersionEndorsement ive
						inner join dbo.EndorsementPeriodic ep on ive.EndorsementPeriodicID = ep.EndorsementPeriodicID and ep.EndorsementCode in ('DA', 'RB')
					on iv.ItemVersionID = ive.ItemVersionID
				on i.CurrentItemVersionID = iv.ItemVersionID
			on d.DocumentID = i.DocumentID
		where d.BatchID = @batchID
			and isnull(i.OriginalItemVersionID, 0) = isnull(i.CurrentItemVersionID, 0)
			and iv.IsVerified = 1
			and ive.ItemVersionID is null;

		-- raiserror('randomSelectionOfUnchangedItems', 10, 1) with nowait;

		declare @randomSelectionOfUnchangedItems table (
			ItemID bigint,
			primary key (ItemID)
		);

		insert into @randomSelectionOfUnchangedItems
		select u.ItemID
		from @itemVersionsVerifiedByUser v
			inner join @unchangedItemVersions u on v.VerifiedBy = u.VerifiedBy
		where RandomSequence <= ceiling(v.NumberOfItems * @samplePercentage)
		group by u.ItemID;

		-- raiserror('update', 10, 1) with nowait;

		update i
		set IsRandomAccuracyCheck = 1
		from @randomSelectionOfUnchangedItems r
			inner join dbo.Item i on r.ItemID = i.ItemID;

	end;

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[SetDefaultFinancialPeriod]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SetDefaultFinancialPeriod]
	@financialPeriodID integer

as

	set nocount on

	update dbo.FinancialPeriod
	set IsDefault = 1
	where FinancialPeriodID = @financialPeriodID;

	return @@error

GO
/****** Object:  StoredProcedure [dbo].[UserStatistics_ActivityExtractionCallback]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UserStatistics_ActivityExtractionCallback]
	@userProfileID integer,
	@batchID bigint = null,
	@frDateTime datetime,
	@toDateTime datetime

as

	set nocount on;
	set transaction isolation level read uncommitted;

	select ua.DateTime, Activity = a.Description,
		Contractor = c.Code, FinancialPeriod = b.FinancialPeriodID,
		FormNumber = isnull(d.FragmentTypeCode, '0') + right('00000' + convert(varchar, d.DocumentNumber), 5),
		Item = i.Sequence, Element = i.SubSequence
	from dbo.UserActivity ua
		inner join dbo.Activity a on ua.ActivityID = a.ActivityID
		left join dbo.Batch b 
			inner join dbo.Contractor c on b.ContractorID = c.ContractorID
		on ua.BatchID = b.BatchID
		left join dbo.Document d on ua.FormID = d.DocumentID
		left join dbo.Item i on ua.ItemID = i.ItemID
	where ua.UserProfileID = @userProfileID
		and (@batchID is null or ua.BatchID = @batchID)
		and ua.DateTime between @frDateTime and @toDateTime
	order by 1

	return @@error;

GO
/****** Object:  StoredProcedure [dbo].[UserStatisticsView_SelectCallback]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UserStatisticsView_SelectCallback]
	@userProfileID int,
	@frDate datetime,
	@toDate datetime = null

as

	set nocount on;
	set transaction isolation level read uncommitted;

	if (@toDate is null) begin
	
		with dateSequence (Date) as (
			select @frDate as Date
			union all
			select dateadd(hour, 1, Date)
			from dateSequence
			where cast(dateadd(hour, 1, Date) as date) = @frDate
		), workingHours as (
			select Date
			from dateSequence
			where datepart(hour, Date) between 08 and 18
		)

		select ds.Date as ActivityDate,
			isnull(results.ChargeStatusVerified, 0) as ChargeStatusVerified,
			isnull(results.ChargeStatusReferred, 0) as ChargeStatusReferred,
			isnull(results.ChargeStatusSwitched, 0) as ChargeStatusSwitched,
			isnull(results.ChargeStatusConfirmed, 0) as ChargeStatusConfirmed,
			isnull(results.ItemVerified, 0) as ItemVerified,
			isnull(results.ItemReferred, 0) as ItemReferred,
			isnull(results.ItemChanged, 0) as ItemChanged,
			isnull(results.ItemRestored, 0) as ItemRestored,
			isnull(results.ItemConfirmed, 0) as ItemConfirmed
		from workingHours ds
			left join (
				select cast(ActivityDate as datetime) as ActivityDate,
					ChargeStatusVerified, ChargeStatusReferred, ChargeStatusSwitched, ChargeStatusConfirmed,
					ItemVerified, ItemReferred, ItemChanged, ItemRestored, ItemConfirmed
				from (
					select dateadd(hour, datediff(hour, 0, datetime), 0) as ActivityDate, 
						case ua.ActivityID
							when 19 then 'ChargeStatusVerified'
							when 21 then 'ChargeStatusReferred'
							when 18 then 'ChargeStatusSwitched'
							when 23 then 'ChargeStatusConfirmed'
							when  4 then 'ItemVerified'
							when  6 then 'ItemReferred'
							when 13 then 'ItemChanged'
							when 15 then 'ItemRestored'
							when  8 then 'ItemConfirmed'
							else 'Error'
						end as Activity,
						isnull(count(*), 0) as N
					from dbo.UserActivity ua
					where UserProfileID = @userProfileID
						and cast(DateTime as date) = @frDate
						and ActivityID in (4, 6, 8, 13, 15, 18, 19, 21, 23)
					group by dateadd(hour, datediff(hour, 0, datetime), 0), ua.ActivityID) s
				pivot (sum(N)
				for Activity in (ChargeStatusVerified, ChargeStatusReferred, ChargeStatusSwitched, ChargeStatusConfirmed, ItemVerified, ItemReferred, ItemChanged, ItemRestored, ItemConfirmed)) as p
			) results on ds.Date = results.ActivityDate
		order by ActivityDate
		option (optimize for (@userProfileID unknown, @frDate unknown));

	end else begin

		with dateSequence (Date) as (
			select @frDate as Date
			union all
			Select dateadd(day, 1, Date)
			from dateSequence
			where dateadd(day, 1, Date) <= @toDate
		)
		
		select ds.Date as ActivityDate,
			isnull(results.ChargeStatusVerified, 0) as ChargeStatusVerified,
			isnull(results.ChargeStatusReferred, 0) as ChargeStatusReferred,
			isnull(results.ChargeStatusSwitched, 0) as ChargeStatusSwitched,
			isnull(results.ChargeStatusConfirmed, 0) as ChargeStatusConfirmed,
			isnull(results.ItemVerified, 0) as ItemVerified,
			isnull(results.ItemReferred, 0) as ItemReferred,
			isnull(results.ItemChanged, 0) as ItemChanged,
			isnull(results.ItemRestored, 0) as ItemRestored,
			isnull(results.ItemConfirmed, 0) as ItemConfirmed
		from dateSequence ds
			left join (
				select cast(ActivityDate as datetime) as ActivityDate,
					ChargeStatusVerified, ChargeStatusReferred, ChargeStatusSwitched, ChargeStatusConfirmed,
					ItemVerified, ItemReferred, ItemChanged, ItemRestored, ItemConfirmed
				from (
					select cast (DateTime as date) as ActivityDate, 
						case ua.ActivityID
							when 19 then 'ChargeStatusVerified'
							when 21 then 'ChargeStatusReferred'
							when 18 then 'ChargeStatusSwitched'
							when 23 then 'ChargeStatusConfirmed'
							when  4 then 'ItemVerified'
							when  6 then 'ItemReferred'
							when 13 then 'ItemChanged'
							when 15 then 'ItemRestored'
							when  8 then 'ItemConfirmed'
							else 'Error'
						end as Activity,
						isnull(count(*), 0) as N
					from dbo.UserActivity ua
					where UserProfileID = @userProfileID
						and cast(DateTime as date) between @frDate and @toDate
						and ActivityID in (4, 6, 8, 13, 15, 18, 19, 21, 23)
					group by cast (DateTime as date), ua.ActivityID) s
				pivot (sum(N)
				for Activity in (ChargeStatusVerified, ChargeStatusReferred, ChargeStatusSwitched, ChargeStatusConfirmed, ItemVerified, ItemReferred, ItemChanged, ItemRestored, ItemConfirmed)) as p
			) results on ds.Date = results.ActivityDate
		order by ActivityDate		
		option (optimize for (@userProfileID unknown, @frDate unknown, @toDate unknown));

	end
	return @@error;
GO
/****** Object:  StoredProcedure [dmd].[DeleteAmp]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dmd].[DeleteAmp]

as

	set nocount on

	delete from dmd.LicensedRoute
	delete from dmd.ActualProductExcipient
	delete from dmd.ApplianceProductInformation

	delete from dmd.ActualMedicinalProduct

	return @@error
GO
/****** Object:  StoredProcedure [dmd].[DeleteAmpp]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dmd].[DeleteAmpp]

as

	set nocount on

	delete from dmd.ReimbursementInformation
	delete from dmd.MedicinalProductPrice
	delete from dmd.ProductPrescribingInformation
	delete from dmd.AppliancePackInformation
	delete from dmd.ActualCombinationPackContent

	delete from dmd.ActualMedicinalProductPack

	return @@error
GO
/****** Object:  StoredProcedure [dmd].[DeleteIngredient]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dmd].[DeleteIngredient]

as

	set nocount on

	delete from dmd.IngredientSubstance

	return @@error
GO
/****** Object:  StoredProcedure [dmd].[DeleteLookup]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dmd].[DeleteLookup]

as

	set nocount on

	delete from dmd.CombinationPackIndicator
	delete from dmd.BasisOfName
	delete from dmd.CombinationProductIndicator
	delete from dmd.ReasonForNameChange
	delete from dmd.VirtualMedicinalProductPrescribingStatus
	delete from dmd.ControlledDrugCategory
	delete from dmd.LicensingAuthority
	delete from dmd.UnitOfMeasurement
	delete from dmd.Form
	delete from dmd.[Route]
	delete from dmd.PaymentCategory
	delete from dmd.Supplier
	delete from dmd.Flavour
	delete from dmd.Colour
	delete from dmd.BasisOfPharmaceuticalStrength
	delete from dmd.ReimbursementStatus
	delete from dmd.SpecialContainerIndicator
	delete from dmd.DiscountNotDeductedIndicator
	delete from dmd.NonAvailabilityIndicator
	delete from dmd.DiscontinuedFlag
	delete from dmd.DoseFormIndicator
	delete from dmd.PriceBasisFlag
	delete from dmd.LegalCategory
	delete from dmd.RestrictionOnAvailability
	delete from dmd.LicensingAuthorityChangeReason
	delete from dmd.VirtualMedicinalProductFormandRoute

	return @@error
GO
/****** Object:  StoredProcedure [dmd].[DeleteVmp]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dmd].[DeleteVmp]

as

	set nocount on

	delete from dmd.VirtualProductIngredient
	delete from dmd.OntologyFormandRouteInformation
	delete from dmd.FormInformation
	delete from dmd.RouteInformation
	delete from dmd.ControlledDrugPrescribingInformation

	delete from dmd.VirtualMedicinalProduct

	return @@error
GO
/****** Object:  StoredProcedure [dmd].[DeleteVmpp]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dmd].[DeleteVmpp]

as

	set nocount on

	delete from dmd.CombinationPackContent
	delete from dmd.DrugTariffCategoryInformation

	delete from dmd.VirtualMedicinalProductPack

	return @@error
GO
/****** Object:  StoredProcedure [dmd].[DeleteVtm]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dmd].[DeleteVtm]

as

	set nocount on

	delete from dmd.VirtualTherapeuticMoiety

	return @@error
GO
/****** Object:  StoredProcedure [dmd].[FreeTextActualMedicinalProductSearch]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dmd].[FreeTextActualMedicinalProductSearch]
	@predicate varchar(4000),
	@startRowIndex integer = null,
	@maximumRows integer = null,
	@orderByClause sysname,
	@totalRowCount integer = null output

as

	set nocount on;

	return 0;
GO
/****** Object:  StoredProcedure [dmd].[FreeTextSupplierSearch]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dmd].[FreeTextSupplierSearch]
	@predicate varchar(4000),
	@startRowIndex integer = null,
	@maximumRows integer = null,
	@orderByClause sysname,
	@totalRowCount integer = null output

as

	set nocount on;

	return 0;
GO
/****** Object:  StoredProcedure [dmd].[FreeTextVirtualMedicinalProductSearch]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dmd].[FreeTextVirtualMedicinalProductSearch]
	@predicate varchar(4000),
	@startRowIndex integer = null,
	@maximumRows integer = null,
	@orderByClause sysname,
	@totalRowCount integer = null output

as

	set nocount on;

	return 0;
GO
/****** Object:  StoredProcedure [ppd].[ApplyMappingUpdates]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [ppd].[ApplyMappingUpdates]
	@financialPeriodID integer

as

	set nocount on;
	set arithabort on;
	set ansi_warnings off;


	raiserror('Applying mapping updates...', 10, 24) with nowait;

	update ppp
	set ActualMedicinalProductPackId = ampp.ActualMedicinalProductPackId,
		VirtualMedicinalProductPackId = vmpp.VirtualMedicinalProductPackId
	from dbo.ProductPeriodic pp
		inner join dbo.ProductPackPeriodic ppp
			inner join dbo.ProductPack p 
				inner join ppd.SnomedMapping m 
					inner join dmd.ActualMedicinalProductPack ampp 
						inner join dmd.VirtualMedicinalProductPack vmpp on ampp.VirtualMedicinalProductPackId = vmpp.VirtualMedicinalProductPackId
					on m.SnomedCode = ampp.ActualMedicinalProductPackId
				on p.Code = m.PpaPackCode
			on ppp.ProductPackID = p.ProductPackID
		on pp.ProductPeriodicID = ppp.ProductPeriodicID
	where pp.FinancialPeriodID = @financialPeriodID;

	update ppp
	set VirtualMedicinalProductPackId = vmpp.VirtualMedicinalProductPackId
	from dbo.ProductPeriodic pp
		inner join dbo.ProductPackPeriodic ppp
			inner join dbo.ProductPack p 
				inner join ppd.SnomedMapping m 
					inner join dmd.VirtualMedicinalProductPack vmpp on m.SnomedCode = vmpp.VirtualMedicinalProductPackId
				on p.Code = m.PpaPackCode
			on ppp.ProductPackID = p.ProductPackID
		on pp.ProductPeriodicID = ppp.ProductPeriodicID
	where pp.FinancialPeriodID = @financialPeriodID;


	raiserror('Updating container size for packs where the special container type is "B" that can be mapped to dm+d...', 10, 26) with nowait;
	update pack
	set pack.ContainerSize = vmpp.VirtualMedicinalProductPackQuantity
	from dbo.ProductPack pack
		inner join dbo.ProductPackPeriodic ppp 
			inner join dbo.ProductPeriodic pp on ppp.ProductPeriodicID = pp.ProductPeriodicID and pp.FinancialPeriodID = @financialPeriodID
			inner join dmd.VirtualMedicinalProductPack vmpp 
				inner join dmd.UnitOfMeasurement uom on vmpp.VirtualMedicinalProductPackUnitOfMeasurementId = uom.UnitOfMeasurementId
			on ppp.VirtualMedicinalProductPackId = vmpp.VirtualMedicinalProductPackId
		on pack.ProductPackID = ppp.ProductPackID
	where ppp.SpecialContainerIndicatorCode = 'B' and uom.Name = 'gram';

GO
/****** Object:  StoredProcedure [ppd].[ApplyMDR]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [ppd].[ApplyMDR]
	@financialPeriodID integer

as

	set nocount on;
	set arithabort on;
	set ansi_warnings off;

	/*
	 * Manufacturer updates
	 *
	 * MANU.TXT => ppd.MANU => dbo.Manufacturer & dbo.ManufacturerCategory
	 *
	 */

	raiserror('Inserting new manufacturer categories...', 10, 1) with nowait;
	insert dbo.ManufacturerCategory
	select distinct Cat, '*Unknown'
	from ppd.MANU
	where Cat not in (
		select ManufacturerCategoryCode
		from dbo.ManufacturerCategory
	);


	raiserror('Applying manufacturer changes...', 10, 2) with nowait;
	merge dbo.Manufacturer as target
	using ppd.MANU as source on target.Code = source.Cde
	when matched then update
		set target.Name = source.Nme,
			target.ManufacturerCategoryCode = source.Cat,
			target.Parent = source.BenOwnr,
			target.IsDeleted = source.DelInd
	when not matched by target then insert (Code, Name, ManufacturerCategoryCode, Parent, IsDeleted)
		values (source.Cde, source.Nme, source.Cat, source.BenOwnr, source.DelInd);

	/*
	 * Appliance product updates
	 *
	 * APPL2.TXT => ppd.APPL2 => dbo.Product & dbo.ProductPeriodic
	 * APPL3.TXT => ppd.APPL3 => dbo.ProductPack & dbo.ProductPackPeriodic
	 *
	 */

	raiserror('Deleting any (appliance) packs that were discontinued before the current period...', 10, 4) with nowait;
	delete from ppd.APPL3
	where PeriodDiscontinued < @financialPeriodID;

	raiserror('Deleting any (appliance) products that have no packs...', 10, 5) with nowait;
	delete from ppd.APPL2
	where ProductCode not in (
		select ProductCode
		from ppd.APPL3
	);

	raiserror('Updating existing (appliance) products...', 10, 6) with nowait;
	update dbo.Product
	set ManufacturerID = m.ManufacturerID, 
		ManufacturersOwnReference = a2.ManufacturerOwnReference, 
		Description = a2.Description, 
		PeriodIntroduced = a2.PeriodIntroduced, 
		PeriodFirstPrescribed = a2.PeriodFirstPrescribed,
		PpCode = a2.PpCode
	from dbo.Product p
		inner join ppd.APPL2 a2
			left join dbo.Manufacturer m on a2.ManufacturerCode = m.Code
		on p.Code = a2.ProductCode;

	raiserror('Updating existing (appliance) product periodics...', 10, 7) with nowait;
	update dbo.ProductPeriodic
	set IsContraceptive = a2.ContraceptiveIndicator, 
		ProductAllowedIndicatorCode = a2.ProductAllowedIndicatorCodeEngland,
		PersonalAdministrationIndicatorCode = a2.PersonalAdministrationIndicatorCode, 
		PreperationClassIndicatorCode = a2.PreparationClassIndicatorCode, 
		StandardQuantityUnitIndicatorCode = a2.StandardQuantityUnitIndicatorCode, 
		WeightedIndicator = a2.WeightingIndicator, 
		ZeroDiscountIndicator = a2.ZeroDiscountIndicator, 
		NumberOfExtraFees = a2.ExtraFees,
		NumberOfExtraCharges = a2.ExtraCharges, 
		SmallQuantity = a2.SmallQuantity,
		LargeQuantity = a2.LargeQuantity,
		IsCombinationPack = a2.CombinationPack, 
		MedicamentCode = a2.MedicamentCode, 
		IsApplianceContractorAllowed = a2.ApplianceContractorIndicator, 
		ApplianceTypeIndicatorCode = a2.ChtApplianceTypeIndicator,
		IsMagenta = a2.IsMagenta,
		DentalFormularyIndicatorCode = '0'
	from dbo.ProductPeriodic pp
		inner join dbo.Product p
			inner join ppd.APPL2 a2 on p.Code = a2.ProductCode
		on pp.ProductID = p.ProductID
	where pp.FinancialPeriodID = @financialPeriodID
	option (optimize for (@financialPeriodID unknown));

	raiserror('Inserting new (appliance) products...', 10, 8) with nowait;
	insert into dbo.Product (
		ManufacturerID, 
		Code, ProductTypeCode, 
		ManufacturersOwnReference, 
		Description, 
		PeriodIntroduced, PeriodFirstPrescribed,
		PpCode
	)
	select m.ManufacturerID, 
		a2.ProductCode, 'A', 
		a2.ManufacturerOwnReference, 
		a2.Description, 
		a2.PeriodIntroduced, a2.PeriodFirstPrescribed,
		a2.PpCode
	from ppd.APPL2 a2
		left join dbo.Manufacturer m on a2.ManufacturerCode = m.Code
		left join dbo.Product p on a2.ProductCode = p.Code
	where p.ProductID is null;

	raiserror('Inserting new (appliance) product periodics...', 10, 9) with nowait;
	insert into dbo.ProductPeriodic (
		ProductID,
		FinancialPeriodID,
		IsContraceptive, 
		ProductAllowedIndicatorCode, 
		PersonalAdministrationIndicatorCode, 
		PreperationClassIndicatorCode, 
		StandardQuantityUnitIndicatorCode, 
		WeightedIndicator, 
		ZeroDiscountIndicator, 
		NumberOfExtraFees, NumberOfExtraCharges, 
		SmallQuantity, LargeQuantity,
		MedicamentCode, 
		IsApplianceContractorAllowed, 
		ApplianceTypeIndicatorCode,
		IsMagenta,
		DentalFormularyIndicatorCode)
	select p.ProductID, 
		@financialPeriodID, 
		a2.ContraceptiveIndicator, 
		a2.ProductAllowedIndicatorCodeEngland,
		a2.PersonalAdministrationIndicatorCode, 
		a2.PreparationClassIndicatorCode, 
		a2.StandardQuantityUnitIndicatorCode, 
		a2.WeightingIndicator, 
		a2.ZeroDiscountIndicator, 
		a2.ExtraFees, a2.ExtraCharges, 
		a2.SmallQuantity, a2.LargeQuantity, 
		a2.MedicamentCode, 
		a2.ApplianceContractorIndicator, 
		a2.ChtApplianceTypeIndicator,
		a2.IsMagenta,
		'0'
	from ppd.APPL2 a2
		inner join dbo.Product p on a2.ProductCode = p.Code
		left join dbo.ProductPeriodic pp on p.ProductID = pp.ProductID and pp.FinancialPeriodID = @financialPeriodID
	where pp.ProductPeriodicID is null
	option (optimize for (@financialPeriodID unknown));

	raiserror('Updating existing (appliance) product packs...', 10, 10) with nowait;
	update dbo.ProductPack
	set CheckDigit = a3.CheckDigit, 
		Description = a3.PackDescription, 
		PeriodDiscontinued = a3.PeriodDiscontinued, 
		CurrentPriceDate = a3.CurrentPriceDate, 
		PackSize = a3.PackSize, ContainerSize = a3.ContainerSize,
		PpCode = a3.PpCode, PpkCode = a3.PpkCode
	from dbo.ProductPack
		inner join ppd.APPL3 a3 on ProductPack.Code = a3.PackCode

	raiserror('Updating existing (appliance) product pack periodics...', 10, 11) with nowait;
	update dbo.ProductPackPeriodic
	set SpecialContainerIndicatorCode = a3.SpecialContainerIndicatorCode,
		PackPrice = a3.CurrentPackPrice, 
		UnitPrice = a3.CurrentUnitPrice, 
		VelocityCode = a3.VelocityCode, 
		VelocityCodeCheckDigit = a3.VelocityCodeCheckDigit,
		MappingPackCode = a3.MappingPackCode,
		MappingPackCheckDigit = a3.MappingPackCheckDigit
	from dbo.ProductPackPeriodic
		inner join dbo.ProductPack
			inner join ppd.APPL3 a3 on ProductPack.Code = a3.PackCode
		on ProductPackPeriodic.ProductPackID = ProductPack.ProductPackID
		inner join dbo.ProductPeriodic on ProductPackPeriodic.ProductPeriodicID = ProductPeriodic.ProductPeriodicID
	where ProductPeriodic.FinancialPeriodID = @financialPeriodID
	option (optimize for (@financialPeriodID unknown));

	raiserror('Inserting new (appliance) product packs...', 10, 12) with nowait;
	insert into dbo.ProductPack (
		ProductID, 
		Code, CheckDigit, 
		Description, 
		PeriodDiscontinued, 
		CurrentPriceDate, 
		PackSize, ContainerSize,
		PpCode, PpkCode)
	select p.ProductID,
		a3.PackCode, a3.CheckDigit, 
		a3.PackDescription, 
		a3.PeriodDiscontinued, 
		a3.CurrentPriceDate, 
		a3.PackSize, a3.ContainerSize,
		a3.PpCode, a3.PpkCode
	from ppd.APPL3 a3
		inner join dbo.Product p on a3.ProductCode = p.Code
		left join dbo.ProductPack pp on a3.PackCode = pp.Code
	where pp.ProductPackID is null;

	raiserror('Inserting new (appliance) product pack periodics...', 10, 13) with nowait;
	insert into dbo.ProductPackPeriodic (
		ProductPackID, 
		ProductPeriodicID,
		SpecialContainerIndicatorCode,
		PackPrice, UnitPrice, 
		VelocityCode, VelocityCodeCheckDigit,
		MappingPackCode, MappingPackCheckDigit,
		IsPatientPack)
	select p.ProductPackID,
		pp.ProductPeriodicID,
		a3.SpecialContainerIndicatorCode, 
		a3.CurrentPackPrice, a3.CurrentUnitPrice, 
		a3.VelocityCode, a3.VelocityCodeCheckDigit,
		a3.MappingPackCode, a3.MappingPackCheckDigit,
		a3.PatientPackIndicator
	from ppd.APPL3 a3
		inner join ProductPack p
			inner join dbo.ProductPeriodic pp on p.ProductID = pp.ProductID and pp.FinancialPeriodID = @financialPeriodID
			left join dbo.ProductPackPeriodic ppp on ppp.ProductPackID = p.ProductPackID and ppp.ProductPeriodicID = pp.ProductPeriodicID
		on a3.PackCode = p.Code
	where ppp.ProductPackPeriodicID is null
	option (optimize for (@financialPeriodID unknown));

	/*
	 * Drug product updates
	 *
	 * DRUG2.TXT => ppd.DRUG2 => dbo.Product & dbo.ProductPeriodic
	 * DRUG3.TXT => ppd.DRUG3 => dbo.ProductPack & dbo.ProductPackPeriodic
	 *
	 */

	-- As per an email exchange between MR & MH on 24/05/2011, we should not be doing this for
	-- drug products as it seems that the pharmacy contractor is allowed to continue dispensing discontinued
	-- drugs in order to allow the depletion of existing stock.

	--raiserror('Deleting any (drug) packs that were discontinued before the current period...', 10, 14) with nowait;
	--delete from ppd.DRUG3
	--where PeriodDiscontinued < @financialPeriodID;

	--raiserror('Deleting any (drug) products that have no packs...', 10, 15) with nowait;
	--delete from ppd.DRUG2
	--where ProductCode not in (
	--	select ProductCode
	--	from ppd.DRUG3
	--);

	raiserror('Updating existing (drug) products...', 10, 16) with nowait;
	update dbo.Product
	set ManufacturerID = m.ManufacturerID, 
		ProductTypeCode = 'D', 
		Description = d2.Description, 
		PeriodIntroduced = d2.PeriodIntroduced, 
		PeriodFirstPrescribed = d2.PeriodFirstPrescribed,
		PpCode = d2.PpCode
	from dbo.Product p
		inner join ppd.DRUG2 d2
			left join dbo.Manufacturer m on d2.ManufacturerCode = m.Code
		on p.Code = d2.ProductCode;

	raiserror('Updating existing (drug) product periodics...', 10, 17) with nowait;
	update dbo.ProductPeriodic
	set IsContraceptive = d2.ContraceptiveIndicator, 
		ControlledSubstanceIndicatorCode = d2.ControlledDrugIndicatorCodeEngland,
		DentalFormularyIndicatorCode = d2.DentalFormularyIndicatorCodeEngland,
		PersonalAdministrationIndicatorCode = d2.PersonalAdministrationIndicatorCode, 
		StandardDrugClassIndicatorCode = d2.StandardDrugClassIndicatorCode,
		BorderlineSubstanceIndicatorCode = d2.BorderlineSubstanceIndicatorCode,
		PreperationClassIndicatorCode = d2.PreparationClassIndicatorCode, 
		DiluentIndicatorCode = d2.DiluentIndicatorCode, 
		InputQuantityUnitIndicatorCode = d2.InputQuantityUnitIndicatorCode,
		StandardQuantityUnitIndicatorCode = d2.StandardQuantityUnitIndicatorCode, 
		UnusualFeeIndicatorCode = d2.UnusualFeeIndicatorCode, 
		IsPrescriptionEventMonitored = d2.PrescriptionEventMonitoredIndicator, 
		IsCommonPack = d2.CommonPackIndicator, 
		IsAdditionalFeeAllowed = d2.AdditionalFeeAllowedIndicator, 
		WeightedIndicator = d2.WeightingIndicator, 
		ZeroDiscountIndicator = d2.ZeroDiscountIndicator, 
		PoisonIndicator = d2.S4PoisonIndicator, 
		NumberOfExtraFees = d2.ExtraFees,
		NumberOfExtraCharges = d2.ExtraCharges, 
		SmallQuantity = d2.SmallQuantity,
		LargeQuantity = d2.LargeQuantity,
		AverageMonthlyDose = d2.AverageMonthlyDose, 
		IsCombinationPack = d2.CombinationPack, 
		WithdrawalStatusIndicatorCode = d2.WithdrawnStatusIndicatorCode, 
		EquivelentProductCode = d2.EquivelentDrugCode, 
		TariffIndicator = d2.TariffIndicatorCode, 
		MedicamentCode = d2.MedicamentCode,
		IsMagenta = d2.IsMagenta,
		IsBrandedMedicine = d2.BrandedMedicineIndicator,
		IsEligibleForSupplementaryControlledDrugFee = d2.SupplementaryControlledDrugFeeIndicator,
		IsEligibleForReconstitutionFee = d2.ReconstitutionIndicator
	from dbo.ProductPeriodic pp
		inner join dbo.Product p
			inner join ppd.DRUG2 d2 on p.Code = d2.ProductCode
		on pp.ProductID = p.ProductID
	where pp.FinancialPeriodID = @financialPeriodID
	option (optimize for (@financialPeriodID unknown));

	raiserror('Inserting new (drug) products...', 10, 18) with nowait;
	insert into dbo.Product (
		ManufacturerID, 
		Code, ProductTypeCode, 
		Description, 
		PeriodIntroduced, PeriodFirstPrescribed,
		PpCode)
	select m.ManufacturerID, 
		d2.ProductCode, 'D', 
		d2.Description, 
		d2.PeriodIntroduced, d2.PeriodFirstPrescribed,
		d2.PpCode
	from ppd.DRUG2 d2
		left join dbo.Manufacturer m on d2.ManufacturerCode = m.Code
		left join dbo.Product p on d2.ProductCode = p.Code
	where p.ProductID is null;

	raiserror('Inserting new (drug) product periodics...', 10, 19) with nowait;
	insert into dbo.ProductPeriodic (
		ProductID,
		FinancialPeriodID,
		IsContraceptive, 
		ControlledSubstanceIndicatorCode, 
		DentalFormularyIndicatorCode, 
		PersonalAdministrationIndicatorCode, 
		StandardDrugClassIndicatorCode, BorderlineSubstanceIndicatorCode,
		PreperationClassIndicatorCode, 
		DiluentIndicatorCode, 
		InputQuantityUnitIndicatorCode, StandardQuantityUnitIndicatorCode, 
		UnusualFeeIndicatorCode, 
		IsPrescriptionEventMonitored, 
		IsCommonPack, 
		IsAdditionalFeeAllowed, 
		WeightedIndicator, 
		ZeroDiscountIndicator, 
		PoisonIndicator, 
		NumberOfExtraFees, NumberOfExtraCharges, 
		SmallQuantity, LargeQuantity, AverageMonthlyDose, 
		IsCombinationPack, 
		WithdrawalStatusIndicatorCode, 
		EquivelentProductCode, 
		TariffIndicator, 
		MedicamentCode,
		IsMagenta,
		IsBrandedMedicine,
		IsEligibleForSupplementaryControlledDrugFee,
		IsEligibleForReconstitutionFee)
	select p.ProductID, 
		@financialPeriodID, 
		d2.ContraceptiveIndicator, 
		d2.ControlledDrugIndicatorCodeEngland,
		d2.DentalFormularyIndicatorCodeEngland,
		d2.PersonalAdministrationIndicatorCode, 
		d2.StandardDrugClassIndicatorCode, d2.BorderlineSubstanceIndicatorCode,
		d2.PreparationClassIndicatorCode, 
		d2.DiluentIndicatorCode, 
		d2.InputQuantityUnitIndicatorCode, d2.StandardQuantityUnitIndicatorCode, 
		d2.UnusualFeeIndicatorCode, 
		d2.PrescriptionEventMonitoredIndicator, 
		d2.CommonPackIndicator, 
		d2.AdditionalFeeAllowedIndicator, 
		d2.WeightingIndicator, 
		d2.ZeroDiscountIndicator, 
		d2.S4PoisonIndicator, 
		d2.ExtraFees, d2.ExtraCharges, 
		d2.SmallQuantity, d2.LargeQuantity, d2.AverageMonthlyDose, 
		d2.CombinationPack, 
		d2.WithdrawnStatusIndicatorCode, 
		d2.EquivelentDrugCode, 
		d2.TariffIndicatorCode, 
		d2.MedicamentCode,
		d2.IsMagenta,
		d2.BrandedMedicineIndicator,
		d2.SupplementaryControlledDrugFeeIndicator,
		d2.ReconstitutionIndicator
	from ppd.DRUG2 d2
		inner join dbo.Product p on d2.ProductCode = p.Code
		left join dbo.ProductPeriodic pp on p.ProductID = pp.ProductID and pp.FinancialPeriodID = @financialPeriodID
	where pp.ProductPeriodicID is null
	option (optimize for (@financialPeriodID unknown));

	raiserror('Updating existing (drug) product packs...', 10, 20) with nowait;
	update dbo.ProductPack
	set CheckDigit = d3.CheckDigit, 
		Description = d3.PackDescription, 
		PeriodDiscontinued = d3.PeriodDiscontinued, 
		CurrentPriceDate = d3.CurrentPriceDate, 
		PackSize = d3.PackSize, ContainerSize = d3.ContainerSize,
		PpCode = d3.PpCode, PpkCode = d3.PpkCode
	from dbo.ProductPack
		inner join ppd.DRUG3 d3 on ProductPack.Code = d3.PackCode;

	raiserror('Updating existing (drug) product pack periodics...', 10, 21) with nowait;
	update dbo.ProductPackPeriodic
	set SpecialContainerIndicatorCode = d3.SpecialContainerIndicatorCode,
		IsEligibleForZeroDiscount = cast (isnull(ProductPeriodic.ZeroDiscountIndicator, '0') as bit),
		PackPrice = d3.CurrentPackPrice, 
		UnitPrice = d3.CurrentUnitPrice,
		IsCommonPack = d3.CommonPackIndicator,
		VelocityCode = d3.VelocityCode, 
		VelocityCodeCheckDigit = d3.VelocityCodeCheckDigit,
		MappingPackCode = d3.MappingPackCode,
		MappingPackCheckDigit = d3.MappingPackCheckDigit,
		IsPatientPack = d3.PatientPackIndicator
	from dbo.ProductPackPeriodic
		inner join dbo.ProductPack
			inner join ppd.DRUG3 d3 on ProductPack.Code = d3.PackCode
		on ProductPackPeriodic.ProductPackID = ProductPack.ProductPackID
		inner join dbo.ProductPeriodic on ProductPackPeriodic.ProductPeriodicID = ProductPeriodic.ProductPeriodicID
	where ProductPeriodic.FinancialPeriodID = @financialPeriodID
	option (optimize for (@financialPeriodID unknown));

	raiserror('Inserting new (drug) product packs...', 10, 22) with nowait;
	insert into dbo.ProductPack (
		ProductID, 
		Code, CheckDigit, 
		Description, 
		PeriodDiscontinued, 
		CurrentPriceDate, 
		PackSize, ContainerSize,
		PpCode, PpkCode)
	select p.ProductID,
		d3.PackCode, d3.CheckDigit, 
		d3.PackDescription, 
		d3.PeriodDiscontinued, 
		d3.CurrentPriceDate, 
		d3.PackSize, d3.ContainerSize,
		d3.PpCode, d3.PpkCode
	from ppd.DRUG3 d3
		inner join dbo.Product p on d3.ProductCode = p.Code
		left join dbo.ProductPack pp on d3.PackCode = pp.Code
	where pp.ProductPackID is null;

	raiserror('Inserting new (drug) product pack periodics...', 10, 23) with nowait;
	insert into dbo.ProductPackPeriodic (
		ProductPackID, 
		ProductPeriodicID,
		SpecialContainerIndicatorCode,
		IsEligibleForZeroDiscount,
		PackPrice, UnitPrice, 
		IsCommonPack,
		VelocityCode, VelocityCodeCheckDigit,
		MappingPackCode, MappingPackCheckDigit,
		IsPatientPack)
	select p.ProductPackID,
		pp.ProductPeriodicID,
		d3.SpecialContainerIndicatorCode, 
		cast (isnull(pp.ZeroDiscountIndicator, '0') as bit),
		d3.CurrentPackPrice, d3.CurrentUnitPrice, 
		d3.CommonPackIndicator,
		d3.VelocityCode, d3.VelocityCodeCheckDigit,
		d3.MappingPackCode, d3.MappingPackCheckDigit,
		d3.PatientPackIndicator
	from ppd.DRUG3 d3
		inner join ProductPack p
			inner join ProductPeriodic pp on p.ProductID = pp.ProductID and pp.FinancialPeriodID = @financialPeriodID
			left join ProductPackPeriodic ppp on ppp.ProductPackID = p.ProductPackID and ppp.ProductPeriodicID = pp.ProductPeriodicID
		on d3.PackCode = p.Code
	where ppp.ProductPackPeriodicID is null
	option (optimize for (@financialPeriodID unknown));

	/*
	 * Zero Discount (DND) indicators
	 *
	 * MDR carries these indicators at the presentation level, but they are applied within CDR at the pack level.
	 * MH has determined an algorithm that will (hopefully) allow us to determine the correct pack level value based upon
	 * the presentation level indicators of eqivelent class2 and class3 products...
	 *
	 */

	exec dbo.DeriveAndApplyPackLevelZeroDiscount @financialPeriodID = @financialPeriodID, @state = 23;

	/*
	 * Mapping updates
	 *
	 * DRUG2.TXT => ppd.SnomedMapping => dbo.ProductPackPeriodic
	 *
	 */

	raiserror('Applying mapping updates (1/2)...', 10, 24) with nowait;
	update ppp
	set ActualMedicinalProductPackId = ampp.ActualMedicinalProductPackId,
		VirtualMedicinalProductPackId = vmpp.VirtualMedicinalProductPackId
	from dbo.ProductPeriodic pp
		inner join dbo.ProductPackPeriodic ppp
			inner join dbo.ProductPack p 
				inner join ppd.SnomedMapping m 
					inner join dmd.ActualMedicinalProductPack ampp 
						inner join dmd.VirtualMedicinalProductPack vmpp on ampp.VirtualMedicinalProductPackId = vmpp.VirtualMedicinalProductPackId
					on m.SnomedCode = ampp.ActualMedicinalProductPackId
				on p.Code = m.PpaPackCode
			on ppp.ProductPackID = p.ProductPackID
		on pp.ProductPeriodicID = ppp.ProductPeriodicID
	where pp.FinancialPeriodID = @financialPeriodID
	option (optimize for (@financialPeriodID unknown));

	raiserror('Applying mapping updates (2/2)...', 10, 24) with nowait;
	update ppp
	set VirtualMedicinalProductPackId = vmpp.VirtualMedicinalProductPackId
	from dbo.ProductPeriodic pp
		inner join dbo.ProductPackPeriodic ppp
			inner join dbo.ProductPack p 
				inner join ppd.SnomedMapping m 
					inner join dmd.VirtualMedicinalProductPack vmpp on m.SnomedCode = vmpp.VirtualMedicinalProductPackId
				on p.Code = m.PpaPackCode
			on ppp.ProductPackID = p.ProductPackID
		on pp.ProductPeriodicID = ppp.ProductPeriodicID
	where pp.FinancialPeriodID = @financialPeriodID
	option (optimize for (@financialPeriodID unknown));

	-- We will clear the container size on all packs where the special container indicator is "B" (sachets).
	-- This can then act as a flag later to help with the pricing of these items.
	raiserror('Clearing container size for packs where the special container type is "B"...', 10, 25) with nowait;
	update pack
	set pack.ContainerSize = 0
	from dbo.ProductPack pack
		inner join dbo.ProductPackPeriodic ppp 
			inner join dbo.ProductPeriodic pp on ppp.ProductPeriodicID = pp.ProductPeriodicID and pp.FinancialPeriodID = @financialPeriodID
		on pack.ProductPackID = ppp.ProductPackID
	where ppp.SpecialContainerIndicatorCode = 'B'
	option (optimize for (@financialPeriodID unknown));

	-- Now we are going to those packs where the special container indicator is "B" (sachets) and the dm+d database indicates that
	-- the official unit of measure is grams. This way, and in conjunction with having zeroed the non "gram" container sizes we can determine
	-- the correct figures to use when pricing these items.
	-- We have to do this each time MDR is applied because the pack details are overwritten each month (these details are not held historically).
	raiserror('Updating container size for packs where the special container type is "B" that can be mapped to dm+d...', 10, 26) with nowait;
	update pack
	set pack.ContainerSize = vmpp.VirtualMedicinalProductPackQuantity
	from dbo.ProductPack pack
		inner join dbo.ProductPackPeriodic ppp 
			inner join dbo.ProductPeriodic pp on ppp.ProductPeriodicID = pp.ProductPeriodicID and pp.FinancialPeriodID = @financialPeriodID
			inner join dmd.VirtualMedicinalProductPack vmpp 
				inner join dmd.UnitOfMeasurement uom on vmpp.VirtualMedicinalProductPackUnitOfMeasurementId = uom.UnitOfMeasurementId
			on ppp.VirtualMedicinalProductPackId = vmpp.VirtualMedicinalProductPackId
		on pack.ProductPackID = ppp.ProductPackID
	where ppp.SpecialContainerIndicatorCode = 'B' and uom.Name = 'gram'
	option (optimize for (@financialPeriodID unknown));

	/*
	 * Unit prices
	 *
	 * Unit prices provided by the PPD are just plain wrong. All they are is [pack price] / [pack size].
	 * They do not take into account the different container types.
	 *
	 * As such, we are going to try and recalculate them here...
	 *
	 */

	raiserror('Recalculating unit prices...', 10, 27) with nowait;
	update ppp
		set UnitPrice = round(case
			when SpecialContainerIndicatorCode = 'S' then case 
				when pack.ContainerSize = 1 and pack.PackSize > 1 and ppp.PackPrice != ppp.UnitPrice then (ppp.PackPrice * 100) / pack.PackSize
				when pack.ContainerSize > 0 then (ppp.PackPrice * 100) / pack.ContainerSize 
				else UnitPrice end
			when SpecialContainerIndicatorCode = 'B' and pack.ContainerSize > 0 then (ppp.PackPrice * 100) / pack.ContainerSize
			else UnitPrice end, 4)
	from ProductPeriodic pp
		inner join ProductPackPeriodic ppp 
			inner join ProductPack pack on ppp.ProductPackID = pack.ProductPackID
		on pp.ProductPeriodicID = ppp.ProductPeriodicID
	where pp.FinancialPeriodID = @financialPeriodID
	option (optimize for (@financialPeriodID unknown));

	/*
	 * Mapped Packs
	 *
	 * Packs that are referred to by mapping packs must not be selectable by the users.
	 * we have implemented a flag "IsEnabled" to control this - that way in the event that this causes problems we can re-enable
	 * the products within the database (without application changes)...
	 *
	 */

	raiserror('Disabling packs that are referenced by mapping packs...', 10, 28) with nowait;
	update ppp
	set IsEnabled = 0
	from ProductPeriodic pp
		inner join ProductPackPeriodic ppp
			inner join ProductPack p 
				inner join ProductPackPeriodic mppp
					inner join ProductPeriodic mpp on mppp.ProductPeriodicID = mpp.ProductPeriodicID and mpp.FinancialPeriodID = @financialPeriodID
				on p.Code = mppp.MappingPackCode
			on ppp.ProductPackID = p.ProductPackID
		on pp.ProductPeriodicID = ppp.ProductPeriodicID
	where pp.FinancialPeriodID = @financialPeriodID
	option (optimize for (@financialPeriodID unknown));

	/*
	 * We also need to disable certain non-tariff packs.
	 * To be honest, I do not fully understand why we do this but the rule is simple...
	 * Any product pack where the product is in Part VIII/A or Part VIII/C, with a Preparation code of "1" or "2" and no price cannot be selected for pricing.
	 *
	 */

	update ppp
	set IsEnabled = 0
	from dbo.ProductPackPeriodic ppp 
		inner join dbo.ProductPeriodic pp on ppp.ProductPeriodicID = pp.ProductPeriodicID and pp.FinancialPeriodID = @financialPeriodID
	where pp.StandardDrugClassIndicatorCode in ('A', 'C', 'M') and pp.PreperationClassIndicatorCode in ('1', '2') and ppp.PackPrice = 0.00
	option (optimize for (@financialPeriodID unknown));

	/*
	 * Drug Tariff Indicators; identify whether specific packs are in Part VIII/IX/X of the drug tariff.
	 *
	 */

	exec dbo.DeriveAndApplyPackLevelDrugTariffCategories @financialPeriodID = @financialPeriodID, @state = 29;

	/*
	 * Dental formulary Indicators; identify whether specific packs are dental formularies.
	 *
	 */

	exec dbo.DeriveAndApplyPackLevelDentalFormularyIndicators @financialPeriodID = @financialPeriodID, @state = 29;

	/*
	 * Unlicensed Medicine Indicators: identify products in Part VIII.B of the Drug Tariff.
	 *
	 */

	exec dbo.DeriveAndApplyUnlicensedMedicineIndicator @financialPeriodID = @financialPeriodID, @state = 29;

	/*
	 * Clause 13b reconstitution Indicators: derive class 3/5 from class 1/2.
	 *
	 */

	exec dbo.DeriveAndApplyReconstitutionIndicator @financialPeriodID = @financialPeriodID, @state = 29;

	/*
	 * Home delivery products
	 *
	 * We currently get a manual feed from MH @ NHSBSA that contains the details of all
	 * Tariff Part IX products, including which of those are eligible for home delivery.
	 * This is to be replaced later with modifications to the APPL2 file in MDR, and that
	 * is expected to be included in the "Simplification" changes scheduled for April 2012
	 * processing.
	 *
	 * The manual feed is provided on a month by month basis, and each of the files provided
	 * is loaded into a seperate table, ppd.PartIX_yymm. These tables are then combined using
	 * the view ppd.PartIX_All.
	 *
	 * Here we update the indicator against the product(s) from that view - that way, all products
	 * that have ever been identified as home delivery eligible are marked, irrespective of when
	 * the product shows in the manual feed. MH has confirmed that this should be the case as
	 * any product that is ever home delivery eligible will remain so for the lifetime of that
	 * product.
	 *
	 */

	if (@financialPeriodID >= 201203) begin

		raiserror('Setting home delivery indicators (1/2)...', 10, 30) with nowait;
		update ppp
			set HomeDeliveryIndicatorCode = a2.HomeDeliveryIndicatorCode
		from dbo.Product p
			inner join dbo.ProductPeriodic pp
				inner join dbo.ProductPackPeriodic ppp on pp.ProductPeriodicID = ppp.ProductPeriodicID
			on p.ProductID = pp.ProductID and pp.FinancialPeriodID = @financialPeriodID
			inner join ppd.APPL2 a2 on p.Code = a2.ProductCode
		where a2.HomeDeliveryIndicatorCode in ('1', '2')
		option (optimize for (@financialPeriodID unknown));

		raiserror('Setting home delivery indicators (2/2)...', 10, 30) with nowait;
		with homeDelivery as (
			select distinct pp.ProductID
			from dmd.VirtualMedicinalProductPack vmpp
				inner join dmd.DrugTariffCategoryInformation dtci on vmpp.VirtualMedicinalProductPackId = dtci.VirtualMedicinalProductPackId
				inner join dbo.ProductPackPeriodic ppp
					inner join dbo.ProductPeriodic pp on ppp.ProductPeriodicID = pp.ProductPeriodicID and pp.FinancialPeriodID = @financialPeriodID
				on vmpp.VirtualMedicinalProductPackId = ppp.VirtualMedicinalProductPackId
			where dtci.PaymentCategoryId in (6, 7, 10)
		)
		update ppp
		set HomeDeliveryIndicatorCode = '1'
		from homeDelivery
			inner join dbo.Product p 
				inner join dbo.ProductPeriodic pp
					inner join dbo.ProductPackPeriodic ppp on pp.ProductPeriodicID = ppp.ProductPeriodicID
				on p.ProductID = pp.ProductID and pp.FinancialPeriodID = @financialPeriodID
			on homeDelivery.ProductID = p.ProductID
		where p.ProductTypeCode = 'A'
		option (optimize for (@financialPeriodID unknown));

	end /* else begin

		raiserror('Setting home delivery indicators (1/3)...', 10, 30) with nowait;
		update p
		set HomeDeliveryIndicatorCode = case
			when hd.HomeDelivery = 'B' then 2 else '1' end
		from dbo.Product p 
			inner join dbo.ProductPack pp 
				inner join (
					select AmppProductPackCode as ProductPackCode, HomeDelivery
					from ppd.PartIX_All
					where AmppProductPackCode is not null
					union 
					select VmppProductPackCode as ProductPackCode, HomeDelivery
					from ppd.PartIX_All
					where VmppProductPackCode is not null
				) hd on pp.Code = hd.ProductPackCode
			on p.ProductID = pp.ProductID
		where p.ProductTypeCode = 'A' and hd.HomeDelivery is not null;

		raiserror('Setting home delivery indicators (2/3)...', 10, 30) with nowait;
		update p
		set HomeDeliveryIndicatorCode = case
			when hd.HomeDelivery = 'B' then 2 else '1' end
		from dbo.Product p 
			inner join (
				select AmpProductCode as ProductCode, HomeDelivery
				from ppd.PartIX_All
				where AmpProductCode is not null
				union 
				select VmpProductCode as ProductCode, HomeDelivery
				from ppd.PartIX_All
				where VmpProductCode is not null
			) hd on p.Code = hd.ProductCode
		where p.ProductTypeCode = 'A' and hd.HomeDelivery is not null;

		raiserror('Setting home delivery indicators (3/3)...', 10, 30) with nowait;
		with homeDelivery as (
			select distinct pp.ProductID
			from dmd.VirtualMedicinalProductPack vmpp
				inner join dmd.DrugTariffCategoryInformation dtci on vmpp.VirtualMedicinalProductPackId = dtci.VirtualMedicinalProductPackId
				inner join dbo.ProductPackPeriodic ppp
					inner join dbo.ProductPeriodic pp on ppp.ProductPeriodicID = pp.ProductPeriodicID
				on vmpp.VirtualMedicinalProductPackId = ppp.VirtualMedicinalProductPackId
			where dtci.PaymentCategoryId in (6, 7, 10)
		)
		update p
		set HomeDeliveryIndicatorCode = '1'
		from homeDelivery
			inner join dbo.Product p on homeDelivery.ProductID = p.ProductID
		where p.ProductTypeCode = 'A';

	end */

	/*
	 * Stoma Customisation
	 *
	 * The manual extraction above also includes details of those products that are eligible for
	 * Stoma Customisation fees. Again, this will be in APPL2 at the same time as home delivery.
	 *
	 * Here we update the bit field against the product from the manual feed.
	 *
	 */

	if (@financialPeriodID >= 201203) begin

		raiserror('Setting Stoma Customisation indicators...', 10, 31) with nowait;
		update p
			set IsEligibleForStomaCustomisationFee = a2.StomaCustomisationIndicator
		from dbo.Product p
			inner join ppd.APPL2 a2 on p.Code = a2.ProductCode
		where a2.StomaCustomisationIndicator = 1;

	end /* else begin

		raiserror('Setting Stoma Customisation indicators (1/2)...', 10, 31) with nowait;
		update p
		set IsEligibleForStomaCustomisationFee = 1
		from dbo.Product p 
			inner join dbo.ProductPack pp 
				inner join (
					select AmppProductPackCode as ProductPackCode, StomaCustomisation
					from ppd.PartIX_All
					where AmppProductPackCode is not null
					union 
					select VmppProductPackCode as ProductPackCode, StomaCustomisation
					from ppd.PartIX_All
					where VmppProductPackCode is not null
				) hd on pp.Code = hd.ProductPackCode
			on p.ProductID = pp.ProductID
		where p.ProductTypeCode = 'A' and hd.StomaCustomisation is not null;

		raiserror('Setting Stoma Customisation indicators (2/2)...', 10, 31) with nowait;
		update p
		set IsEligibleForStomaCustomisationFee = 1
		from dbo.Product p 
			inner join (
				select AmpProductCode as ProductCode, StomaCustomisation
				from ppd.PartIX_All
				where AmpProductCode is not null
				union 
				select VmpProductCode as ProductCode, StomaCustomisation
				from ppd.PartIX_All
				where VmpProductCode is not null
			) hd on p.Code = hd.ProductCode
		where p.ProductTypeCode = 'A' and hd.StomaCustomisation is not null;

	end */

	if (201207 <= @financialPeriodID) begin

		/* July 2012 Simplification */

		-- The concept of a "common pack" has been removed from the tariff but the corresponding attribute is still
		-- set in MDR. It should therefore be cleared.
		raiserror('Clearing common pack indicators...', 10, 32) with nowait;
		update ppp
		set IsCommonPack = 0
		from dbo.ProductPeriodic pp
			inner join dbo.ProductPackPeriodic ppp on pp.ProductPeriodicID = ppp.ProductPeriodicID
		where pp.FinancialPeriodID = @financialPeriodID;
	
		-- The concept of a "calendar pack" has been removed from the tariff but the corresponding attribute is still
		-- set in MDR. It should therefore be cleared.
		raiserror('Clearing calendar pack indicators...', 10, 32) with nowait;
		update ppp
		set SpecialContainerIndicatorCode = null
		from dbo.ProductPeriodic pp
			inner join dbo.ProductPackPeriodic ppp on pp.ProductPeriodicID = ppp.ProductPeriodicID
		where pp.FinancialPeriodID = @financialPeriodID
			and ppp.SpecialContainerIndicatorCode = 'C';
		
	end

	raiserror('Finished.', 10, 255) with nowait;

	return @@error;

GO
/****** Object:  StoredProcedure [ppd].[TruncateAPPL2]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [ppd].[TruncateAPPL2]

as

	set nocount on

	truncate table ppd.APPL2

	return @@error
GO
/****** Object:  StoredProcedure [ppd].[TruncateAPPL3]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [ppd].[TruncateAPPL3]

as

	set nocount on

	truncate table ppd.APPL3

	return @@error
GO
/****** Object:  StoredProcedure [ppd].[TruncateDRUG2]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [ppd].[TruncateDRUG2]
as

	set nocount on

	truncate table ppd.DRUG2

	return @@error
GO
/****** Object:  StoredProcedure [ppd].[TruncateDRUG3]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [ppd].[TruncateDRUG3]

as

	set nocount on

	truncate table ppd.DRUG3

	return @@error
GO
/****** Object:  StoredProcedure [ppd].[TruncateMANU]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [ppd].[TruncateMANU]

as

	set nocount on

	truncate table ppd.MANU

	return @@error
GO
/****** Object:  StoredProcedure [ppd].[TruncateSnomedMapping]    Script Date: 28/10/2015 11:24:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [ppd].[TruncateSnomedMapping]
as

	set nocount on

	truncate table ppd.SnomedMapping

	return @@error

GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Internal identifier used to represent this entity throughout the database.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ScheduleSpecialItem', @level2type=N'COLUMN',@level2name=N'ScheduleSpecialItemID'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Internal identifier of the schedule of payments that this special item is associated with.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ScheduleSpecialItem', @level2type=N'COLUMN',@level2name=N'ScheduleID'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Internal identifier used to represent this entity throughout the database.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ScheduleTop10CatM', @level2type=N'COLUMN',@level2name=N'ScheduleTop10CatMID'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Internal identifier of the schedule of payments that this CatM is associated with.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ScheduleTop10CatM', @level2type=N'COLUMN',@level2name=N'ScheduleID'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Internal identifier used to represent this entity throughout the database.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ScheduleTop10ZD', @level2type=N'COLUMN',@level2name=N'ScheduleTop10ZDID'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Internal identifier of the schedule of payments that this ZD is associated with.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ScheduleTop10ZD', @level2type=N'COLUMN',@level2name=N'ScheduleID'
--GO