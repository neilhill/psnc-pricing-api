--Use [PSNCCloud-Dev-AuthV3]

CREATE SCHEMA auth
GO

ALTER SCHEMA auth TRANSFER dbo.AspNetUsers;
GO
sp_rename '[auth].[AspNetUsers].[PK_dbo.AspNetUsers]','PK_auth.AspNetUsers'
GO
ALTER SCHEMA auth TRANSFER dbo.AspNetRoles
GO
sp_rename '[auth].[AspNetRoles].[PK_dbo.AspNetRoles]','PK_auth.AspNetRoles'
GO

ALTER SCHEMA auth TRANSFER dbo.AspNetUserRoles
GO
sp_rename '[auth].[AspNetUserRoles].[PK_dbo.AspNetUserRoles]','PK_auth.AspNetUserRoles'
GO
sp_rename '[auth].[FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]','FK_auth.AspNetUserRoles_auth.AspNetRoles_RoleId'
GO
sp_rename '[auth].[FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]','FK_auth.AspNetUserRoles_auth.AspNetUsers_UserId'
GO

ALTER SCHEMA auth TRANSFER dbo.AspNetUserLogins
GO
sp_rename '[auth].[AspNetUserLogins].[PK_dbo.AspNetUserLogins]', 'PK_auth.AspNetUserLogins'
GO
sp_rename '[auth].[FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]','FK_auth.AspNetUserLogins_auth.AspNetUsers_UserId'
GO

ALTER SCHEMA auth TRANSFER dbo.AppClients
GO

ALTER SCHEMA auth TRANSFER dbo.AppScopes
GO

ALTER SCHEMA auth TRANSFER dbo.AppClientScopes
GO
sp_rename '[auth].[AppClientScopes].[PK_dbo.AppClientScopes]','PK_auth.AppClientScopes'
GO
sp_rename '[auth].[FK_dbo.AppClientScopes_dbo.AppClients_ClientId]','FK_auth.AppClientScopes_auth.AppClients_ClientId'
GO
sp_rename '[auth].[FK_dbo.AppClientScopes_dbo.AppScopes_Name]','FK_auth.AppClientScopes_auth.AppScopes_Name'
GO

ALTER SCHEMA auth TRANSFER dbo.AspNetUserClaims
GO
sp_rename '[auth].[AspNetUserClaims].[PK_dbo.AspNetUserClaims]', 'PK_auth.AspNetUserClaims'
GO
sp_rename '[auth].[FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]','FK_auth.AspNetUserClaims_auth.AspNetUsers_UserId'
GO

ALTER SCHEMA auth TRANSFER dbo.__MigrationHistory
GO
sp_rename '[auth].[__MigrationHistory].[PK_dbo.__MigrationHistory]', 'PK_auth.__MigrationHistory'
GO
