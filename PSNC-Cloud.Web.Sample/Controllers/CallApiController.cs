﻿using IdentityModel.Client;
using Newtonsoft.Json.Linq;
using System;
using System.Configuration;
using System.IO;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PSNCCloud.Web.Sample.Controllers
{
    public class CallApiController : Controller
    {
        // GET: CallApi/ClientCredentials
        public async Task<ActionResult> ClientCredentials()
        {
            var response = await GetTokenAsync();
            var result = await CallApi(response.AccessToken);

            ViewBag.Json = result;
            return View("ShowApiResult");
        }

        // GET: CallApi/UserCredentials
        public async Task<ActionResult> UserCredentials()
        {
            var user = User as ClaimsPrincipal;
            var token = user.FindFirst("access_token").Value;
            var result = await CallApi(token);

            ViewBag.Json = result;
            return View("ShowApiResult");
        }

        private async Task<string> CallApi(string token)
        {
            var client = new HttpClient();
            client.SetBearerToken(token);

            

            //var json = await client.GetStringAsync(ConfigurationManager.AppSettings["BaseApiUrl"] + "identity");
            //var json = await client.GetStringAsync(ConfigurationManager.AppSettings["BaseApiUrl"] + endpoint);
            //return JArray.Parse(json).ToString();

            StringContent bodyContent = new StringContent(@"
[
    {
        ""PriceItemIndex"":1,
        ""FinancialPeriodID"":201712,
        ""contractorCode"":""FA641"",
        ""formNumber"":1,
        ""itemNumber"":1,
        ""subItemNumber"":1,
        ""groupType"":2,
        ""productCode"":""34730511000001105"",
        ""quantity"":1,
        ""endorsementCodes"":""NB""
    },
    
     {
        ""PriceItemIndex"":2,
        ""FinancialPeriodID"":201712,
        ""contractorCode"":""FA641"",
        ""formNumber"":1,
        ""itemNumber"":1,
        ""subItemNumber"":1,
        ""groupType"":2,
        ""productCode"":""34730511000001105"",
        ""quantity"":1,
        ""endorsementCodes"":""NB""
    }
]
");
            bodyContent.Headers.Remove("Content-Type");
            bodyContent.Headers.Add("Content-Type", "application/json");

            string endpoint = "/v1/sqlstoredprocedures/priceitems";
            HttpResponseMessage responseMessage = await client.PostAsync(ConfigurationManager.AppSettings["BaseApiUrl"] + endpoint, bodyContent);
            //HttpResponseMessage responseMessage = await client.GetAsync(ConfigurationManager.AppSettings["BaseApiUrl"] + endpoint);

            string jsonMessage;
            using (Stream responseStream = await responseMessage.Content.ReadAsStreamAsync())
            {
                jsonMessage = new StreamReader(responseStream).ReadToEnd();
            }

            try
            {
                return JArray.Parse(jsonMessage).ToString();
            }
            catch (Exception ex)
            {
                return jsonMessage.ToString();
            }
        }

        private async Task<TokenResponse> GetTokenAsync()
        {
            var client = new TokenClient(
                ConfigurationManager.AppSettings["BaseAuthUrl"] + "core/connect/token",
                "mvc_service",
                "secret");

            return await client.RequestClientCredentialsAsync("psncapi");
        }
    }
}