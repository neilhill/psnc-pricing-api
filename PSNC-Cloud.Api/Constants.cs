﻿namespace PSNCCloud.Api
{
    public static class ErrorMessage
    {
        public static string MissingAllParameters = "Missing all required parameters.";
    }

    public static class Constants
    {
        public static class AuditTypes
        {

            public const int Routine = 1,
                Switching = 2,
                Special = 3;
        }

        public static class BatchStatuses
        {
            public const int ScheduleOfPaymentsLoaded = 100,
                FullDetailsRequestPending = 110,
                FullDetailsRequested = 120,
                FullDetailsReceivedWaitingForFP34c = 200,
                ReadyForFirstCheck = 300,
                FirstCheckInProgress = 320,
                ReadyForSecondCheck = 400,
                SecondCheckInProgress = 420,
                SecondCheckCompleted = 490,
                CheckingCompletedAwaitingGenerationOfPaymentAuthorityCorrespondence = 500,
                SubmittedToPaymentAuthorityAwaitingResponse = 520,
                CorrespondenceFromPaymentAuthorityReceivedAgreementDenied = 540,
                FinalChangesInProgress = 560,
                FinalChangesCompleted = 580,
                AgreedByPpdAwaitingGenerationOfContractorAndLpcCorrespondence = 600,
                Completed = 900,
                Error = 999;
        }

        public partial class PaperTrackingStatus
        {
            public const int Requested = 1,
                ReRequested = 2,
                ReceivedPartial = 3,
                ReceivedInFull = 4,
                ReturnedToSource = 5,
                ReceivedBySource = 6,
                None = 99;
        }

        public enum BatchListViewMode
        {
            Contractor,

            Period
        }

        public partial class PaperType
        {
            public const int Bundles = 1,
                Fragments = 2;
        }
    }
}