﻿using System.ComponentModel.DataAnnotations;

namespace PSNCCloud.Api.RequestParameterModels
{
    /// <summary>
    /// Price items request parameters.
    /// </summary>
    public class PriceItemsRequestParameters
    {
        /// <summary>
        /// Unique index of price item.
        /// </summary>
        [Required]
        public int? PriceItemIndex { get; set; }

        /// <summary>
        /// id of financial period.
        /// </summary>
        [Required]
        public int? FinancialPeriodID { get; set; }
        
        /// <summary>
        /// Contractor code.
        /// </summary>
        [Required]
        [MaxLength(7, ErrorMessage = "contractor code can not be longer than 7 characters.")]
        public string ContractorCode { get; set; }

        /// <summary>
        /// Form Number
        /// </summary>
        [Required]
        public int? FormNumber { get; set; }
        
        /// <summary>
        /// Item Number
        /// </summary>
        [Required]
        public int? ItemNumber { get; set; }
        
        /// <summary>
        /// Sub Item/Sequence Number
        /// </summary>
        [Required]
        public int? SubItemNumber { get; set; }
        
        /// <summary>
        /// Group Type
        /// </summary>
        [Required]
        public int? GroupType { get; set; }

        /// <summary>
        /// Product code.
        /// </summary>
        [Required]
        public string ProductCode { get; set; }

        /// <summary>
        /// Quantity
        /// </summary>
        [Required]
        public int? Quantity { get; set; }
        
        /// <summary>
        /// Endorsement codes.
        /// </summary>
        public string EndorsementCodes { get; set; }
    }
}