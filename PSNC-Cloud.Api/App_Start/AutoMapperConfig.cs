﻿using AutoMapper;
using PSNC.Data.DbContext;
using PSNC.Data.Entities.Models.SP;

namespace PSNCCloud.Api.App_Start
{

    public static class AutoMapperConfig
    {
        public static void Configure()
        {
            //SPS
            Mapper.CreateMap<PriceItem_Result, PriceItem>().IgnoreAllNonExisting();
            
            Mapper.AssertConfigurationIsValid();
        }
    }
}