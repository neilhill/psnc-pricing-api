﻿using System.Web.Http;

namespace PSNCCloud.Api.App_Start
{

    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);

            //This is required to avoid self referencing object issues for json formatter
            config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;

            //XML support has been disabled, as it causes error for self referencing objects. If web API XML support required
            //then create custom XML formatter.
            config.Formatters.Remove(config.Formatters.XmlFormatter);
        }
    }
}