﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Http;

namespace PSNCCloud.Api
{
    public static class Helper
    {
        public static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> action)
        {

            if (enumerable == null)
                throw new ArgumentNullException(@"enumerable");

            if (action == null)
                throw new ArgumentNullException(@"action");

            enumerable.ToList().ForEach(item => action(item));

        }

        public static IMappingExpression<TSource, TDestination> IgnoreAllNonExisting<TSource, TDestination>(this IMappingExpression<TSource, TDestination> expression)
        {
            var sourceType = typeof(TSource);
            var destinationType = typeof(TDestination);
            var existingMaps = Mapper.GetAllTypeMaps().First(x => x.SourceType.Equals(sourceType)
                && x.DestinationType.Equals(destinationType));
            foreach (var property in existingMaps.GetUnmappedPropertyNames())
            {
                expression.ForMember(property, opt => opt.Ignore());
            }
            return expression;
        }

        //public static IOrderedQueryable<TEntity> OrderBy<TEntity>(this IQueryable<TEntity> source, string orderByProperty,
        //                  bool desc)
        //{
        //    string command = desc ? "OrderByDescending" : "OrderBy";
        //    var type = typeof(TEntity);
        //    var property = type.GetProperty(orderByProperty);
        //    var parameter = Expression.Parameter(type, "p");
        //    var propertyAccess = Expression.MakeMemberAccess(parameter, property);
        //    var orderByExpression = Expression.Lambda(propertyAccess, parameter);
        //    var resultExpression = Expression.Call(typeof(Queryable), command, new Type[] { type, property.PropertyType },
        //                                  source.Expression, Expression.Quote(orderByExpression));
        //    return source.Provider.CreateOre<TEntity>(resultExpression);
        //}

        //public static IOrderedQueryable<T> OrderBy<T>(this IQueryable<T> enumerable, string sortColumn, string direction)
        //{
        //    var param = Expression.Parameter(typeof(T), "x");

        //    Expression conversion = Expression.Convert(Expression.Property(param, sortColumn), typeof(object));
        //    var mySortExpression = Expression.Lambda<Func<T, object>>(conversion, param);

        //    IOrderedQueryable<T> iQuery;
        //    switch (direction)
        //    {
        //        case "desc":
        //            iQuery = enumerable.OrderByDescending(mySortExpression);
        //            break;
        //        case "asc":
        //        default:
        //            iQuery = enumerable.OrderBy(mySortExpression);
        //            break;
        //    }
        //    return iQuery;
        //}
    }

}