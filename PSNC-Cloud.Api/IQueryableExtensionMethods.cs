﻿namespace System.Linq
{

    #region Namespace references

    using System;
    using System.Linq.Expressions;
    using System.Web.UI.WebControls;

    #endregion

    public static class IQueryableExtensionMethods
    {

        #region Methods

        public static IOrderedQueryable<T> OrderBy<T>(this IQueryable<T> source, string sortExpression, string sortDirection)
            where T : class
        {

            //string command = sortDirection == SortDirection.Ascending ? "OrderBy" : "OrderByDescending";
            
            string command = (sortDirection.Equals("asc", StringComparison.OrdinalIgnoreCase)
                || sortDirection.Equals("ascending", StringComparison.OrdinalIgnoreCase)) ? "OrderBy" : "OrderByDescending";

            var type = typeof(T);
            var property = type.GetProperty(sortExpression);
            var parameter = Expression.Parameter(type, "p");
            var propertyAccess = Expression.MakeMemberAccess(parameter, property);
            var orderByExpression = Expression.Lambda(propertyAccess, parameter);
            var resultExpression = Expression.Call(typeof(Queryable), command, new Type[] { type, property.PropertyType }, source.Expression, Expression.Quote(orderByExpression));

            return source.Provider.CreateQuery<T>(resultExpression) as IOrderedQueryable<T>;

        }

        public static IOrderedQueryable<T> ThenBy<T>(this IOrderedQueryable<T> source, string sortExpression, string sortDirection)
            where T : class
        {

            //string command = sortDirection == SortDirection.Ascending ? "ThenBy" : "ThenByDescending";

            string command = (sortDirection.Equals("asc", StringComparison.OrdinalIgnoreCase)
                || sortDirection.Equals("ascending", StringComparison.OrdinalIgnoreCase)) ? "ThenBy" : "ThenByDescending";

            var type = typeof(T);
            var property = type.GetProperty(sortExpression);
            var parameter = Expression.Parameter(type, "p");
            var propertyAccess = Expression.MakeMemberAccess(parameter, property);
            var orderByExpression = Expression.Lambda(propertyAccess, parameter);
            var resultExpression = Expression.Call(typeof(Queryable), command, new Type[] { type, property.PropertyType }, source.Expression, Expression.Quote(orderByExpression));

            return source.Provider.CreateQuery<T>(resultExpression) as IOrderedQueryable<T>;

        }

        #endregion

    }

}
