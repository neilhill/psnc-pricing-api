﻿using IdentityServer3.AccessTokenValidation;
using Microsoft.Owin;
using Owin;
using PSNCCloud.Api.App_Start;
using System.Configuration;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;

[assembly: OwinStartup(typeof(PSNCCloud.Api.Startup))]

namespace PSNCCloud.Api
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // token validation
            app.UseIdentityServerBearerTokenAuthentication(new IdentityServerBearerTokenAuthenticationOptions
            {
                Authority = ConfigurationManager.AppSettings["BaseAuthUrl"] + "core",
                RequiredScopes = new[] { "psncapi" }
            });

            // add app local claims per request
            app.UseClaimsTransformation(incoming =>
            {
                // either add claims to incoming, or create new principal

                var appPrincipal = new ClaimsPrincipal(incoming);
                incoming.Identities.First().AddClaim(new Claim("appSpecific", "some_value"));

                return Task.FromResult(appPrincipal);
            });

            app.UseResourceAuthorization(new AuthorizationManager());

            // web api configuration
            var config = new HttpConfiguration();
            //config.MapHttpAttributeRoutes();

            app.UseWebApi(config);

            WebApiConfig.Register(config);
        }
    }
}