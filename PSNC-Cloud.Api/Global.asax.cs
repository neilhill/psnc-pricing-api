﻿using Newtonsoft.Json;
using PSNCCloud.Api.App_Start;
using System;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace PSNCCloud.Api
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            AutoMapperConfig.Configure();
        }
    }
}