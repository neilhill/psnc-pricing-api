﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PSNCCloud.Api.Areas.HelpPage
{
    public class HelpPagesCustomAttribute : Attribute
    {
        public bool IsPaged { get; set; }
    }
}