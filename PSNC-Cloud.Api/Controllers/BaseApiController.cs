﻿using AutoMapper;
using PSNC.Data.Entities.Models.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace PSNCCloud.Api.Controllers
{
    //[ResourceAuthorize("Read", "PSNCAPI")]
    public class BaseApiController : ApiController
    {
        public Result<T2> GetResultWithPagination<T1, T2>(IQueryable<T1> queryable, string defaultOrderBy)
            where T2 : class
            where T1 : class
        {
            queryable = (IQueryable<T1>)this.ApplyOrderBy(queryable, defaultOrderBy);

            Result<T2> result = new Result<T2>();

            result.TotalCount = queryable.Count();

            SetPagingInfo(result);

            queryable = (IQueryable<T1>)this.ApplyPagination(queryable, result.PageIndex, result.PageSize);

            result.Data = Mapper.Map<T2>(queryable.AsNoTracking().ToList());

            return result;
        }

        public Result<IEnumerable<T1>> GetResultWithPagination<T1>(IQueryable<T1> queryable, string defaultOrderBy)
            where T1 : class
        {
            queryable = (IQueryable<T1>)this.ApplyOrderBy(queryable, defaultOrderBy);

            Result<IEnumerable<T1>> result = new Result<IEnumerable<T1>>();

            result.TotalCount = queryable.Count();

            SetPagingInfo(result);

            queryable = (IQueryable<T1>)this.ApplyPagination(queryable, result.PageIndex, result.PageSize);

            result.Data = queryable.AsNoTracking().ToList();

            return result;
        }

        /*
        public Result<T2> GetResult<T1, T2>(IQueryable<T1> queryable, string defaultOrderBy) 
            where T2 : class
            where T1 : class
        {

            queryable = (IQueryable<T1>)this.ApplyOrderBy(queryable, defaultOrderBy);

            Result<T2> result = new Result<T2>();

            result.Data = Mapper.Map<T2>(queryable.AsNoTracking().ToList());

            return result;
        }
        */

        public IQueryable ApplyOrderBy<T1>(IQueryable<T1> queryable, string defaultOrderBy) where T1 : class
        {
            string orderBy = HttpContext.Current.Request.QueryString["orderby"];
            if (string.IsNullOrWhiteSpace(orderBy))
            {
                orderBy = defaultOrderBy;
            }

            if (!string.IsNullOrWhiteSpace(orderBy))
            {
                string[] orderByFields = orderBy.Split(',');
                if (orderByFields.Length > 0)
                {
                    int loopCount = 0;
                    foreach (string field in orderByFields)
                    {
                        if (!string.IsNullOrWhiteSpace(field))
                        {
                            string[] keyValue = field.Split(';');
                            if (keyValue.Length == 2 && !string.IsNullOrWhiteSpace(keyValue[0])
                                && !string.IsNullOrWhiteSpace(keyValue[1]))
                            {
                                if (loopCount == 0)
                                {
                                    queryable = queryable.OrderBy(keyValue[0], keyValue[1]);
                                }
                                else
                                {
                                    queryable = ((IOrderedQueryable<T1>)queryable).ThenBy(keyValue[0], keyValue[1]);
                                }

                                loopCount++;
                            }
                        }
                    }
                }
            }

            return queryable;
        }

        public IQueryable ApplyPagination<T1>(IQueryable<T1> queryable, int pageIndex, int pageSize) where T1 : class
        {
            queryable = queryable.Skip(pageIndex * pageSize).Take(pageSize);

            return queryable;
        }

        private void SetPagingInfo<T>(Result<T> result) where T : class
        {
            string pageIndexQS = HttpContext.Current.Request.QueryString["pageindex"];
            Int32 pageIndex;
            if (!Int32.TryParse(pageIndexQS, out pageIndex))
            {
                pageIndex = 0;
            }

            result.PageIndex = pageIndex;

            string pageSizeQS = HttpContext.Current.Request.QueryString["pagesize"];
            Int32 pageSize;
            if (!Int32.TryParse(pageSizeQS, out pageSize))
            {
                pageSize = Int32.Parse(ConfigurationManager.AppSettings["DefaultPageSize"]);
            }

            result.PageSize = pageSize;
        }
    }
}