﻿using AutoMapper;
using PSNC.Data.Entities.Models.Common;
using PSNC.Data.Entities.Models.SP;
using PSNC.Data.Entities.Models.Tables;
using PSNCCloud.Api.Attributes;
using PSNCCloud.Api.RequestParameterModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;

namespace PSNCCloud.Api.Controllers
{
    /// <summary>
    /// Controller for  SQL stored procedures resource.
    /// </summary>
    [RoutePrefix("api/v1/sqlstoredprocedures")]
    public class SQLStoredProceduresController : BaseApiController
    {
        /// <summary>
        /// Returns price items filtered by specified parameters within request body.
        /// </summary>
        /// <param name="requestParameters">Filter parameters(request body)</param>
        /// <returns>Filtered collection of price items.</returns>
        [Route("priceitems")]
        [AcceptVerbs("POST")]
        [ResponseType(typeof(List<PriceItem>))]
        [ValidateParameters]
        public IHttpActionResult PriceItems([FromBody] List<PriceItemsRequestParameters> requestParameters)
        {
            //This check is required because ModelState is true when request body is empty (not single parameter is available).
            if (requestParameters == null)
            {
                return BadRequest(ErrorMessage.MissingAllParameters);
            }
            else
            {
                List<PriceItem> result = new List<PriceItem>();
                using (var context = new PSNC.Data.DbContext.DataEntities())
                {
                    List<PSNC.Data.DbContext.PriceItem_Result> priceItems = null;
                    List<PriceItem> mappedPriceItems = null;

                    foreach (PriceItemsRequestParameters requestParamItem in requestParameters)
                    {
                        priceItems = context.PriceItem(
                                    requestParamItem.FinancialPeriodID,
                                    requestParamItem.ContractorCode,
                                    requestParamItem.FormNumber,
                                    requestParamItem.ItemNumber,
                                    requestParamItem.SubItemNumber,
                                    requestParamItem.GroupType,
                                    requestParamItem.ProductCode,
                                    requestParamItem.Quantity,
                                    requestParamItem.EndorsementCodes
                                ).ToList();

                        mappedPriceItems = Mapper.Map<List<PriceItem>>(priceItems);

                        mappedPriceItems.ForEach(item => item.PriceItemIndex = requestParamItem.PriceItemIndex.Value);

                        result.AddRange(mappedPriceItems);
                    }
                }

                return Ok(result);
            }
        }
    }
}
