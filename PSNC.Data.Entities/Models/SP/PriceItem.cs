﻿using System;

namespace PSNC.Data.Entities.Models.SP
{
    public class PriceItem
    {
        public int PriceItemIndex { get; set; }
        public Nullable<int> FinancialPeriodID { get; set; }
        public string Contractor { get; set; }
        public Nullable<int> JurisdictionID { get; set; }
        public Nullable<int> FormNumber { get; set; }
        public Nullable<int> ItemId { get; set; }
        public Nullable<int> ChargeStatus { get; set; }
        public string ProductDescription { get; set; }
        public string ProductCode { get; set; }
        public Nullable<int> Quantity { get; set; }
        public string UnitOfMeasure { get; set; }
        public Nullable<double> PackPrice { get; set; }
        public Nullable<decimal> PackSize { get; set; }
        public Nullable<double> DMDPrice { get; set; }
        public string DMDPriceDate { get; set; }
        public Nullable<double> MDRPrice { get; set; }
        public string MDRPriceDate { get; set; }
        public Nullable<double> BasicPricePrism { get; set; }
        public Nullable<double> PaymentForConsumables { get; set; }
        public Nullable<double> PaymentForContainers { get; set; }
        public Nullable<double> AdditionalFeesOnItem { get; set; }
        public Nullable<double> ControlledDrugFee { get; set; }
        public Nullable<double> StomaCustomisationFeeValue { get; set; }
        public Nullable<double> HomeDeliveryFee { get; set; }
        public Nullable<double> ExpensiveItemFee { get; set; }
        public Nullable<double> EndorsementFees { get; set; }
        public Nullable<double> TotalValueAdditionalFee { get; set; }
        public Nullable<int> NumberOfProfessionalFees { get; set; }
        public string EndorsementCodes { get; set; }
        public string PK { get; set; }
        public Nullable<double> SDRProfessionalFeeValue { get; set; }
        public Nullable<int> SDRProfessionalFeeNumber { get; set; }
        public Nullable<double> ZDRProfessionalFeeValue { get; set; }
        public Nullable<int> ZDRProfessionalFeeNumber { get; set; }
        public Nullable<double> SPUnlicensedMedsFeeValue { get; set; }
        public Nullable<double> EDUnlicensedMedsFeeValue { get; set; }
        public Nullable<double> MFHosieryFeeValue { get; set; }
        public Nullable<double> MFTrussFeeValue { get; set; }
        public Nullable<double> MFBeltAndGirdleFeeValue { get; set; }
        public Nullable<double> MethadoneFeeValue { get; set; }
        public Nullable<double> MethadonePckgdDoseFeeValue { get; set; }
        public Nullable<double> HomeDelSRApplAddFeeValue { get; set; }
        public Nullable<double> HomeDelHRApplAddFeeValue { get; set; }
        public Nullable<double> CDSchedule2FeeValue { get; set; }
        public Nullable<double> CDSchedule3FeeValue { get; set; }
    }
}