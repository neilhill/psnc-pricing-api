﻿using System.ComponentModel.DataAnnotations;

namespace PSNC.Data.Entities.Models.Tables
{
    public class UserProfile
    {
        #region Scalar Properties
        [Required]
        public long UserProfileID { get; set; }

        [Required]
        public string LoginName { get; set; }

        [Required]
        public bool Can1stCheck { get; set; }

        [Required]
        public bool Can2ndCheck { get; set; }
        #endregion
    }
}