//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PSNC.Dmd.Data.DbContext
{
    using System;
    using System.Collections.Generic;
    
    public partial class ProductPrescribingInformation
    {
        public long ProductPrescribingInformationId { get; set; }
        public long ActualMedicinalProductPackId { get; set; }
        public Nullable<bool> IsSchedule2 { get; set; }
        public Nullable<bool> IsSchedule1 { get; set; }
        public Nullable<bool> IsAllowedOnHospitalPrescriptionOnly { get; set; }
        public Nullable<bool> IsBorderlineSubstance { get; set; }
        public Nullable<bool> IsPersonallyAdministered { get; set; }
        public Nullable<bool> IsAllowedOnMdaPrescription { get; set; }
        public Nullable<bool> IsAllowedOnNursingFormularyPrescription { get; set; }
        public Nullable<bool> IsAllowedOnNurseExtendedFormularyPrescription { get; set; }
        public Nullable<bool> IsAllowedOnDentalFormularyPrescription { get; set; }
    
        public virtual ActualMedicinalProductPack ActualMedicinalProductPack { get; set; }
    }
}
