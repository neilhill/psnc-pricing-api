//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PSNC.Dmd.Data.DbContext
{
    using System;
    using System.Collections.Generic;
    
    public partial class RestrictionOnAvailability
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public RestrictionOnAvailability()
        {
            this.ActualMedicinalProducts = new HashSet<ActualMedicinalProduct>();
        }
    
        public long RestrictionOnAvailabilityId { get; set; }
        public string Description { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ActualMedicinalProduct> ActualMedicinalProducts { get; set; }
    }
}
